<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tasks per Order View of Client Dashboard
 *
 */
$export = '
							<div class="btn-group">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span title="Export List of Order Details" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-external-link" aria-hidden="true"></i> Export <span class="caret"></span></span></button>
								<ul class="dropdown-menu text-left">
									<li><a href="'. current_url() .'/pdf" target="_blank" title="Export in Portable Document Format" data-toggle="tooltip" data-placement="bottom"><i class="fam-page-white-acrobat" aria-hidden="true"></i> PDF</a></li>
									<li><a href="'. current_url() .'/xls" target="_blank" title="Export in Excel / Spread Sheet Format" data-toggle="tooltip" data-placement="bottom"><i class="fam-page-excel" aria-hidden="true"></i> Excel</a></li>
									<li><a href="'. current_url() .'/txt" target="_blank" title="Export in Text Format" data-toggle="tooltip" data-placement="bottom"><i class="fam-page-white-text" aria-hidden="true"></i> Text</a></li>
								</ul>
							</div>
							&nbsp;<a href="'. current_url() .'/print" target="_blank" title="Print List of Order Details" data-toggle="tooltip" data-placement="bottom" role="button" class="btn btn-primary"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
';
?>
				<?php
				if (isset($userClients) && isset($orderTaskDetails)) :
					$orderName = $orderTaskDetails[0]["order_name"];
				?>
				<div class="client-panel-group">
					<script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>
					<div id="chartContainer" class="chart"></div>
					<h6>&nbsp;</h6>
					<?php
					echo "
					<script type=\"text/javascript\">
						var chart;
						chart = new CanvasJS.Chart(\"chartContainer\", {
							title:{
								text: \"$orderName's Progression\"
							},
							animationEnabled: true,
							exportEnabled: true,
							legend: {
								verticalAlign: \"bottom\",
								horizontalAlign: \"center\"
							},data: [
							{
								type: \"bar\",
								toolTipContent: \"{y} {label} ({indexLabel})\",
								showInLegend: true,
								legendText: \"$chartLegend\",
								legendMarkerColor: \"white\",
								indexLabelPlacement: \"outside\",
								indexLabelFontColor: \"black\",
								indexLabelFontSize: 14,
								dataPoints: ". json_encode($dpOverallOrderTask) ."
							}
							]
						});
						chart.render();
					</script>";
					?>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading text-right" role="tab" id="heading">
								<h4 class="panel-title pull-left">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse"><div class="status_image" title="<?=$status[$orderTaskDetails[0]["order_status"]]?>" data-toggle="tooltip" data-placement="bottom" id="<?=strtolower(str_replace(" ", "_", $status[$orderTaskDetails[0]["order_status"]]))?>"></div><?=$orderName?></a>
								</h4>
								<?=$export?>
							</div><!-- /.panel-heading role=tab -->
							<div id="collapse" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading">
								<?php
								if (isset($orderTaskDetails)) :
								?>
								<table class="table table-striped table-hover table-bordered tablesorter">
									<thead>
										<tr>
											<th>#</th>
											<th>Task Name <i class="fa fa-sort"></i></th>
											<?php if ($isTrafficking) : ?>
											<th>Process Step <i class="fa fa-sort"></i></th>
											<th>Placement <i class="fa fa-sort"></i></th>
											<?php endif; ?>
											<th>Created Date <i class="fa fa-sort"></i></th>
											<th>Status</i></th>
										</tr>
									</thead>
									<tbody>
										<?php
										$ctr = 0;
										foreach ($orderTaskDetails as $cntOrder => $orderTask) :
												$ctr++;
										?>
										<tr>
											<td class="text-center"><?=$ctr?></td>
											<td><?=$orderTask["task_name"]?></td>
											<?php if ($isTrafficking) : ?>
											<td><?=$orderTask["process_step"]?></td>
											<td><?=$orderTask["placement"]?></td>
											<?php endif; ?>
											<td><?=mdate("%Y-%m-%d %H:%i:%s", syncDate($orderTask["task_created_date"]))?></td>
											<td class="text-center"><div title="<?=($orderTask["task_status"] ? $status[$orderTask["task_status"]] : "Not Applicable")?>" data-toggle="tooltip" data-placement="bottom" class="status_image" id="<?=strtolower(str_replace(" ", "_", ($orderTask["task_status"] ? $status[$orderTask["task_status"]] : "Not Applicable")))?>"></div></td>
										</tr>
										<?php
										endforeach;//tasks
										?>
									</tbody>
								</table>
								<?php
								endif;//order details
								if (!isset($orderTaskDetails)) :
								?>
								<h4 class="text-center text-danger">No Order Task(s) found for <?=$orderName?>...</h4>
								<?php
								endif;
								?>
							</div><!-- /.panel-collapse role=tabpanel -->
							<div class="panel-footer text-right">
								<?=str_replace("btn-group", "btn-group dropup", $export)?>
							</div>
						</div><!-- /.panel-default -->
					</div><!-- /.panel-group #accordion -->
				</div><!-- /.client-panel-group -->
				<script src="<?php echo HTTP_JS_PATH ?>tables.js"></script>
				<script src="<?php echo HTTP_JS_PATH ?>jquery.tablesorter.js"></script>
				<?php
				endif;//clients
				if (!isset($orderTaskDetails)) :
				?>
				<h4 class="text-center text-danger">No Order Task(s) found...</h4>
				<?php
				endif;
				?>
