<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Contact Form View of Client Dashboard
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
			if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 10 seconds you will be redirected to Home. <a href=\"". current_url() ."\" title=\"Contact Us\">Click here to Contact Us again.</a></p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("home") ."';}, 10000);\n</script></div>";
		?>
		<div class="panel panel-info">
			<div class="panel-heading text-right"><h4 class="panel-title text-left"><i class="fa fa-phone"></i> Contact Us</h4></div>
			<form class="form-horizontal" method="post">
				<div class="panel-body">
					<div class="form-group">
						<div class="form-group">
							<label for="message_type" class="col-sm-3 control-label" title="Required field">Message Type <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<select class="form-control" name="message_type" id="message_type" required>
									<option<?php echo (!set_value("message_type") ? " selected" : ""); ?> disabled>Select Message Type</option>
									<?php
									if (isset($messageTypes)) :
										foreach ($messageTypes as $messageType) :
									?>
									<option value="<?=$messageType?>"<?php echo (set_value("message_type") == $messageType ? " selected" : ""); ?>><?=$messageType?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="contact_name" class="col-sm-3 control-label" title="Required field">Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Representative's Name or Company Name" name="contact_name" id="contact_name" required value="<?php echo set_value("contact_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-3 control-label" title="Required field">Email Address <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="email" class="form-control" placeholder="Email Address" name="email" id="email" required value="<?php echo set_value("email"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="message" class="col-sm-3 control-label" title="Required field">Message <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<textarea class="form-control" name="message" id="message" placeholder="Message/Feedback"><?php echo set_value("message"); ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer"><center>
					<button type="submit" class="btn btn-info">Submit</button>
					<a href="<?php echo current_url(); ?>" class="btn btn-default" role="button" title="Cancel Contact Us">Cancel</a>
				</center></div>
			</form>
		</div><!-- /.panel-info -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
