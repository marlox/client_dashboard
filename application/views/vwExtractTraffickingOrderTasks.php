<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Trafficking Tasks Extraction per Order View of Client Dashboard
 *
 */
$client = $this->session->userdata("clientName") ? $this->session->userdata("clientName") : "Client";
$project = isset($project) ? $project : "Project";
$order = isset($order) ? $order : "Order";
if (preg_match("/^(pdf|print)/i", $action)) { //pdf, print
$progressLabel = $completionPercentage > 0 ? "$completionPercentage% $completeLabel" : "";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Eyes-T Dashboard - <?=$order?> Task Details</title>
		<!-- Bootstrap core CSS -->
		<link href="<?php echo HTTP_CSS_PATH; ?>bootstrap.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="<?php echo HTTP_JS_PATH; ?>html5shiv.js"></script>
			<script src="<?php echo HTTP_JS_PATH; ?>respond.min.js"></script>
		<![endif]-->
		<link href="<?php echo HTTP_CSS_PATH; ?>generic.css" rel="stylesheet">
		<link href="<?php echo HTTP_CSS_PATH; ?>styles.css" rel="stylesheet">
	</head>
	<body style="background-color:#eee;padding-top:20px">
		<div class="container">
			<div class="panel-group">
				<?php if ($action == "print") : ?>
				<ul class="breadcrumb">
					<li><?=$client?></li>
					<li><?=$project?></li>
					<li class="active"><?=$order?></li>
				</ul>
				<?php 
				elseif ($action == "pdf") :
				?>
				<div class="breadcrumb">
					<?=$client?> <span class="active"> &gt; </span> <?=$project?> <span class="active"> &gt; </span> <span class="active"><?=$order?></span>
				</div>
				<?php endif; ?>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title text-center"><?=$order?> Task Details</h4>
					</div>
				</div>
				<br />
			</div>
			<div class="text-center">
				<h5><?=$order?>'s Progression</h5>
			</div>
			<div class="row text-center">
				<div class="col-md-2"><?=$orderCurrentStep?></div>
				<div class="col-md-8">
					<div class="progress">
						<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="<?=$completionPercentage?>" aria-valuemin="0" aria-valuemax="100" style="width:<?=$completionPercentage?>%">
							<?=$progressLabel?>
						</div>
						<?=!$progressLabel ? "100% Incomplete" : ""?>
					</div>
				</div>
				<div class="col-md-2"><?=$nextProcessStep?></div>
			</div>
			<div class="panel-group">
				<div class="panel panel-default">
					<?php
					if (isset($records)) :
					?>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>Task Name</th>
								<th>Placement</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$ctr = 0;
							foreach ($records as $cntOrder => $orderTask) :
								$ctr++;
							?>
							<tr>
								<td><?=$ctr?></td>
								<td><?=$orderTask["task_name"]?></td>
								<td><?=$orderTask["placement"]?></td>
								<td><?=($orderTask["status"] ? $status[$orderTask["status"]] : $status["KO"])?></td>
							</tr>
							<?php
							endforeach;//($records as $cntOrder => $orderTask)
							?>
						</tbody>
					</table>
					<?php
					endif;//order details
					?>
				</div><!-- /.panel-default -->
			</div><!-- /.panel-group -->

		</div><!-- /.container -->
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>bootstrap.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>generic.js"></script>
	</body>
</html>
<?php
} else { //excel, word
	$strData = "#\tTASK NAME\tPLACEMENT\tSTATUS\n";
	$num = 0;
	foreach ($records as $ctr => $record) {
		$num++;
		$taskName = $record["task_name"];
		if (strstr($taskName, "\n")) $taskName = trim(preg_replace("/\s+/", " ", $taskName));
		$strStatus = trim($record["status"]) ? $status[$record["status"]] : "";
		$strData .= $num;
		$strData .= "\t". $taskName ? $taskName : " ";
		$strData .= "\t". $record["placement"];
		$strData .= "\t$strStatus";
		$strData .= "\n";
	}
	echo $strData;
}
?>
