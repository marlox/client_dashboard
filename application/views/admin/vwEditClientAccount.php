<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Edit Client Account View of Client Dashboard Admin Interface
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
			if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 8 seconds you will be redirected to Manage Clients Account.</p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("admin/clients/view") ."';}, 8000);\n</script></div>";
		?>
		<div class="panel panel-primary">
			<div class="panel-heading text-right">
					<h4>&nbsp;</h4>
			</div>
			<form class="form-horizontal" id="frmEditClientAccount" method="post" enctype="multipart/form-data" accept-charset="utf-8">
				<div class="panel-body">
					<div class="form-group">
						<div class="form-group">
							<label for="client_name" class="col-sm-2 control-label">Client Name &nbsp;</label>
							<div class="col-sm-9">
								<h4><u><?php echo isset($client_name) && !set_value("client_name") ? $client_name : set_value("client_name"); ?></u></h4>
								<input type="hidden" class="form-control" name="client_id" id="client_id" required value="<?php echo isset($client_id) && !set_value("client_id") ? $client_id : set_value("client_id"); ?>" />
								<input type="hidden" class="form-control" name="client_name" id="client_name" required value="<?php echo isset($client_name) && !set_value("client_name") ? $client_name : set_value("client_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="logo" class="col-sm-2 control-label">Logo &nbsp;</label>
							<div class="col-sm-9">
								<input type="file" class="form-control" placeholder="Client Logo" title="Client Logo" name="logo" id="logo" />
								<div class="text-info"><em>Logo should not be more than 50px of height and 150px of width. .PNG file format only.</em></div>
								<?php
								if (isset($logo)) {
									$urlLogo = HTTP_IMAGES_PATH . $logo;
									echo '<div class="text-warning"><em>Leave Client Logo blank to keep this logo <a href="'. $urlLogo .'" title="Click to see original size" target="_blank"><div class="logo_holder" style="background-image:url('. $urlLogo .')"><span class="sr-only">'. str_replace("clients/", "", $logo) .'</span></div></a></em></div>';
								}
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="theme" class="col-sm-2 control-label" title="Preview purposes only in Admin Interface (Required field)">Theme <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-9">
								<?php
								if (isset($themes)) :
									foreach ($themes as $theme) :
								?>
								<a href="#" onclick="return changeCSS(this);" id="<?=$theme["theme_id"]?>" rel="<?=$theme["css_file"]?>"><span class="color_box<?=((isset($theme_id) && $theme_id == $theme["theme_id"]) || set_value("theme_id") == $theme["theme_id"] ? " selected" : "")?>" style="background-color:<?=($theme["theme_name"] == "Bronze" ? "#c09853" : ($theme["theme_name"] == "Default" ? "#428bca" : str_replace(" ", "", $theme["theme_name"])))?>" title="<?=$theme["theme_name"]?>" data-toggle="tooltip" data-placement="bottom">&nbsp;</span></a> &nbsp;
								<?php
									endforeach;
								endif;
								?>
								<input type="hidden" name="theme_id" id="theme" value="<?php echo isset($theme_id) && !set_value("theme_id") ? $theme_id : set_value("theme_id"); ?>" required />
								<div class="text-info"><em>Theme will only take effect in Client Interface. Selected theme only for preview in Admin Interface.</em></div>
							</div>
						</div>
						<div class="form-group">
							<label for="project" class="col-sm-2 control-label" title="Required field">Project(s) <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-9">
								<select class="form-control" name="project_id[]" id="project" multiple required>
									<option<?php echo (!($selectedProjects = set_value("project_id")) && !isset($clientProjects) ? " selected" : ""); ?> disabled>Select Project(s)</option>
									<?php
									if (isset($projects)) :
										if (!$selectedProjects) $selectedProjects = array();
										foreach ($projects as $project) :
									?>
									<option value="<?=$project["project_id"]?>"<?php echo ((isset($clientProjects) && array_search($project["project_id"], $clientProjects) !== false) || array_search($project["project_id"], $selectedProjects) !== false ? " selected" : ""); ?>><?=$project["project_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
								<div class="text-danger"><em>Deselecting existing Project(s) will automatically remove it and all its User(s) from <u><?=$client_name?></u> Client Account.</em></div>
							</div>
						</div>
						<div class="form-group">
							<label for="users" class="col-sm-2 control-label" title="Required field">User(s) <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-9">
								<select class="form-control" name="users[]" id="users" multiple required>
									<option<?php echo (!($selectedUsers = set_value("users")) && !isset($clientUsers) ? " selected" : ""); ?> disabled>Select User(s)</option>
									<?php
									if (isset($users)) :
										if (!$selectedUsers) $selectedUsers = array();
										foreach ($users as $user) :
											$isSelected = (isset($clientUsers) && array_search($user["username"], $clientUsers) !== false) || preg_match("/(\ ". str_replace(".", "\\.", $user["username"]) .")+/i", (" ". implode(", ", $selectedUsers)));
									?>
									<option value="<?=$user["username"]?>"<?php echo ($isSelected ? " focus selected" : ""); ?><?=($user["username"] === $this->session->userdata("username") && $isSelected ? ' class="hide"' : "")?>><?=(isset($user["full_name"]) ? $user["full_name"] : $user["username"])?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
								<div class="text-danger"><em>Deselecting existing User(s) will automatically remove it from <u><?=$client_name?></u> Client Account and all its Project(s).</em></div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body"><center>
					<button type="submit" class="btn btn-primary" data-href="<?=site_url("admin/clients/edit/$client_id/submit")?>" data-action="Edit" data-toggle="modal" data-target="#dialogBox">Save</button>
					<button href="#" class="btn btn-default" role="button" data-dismiss="modal" aria-label="Cancel" title="Cancel Edit Client Account">Cancel</button>
				</center></div>
			</form>
		</div><!-- /.panel-primary -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
