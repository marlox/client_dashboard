<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Manage Client Accounts View of Client Dashboard Admin Interface
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-right">
					<a href="<?php echo base_url(); ?>admin/clients" class="btn btn-default" title="New Client Account">New Client Account</a>
				</div>
				<div class="table-responsive">
				<table class="table table-striped table-hover tablesorter">
					<?php
					if (isset($clientAccounts) && $total) {
					?>
					<thead>
						<tr>
							<th>#</i></th>
							<th title="Sort by Client Name" data-toggle="tooltip" data-placement="bottom">Client Name <i class="fa fa-sort"></i></th>
							<th>Logo</i></th>
							<th title="Sort by Theme" data-toggle="tooltip" data-placement="bottom">Theme <i class="fa fa-sort"></i></th>
							<th title="Sort by Project" data-toggle="tooltip" data-placement="bottom">Project <i class="fa fa-sort"></i></th>
							<?php
							if ($this->session->userdata("user_type") == "Super Administrator") :
							?>
							<th title="Sort by User" data-toggle="tooltip" data-placement="bottom">User <i class="fa fa-sort"></i></th>
							<th title="Sort by Recent Update" data-toggle="tooltip" data-placement="bottom">Recent Update <i class="fa fa-sort"></i></th>
							<th title="Sort by Updated By" data-toggle="tooltip" data-placement="bottom">Updated By <i class="fa fa-sort"></i></th>
							<?php
							endif;
							?>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<?php
						$currentClient = $currentProject = $users = "";
						foreach ($clientAccounts as $ctr => $record) {
							$currentClient = $record["client_name"];
							if ($this->session->userdata("user_type") !== "Super Administrator") {
								$currentProject = $users = array();
								foreach ($record["project_id"] as $project) {
									if (array_search($project, $currentProject) === false) $currentProject[] = $projects[$project];
								}
								$currentProject = implode("<br />", $currentProject);
							} else {
								$currentProject = $projects[$record["project_id"]];
								$users = $record["full_name"];
							}
							$urlLogo = HTTP_IMAGES_PATH . $record["logo"];
					?>
						<tr>
							<td><?=($ctr + 1 + $offset)?></td>
							<td><?=$currentClient?></td>
							<td><?=(trim($record["logo"]) ? '<a href="'. $urlLogo .'" target="_blank" title="Click to see original size" data-toggle="tooltip" data-placement="bottom"><div class="logo_holder" style="background-image:url('. $urlLogo .')"><span class="sr-only">'. str_replace("clients/", "", $record["logo"]) .'</span></div></a>' : "&nbsp;")?></td>
							<td><?=$record["theme_name"]?></td>
							<td><?=$currentProject?></td>
							<?php
							if ($this->session->userdata("user_type") == "Super Administrator") :
							?>
							<td><?=$users?></td>
							<td><?=mdate("%d-%M-%Y %h:%i%a", syncDate($record["recent_update"]))?></td>
							<td><?=$record["updated_by"]?></td>
							<?php
							endif;
							?>
							<td>
								<a data-href="<?=site_url("admin/$page/edit/". $record["client_id"])?>" data-action="Edit" data-toggle="modal" data-target="#dialogBox"><i class="fam-group-edit" title="Edit Client <?="$currentClient's"?> Account" data-toggle="tooltip" data-placement="bottom"></i></a>
								<?php
								if ($this->session->userdata("user_type") == "Super Administrator") :
								?>
								<a data-href="<?=site_url("admin/$page/delete/". $record["client_project_user_id"])?>" data-action="Remove" data-toggle="modal" data-target="#dialogBox"><i class="fam-group-delete" title="Remove User <?=$record["full_name"]?> from Client <?="$currentClient's ". ucwords(strtolower(str_replace("_", " ", $currentProject)))?> Project" data-toggle="tooltip" data-placement="bottom"></i></a>
								<?php
								endif;
								?>
							</td>
						</tr>
					<?php
						}
					} else {
						echo '<tr><th colspan="7" class="text-danger text-center"><h4>No '. ucfirst($page) .' Account found...</h4></th></tr>';
					}
					?>
				</table>
				</div>
			</div><!-- /.panel-primary -->
			<?php
			if (isset($clientAccounts) && $total && $totalPages > 1) :
			?>
			<ul class="pagination pull-right">
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view")?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Start" data-toggle="tooltip" data-placement="bottom">&laquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/". ($pageNum > 2 ? $pageNum - 1 : ""))?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Previous" data-toggle="tooltip" data-placement="bottom">&lsaquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/view")?>" title="Page 1" data-toggle="tooltip" data-placement="bottom">1<?=(!$pageNum || $pageNum == 1 ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				for ($ctr = 2; $ctr <= $totalPages; $ctr++) :
				?>
				<li<?=($pageNum == $ctr ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/view/$ctr")?>"<?=($pageNum == $ctr ? ' onclick="return false;"' : "")?> title="Page <?=$ctr?>" data-toggle="tooltip" data-placement="bottom"><?=$ctr?><?=($pageNum == $ctr ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				endfor;
				?>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/". ($pageNum ? $pageNum + 1 : 2))?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="Next" data-toggle="tooltip" data-placement="bottom">&rsaquo;</a></li>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/end")?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="End" data-toggle="tooltip" data-placement="bottom">&raquo;</a></li>
			</ul>
			<?php
			endif;
			?>
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
