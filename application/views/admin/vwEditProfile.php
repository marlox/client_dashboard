<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Edit Profile View of Client Dashboard
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
			if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 8 seconds you will be redirected to ". ($this->uri->segment(1) == "admin" ? "Dashboard" : "Home") .". <a href=\"". current_url() ."\" title=\"Edit Profile\">Click here to Edit Profile again.</a></p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url(($this->uri->segment(1) == "admin" ? "admin/" : "") ."home") ."';}, 8000);\n</script></div>";
		?>
		<div class="panel panel-success">
			<div class="panel-heading text-right"><?=($this->uri->segment(1) !== "admin" ? '<h4 class="panel-title text-left"><i class="fa fa-user"></i> User Profile</h4>' : "&nbsp;")?></div>
			<form class="form-horizontal" method="post">
				<div class="panel-body">
					<div class="form-group">
						<div class="form-group">
							<label for="given_name" class="col-sm-3 control-label" title="Required field">Given Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Given Name" name="given_name" id="given_name" required value="<?php echo isset($given_name) && !trim(set_value("given_name")) ? $given_name : set_value("given_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="last_name" class="col-sm-3 control-label" title="Required field">Last Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Last Name" name="last_name" id="last_name" required value="<?php echo isset($last_name) && !trim(set_value("last_name")) ? $last_name : set_value("last_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-3 control-label" title="Required field">Email Address <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="email" class="form-control" placeholder="Email Address" name="email" id="email" required value="<?php echo isset($email) && !set_value("email") ? $email : set_value("email"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="col-sm-3 control-label">Password &nbsp;</label>
							<div class="col-sm-8">
								<input type="password" class="form-control" placeholder="Password" name="password" id="password" value="<?php echo set_value("password"); ?>" />
								<div class="text-info"><em>Leave password blank to keep your current password.</em></div>
							</div>
						</div>
						<div class="form-group">
							<label for="confirm_password" class="col-sm-3 control-label">Confirm Password &nbsp;</label>
							<div class="col-sm-8">
								<input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password" id="confirm_password" value="<?php echo set_value("confirm_password"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="timezone" class="col-sm-3 control-label" title="Required field">Timezone <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<select class="form-control" name="timezone" id="timezone" required>
									<option<?php echo (!isset($usrTimezone) && !trim(set_value("timezone")) ? " selected" : ""); ?> disabled>Select Timezone</option>
									<?php
									$arrTimezone = timezone_identifiers_list();
									foreach ($arrTimezone as $timezone) :
										if (trim($timezone) != "UTC") :
									?>
									<option value="<?=$timezone?>"<?php echo ((isset($usrTimezone) && $usrTimezone == $timezone) || set_value("timezone") == $timezone ? " selected" : ""); ?>><?=$timezone?></option>
									<?php
										endif;
									endforeach;
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body"><center>
					<button type="submit" class="btn btn-success">Save</button>
					<a href="<?php echo current_url(); ?>" class="btn btn-default" role="button" title="Cancel Edit Profile">Cancel</a>
				</center></div>
			</form>
		</div><!-- /.panel-success -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
