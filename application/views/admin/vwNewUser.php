<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Create New User View of Client Dashboard Admin Interface
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
			if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 8 seconds you will be redirected to Dashboard. <a href=\"". current_url() ."\" title=\"Create New User\">Click here to Create New User again.</a></p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("admin/dashboard") ."';}, 8000);\n</script></div>";
		?>
		<div class="panel panel-success">
			<div class="panel-heading text-right">
				<?php
				if ($this->session->userdata("user_type") == "Super Administrator") {
				?>
				<a href="<?php echo site_url("admin/users/view"); ?>" class="btn btn-default" role="button" title="Show Users Info">View Users</a>
				<?php
				} else {
					echo "<h6>&nbsp;</h6>";
				}
				?>
			</div>
			<form class="form-horizontal" method="post">
				<div class="panel-body">
					<div class="form-group">
						<div class="form-group">
							<label for="given_name" class="col-sm-3 control-label" title="Required field">Given Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Given Name" name="given_name" id="given_name" required value="<?php echo set_value("given_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="last_name" class="col-sm-3 control-label" title="Required field">Last Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Last Name" name="last_name" id="last_name" required value="<?php echo set_value("last_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-3 control-label" title="Required field">Email Address <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="email" class="form-control" placeholder="Email Address" name="email" id="email" required value="<?php echo set_value("email"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="col-sm-3 control-label" title="Required field">Password <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="password" class="form-control" placeholder="Password" name="password" id="password" required value="<?php echo set_value("password"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="confirm_password" class="col-sm-3 control-label" title="Required field">Confirm Password <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password" id="confirm_password" required value="<?php echo set_value("confirm_password"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="user_role" class="col-sm-3 control-label" title="Required field">Profile/Role <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<select class="form-control" name="user_role" id="user_role" required>
									<option<?php echo (!set_value("user_role") ? " selected" : ""); ?> disabled>Select Profile/Role</option>
									<?php
									if (isset($userRoles)) :
										foreach ($userRoles as $userRole) :
									?>
									<option value="<?=$userRole["user_role_name"]?>"<?php echo (set_value("user_role") == $userRole["user_role_name"] ? " selected" : ""); ?>><?=$userRole["user_role_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="timezone" class="col-sm-3 control-label" title="Required field">Timezone <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<select class="form-control" name="timezone" id="timezone" required>
									<option<?php echo (!set_value("timezone") ? " selected" : ""); ?> disabled>Select Timezone</option>
									<?php
									$arrTimezone = timezone_identifiers_list();
									foreach ($arrTimezone as $timezone) :
										if (trim($timezone) != "UTC") :
									?>
									<option value="<?=$timezone?>"<?php echo (set_value("timezone") == $timezone ? " selected" : ""); ?>><?=$timezone?></option>
									<?php
										endif;
									endforeach;
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body"><center>
					<button type="submit" class="btn btn-success">Save</button>
					<a href="<?php echo current_url(); ?>" class="btn btn-default" role="button" title="Cancel Create New User">Cancel</a>
				</center></div>
			</form>
		</div><!-- /.panel-success -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
