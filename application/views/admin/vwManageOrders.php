<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Manage Orders Information View of Client Dashboard Admin Interface
 *
 */
$orderAction = $this->input->post("order_action");
$postOrderInfoID = $this->input->post("order_info_id");
$urlCancel = (strpos($_SERVER["HTTP_REFERER"], "/new") === false ? $_SERVER["HTTP_REFERER"] : str_replace("/new", "", $_SERVER["HTTP_REFERER"]));
$urlSave = (strpos(current_url(), "/new") === false ? current_url() : str_replace("/new", "", current_url()));
$clients = $this->session->userdata("clientNames");
?>
	<div class="row">
		<div class="col-lg-12">
			<?php
				if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
				if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After ". (!$postOrderInfoID ? "8" : "3") ." seconds ". ($postOrderInfoID ? "Manage Orders Information page will be refreshed" : "you will be redirected to Manage Submitted Orders") .".</p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("admin/orders". ($postOrderInfoID ? "/view" : "")) ."';}, ". (!$postOrderInfoID ? "8" : "3") ."000);\n</script></div>";
			?>
			<div class="panel panel-default">
				<div class="panel-heading text-right">
					<h4 class="panel-title pull-left">&nbsp;</h4>
					<a href="#" data-href="<?=site_url("admin/orders/new_order_info")?>" data-action="New" data-toggle="modal" data-target="#dialogBox"><span class="sr-only">New Order/Campaign</span><i class="fam-add" title="Create New Campaign/Order" data-toggle="tooltip" data-placement="bottom"></i></a>
				</div>
				<div class="table-responsive">
					<small>
					<table class="table table-striped table-hover tablesorter">
						<thead>
							<tr>
								<th>#</th>
								<th title="Sort by Campaign/Order Name" data-toggle="tooltip" data-placement="bottom">Campaign/Order <i class="fa fa-sort"></i></th>
								<?php if (count($clients) > 1) : ?>
								<th title="Sort by Client Name" data-toggle="tooltip" data-placement="bottom">Client <i class="fa fa-sort"></i></th>
								<?php endif; ?>
								<th title="Sort by Advertiser" data-toggle="tooltip" data-placement="bottom">Advertiser <i class="fa fa-sort"></i></th>
								<th title="Sort by Ad Server" data-toggle="tooltip" data-placement="bottom">Ad Server <i class="fa fa-sort"></i></th>
								<th title="Sort by Start Date" data-toggle="tooltip" data-placement="bottom">Start Date <i class="fa fa-sort"></i></th>
								<th title="Sort by End Date" data-toggle="tooltip" data-placement="bottom">End Date <i class="fa fa-sort"></i></th>
								<th title="Sort by Comments" data-toggle="tooltip" data-placement="bottom">Comments <i class="fa fa-sort"></i></th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
				<?php
				if ($pageNum == "new") :
				?>
						<tr class="new"><form class="form-horizontal" method="post">
							<input type="hidden" name="order_code" value="<?=$orderCode?>" />
							<input type="hidden" name="order_name" value="<?=$originalOrderName?>" />
							<td>&nbsp;</td>
							<td><?=$orderName?></td>
							<?php if (count($clients) > 1) : ?>
							<td><?=$clientName?></td>
							<?php endif; ?>
							<td><input type="text" class="form-control input-sm" name="advertiser" value="<?=set_value("advertiser")?>" placeholder="Advertiser" size="10" required /></td>
							<td><input type="text" class="form-control input-sm" name="ad_server" value="<?=set_value("ad_server")?>" placeholder="Ad Server" size="10" required /></td>
							<td><input type="date" class="form-control input-sm" name="start_date" value="<?=set_value("start_date")?>" placeholder="yyyy-mm-dd" size="9" required /></td>
							<td><input type="date" class="form-control input-sm" name="end_date" value="<?=set_value("end_date")?>" placeholder="yyyy-mm-dd" size="9" required /></td>
							<td><input type="text" class="form-control input-sm" name="comments" value="<?=set_value("comments")?>" placeholder="Comments" size="10" /></td>
							<td style="vertical-align:middle"><button type="submit" class="btn btn-danger btn-sm btn-save">Save</button> <a href="<?=$urlCancel?>" role="button" class="btn btn-default btn-sm btn-cancel">Cancel</a></td>
							<input type="hidden" name="order_submission_id" value="<?=$orderSubmissionID?>" />
							</form>
						</tr>
				<?php
				endif;
				if (isset($records) && $total) :
					foreach ($records as $ctr => $record) :
						$id = $record["order_info_id"];
						$dateFormat = "%F %j, %Y";
						if ($orderAction == "edit" && $postOrderInfoID == $id) $dateFormat = "%Y-%m-%d";
						$startDate = mdate($dateFormat, syncDate($record["start_date"]));
						$endDate = mdate($dateFormat, syncDate($record["end_date"]));
						$orderName = $record["order_name"];
				?>
						<tr>
							<td><?=($ctr + 1 + $offset)?></td>
							<td><a data-href="<?=site_url("admin/orders/checklist/$id")?>" onclick="return false;" data-action="Manage" data-toggle="modal" data-target="#dialogBox"><span title="Manage Campaign Checklist" data-toggle="tooltip" data-placement="bottom"><?=$orderName?></span></a></td>
							<?php if (count($clients) > 1) : ?>
							<td><?=$record["client_name"]?></td>
							<?php endif; ?>
							<?=($orderAction == "edit" && $postOrderInfoID == $id ? '<form class="form-horizontal" action="'. $urlSave .'" method="post"><input type="hidden" name="order_info_id" value="'. $id .'" />' : '')?>
							<td><?=($orderAction == "edit" && $postOrderInfoID == $id ? '<input type="text" class="form-control input-sm" name="advertiser" value="'. $record["advertiser"] .'" placeholder="Advertiser" size="10" required />' : $record["advertiser"])?></td>
							<td><?=($orderAction == "edit" && $postOrderInfoID == $id ? '<input type="text" class="form-control input-sm" name="ad_server" value="'. $record["ad_server"] .'" placeholder="Ad Server" size="10" required />' : $record["ad_server"])?></td>
							<td><?=($orderAction == "edit" && $postOrderInfoID == $id ? '<input type="date" class="form-control input-sm" name="start_date" value="'. $startDate .'" placeholder="yyyy-mm-dd" size="9" required />' : $startDate)?></td>
							<td><?=($orderAction == "edit" && $postOrderInfoID == $id ? '<input type="date" class="form-control input-sm" name="end_date" value="'. $endDate .'" placeholder="yyyy-mm-dd" size="9" required />' : $endDate)?></td>
							<td><?=($orderAction == "edit" && $postOrderInfoID == $id ? '<input type="text" class="form-control input-sm" name="comments" value="'. $record["comments"] .'" placeholder="Comments" size="10" />' : $record["comments"])?></td>
						<?php
						if ($orderAction == "edit" && $postOrderInfoID == $id) :
						?>
							<td style="vertical-align:middle"><button type="submit" class="btn btn-danger btn-sm btn-save">Save</button> <a href="<?=$urlCancel?>" role="button" class="btn btn-default btn-sm btn-cancel">Cancel</a></td>
							<div class="hide"><input type="hidden" name="order_code" value="<?=$record["order_code"]?>" />
							<input type="hidden" name="status_id" value="<?=$record["status_id"]?>" />
							<input type="hidden" name="order_name" value="<?=$orderName?>" />
							<input type="hidden" name="order_submission_id" value="<?=$record["order_submission_id"]?>" /></div>
							</form>
						<?php
						else :
						?>
							<td>
								<form id="frmEdit<?=$id?>" style="display:inline-block" action="<?=$urlSave?>" method="post"><a href="#" onclick="document.getElementById('frmEdit<?=$id?>').submit();return false;" title="Edit Campaign/Order Information" data-toggle="tooltip" data-placement="left"><i class="fam-pencil"></i></a><input type="hidden" name="order_info_id" value="<?=$id?>" /><input type="hidden" name="order_action" value="edit" /></form>
								<a href="#" data-href="<?=site_url("admin/$page/remove_order_info/$id")?>" data-action="Remove" data-toggle="modal" data-target="#dialogBox"><i class="fam-cancel" title="Delete Campaign/Order" data-toggle="tooltip" data-placement="left"></i></a>
							</td>
						<?php
						endif;
						?>
						</tr>
					<?php
					endforeach;//foreach ($records as $ctr => $record)
				elseif (!$this->input->post()) :
				?>
						<tr class="text-danger text-center">
							<td colspan="10">
								<h4>No <?=ucfirst($page)?> information found...</h4>
							</td>
						</tr>
				<?php
				endif;//if (isset($records) && $total)
				?>
					</tbody>
					</table></small>
				</div>
			</div><!-- /.panel-default -->
			<?php
			if (isset($records) && $total && $totalPages > 1) :
			?>
			<ul class="pagination pull-right">
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view")?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Start" data-toggle="tooltip" data-placement="bottom">&laquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/". ($pageNum > 2 ? $pageNum - 1 : ""))?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Previous" data-toggle="tooltip" data-placement="bottom">&lsaquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/view")?>" title="Page 1" data-toggle="tooltip" data-placement="bottom">1<?=(!$pageNum || $pageNum == 1 ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				for ($ctr = 2; $ctr <= $totalPages; $ctr++) :
				?>
				<li<?=($pageNum == $ctr ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/view/$ctr")?>"<?=($pageNum == $ctr ? ' onclick="return false;"' : "")?> title="Page <?=$ctr?>" data-toggle="tooltip" data-placement="bottom"><?=$ctr?><?=($pageNum == $ctr ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				endfor;
				?>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/". ($pageNum ? $pageNum + 1 : 2))?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="Next" data-toggle="tooltip" data-placement="bottom">&rsaquo;</a></li>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/end")?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="End" data-toggle="tooltip" data-placement="bottom">&raquo;</a></li>
			</ul>
			<?php
			endif;
			?>
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
