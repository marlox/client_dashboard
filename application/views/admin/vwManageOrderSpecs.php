<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Manage Order Specifications View of Client Dashboard Admin Interface
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
			<?php
				if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
				if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 3 seconds Manage Order Specifications page will be refreshed.</p><form id=\"frmViewSpecs\" style=\"display:inline\" action=\"". site_url("admin/$page/specs") ."\" method=\"post\"><input type=\"hidden\" name=\"order_info_id\" value=\"". $orderInfoID ."\" /><input type=\"hidden\" name=\"specs_action\" value=\"view\" /></form><script type=\"text/javascript\">\nwindow.setTimeout(function(){document.getElementById('frmViewSpecs').submit();}, 3000);\n</script></div>";
			?>
			<div class="panel panel-white panel-default">
				<div class="panel-heading text-right">
				<?php if (isset($orderInfoID)) : ?>
					<h4 class="panel-title pull-left" title="Showing <?=$orderName?> Specifications" data-toggle="tooltip" data-placement="left"><?=$orderName?></h4>
				<?php endif; ?>
				<?php if (isset($orderInfoID) && $action !== "add_to") : ?>
					<form id="frmAddSpecs" method="post"><a href="#" onclick="document.getElementById('frmAddSpecs').submit();return false;" title="Create New Order Specification" data-toggle="tooltip" data-placement="bottom"><span class="sr-only">New Specification</span><i class="fam-add"></i></a><input type="hidden" name="order_info_id" value="<?=$orderInfoID?>" /><input type="hidden" name="specs_action" value="add_to" /></form>
				<?php endif; ?>
					<span>&nbsp;</span>
				</div>
				<div class="table-responsive">
					<small>
					<table class="table table-striped table-hover tablesorter">
						<thead>
							<tr>
								<th>#</th>
								<th title="Sort by Specs Type/Placement" data-toggle="tooltip" data-placement="bottom">Type/Placement <i class="fa fa-sort"></i></th>
								<th title="Sort by Type Tag" data-toggle="tooltip" data-placement="bottom">Type Tag <i class="fa fa-sort"></i></th>
								<th title="Sort by Placement Name" data-toggle="tooltip" data-placement="bottom">Placement Name <i class="fa fa-sort"></i></th>
								<th title="Sort by Package Name" data-toggle="tooltip" data-placement="bottom">Package Name <i class="fa fa-sort"></i></th>
								<th title="Sort by Creative Status" data-toggle="tooltip" data-placement="bottom">Creative <i class="fa fa-sort"></i></th>
								<th title="Sort by URL Status" data-toggle="tooltip" data-placement="bottom">URL <i class="fa fa-sort"></i></th>
								<th title="Sort by Message Status" data-toggle="tooltip" data-placement="bottom">Message <i class="fa fa-sort"></i></th>
								<th title="Sort by Specs Status" data-toggle="tooltip" data-placement="bottom"<?=($action !== "view" ? ' class="hide"' : "")?>>Status <i class="fa fa-sort"></i></th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
				<?php
				if ($action == "add_to" && $this->input->post()) :
					$orderSpecType = set_value("order_spec_type") ? set_value("order_spec_type") : $specName;
				?>
						<tr class="new"><form class="form-horizontal" method="post">
							<input type="hidden" name="order_info_id" value="<?=$orderInfoID?>" />
							<input type="hidden" name="specs_action" value="save" />
							<td>&nbsp;</td>
							<td><input type="text" class="form-control input-sm" name="order_spec_type" id="order_spec_type" value="<?=$orderSpecType?>" placeholder="Specs Type/Placement" title="Specs Type/Placement" data-toggle="tooltip" data-placement="bottom" size="10" required /></td>
							<td><input type="text" class="form-control input-sm" name="spec_type_tag" id="spec_type_tag" value="<?=set_value("spec_type_tag")?>" placeholder="Type Tag" size="5" required /></td>
							<td id="order_spec_name">
								<input type="hidden" name="order_spec_name" value="<?=set_value("order_spec_name")?>" />
								<input type="text" class="form-control input-sm input-naming-convention" name="spec_site_name" id="spec_site_name" value="<?=set_value("spec_site_name")?>" placeholder="Site Name" title="Site Name" data-toggle="tooltip" data-placement="bottom" required />_<input type="text" class="form-control input-sm input-naming-convention" name="spec_buy_mode" value="<?=set_value("spec_buy_mode")?>" placeholder="Buy Mode" title="Buy Mode" data-toggle="tooltip" data-placement="bottom" required />_<div style="display:inline" class="placement" title="Placement" data-toggle="tooltip" data-placement="bottom"> <?=str_replace(" ", "", ucwords($orderSpecType))?></div>_<input type="text" class="form-control input-sm input-naming-convention" name="spec_format_code" value="<?=set_value("spec_format_code")?>" placeholder="Format Code" title="Format Code" data-toggle="tooltip" data-placement="bottom" required />_<div style="display:inline" id="type_tag" title="Type Tag" data-toggle="tooltip" data-placement="bottom"> <?=str_replace(" ", "", ucwords(set_value("spec_type_tag")))?></div>_<div style="display:inline" title="ID" data-toggle="tooltip" data-placement="bottom" class="spec_id"><?=$specID?></div>
								<input type="hidden" name="spec_id" value="<?=$specID?>" />
							</td>
							<td id="spec_package">
								<input type="hidden" name="spec_package_name" value="<?=set_value("spec_package_name")?>" />
								Pack <div style="display:inline" id="site_name" title="Site name" data-toggle="tooltip" data-placement="bottom"><?=str_replace(" ", "", ucwords(set_value("spec_site_name")))?></div>_<div style="display:inline" class="placement" title="Placement" data-toggle="tooltip" data-placement="bottom"><?=str_replace(" ", "", ucwords($orderSpecType))?></div>_<div style="display:inline" title="ID" data-toggle="tooltip" data-placement="bottom" class="spec_id"><?=$specID?></div>
							</td>
							<td>
								<select class="form-control input-sm" name="creative_status_id" id="creative_status_id" title="Select Status for Creative Specifications" required>
									<option<?php echo (!($selected = set_value("creative_status_id")) ? " selected" : ""); ?> disabled>Select Creative Status</option>
									<?php
									if (isset($status)) :
										foreach ($status as $record) :
									?>
									<option value="<?=$record["status_id"]?>"<?php echo ($record["status_id"] == $selected ? " selected" : ""); ?>><?=$record["status_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</td>
							<td>
								<select class="form-control input-sm" name="url_status_id" id="url_status_id" title="Select Status for URL Specifications" required>
									<option<?php echo (!($selected = set_value("url_status_id")) ? " selected" : ""); ?> disabled>Select URL Status</option>
									<?php
									if (isset($status)) :
										foreach ($status as $record) :
									?>
									<option value="<?=$record["status_id"]?>"<?php echo ($record["status_id"] == $selected ? " selected" : ""); ?>><?=$record["status_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</td>
							<td>
								<select class="form-control input-sm" name="message_status_id" id="message_status_id" title="Select Status for Message Specifications" required>
									<option<?php echo (!($selected = set_value("message_status_id")) ? " selected" : ""); ?> disabled>Select Message Status</option>
									<?php
									if (isset($status)) :
										foreach ($status as $record) :
									?>
									<option value="<?=$record["status_id"]?>"<?php echo ($record["status_id"] == $selected ? " selected" : ""); ?>><?=$record["status_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</td>
							<td class="hide">&nbsp;</td>
							<td style="vertical-align:middle"><button type="submit" class="btn btn-danger btn-sm btn-save" id="btnSave">Save</button> <a href="<?=$urlCancel?>" role="button" class="btn btn-default btn-sm btn-cancel">Cancel</a></td>
							</form>
						</tr>
				<?php
				endif;//if ($action == "add_to" && $this->input->post())
				if (isset($records) && $total) :
					foreach ($records as $ctr => $record) :
						$id = $record["order_spec_id"];
						$orderSpecType = set_value("order_spec_type") ? set_value("order_spec_type") : $record["order_spec_type"];
						$orderSpecName = set_value("order_spec_name") ? set_value("order_spec_name") : $record["order_spec_name"];
						$specSiteName = set_value("spec_site_name") ? set_value("spec_site_name") : $record["spec_site_name"];
						$specBuyMode = set_value("spec_buy_mode") ? set_value("spec_buy_mode") : $record["spec_buy_mode"];
						$specFormatCode = set_value("spec_format_code") ? set_value("spec_format_code") : $record["spec_format_code"];
						$specTypeTag = set_value("spec_type_tag") ? set_value("spec_type_tag") : $record["spec_type_tag"];
						$specID = set_value("spec_id") ? set_value("spec_id") : $record["spec_id"];
						$specPackageName = set_value("spec_package_name") ? set_value("spec_package_name") : $record["package_name"];
						$creativeStatusID = set_value("creative_status_id") ? set_value("creative_status_id") : $record["creative_status_id"];
						$urlStatusID = set_value("url_status_id") ? set_value("url_status_id") : $record["url_status_id"];
						$messageStatusID = set_value("message_status_id") ? set_value("message_status_id") : $record["message_status_id"];
				?>
						<tr>
							<td><?=($ctr + 1 + $offset)?></td>
						<?php if ($action == "edit" && $orderSpecID == $id) : ?>
							<form class="form-horizontal" action="<?=$urlSave?>" method="post"><input type="hidden" name="order_spec_id" value="<?=$id?>" /><input type="hidden" name="order_info_id" value="<?=$orderInfoID?>" /><input type="hidden" name="specs_action" value="save" />
							<td><input type="text" class="form-control input-sm" name="order_spec_type" id="order_spec_type" value="<?=$orderSpecType?>" placeholder="Specs Type/Placement" title="Specs Type/Placement" data-toggle="tooltip" data-placement="bottom" size="10" required /></td>
							<td><input type="text" class="form-control input-sm" name="spec_type_tag" id="spec_type_tag" value="<?=$specTypeTag?>" placeholder="Type Tag" size="5" required /></td>
							<td id="order_spec_name">
								<input type="hidden" name="order_spec_name" value="<?=$orderSpecName?>" />
								<input type="text" class="form-control input-sm input-naming-convention" name="spec_site_name" id="spec_site_name" value="<?=$specSiteName?>" placeholder="Site Name" title="Site Name" data-toggle="tooltip" data-placement="bottom" required />_<input type="text" class="form-control input-sm input-naming-convention" name="spec_buy_mode" value="<?=$specBuyMode?>" placeholder="Buy Mode" title="Buy Mode" data-toggle="tooltip" data-placement="bottom" required />_<div style="display:inline" class="placement" title="Placement" data-toggle="tooltip" data-placement="bottom"><?=str_replace(" ", "", ucwords($orderSpecType))?></div>_<input type="text" class="form-control input-sm input-naming-convention" name="spec_format_code" value="<?=$specFormatCode?>" placeholder="Format Code" title="Format Code" data-toggle="tooltip" data-placement="bottom" required />_<div style="display:inline" id="type_tag" title="Type Tag" data-toggle="tooltip" data-placement="bottom"><?=str_replace(" ", "", ucwords($specTypeTag))?></div>_<div style="display:inline" title="ID" data-toggle="tooltip" data-placement="bottom" class="spec_id"><?=$specID?></div>
								<input type="hidden" name="spec_id" value="<?=$specID?>" />
							</td>
							<td id="spec_package">
								<input type="hidden" name="spec_package_name" value="<?=set_value("spec_package_name")?>" />
								Pack <div style="display:inline" id="site_name" title="Site name" data-toggle="tooltip" data-placement="bottom"><?=str_replace(" ", "", ucwords($specSiteName))?></div>_<div style="display:inline" class="placement" title="Placement" data-toggle="tooltip" data-placement="bottom"><?=str_replace(" ", "", ucwords($orderSpecType))?></div>_<div style="display:inline" title="ID" data-toggle="tooltip" data-placement="bottom" class="spec_id"><?=$specID?></div>
							</td>
							<td>
								<select class="form-control input-sm" name="creative_status_id" id="creative_status_id" title="Select Status for Creative Specifications" required>
									<option<?php echo (!($selected = $creativeStatusID) ? " selected" : ""); ?> disabled>Select Creative Status</option>
									<?php
									if (isset($status)) :
										foreach ($status as $record) :
									?>
									<option value="<?=$record["status_id"]?>"<?php echo ($record["status_id"] == $selected ? " selected" : ""); ?>><?=$record["status_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</td>
							<td>
								<select class="form-control input-sm" name="url_status_id" id="url_status_id" title="Select Status for URL Specifications" required>
									<option<?php echo (!($selected = $urlStatusID) ? " selected" : ""); ?> disabled>Select URL Status</option>
									<?php
									if (isset($status)) :
										foreach ($status as $record) :
									?>
									<option value="<?=$record["status_id"]?>"<?php echo ($record["status_id"] == $selected ? " selected" : ""); ?>><?=$record["status_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</td>
							<td>
								<select class="form-control input-sm" name="message_status_id" id="message_status_id" title="Select Status for Message Specifications" required>
									<option<?php echo (!($selected = $messageStatusID) ? " selected" : ""); ?> disabled>Select Message Status</option>
									<?php
									if (isset($status)) :
										foreach ($status as $record) :
									?>
									<option value="<?=$record["status_id"]?>"<?php echo ($record["status_id"] == $selected ? " selected" : ""); ?>><?=$record["status_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</td>
							<td class="hide">&nbsp;</td>
							<td style="vertical-align:middle"><button type="submit" class="btn btn-danger btn-sm btn-save" id="btnSave">Save</button> <a href="<?=$urlCancel?>" role="button" class="btn btn-default btn-sm btn-cancel">Cancel</a></td>
							</form>
						<?php else : ?>
							<td><small><?=$record["order_spec_type"]?></small></td>
							<td><small><?=$record["spec_type_tag"]?></small></td>
							<td><small><?=$record["placement_name"]?></small></td>
							<td><small><?=$record["package_name"]?></small></td>
							<td><small><?=$record["creative_status"]?></small></td>
							<td><small><?=$record["url_status"]?></small></td>
							<td><small><?=$record["message_status"]?></small></td>
							<td<?=($action !== "view" ? ' class="hide"' : "")?>><small><?=$record["status"]?></small></td>
							<td>
								<form id="frm<?=$id?>" style="display:inline" action="<?=$urlSave?>" method="post"><a href="#" onclick="document.getElementById('frm<?=$id?>').submit();return false;" title="Edit Order Specifications" data-toggle="tooltip" data-placement="left"><i class="fam-pencil"></i></a><input type="hidden" name="order_spec_id" value="<?=$id?>" /><input type="hidden" name="order_info_id" value="<?=$orderInfoID?>" /><input type="hidden" name="specs_action" value="edit" /></form>
								<a href="#" data-href="<?=site_url("admin/$page/remove_order_specs/$id/$orderInfoID")?>" data-action="Remove" data-toggle="modal" data-target="#dialogBox"><i class="fam-cancel" title="Delete Order Specifications" data-toggle="tooltip" data-placement="left"></i></a>
							</td>
						<?php
						endif;//if ($action == "edit" && $orderSpecID == $id)
						?>
						</tr>
					<?php
					endforeach;//foreach ($records as $ctr => $record)
				elseif (!isset($orderInfoID)) :
				?>
						<tr class="text-danger text-center">
							<td colspan="10">
								<h4>Invalid or blank Order Information found...</h4>
							</td>
						</tr>
				<?php
				elseif ($action != "add_to" && !$total) :
				?>
						<tr class="text-danger text-center">
							<td colspan="10">
								<h4>No <?=$orderName?> specifications found...</h4>
							</td>
						</tr>
				<?php
				endif;//if (isset($records) && $total)
				?>
					</tbody>
					</table></small>
				</div>
			</div><!-- /.panel-white -->
			<?php
			if (isset($records) && $total && $totalPages > 1) :
			?>
			<ul class="pagination pull-right">
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/specs")?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Start" data-toggle="tooltip" data-placement="bottom">&laquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/specs/". ($pageNum > 2 ? $pageNum - 1 : ""))?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Previous" data-toggle="tooltip" data-placement="bottom">&lsaquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/specs")?>" title="Page 1" data-toggle="tooltip" data-placement="bottom">1<?=(!$pageNum || $pageNum == 1 ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				for ($ctr = 2; $ctr <= $totalPages; $ctr++) :
				?>
				<li<?=($pageNum == $ctr ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/specs/$ctr")?>"<?=($pageNum == $ctr ? ' onclick="return false;"' : "")?> title="Page <?=$ctr?>" data-toggle="tooltip" data-placement="bottom"><?=$ctr?><?=($pageNum == $ctr ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				endfor;
				?>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/specs/". ($pageNum ? $pageNum + 1 : 2))?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="Next" data-toggle="tooltip" data-placement="bottom">&rsaquo;</a></li>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/specs/end")?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="End" data-toggle="tooltip" data-placement="bottom">&raquo;</a></li>
			</ul>
			<?php
			endif;
			?>
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
