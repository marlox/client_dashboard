<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * New Client Account View of Client Dashboard Admin Interface
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
			if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 8 seconds you will be redirected to Dashboard. <a href=\"". current_url() ."\" title=\"New Client Account\">Click here to set New Client Account again.</a></p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("admin/dashboard") ."';}, 8000);\n</script></div>";
		?>
		<div class="panel panel-primary">
			<div class="panel-heading text-right">
					<a href="<?php echo site_url("admin/clients/view"); ?>" class="btn btn-default" role="button" title="Show Clients Account">View Clients Account</a>
			</div>
			<form class="form-horizontal" method="post" enctype="multipart/form-data" accept-charset="utf-8">
				<div class="panel-body">
					<div class="form-group">
						<div class="form-group">
							<label for="client_name" class="col-sm-2 control-label" title="Required field">Client Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-9">
								<input type="text" class="form-control" placeholder="Client Name" name="client_name" id="client_name" required value="<?php echo set_value("client_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="logo" class="col-sm-2 control-label">Logo &nbsp;</label>
							<div class="col-sm-9">
								<input type="file" class="form-control" placeholder="Client Logo" title="Client Logo" name="logo" id="logo" value="" />
								<div class="text-info"><em>Logo should not be more than 50px of height and 150px of width. .PNG file format only.</em></div>
							</div>
						</div>
						<div class="form-group">
							<label for="theme" class="col-sm-2 control-label" title="Preview purposes only in Admin Interface (Required field)">Theme <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-9">
								<?php
								if (isset($themes)) :
									foreach ($themes as $theme) :
								?>
								<a href="#" onclick="return changeCSS(this);" id="<?=$theme["theme_id"]?>" rel="<?=$theme["css_file"]?>"><span class="color_box<?=(set_value("theme_id") == $theme["theme_id"] ? " selected" : "")?>" style="background-color:<?=($theme["theme_name"] == "Bronze" ? "#c09853" : ($theme["theme_name"] == "Default" ? "#428bca" : str_replace(" ", "", $theme["theme_name"])))?>" title="<?=$theme["theme_name"]?>" data-toggle="tooltip" data-placement="bottom">&nbsp;</span></a> &nbsp;
								<?php
									endforeach;
								endif;
								?>
								<input type="hidden" name="theme_id" id="theme" required />
								<div class="text-info"><em>Theme will only take effect in Client Interface. Selected theme only for preview in Admin Interface.</em></div>
							</div>
						</div>
						<div class="form-group">
							<label for="project" class="col-sm-2 control-label" title="Required field">Project(s) <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-9">
								<select class="form-control" name="project_id[]" id="project" multiple="" required>
									<option<?php echo (!($selectedProjects = set_value("project_id")) ? " selected" : ""); ?> disabled>Select Project(s)</option>
									<?php
									if (isset($projects)) :
										if (!$selectedProjects) $selectedProjects = array();
										foreach ($projects as $project) :
									?>
									<option value="<?=$project["project_id"]?>"<?php echo (array_search($project["project_id"], $selectedProjects) !== false ? " selected" : ""); ?>><?=$project["project_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="users" class="col-sm-2 control-label" title="Required field">User(s) <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-9">
								<select class="form-control" name="users[]" id="users" multiple required>
									<option<?php echo (!($selectedUsers = set_value("users")) ? " selected" : ""); ?> disabled>Select User(s)</option>
									<?php
									if (isset($users)) :
										if (!$selectedUsers) $selectedUsers = array();
										foreach ($users as $user) :
									?>
									<option value="<?=$user["username"]?>"<?php echo (preg_match("/(\ ". str_replace(".", "\\.", $user["username"]) .")+/i", (" ". implode(", ", $selectedUsers))) ? " selected" : ""); ?>><?=(isset($user["full_name"]) ? $user["full_name"] : $user["username"])?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body"><center>
					<button type="submit" class="btn btn-primary">Save</button>
					<a href="<?php echo current_url(); ?>" class="btn btn-default" role="button" title="Cancel New Client Account">Cancel</a>
				</center></div>
			</form>
		</div><!-- /.panel-primary -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
