<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Manage Users View of Client Dashboard Admin Interface
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success">
				<div class="panel-heading text-right"><a href="<?php echo site_url("admin/users"); ?>" class="btn btn-default" title="Create New User">New User</a></div>
				<div class="table-responsive">
				<table class="table table-striped table-hover tablesorter">
					<?php
					if (isset($records) && $total) {
						$fields = array_keys($records[0]);
					?>
					<thead>
						<tr>
							<th>#</th>
						<?php
						foreach ($fields as $field) {
							if (!preg_match("/(user\_id|\_name|is\_)+/i", $field)) {
								if ($field == "user_role_id") $field = "user role";
						?>
							<th title="Sort by <?=ucwords(str_replace("_", " ", $field))?>" data-toggle="tooltip" data-placement="bottom"><?=ucwords(str_replace("_", " ", $field))?> <i class="fa fa-sort"></i></th>
						<?php
							}
						}
						?>
							<th<?=$this->session->userdata("user_type") == "Super Administrator" ? "" : ' class="hide"'?>>&nbsp;</th>
						</tr>
					</thead>
					<?php
						foreach ($records as $ctr => $record) {
					?>
						<tr id="<?=$record["user_id"]?>">
							<td><?=($ctr + 1 + $offset)?></td>
							<?php
							foreach ($record as $field => $value) {
								if (!preg_match("/(user\_id|\_name|is\_)+/i", $field)) {
									if (preg_match("/(\_update)+/i", $field)) $value = mdate("%d-%M-%Y %h:%i%a", syncDate($value));
									if ($field == "user_role_id") $value = $userRoles[$value];
							?>
							<td><?=$value?></td>
							<?php
								}
							}
							?>
							<td<?=$this->session->userdata("user_type") == "Super Administrator" ? "" : ' class="hide"'?>>
								<a data-href="<?=site_url("admin/$page/edit/". $record["user_id"])?>" data-action="Edit" data-toggle="modal" data-target="#dialogBox"><i class="fam-user-edit" title="Edit User Information" data-toggle="tooltip" data-placement="bottom"></i></a>
								<a data-href="<?=site_url("admin/$page/delete/". $record["user_id"])?>" data-action="Delete" data-toggle="modal" data-target="#dialogBox"><i class="fam-user-delete" title="Delete User" data-toggle="tooltip" data-placement="bottom"></i></a>
							</td>
						</tr>
					<?php
						}
					} else {
						echo '<tr><th colspan="7" class="text-danger text-center"><h4>No '. ucfirst($page) .' found...</h4></th></tr>';
					}
					?>
				</table>
				</div>
			</div><!-- /.panel-success -->
			<?php
			if (isset($records) && $total && $totalPages > 1) :
			?>
			<ul class="pagination pull-right">
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view")?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Start" data-toggle="tooltip" data-placement="bottom">&laquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/". ($pageNum > 2 ? $pageNum - 1 : ""))?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Previous" data-toggle="tooltip" data-placement="bottom">&lsaquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/view")?>" title="Page 1" data-toggle="tooltip" data-placement="bottom">1<?=(!$pageNum || $pageNum == 1 ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				for ($ctr = 2; $ctr <= $totalPages; $ctr++) :
				?>
				<li<?=($pageNum == $ctr ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/view/$ctr")?>"<?=($pageNum == $ctr ? ' onclick="return false;"' : "")?> title="Page <?=$ctr?>" data-toggle="tooltip" data-placement="bottom"><?=$ctr?><?=($pageNum == $ctr ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				endfor;
				?>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/". ($pageNum ? $pageNum + 1 : 2))?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="Next" data-toggle="tooltip" data-placement="bottom">&rsaquo;</a></li>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/end")?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="End" data-toggle="tooltip" data-placement="bottom">&raquo;</a></li>
			</ul>
			<?php
			endif;
			?>
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
