<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * New Order Form View of Client Dashboard
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
			if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 8 seconds you will be redirected to Manage Orders Information. <a href=\"#\" data-href=\"". site_url("admin/orders/new_order_info") ."\" data-action=\"New\" data-toggle=\"modal\" data-target=\"#dialogBox\" title=\"Create New Order/Campaign Information\">Click here for New Order Info again.</a></p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("admin/orders/view") ."';}, 8000);\n</script></div>";
		?>
		<div class="panel panel-danger">
			<div class="panel-heading text-right">
				<h4>&nbsp;</h4>
			</div>
			<form class="form-horizontal" id="frmNewOrderInfo" method="post">
				<div class="panel-body">
					<div class="form-group">
						<div class="form-group">
							<label for="order_type" class="col-sm-3 control-label" title="Required field">Order Type <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<select class="form-control" name="order_type" id="order_type" title="Order Type" required>
									<option<?php echo (!set_value("order_type") ? " selected" : ""); ?> disabled>Select Order Type</option>
									<?php
									if (isset($types)) :
										foreach ($types as $type) :
									?>
									<option value="<?=$type?>"<?php echo (set_value("order_type") == $type ? " selected" : ""); ?>><?=$type?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</div>
						</div>
						<div class="form-group<?=count($clients) > 1 ? "" : " hide"?>">
							<label for="client_name" class="col-sm-3 control-label" title="Required field">Client <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
							<?php if (count($clients) > 1) : ?>
								<select class="form-control" name="client_name" id="client_name" title="Select Client" onchange="showProjects(this);" required>
									<option<?php echo (!set_value("client_name") && $currentClient !== set_value("client_name") ? " selected" : ""); ?> disabled>Select Client</option>
									<?php
									if (isset($clients)) :
										foreach ($clients as $client) :
									?>
									<option value="<?=$client?>"<?php echo (set_value("client_name") == $client || $client == $currentClient ? " selected" : ""); ?>><?=$client?></option>
									<?php
										endforeach;
										if (set_value("client_name")) $currentClient = set_value("client_name");
									endif;
									?>
								</select>
							<?php else : ?>
								<input type="text" class="form-control" placeholder="Client Name" name="client_name" id="client_name" title="Client Name" required value="<?=$currentClient?>" />
							<?php endif; ?>
							</div>
						</div>
						<div class="form-group">
							<label for="project" class="col-sm-3 control-label" title="Required field">Project <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<select class="form-control" name="project_id" id="project" title="Select Project" onchange="getProjectName(this);" required>
									<option<?php echo (!($selectedProject = set_value("project_id")) ? " selected" : ""); ?> disabled>Select Project</option>
									<?php
									$projectName = null;
									if (isset($projects) && isset($clientProjects)) :
										foreach ($clientProjects as $client => $arrProjects) :
											foreach ($arrProjects as $project_id => $clientClassName) :
									?>
									<option value="<?=$project_id?>"<?php echo ($project_id == $selectedProject ? " selected" : ""); ?> class="<?=(str_replace(" ", "_", $currentClient) == $client  ? $client : "$client hide")?>"><?=$projects[$project_id]?></option>
									<?php
											if (!isset($projectName)) $projectName = ($project_id == $selectedProject ? $projects[$project_id] : null);
											endforeach;
										endforeach;
									endif;
									?>
								</select>
								<input type="hidden" name="project_name" id="project_name" value="<?=$projectName?>" class="hide" />
							</div>
						</div>
						<div class="form-group">
							<label for="order_name" class="col-sm-3 control-label" title="Required field">Order Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Order/Campaign Name" title="Order/Campaign Name" name="order_name" id="order_name" required value="<?php echo set_value("order_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="delivery_date" class="col-sm-3 control-label" title="Required field">Delivery Date <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="date" class="form-control" title="Delivery Date" placeholder="yyyy-mm-dd" name="delivery_date" id="delivery_date" required value="<?php echo set_value("delivery_date"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="order_specifications" class="col-sm-3 control-label">Order Specifications &nbsp;</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="order_specifications" id="order_specifications" title="Order Specifications"><?php echo set_value("order_specifications"); ?></textarea>
							</div>
						</div>
						<div class="divider"><hr /></div>
						<div class="form-group">
							<label for="advertiser" class="col-sm-3 control-label" title="Required field">Advertiser <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Advertiser" title="Advertiser" name="advertiser" id="advertiser" required value="<?php echo set_value("advertiser"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="ad_server" class="col-sm-3 control-label" title="Required field">Ad Server <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Ad Server" title="Ad Server" name="ad_server" id="ad_server" required value="<?php echo set_value("ad_server"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="start_date" class="col-sm-3 control-label" title="Required field">Start Date <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="date" class="form-control" title="Start Date" placeholder="yyyy-mm-dd" name="start_date" id="start_date" required value="<?php echo set_value("start_date"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="end_date" class="col-sm-3 control-label" title="Required field">End Date <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="date" class="form-control" title="End Date" placeholder="yyyy-mm-dd" name="end_date" id="end_date" required value="<?php echo set_value("end_date"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="comments" class="col-sm-3 control-label">Comments &nbsp;</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="comments" id="comments" title="Comments"><?php echo set_value("comments"); ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer"><center>
					<button type="submit" class="btn btn-danger" data-href="<?=site_url("admin/orders/new_order_info/submit")?>" data-action="New" data-toggle="modal" data-target="#dialogBox">Save</button>
					<button href="#" class="btn btn-default" role="button" data-dismiss="modal" aria-label="Cancel" title="Cancel New Order Information">Cancel</button>
				</center></div>
			</form>
		</div><!-- /.panel-danger -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
