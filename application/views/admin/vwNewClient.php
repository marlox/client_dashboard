<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Create New Client View of Client Dashboard Admin Interface
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
			if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 8 seconds you will be redirected to Dashboard. <a href=\"". base_url() ."admin/users\" title=\"Create New Client\">Click here to Create New User again.</a></p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". base_url() ."admin/dashboard';}, 8000);\n</script></div>";
		?>
		<div class="panel panel-primary">
			<div class="panel-heading text-right">&nbsp;</div>
			<form class="form-horizontal" method="post">
				<div class="panel-body">
					<div class="form-group">
						<div class="form-group">
							<label for="client_name" class="col-sm-2 control-label" title="Required field">Client Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-9">
								<input type="text" class="form-control" placeholder="Client Name" name="client_name" id="client_name" required value="<?php echo set_value("client_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="theme" class="col-sm-2 control-label" title="Required field">Theme <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-9">
								<select class="form-control" name="theme" id="theme" onchange="changeCSS(this);" required>
									<option<?php echo (!set_value("theme") ? " selected" : ""); ?> disabled>Select Theme</option>
									<?php
									if (isset($themes)) :
										foreach ($themes as $theme) :
									?>
									<option value="<?=$theme["theme_id"]?>" rel="<?=$theme["css_file"]?>"<?php echo (set_value("theme") == $theme["theme_id"] ? " selected" : ""); ?>><?=$theme["theme_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
								<div class="text-info"><em>Theme will only take effect in Client Interface, only for preview in Admin Interface.</em></div>
							</div>
						</div>
						<div class="form-group">
							<label for="address1" class="col-sm-2 control-label" title="Required field">Address 1 <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-9">
								<input type="text" class="form-control" placeholder="Address 1" name="address1" id="address1" required value="<?php echo set_value("address1"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="address2" class="col-sm-2 control-label">Address 2 &nbsp;</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" placeholder="Address 2" name="address2" id="address2" value="<?php echo set_value("address2"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="phone_number" class="col-sm-2 control-label">Phone Number &nbsp;</label>
							<div class="col-sm-9">
								<input type="number" class="form-control" placeholder="Phone Number" name="phone_number" id="phone_number" value="<?php echo set_value("phone_number"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="mobile_number" class="col-sm-2 control-label">Mobile Number &nbsp;</label>
							<div class="col-sm-9">
								<input type="number" class="form-control" placeholder="Mobile Number" name="mobile_number" id="mobile_number" value="<?php echo set_value("mobile_number"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">Email Address &nbsp;</label>
							<div class="col-sm-9">
								<input type="email" class="form-control" placeholder="Email Address" name="email" id="email" value="<?php echo set_value("email"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="website" class="col-sm-2 control-label">Website &nbsp;</label>
							<div class="col-sm-9">
								<input type="url" class="form-control" placeholder="Website" name="website" id="website" value="<?php echo set_value("website"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="logo" class="col-sm-2 control-label">Logo &nbsp;</label>
							<div class="col-sm-9">
								<input type="file" class="form-control" placeholder="Client Logo" name="logo" id="logo" value="" />
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body"><center>
					<button type="submit" class="btn btn-primary">Save</button>
					<a href="<?php echo base_url(); ?>admin/clients/newClient" class="btn btn-default" role="button" title="Cancel Create New Client">Cancel</a>
				</center></div>
			</form>
		</div><!-- /.panel-primary -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
