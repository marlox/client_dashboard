<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Messages View of Client Dashboard Admin Interface
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-info">
				<div class="panel-heading text-right">
					<a href="<?php echo site_url("contact/message"); ?>" class="btn btn-primary" title="Send Email Message" data-toggle="tooltip" data-placement="bottom">Send Email</a>
				</div>
				<div class="table-responsive">
				<table class="table table-striped table-hover tablesorter">
					<?php
					if (isset($records) && $total) {
						$fields = array_keys($records[0]);
					?>
					<thead>
						<tr>
							<th>#</th>
						<?php
						foreach ($fields as $field) {
							if (!preg_match("/(\_id|\_type|is\_|full\_)+/i", $field)) {
						?>
							<th title="Sort by <?=ucwords(str_replace("_", " ", $field))?>" data-toggle="tooltip" data-placement="bottom"><?=ucwords(str_replace("_", " ", $field))?> <i class="fa fa-sort"></i></th>
						<?php
							}
						}
						?>
							<th<?=$this->session->userdata("user_type") == "Super Administrator" ? "" : ' class="hide"'?>>&nbsp;</th>
						</tr>
					</thead>
					<?php
						foreach ($records as $ctr => $record) {
					?>
						<tr id="<?=$record["contact_message_id"]?>">
							<td><?=($ctr + 1 + $offset)?></td>
							<?php
							foreach ($record as $field => $value) {
								if (!preg_match("/(\_id|\_type|is\_|full\_)+/i", $field)) {
									if (preg_match("/(\_date)+/i", $field)) $value = isset($value) && trim($value) ? mdate("%d-%M-%Y %h:%i%a", syncDate($value)) : "";
									$strIsEmailSent = "";
									if (array_key_exists("is_email_sent", $record)) $strIsEmailSent = '<i class="fam-'. ($record["is_email_sent"] ? 'accept' : 'cancel') .'" title="'. ($record["is_email_sent"] ? 'Email Sent' : 'Not Sent') .'" data-toggle="tooltip" data-placement="bottom"></i>'.'<span class="sr-only">'. ($record["is_email_sent"] ? 'Email Sent' : 'Not Sent') .'</span> ';
							?>
							<td><?=($field == "message" ? $strIsEmailSent .'<i class="fa fa-'. $messageIcon[$record["message_type"]] .'" title="'. $record["message_type"] .'" data-toggle="tooltip" data-placement="top"></i> <span title="'. strip_tags($record["full_message"]) .'" data-toggle="tooltip" data-placement="right">' : "") . $value?></td>
							<?php
								}
							}
							?>
							<td>
								<?php
								if ($this->session->userdata("user_type") == "Super Administrator") :
								?>
								<a href="#" title="Show Message Info" data-toggle="tooltip" data-placement="bottom"><i class="fam-email-open"></i></a>
								<?php
								endif;
								?>
								<a href="<?php echo site_url("contact/message"); ?>?email=<?=$record["email"]?>&name=<?=$record["sent_by"]?>" title="Send Message Response" data-toggle="tooltip" data-placement="bottom"><i class="fam-email-go"></i></a>
							</td>
						</tr>
					<?php
						}
					} else {
						echo '<tr><th colspan="7" class="text-danger text-center"><h4>No '. ucfirst($page) .' found...</h4></th></tr>';
					}
					?>
				</table>
				</div>
			</div><!-- /.panel-info -->
			<?php
			if (isset($records) && $total && $totalPages > 1) :
			?>
			<ul class="pagination pull-right">
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view")?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Start" data-toggle="tooltip" data-placement="bottom">&laquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/". ($pageNum > 2 ? $pageNum - 1 : ""))?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Previous" data-toggle="tooltip" data-placement="bottom">&lsaquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/view")?>" title="Page 1" data-toggle="tooltip" data-placement="bottom">1<?=(!$pageNum || $pageNum == 1 ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				for ($ctr = 2; $ctr <= $totalPages; $ctr++) :
				?>
				<li<?=($pageNum == $ctr ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/view/$ctr")?>"<?=($pageNum == $ctr ? ' onclick="return false;"' : "")?> title="Page <?=$ctr?>" data-toggle="tooltip" data-placement="bottom"><?=$ctr?><?=($pageNum == $ctr ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				endfor;
				?>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/". ($pageNum ? $pageNum + 1 : 2))?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="Next" data-toggle="tooltip" data-placement="bottom">&rsaquo;</a></li>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/view/end")?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="End" data-toggle="tooltip" data-placement="bottom">&raquo;</a></li>
			</ul>
			<?php
			endif;
			?>
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
