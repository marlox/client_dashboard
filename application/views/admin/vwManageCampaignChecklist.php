<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Manage Campaign Checklist View of Admin Interface
 *
 */
if (isset($category)) :
	$orderName = "";
	$ctr = 0;
?>
<h4 class="text-center"><?=$campaignName?></h4>
<form id="campaignChecklist">
<?php
	foreach ($category as $cntCateg => $categ) :
?>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading<?=$cntCateg?>">
			<h6 class="panel-title">
				<a role="button" data-toggle="collapse" href="#collapse<?=$cntCateg?>" aria-expanded="true" aria-controls="collapse<?=$cntCateg?>"><?=$categ?></a>
			</h6>
		</div><!-- /.panel-heading role=tab -->
		<div id="collapse<?=$cntCateg?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?=$cntCateg?>">
			<table class="table table-striped table-hover">
				<tbody>
				<?php
				if (isset($checklist[$categ]) && count($checklist[$categ])) :
					$categDetails = $checklist[$categ];
					$ctr = 0;
					//foreach ($categDetails as $name => $value) :
					foreach ($translation as $key => $val) :
						if (isset($categDetails[$key])) :
							$name = $key;
							$value = isset($categDetails[$key]) ? $categDetails[$key] : "";
							$remarks = isset($categDetails[$key ."_remarks"]) ? $categDetails[$key ."_remarks"] : "";
							$value = ($value == "true");
							$ctr++;
					?>
						<tr>
							<td width="5%"><?=$ctr?></td>
							<td><?=$val/*$translation[$name]*/?></td>
							<td class="text-right">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-xs btn-success<?=$value ? " active" : ""?>"><input type="radio" name="<?=$name?>" id="<?=$name?>" autocomplete="off" value="true"<?=$value ? " checked" : ""?> /><i class="glyphicon glyphicon-ok"></i></label>
									<label class="btn btn-xs btn-danger<?=!$value ? " active" : ""?>"><input type="radio" name="<?=$name?>" id="<?=$name?>" autocomplete="off" value="false"<?=!$value ? " checked" : ""?> /><i class="glyphicon glyphicon-remove"></i></label>
								</div>
							</td>
							<td width="25%"><input type="text" class="form-control input-sm" name="<?=$key?>_remarks" value="<?=$remarks?>" placeholder="Remarks" /></td>
						</tr>
					<?php
						endif;
					endforeach;//foreach ($categDetails as $name => $value)
				endif;//if (isset($checklist[$categ]) && count($checklist[$categ]))
				?>
				</tbody>
			</table>
		</div><!-- /.panel-collapse role=tabpanel -->
	</div><!-- /.panel-default -->
<?php
	endforeach;//foreach ($category as $cntCateg => $categ)
endif;//if (isset($category))
?>
<div class="text-center">
	<button class="btn btn-primary" data-href="<?=site_url("admin/orders/checklist/{$this->uri->segment(4)}/submit/form")?>" data-action="Manage" data-toggle="modal" data-target="#dialogBox">Save</button>
	<button class="btn btn-default" data-dismiss="modal">Cancel</button>
</div>
</form>