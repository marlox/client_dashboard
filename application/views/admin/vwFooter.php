<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Footer View of Client Dashboard Admin Interface
 *
 */
?>
			</div><!-- /#page-wrapper -->
		</div><!-- /#wrapper -->

		<footer>
			<p>&trade; Ingedata Eyes-T Dashboard Admin Interface &copy; <?=mdate("%Y")?></p>
		</footer>
		<div class="modal fade" id="dialogBox" tabindex="-1" role="dialog" aria-labelledby="dialogBoxLabel">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="dialogBoxLabel">&nbsp;</h4>
					</div>
					<div class="modal-body">
						<h4 class="text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i> Loading...</h4>
					</div>
				</div>
			</div>
		</div>
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>das.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>bootstrap.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>tables.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.tablesorter.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>generic.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>tooltip.js"></script>
	</body>
</html>