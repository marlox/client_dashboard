<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Manage Order Submission View of Client Dashboard Admin Interface
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-danger">
				<div class="panel-heading text-right"><h6>&nbsp;</h6></div>
				<div class="table-responsive">
				<?php
				if (isset($records) && $total) {
					$fields = array_keys($records[0]);
					$clients = $this->session->userdata("clientNames");
				?>
				<small>
				<table class="table table-striped table-hover tablesorter">
					<thead>
						<tr>
							<th>#</th>
						<?php
						foreach ($fields as $field) :
							if (!preg_match("/(parent\_|\_id|\_name)+/i", $field)) :
								$strClientHeading = "";
								if ($field == "order_type") $field = "order_name";
								elseif ($field == "project" && count($clients) > 1) $strClientHeading = '<th title="Sort by Client Name" data-toggle="tooltip" data-placement="bottom">Client <i class="fa fa-sort"></i></th>';
								elseif ($field == "is_new_order") $field = "new_order?";
						?>
							<th title="Sort by <?=ucwords(str_replace("_", " ", $field))?>" data-toggle="tooltip" data-placement="bottom"><?=ucwords(str_replace("_", " ", $field))?> <i class="fa fa-sort"></i></th><?=$strClientHeading?>
						<?php
							endif;
						endforeach;
						?>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<?php
					foreach ($records as $ctr => $record) :
						$id = $record["order_submission_id"];
					?>
						<tr>
							<td><?=($ctr + 1 + $offset)?></td>
							<?php
							foreach ($record as $field => $value) :
								if (!preg_match("/(parent\_|\_id|\_name|is\_)+/i", $field)) :
									if (preg_match("/(\_date)+/i", $field)) {
										$value = mdate(($field == "ordered_date" ? "%d-%M-%Y %h:%i%a" : "%F %j, %Y"), syncDate($value));
									} elseif ($field == "order_type") {
										$value = '<i class="fa fa-'. $types[$value] .'" title="'. $value .'" data-toggle="tooltip" data-placement="bottom"></i><span class="sr-only">'. $value .'</span> '. $record["order_name"];
									} elseif ($field == "project") {
										$value = $projects[$value];
										if (count($clients) > 1) $value .= "</td><td>". $record["client_name"];
									} elseif ($field == "document(s)") {
										if ($value) {
											$arrDocs = (preg_match("/(\,)+/i", $value) ? explode(", ", $value) : array($value));
											$arrValue = array();
											foreach ($arrDocs as $doc) {
												$arrDocFilename = preg_split("/[-]+/", $doc);
												$docName = "Document-". $arrDocFilename[count($arrDocFilename)-1];
												$arrValue[] = '<a href="'. HTTP_DOC_DIR . $doc .'" target="_blank" title="Click to download and see contents of \''. $docName .'\'" data-toggle="tooltip" data-placement="bottom">'. $docName .'</a>';
											}
											$value = implode("<br />", $arrValue);
										}
									} elseif ($field == "specs") {
										$value = !$value ? '' : '<div title="'. htmlspecialchars($value) .'" data-toggle="tooltip" data-placement="bottom">'. substr(htmlspecialchars($value), 0, 9) .'...</div>';
									}
							?>
							<td<?=$field == "specs" && $value ? ' class="order_specs" data-toggle="modal" data-target="#order_specs_dialog" style="cursor:pointer"' : ''?>><?=$value?></td>
							<?php
								endif;
							endforeach;//foreach ($record as $field => $value)
							$isNew = $record["is_new_order"];
							$strIsNew = '<i class="fam-'. ($isNew ? 'tick' : 'cross') .'" title="'. ($isNew ? 'New Order/Campaign' : 'Order/Campaign Specification for \''. $record["parent_order_name"] .'\'') .'" data-toggle="tooltip" data-placement="bottom"></i><span class="sr-only">'. ($isNew ? 'Yes' : 'No') .'</span></i>';
							?>
							<td class="text-center"><?=$strIsNew?></td>
							<td>
								<?php if ($isNew) { ?>
								<a href="#" data-href="<?=site_url("admin/orders/create_new_order/". $record["order_submission_id"])?>" data-action="Add" data-toggle="modal" data-target="#dialogBox"><i class="fam-money" title="Create New Order/Campaign based on details of '<?=$record["order_name"]?>'" data-toggle="tooltip" data-placement="bottom"></i></a>
								<?php } else { ?>
								<a href="#" data-href="<?=site_url("admin/orders/add_to_submitted_order/". $record["parent_order_submission"] ."/". $record["order_submission_id"])?>" data-action="Add" data-toggle="modal" data-target="#dialogBox"><i class="fam-money-add" title="Add '<?=$record["order_name"]?>' as Order Specs to '<?=$record["parent_order_name"]?>'" data-toggle="tooltip" data-placement="bottom"></i></a>
								<?php } ?>
							</td>
						</tr>
					<?php
					endforeach;//foreach ($records as $ctr => $record)
					?>
				</table>
				</small>
				<?php
				} else {
					echo '<div class="text-danger text-center"><h4>No submitted '. ucfirst($page) .' found...</h4></div>';
				}
				?>
				</div>
			</div><!-- /.panel-danger -->
			<?php
			if (isset($records) && $total && $totalPages > 1) :
			?>
			<ul class="pagination pull-right">
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/submitted_orders")?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Start" data-toggle="tooltip" data-placement="bottom">&laquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/submitted_orders/". ($pageNum > 2 ? $pageNum - 1 : ""))?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Previous" data-toggle="tooltip" data-placement="bottom">&lsaquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/submitted_orders")?>" title="Page 1" data-toggle="tooltip" data-placement="bottom">1<?=(!$pageNum || $pageNum == 1 ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				for ($ctr = 2; $ctr <= $totalPages; $ctr++) :
				?>
				<li<?=($pageNum == $ctr ? ' class="active"' : "")?>><a href="<?=site_url("admin/$page/submitted_orders/$ctr")?>"<?=($pageNum == $ctr ? ' onclick="return false;"' : "")?> title="Page <?=$ctr?>" data-toggle="tooltip" data-placement="bottom"><?=$ctr?><?=($pageNum == $ctr ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				endfor;
				?>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/submitted_orders/". ($pageNum ? $pageNum + 1 : 2))?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="Next" data-toggle="tooltip" data-placement="bottom">&rsaquo;</a></li>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("admin/$page/submitted_orders/end")?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="End" data-toggle="tooltip" data-placement="bottom">&raquo;</a></li>
			</ul>
			<?php
			endif;
			?>
		</div><!-- /.col-lg-12 -->
		<div class="modal fade" id="order_specs_dialog" tabindex="-1" role="dialog" aria-labelledby="specsDialogBoxLabel">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header bg-info text-info" style="border-radius:5px 5px 0 0">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="specsDialogBoxLabel">&nbsp;</h4>
					</div>
					<div class="modal-body" style="border-radius: 0 0 5px 5px;margin-bottom:1px;box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05)">
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.row -->
