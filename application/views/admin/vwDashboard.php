<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Dashboard View of Client Dashboard Admin Interface
 *
 */
?>
		<div class="row">
			<div class="col-lg-3">
				<div class="panel panel-success">
					<div class="panel-heading">
						<div class="row">
							<a href="<?php echo site_url("admin/users"); ?>" title="New User" class="text-success" data-toggle="tooltip" data-placement="top">
							<div class="col-xs-6">
								<i class="fa fa-user fa-5x"></i>
							</div>
							<div class="col-xs-6 text-right">
								<p class="announcement-heading"><?=$numUsers?></p>
								<p class="announcement-text">Users</p>
							</div>
							</a>
						</div>
					</div>
					<a href="<?php echo site_url("admin/users/view"); ?>" title="Show Users Information">
						<div class="panel-footer announcement-bottom">
							<div class="row">
								<a href="<?php echo site_url("admin/users/view"); ?>" title="Show Users Information" data-toggle="tooltip" data-placement="left">
								<div class="col-xs-6">
									Users Information
								</div>
								<div class="col-xs-6 text-right">
									<i class="fa fa-arrow-circle-right"></i>
								</div>
								</a>
							</div>
						</div>
					</a>
				</div><!-- /.panel-success -->
			</div><!-- /.col-lg-3 -->
			<div class="col-lg-3">
				<div class="panel panel-danger">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<i class="fa fa-ticket fa-5x"></i>
							</div>
							<div class="col-xs-6 text-right">
								<p class="announcement-heading"><?=$numOrders?></p>
								<p class="announcement-text">Orders</p>
							</div>
						</div>
					</div>
					<a href="<?php echo site_url("admin/orders/submitted_orders"); ?>" title="Show Submitted Orders">
						<div class="panel-footer announcement-bottom">
							<div class="row">
								<a href="<?php echo site_url("admin/orders/submitted_orders"); ?>" title="Show Submitted Orders" data-toggle="tooltip" data-placement="left">
								<div class="col-xs-6">
									Submitted Orders
								</div>
								<div class="col-xs-6 text-right">
									<i class="fa fa-arrow-circle-right"></i>
								</div>
								</a>
							</div>
						</div>
					</a>
				</div><!-- /.panel-danger -->
			</div><!-- /.col-lg-3 -->
			<div class="col-lg-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<a href="<?php echo site_url("admin/clients"); ?>" title="New Client Account" class="text-white" data-toggle="tooltip" data-placement="top">
							<div class="col-xs-6">
								<i class="fa fa-group fa-5x"></i>
							</div>
							<div class="col-xs-6 text-right">
								<p class="announcement-heading"><?=$numClients?></p>
								<p class="announcement-text">Clients</p>
							</div>
							</a>
						</div>
					</div>
					<a href="<?php echo site_url("admin/clients/view"); ?>" title="Show Clients Account">
						<div class="panel-footer announcement-bottom">
							<div class="row">
								<a href="<?php echo site_url("admin/clients/view"); ?>" title="Show Clients Account" data-toggle="tooltip" data-placement="left">
								<div class="col-xs-6">
									Clients Account
								</div>
								<div class="col-xs-6 text-right">
									<i class="fa fa-arrow-circle-right"></i>
								</div>
								</a>
							</div>
						</div>
					</a>
				</div><!-- /.panel-primary -->
			</div><!-- /.col-lg-3 -->
			<div class="col-lg-3">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="row">
							<a href="<?php echo site_url("contact/message"); ?>" title="Send Email Message" class="text-info" data-toggle="tooltip" data-placement="top">
							<div class="col-xs-6">
								<i class="fa fa-envelope fa-5x"></i>
							</div>
							<div class="col-xs-6 text-right">
								<p class="announcement-heading"><?=$numMessages?></p>
								<p class="announcement-text">Messages</p>
							</div>
							</a>
						</div>
					</div>
					<a href="<?php echo site_url("admin/messages/view"); ?>" title="Show Messages">
						<div class="panel-footer announcement-bottom">
							<div class="row">
								<a href="<?php echo site_url("admin/messages/view"); ?>" title="Show Messages" data-toggle="tooltip" data-placement="left">
								<div class="col-xs-6">
									View Messages
								</div>
								<div class="col-xs-6 text-right">
									<i class="fa fa-arrow-circle-right"></i>
								</div>
								</a>
							</div>
						</div>
					</a>
				</div><!-- /.panel-info -->
			</div><!-- /.col-lg-3 -->
		</div><!-- /.row -->
		<div class="row">
			<div class="col-lg-6">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-user"></i> Recent Users</h3>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-striped tablesorter">
							<thead>
								<tr>
									<th>#</i></th>
									<th>Name <i class="fa fa-sort"></i></th>
									<th>Username <i class="fa fa-sort"></i></th>
									<th>Email <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
							<?php
							foreach ($recentUsers as $ctr => $user) :
							?>
								<tr>
									<td><?=$ctr + 1?></td>
									<td><?=$user["full_name"]?></td>
									<td><?=$user["username"]?></td>
									<td><?=$user["email"]?></td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
						</table>
					</div><!-- /.table-responsive -->
					<div class="panel-footer text-right">
						<a href="<?php echo site_url("admin/users/view"); ?>" title="Show Users Information" data-toggle="tooltip" data-placement="bottom">View Users <i class="fa fa-arrow-circle-right"></i></a>
					</div><!-- /.panel-footer -->
				</div><!-- /.panel-success -->
			</div><!-- /.col-lg-6 -->
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-danger">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-ticket"></i> Recent Submitted Orders</h3>
							</div>
							<div class="list-group">
								<?php
								foreach ($recentOrderSubmission as $record) :
									$syncDate = syncDate($record["last_updated_date"]);
								?>
								<a onclick="return false;" class="list-group-item">
									<span class="badge" title="<?=$record["order_type"]?>"><?=ago($syncDate)?></span>
									<i class="fa fa-<?=$orderTypesIcon[$record["order_type"]]?>" title="<?=$record["order_type"]?>"></i> <?=$record["order_name"]?> &rsaquo; <?=$projects[$record["project_id"]]?> [<?=$record["ordered_by"]?>]
								</a>
								<?php
								endforeach;
								if (!count($recentOrderSubmission)) echo '<h5 class="list-group-item text-center text-danger">No order(s) submitted...</h5>';
								?>
							</div>
							<?php
							if (count($recentOrderSubmission)) :
							?>
							<div class="panel-footer text-right">
								<a href="<?php echo site_url("admin/orders/submitted_orders"); ?>" title="Show Submitted Orders" data-toggle="tooltip" data-placement="bottom">View Submitted Orders <i class="fa fa-arrow-circle-right"></i></a>
							</div>
							<?php
							endif;
							?>
						</div><!-- /.panel-danger -->
					</div><!-- /.col-lg-12 -->
				</div><!-- /.row -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-envelope-o"></i> Recent Messages</h3>
							</div>
							<div class="list-group">
								<?php
								foreach ($recentMessages as $message) :
									$syncDate = syncDate($message["last_updated_date"]);
								?>
								<a onclick="return false;" class="list-group-item">
									<span class="badge" title="<?=$message["message_type"]?>"><?=ago($syncDate)?></span>
									<i class="fa fa-<?=$messageTypesIcon[$message["message_type"]]?>" title="<?=$message["message_type"]?>"></i> <?=$message["message"]?> [<?=$message["email"]?>]
								</a>
								<?php
								endforeach;
								if (!count($recentMessages)) echo '<h5 class="list-group-item text-center text-danger">No message(s) received...</h5>';
								?>
							</div>
							<?php
							if (count($recentMessages)) :
							?>
							<div class="panel-footer text-right">
								<a href="<?php echo site_url("admin/messages/view"); ?>" title="Show Messages" data-toggle="tooltip" data-placement="bottom">View Messages <i class="fa fa-arrow-circle-right"></i></a>
							</div>
							<?php
							endif;
							?>
						</div><!-- /.panel-info -->
					</div><!-- /.col-lg-12 -->
				</div><!-- /.row -->
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-money"></i> Eyes-T System's Recent Orders</h3>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-striped tablesorter">
							<thead>
								<tr>
									<th>ID <i class="fa fa-sort"></i></th>
									<th>Order <i class="fa fa-sort"></i></th>
									<th>Target Date <i class="fa fa-sort"></i></th>
									<th>Order Date <i class="fa fa-sort"></i></th>
									<th>Client / Project <i class="fa fa-sort"></i></th>
									<th>Type <i class="fa fa-sort"></i></th>
								</tr>
							</thead>
							<tbody>
							<?php
							foreach ($recentEyesTOrders as $order) :
								$orderDate = mdate("%j-%M %h:%i%a", syncDate($order["order_date"]));
								$targetDate = isset($order["order_actual_date"]) && trim($order["order_actual_date"]) ? $order["order_actual_date"] : $order["order_target_date"];
								if (trim($targetDate)) $targetDate = mdate("%F %j, %Y", syncDate($targetDate));
							?>
								<tr>
									<td><?=$order["order_id"]?></td>
									<td><?=isset($order["order_status"]) ? '<div class="status_image" id="'. strtolower(str_replace(" ", "_", $status[$order["order_status"]])) .'" title="'. $status[$order["order_status"]] .'" data-toggle="tooltip" data-placement="bottom"></div>' : ""?><?=$order["order_name"]?></td>
									<td class="text-center"><small><?=$targetDate?></small></td>
									<td class="text-center"><small><?=$orderDate?></small></td>
									<td><small><?=$order["project_name"]?></small></td>
									<td><small><?=$order["type"]?></small></td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
						</table>
					</div><!-- /.table-responsive -->
				</div><!-- /.panel-default -->
			</div><!-- /.col-lg-12 -->
		</div><!-- /.row -->
