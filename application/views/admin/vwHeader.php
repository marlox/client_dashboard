<?php
defined("BASEPATH") OR exit("No direct script access allowed");
/**
 * Header View of Client Dashboard Admin Interface
 *
 */
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="<?php echo HTTP_IMAGES_PATH; ?>favicon.ico">
		<title>Eyes-T Dashboard Admin Interface</title>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
		<!-- Bootstrap core CSS -->
		<link id="site_theme" href="<?php echo HTTP_CSS_PATH . "bootstrap.css"; ?>" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>font-awesome.min.css" />
		<!-- Add custom CSS here -->
		<link href="<?php echo HTTP_CSS_PATH; ?>admin.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="<?php echo HTTP_JS_PATH; ?>html5shiv.js"></script>
			<script src="<?php echo HTTP_JS_PATH; ?>respond.min.js"></script>
		<![endif]-->
		<link href="<?php echo HTTP_CSS_PATH; ?>fam-icons.css" rel="stylesheet">
		<link href="<?php echo HTTP_CSS_PATH; ?>generic.css" rel="stylesheet">
		<link href="<?php echo HTTP_CSS_PATH; ?>styles.css" rel="stylesheet">
	</head>
<body>
	<div id="wrapper">
		<!-- Sidebar -->
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo site_url("admin"); ?>"><div><img src="<?php echo HTTP_IMAGES_PATH . "logo.png"; ?>" class="site_logo" title="Eyes-T Dashboard" alt="Eyes-T Dashboard" /> Admin Interface</div></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav side-nav">
					<li<?php echo  $page == "dashboard" ? ' class="active"' : ""; ?>><a href="<?php echo site_url("admin/dashboard"); ?>" title="Statistics Overview" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li<?php echo  $page == "users" ? ' class="active"' : ""; ?>><a href="<?php echo site_url("admin/users"); ?>" title="New User" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-user"></i> Users</a></li>
					<li<?php echo  $page == "clients" ? ' class="active"' : ""; ?>><a href="<?php echo site_url("admin/clients"); ?>" title="New Client Account" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-group"></i> Clients</a></li>
					<li<?php echo  $page == "orders" ? ' class="active"' : ""; ?>>
						<a href="#" data-toggle="dropdown"><i class="fa fa-ticket"></i> Orders</a>
						<ul class="dropdown-menu orders-dropdown">
							<li><a href="<?php echo site_url("admin/orders/submitted_orders"); ?>" title="Manage Submitted Orders" data-toggle="tooltip" data-placement="bottom">Submitted Orders</a></li>
							<li><a href="<?php echo site_url("admin/orders/view"); ?>" title="Manage Orders/Campaign Information" data-toggle="tooltip" data-placement="bottom">Orders/Campaign</a></li>
						</ul>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right navbar-user">
					<li class="dropdown user-dropdown">
						<a href="#" class="dropdown-toggle text-center" data-toggle="dropdown"><span title="Click to see User Options" data-toggle="tooltip" data-placement="bottom">Hello, <?php echo $this->session->userdata("username"); ?>! <i class="fa fa-user fa-2x"></i></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo site_url("home"); ?>"><i class="fa fa-credit-card"></i> Client Interface</a></li>
							<li id="clock" title="<?php echo $this->session->userdata("timezone"); ?>" data-toggle="tooltip" data-placement="bottom"><a><i class="fa fa-clock-o"></i> <span id="clockDate"><?=mdate("%j-%M")?></span> <span id="clockHours"><?=mdate("%H")?></span>:<span id="clockMinutes"><?=mdate("%i")?></span>:<span id="clockSeconds"><?=mdate("%s")?></span></a></li>
							<li class="divider"></li>
							<li><a href="<?php echo site_url("admin/users/editProfile"); ?>"><i class="fa fa-edit"></i> Edit Profile</a></li>
							<li><a href="<?php echo site_url("admin/home/logout"); ?>"><i class="fa fa-power-off"></i> Sign Out</a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</nav>
		<?php
			$strPage = ucfirst($page);
			$strPageDesc = "Statistics Overview";
			$action = $this->uri->segment(3);
			$isNew = ($isNew || preg_match("/(new)+/i", $action));
			if ($isNew) {
				if ($page == "clients" && !preg_match("/(new)+/i", $action)) $strPage = "New ". rtrim($strPage, "s") ." Account";
				else $strPage = ($page !== "dashboard" ? "New " : "") . rtrim($strPage, "s");
			}
			if ($page !== "dashboard") $strPageDesc = ($isNew ? "" : "Manage $strPage ". ($page == "clients" && $action == "view" ? "Account" : "Information"));
			if ($action == "submitted_orders") $strPageDesc = "Manage ". ucwords(str_replace("_", " ", $action));
			if ($action == "specs") $strPageDesc = "Manage Order Specifications";
			if ($action == "editProfile") $strPage = "Edit Profile";
			if ($page == "messages" || ($page == "users" && $action != "view")) $strPageDesc = "";
		?>
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1><?php echo $strPage; ?> <small><?php echo $strPageDesc; ?></small></h1>
					<hr />
				</div><!-- /.col-lg-12 -->
			</div><!-- /.row -->
