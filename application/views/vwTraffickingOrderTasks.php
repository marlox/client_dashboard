<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tasks per Order View of Client Dashboard
 *
 */
$export = '
							<div class="btn-group">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span title="Export List of Tasks" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-external-link" aria-hidden="true"></i> Export <span class="caret"></span></span></button>
								<ul class="dropdown-menu text-left">
									<li><a href="'. current_url() .'/pdf" target="_blank" title="Export in Portable Document Format" data-toggle="tooltip" data-placement="bottom"><i class="fam-page-white-acrobat" aria-hidden="true"></i> PDF</a></li>
									<li><a href="'. current_url() .'/xls" target="_blank" title="Export in Excel / Spread Sheet Format" data-toggle="tooltip" data-placement="bottom"><i class="fam-page-excel" aria-hidden="true"></i> Excel</a></li>
									<li><a href="'. current_url() .'/txt" target="_blank" title="Export in Text Format" data-toggle="tooltip" data-placement="bottom"><i class="fam-page-white-text" aria-hidden="true"></i> Text</a></li>
								</ul>
							</div>
							&nbsp;<a href="'. current_url() .'/print" target="_blank" title="Print List of Tasks" role="button" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
';
$progressLabel = $completionPercentage > 0 ? "$completionPercentage% $completeLabel" : "";
?>
				<?php
				if (isset($userClients) && isset($orderTaskDetails)) :
					$orderName = $orderTaskDetails[0]["order_name"];
				?>
				<div class="client-panel-group">
					<div class="title text-center">
						<h4><?=$orderName?>'s Progression</h4>
					</div>
					<div class="row text-center">
						<div class="col-md-2"><?=$orderCurrentStep?></div>
						<div class="col-md-8">
							<div class="progress">
								<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="<?=$completionPercentage?>" aria-valuemin="0" aria-valuemax="100" style="width:<?=$completionPercentage?>%">
									<?=$progressLabel?>
								</div>
								<?=!$progressLabel ? "100% Incomplete" : ""?>
							</div>
						</div>
						<div class="col-md-2"><?=$nextProcessStep?></div>
					</div>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading text-right" role="tab" id="heading">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse"><?=$orderName?> <div class=status_image title="<?="$orderName Current Process Step"?>" data-toggle="tooltip" data-placement="bottom"><?=$orderCurrentStep?></div></a>
									<?=$export?>
								</h4>
							</div><!-- /.panel-heading role=tab -->
							<div id="collapse" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading">
								<?php
								if (isset($orderTaskDetails)) :
								?>
								<table class="table table-striped table-hover tablesorter">
									<thead>
										<tr>
											<th>#</th>
											<th>Task Name <i class="fa fa-sort"></i></th>
											<th>Placement <i class="fa fa-sort"></i></th>
											<th>Status <i class="fa fa-sort"></i></th>
										</tr>
									</thead>
									<tbody>
										<?php
										$ctr = 0;
										foreach ($orderTaskDetails as $cntOrder => $orderTask) :
											$startDate = $orderTask["start_date"] ? mdate("%d-%M %h:%i:%s%a", syncDate($orderTask["start_date"])) : "";
											$endDate = $orderTask["end_date"] ? mdate("%d-%M %h:%i:%s%a", syncDate($orderTask["end_date"])) : "";
											$taskDuration = "";
											if ($startDate && $endDate) {
												$startTime = syncDate($orderTask["start_date"]);
												$endTime = syncDate($orderTask["end_date"]);
												$taskDuration = elapsedTimeInWords($startTime, $endTime);
											}
											$ctr++;
										?>
										<tr>
											<td><?=$ctr?></td>
											<td><?=$orderTask["task_name"]?></td>
											<td><?=$orderTask["placement"]?></td>
											<td><?=($orderTask["status"] ? $status[$orderTask["status"]] : $status["KO"])?></td>
										</tr>
										<?php
										endforeach;//($orderTaskDetails as $cntOrder => $orderTask)
										?>
									</tbody>
								</table>
								<?php
								endif;//order details
								if (!isset($orderTaskDetails)) :
								?>
								<h4 class="text-center text-danger">No Order Task(s) found for <?=$orderName?>...</h4>
								<?php
								endif;
								?>
							</div><!-- /.panel-collapse role=tabpanel -->
							<div class="panel-footer text-right">
								<?=str_replace("btn-group", "btn-group dropup", $export)?>
							</div>
						</div><!-- /.panel-default -->
					</div><!-- /.panel-group #accordion -->
				</div><!-- /.client-panel-group -->
				<script src="<?php echo HTTP_JS_PATH ?>tables.js"></script>
				<script src="<?php echo HTTP_JS_PATH ?>jquery.tablesorter.js"></script>
				<?php
				endif;//clients
				if (!isset($orderTaskDetails)) :
				?>
				<h4 class="text-center text-danger">No Order Task(s) found...</h4>
				<?php
				endif;
				?>
