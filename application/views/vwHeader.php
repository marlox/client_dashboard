<?php
defined("BASEPATH") OR exit("No direct script access allowed");
/**
 * Header View of Eyes-T Dashboard Client Interface
 *
 */
$clientCSS = $this->session->userdata("css");
$clientLogo = $this->session->userdata("logo");
$clientName = $this->session->userdata("clientName");
$clientNames = $this->session->userdata("clientNames");
$clientType = $this->session->userdata("client_type");
$clientSelections = '
							<li class="dropdown client-dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span title="Select Client Account" data-toggle="tooltip" data-placement="bottom"><i class="sr-only">Clients </i><i class="fa fa-user-secret fa-2x" aria-hidden="true"></i></span></a>
								<ul class="dropdown-menu">';
$strClients = "";
if (count($clientNames) > 1) {
	foreach ($clientNames as $client) {
		$strClients .= '
									<li><a data-href="'. site_url("home/setClient/". str_replace(" ", "_", $client)) .'" data-action="Set" data-toggle="modal" data-target="#dialogBox"><span title="Show Client '. $client .' Account" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-user-secret"></i> '. $client .'</span></a></li>';
	}
}
$clientSelections .= $strClients .'
								</ul>
							</li>';
if (count($clientNames) <= 1 && !trim($strClients)) $clientSelections = "";
$reportDate = $this->session->userdata("reportDateRange");
$startDate = $endDate = "";
if ($reportDate) {
	$reportDate = explode(",", $reportDate);
	$startDate = $reportDate[0];
	$endDate = $reportDate[1];
}
$dateSelections = '
							<li class="dropdown date-dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span title="Set Date Range" data-toggle="tooltip" data-placement="bottom"><i class="sr-only">Date Range </i><i class="fa fa-calendar fa-2x" aria-hidden="true"></i></span></a>
								<ul class="dropdown-menu">
								<form id="setReportDate" class="form-horizontal" action="'. site_url("home/calendar") .'" method="post" onsubmit="return false;">
									<li><input type="date" name="start_date" class="form-control input-sm"'. ($startDate ? ' value="'. $startDate .'"' : '') .' placeholder="yyyy-mm-dd" required /></li>
									<li><input type="date" name="end_date" class="form-control input-sm"'. ($endDate ? ' value="'. $endDate .'"' : '') .' placeholder="yyyy-mm-dd" required /></li>
									<li class="text-right"><button type="submit" class="btn btn-warning btn-sm" data-href="'. site_url("home/calendar") .'" data-action="Set" data-toggle="modal" data-target="#dialogBox">&nbsp; Set &nbsp;</button></li>
								</form>
								</ul>
							</li>';
$clientSelections .= $dateSelections;
if (!trim($clientName)) $clientSelections = "";
$isCampaign = (preg_match("/(campaigns)+/i", current_url()));
$projectID = 139;
if ($clientType == "trafficking" && isset($clientProjects[0]["project_name"])) {
	$projectID = count($clientProjects) > 0 ? $clientProjects[0]["project_id"] : 139;
	$projectName = "TRAFFICKING";
	/*if (count($clientProjects) > 0) {
		$projectName = $clientProjects[0]["project_name"];
		$underscorePosition = strpos($projectName, "_");
		if ($underscorePosition === false) $underscorePosition = strlen($projectName);
		$projectName = substr($projectName, 0, $underscorePosition);
	}*/
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="<?php echo HTTP_IMAGES_PATH; ?>favicon.ico">
		<title>Eyes-T Dashboard Client Interface</title>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
		<!-- Bootstrap core CSS -->
		<link id="site_theme" href="<?php echo HTTP_CSS_PATH . ($clientCSS ? $clientCSS : "bootstrap.css"); ?>" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>font-awesome.min.css" />
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="<?php echo HTTP_JS_PATH; ?>html5shiv.js"></script>
			<script src="<?php echo HTTP_JS_PATH; ?>respond.min.js"></script>
		<![endif]-->
		<link href="<?php echo HTTP_CSS_PATH; ?>fam-icons.css" rel="stylesheet">
		<link href="<?php echo HTTP_CSS_PATH; ?>generic.css" rel="stylesheet">
		<link href="<?php echo HTTP_CSS_PATH; ?>styles.css" rel="stylesheet">
	</head>
<body>
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo site_url("home"); ?>"><div><?=($clientName && !$clientLogo ? "" : '<img src="'. HTTP_IMAGES_PATH . ($clientLogo ? $clientLogo : "logo.png") .'" class="site_logo" title="Eyes-T Dashboard" alt="Eyes-T Dashboard" /> ') . '<span class="site_title">'. ($clientName ? $clientName : "Client Interface") .'</span>';?></div></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
					<?php if ($clientType == "trafficking") : ?>
						<li class="dropdown projects-dropdown<?php echo $page == "projects" ? ' active' : "" ?>">
							<a href="<?php echo (count($clientProjects) > 0 ? site_url("projects/$projectID") : site_url("orders")); ?>" title="Show <?php echo (count($clientProjects) > 0 ? "Project '$projectName'" : "Orders"); ?>" data-toggle="tooltip" data-placement="bottom">En Cours</a>
						</li>
						<li<?php echo $isCampaign ? ' class="active"' : "" ?>><a href="<?php echo site_url("orders/campaigns"); ?>">À Valider</a></li>
						<li<?php echo $page == "directories" ? ' class="active"' : "" ?>><a href="<?php echo site_url("directories/$projectID"); ?>">Régies</a></li>
					<?php else : ?>
						<li<?php echo $page == "dashboard" ? ' class="active"' : "" ?>><a href="<?php echo site_url("dashboard"); ?>">Dashboard</a></li>
						<li<?php echo $page == "orders" && !$isCampaign ? ' class="active"' : "" ?>><a href="<?php echo site_url("orders"); ?>">Orders</a></li>
					<?php endif; ?>
						<li<?php echo $page == "contact" ? ' class="active"' : "" ?>><a href="<?php echo site_url("contact"); ?>">Contact</a></li>
					<?php if ($clientType == "trafficking") : ?>
						<li<?php echo $page == "orders" && $this->uri->segment(2) == "order_submission" ? ' class="active"' : "" ?>>
							<p class="btn-group">
								<a href="<?php echo site_url("orders/order_submission/$projectID"); ?>" title="Créer Campagne" role="button" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom">Créer Campagne</a>
							</p>
						</li>
					<?php endif; ?>
					</ul>
					<ul class="nav navbar-nav navbar-right">
					<?php
					if ($clientSelections) echo $clientSelections;
					if ($this->session->userdata("username")) :
					?>
						<li class="dropdown user-dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span title="Click to see User Options" data-toggle="tooltip" data-placement="bottom">Hello, <?php echo $this->session->userdata("username"); ?>! <i class="fa fa-user fa-2x"></i></span></a>
							<ul class="dropdown-menu">
							<?php if ($this->session->userdata("is_admin_login") === true) : ?>
								<li><a href="<?php echo site_url("admin"); ?>"><i class="fa fa-gear"></i> Admin Interface</a></li>
							<?php endif; ?>
								<li id="clock" title="<?php echo $this->session->userdata("timezone"); ?>" data-toggle="tooltip" data-placement="bottom"><a><i class="fa fa-clock-o"></i> <span id="clockDate"><?=mdate("%j-%M")?></span> <span id="clockHours"><?=mdate("%H")?></span>:<span id="clockMinutes"><?=mdate("%i")?></span>:<span id="clockSeconds"><?=mdate("%s")?></span></a></li>
								<li class="divider"></li>
								<li><a href="<?php echo site_url("home/profile"); ?>"><i class="fa fa-edit"></i> User Profile</a></li>
								<li><a href="<?php echo site_url("home/logout"); ?>"><i class="fa fa-power-off"></i> Sign Out</a></li>
							</ul>
						</li>
					<?php endif; ?>
					</ul>
				</div><!--/.navbar-collapse -->
			</div>
		</div>
		<h6>&nbsp;</h6>
		<div class="container">
			<div class="row">
				<h6>&nbsp;</h6>
				<div class="col-lg-12">
					<h6>&nbsp;</h6>
					<ul class="breadcrumb">
						<?php
						$subPage = trim(ucwords(str_replace("_", " ", $this->uri->segment(2))));
						if ($page == "projects" && is_numeric($subPage) && $clientType == "trafficking") $subPage = "TRAFFICKING";
						if ($page == "directories" && is_numeric($subPage) && $clientType == "trafficking") $subPage = "Régies";
						if ($page == "projects" && $clientType == "trafficking") $page = "En Cours";
						$isDashboard = ($page == "dashboard");
						echo "\t\t\t\t\t\t<li". ($isDashboard ? ' class="active"' : "") .">Dashboard</li>\n";
						if (!$isDashboard) {
							$page = ucwords(str_replace("_", " ", $page));
							$isTask = (isset($orderTaskDetails) && isset($orderTaskDetails[0])) || $page == "Tasks";
							if ($isTask) $page = "$page</li>\n\t\t\t\t\t\t<li class=\"active\">". $orderTaskDetails[0]["order_name"];
							elseif ($subPage == "Campaigns") $page = (!($subSubPage = $this->uri->segment(3)) ? $subPage : "$subPage</li>\n\t\t\t\t\t\t<li class=\"active\">". ucwords($subSubPage));
							elseif ($subPage) $page = ($page == "Home" ? $subPage : "$page</li>\n\t\t\t\t\t\t<li". (!$isTask ? " class=\"active\"" : "") .">$subPage");
							echo "\t\t\t\t\t\t<li". ((!$subPage && !$isTask) || ($subPage == "Campaigns" && !isset($subSubPage)) || ($subPage == "Profile") ? " class=\"active\"" : "") .">$page</li>\n";
						}
						?>
					</ul>
				</div>
				<div class="col-lg-12">
				<script src="<?php echo HTTP_JS_PATH; ?>jquery.min.js"></script>
				<script src="<?php echo HTTP_JS_PATH; ?>bootstrap.min.js"></script>
