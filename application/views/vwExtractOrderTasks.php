<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tasks Extraction per Order View of Client Dashboard
 *
 */
$client = $this->session->userdata("clientName") ? $this->session->userdata("clientName") : "Client";
$project = isset($project) ? $project : "Project";
$order = isset($order) ? $order : "Order";
if (preg_match("/^(pdf|print)/i", $action)) { //pdf, print
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Eyes-T Dashboard - <?=$order?> Task Details</title>
		<!-- Bootstrap core CSS -->
		<link href="<?php echo HTTP_CSS_PATH; ?>bootstrap.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="<?php echo HTTP_JS_PATH; ?>html5shiv.js"></script>
			<script src="<?php echo HTTP_JS_PATH; ?>respond.min.js"></script>
		<![endif]-->
		<link href="<?php echo HTTP_CSS_PATH; ?>generic.css" rel="stylesheet">
		<link href="<?php echo HTTP_CSS_PATH; ?>styles.css" rel="stylesheet">
	</head>
	<body style="background-color:#eee;padding-top:20px">
		<div class="container">
			<div class="panel-group">
				<?php if ($action == "print") : ?>
				<ul class="breadcrumb">
					<li><?=$client?></li>
					<li><?=$project?></li>
					<li class="active"><?=$order?></li>
				</ul>
				<?php 
				elseif ($action == "pdf") :
				?>
				<div class="breadcrumb">
					<?=$client?> <span class="active"> &gt; </span> <?=$project?> <span class="active"> &gt; </span> <span class="active"><?=$order?></span>
				</div>
				<?php endif; ?>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title text-center"><?=$order?> Task Details</h4>
					</div>
				</div>
				<br />
			</div>

			<div class="panel-group">
				<div class="panel panel-default">
					<?php
					if (isset($records)) :
					?>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>Task Name</th>
								<?php if ($isTrafficking) : ?>
								<th>Process Step</th>
								<th>Placement</th>
								<?php endif; ?>
								<th>Created Date</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$ctr = 0;
							foreach ($records as $cntOrder => $orderTask) :
								$ctr++;
								$strStatus = trim($orderTask["task_status"]) ? $status[$orderTask["task_status"]] : "Not Applicable";
								$strStatus .= '<div class="status_image" id="'. strtolower(str_replace(" ", "_", $strStatus)) .'"></div>';
							?>
							<tr>
								<td><?=$ctr?></td>
								<td><?=$orderTask["task_name"]?></td>
								<?php if ($isTrafficking) : ?>
								<td><?=$orderTask["process_step"]?></td>
								<td><?=$orderTask["placement"]?></td>
								<?php endif; ?>
								<td><?=mdate("%Y-%m-%d %H:%i:%s", syncDate($orderTask["task_created_date"]))?></td>
								<td><?=$strStatus?></td>
							</tr>
							<?php
							endforeach;//tasks
							?>
						</tbody>
					</table>
					<?php
					endif;//order details
					?>
				</div><!-- /.panel-default -->
			</div><!-- /.panel-group -->

		</div><!-- /.container -->
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>bootstrap.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>generic.js"></script>
	</body>
</html>
<?php
} else { //excel, word
	$strData = "#\tTASK NAME\t \tCREATED DATE\tSTATUS\n";
	foreach ($records as $ctr => $record) {
		$strStatus = trim($record["task_status"]) ? $status[$record["task_status"]] : "Not Applicable";
		$strData .= $ctr + 1;
		$strData .= "\t". $record["task_name"];
		$strData .= "\t". mdate("%Y-%m-%d %H:%i:%s", syncDate($record["task_created_date"]));
		$strData .= "\t$strStatus";
		$strData .= "\n";
	}
	echo $strData;
}
?>
