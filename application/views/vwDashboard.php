<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Dashboard View of Client Dashboard
 *
 */
$numProjects = count($clientProjects);
$arrChartTypes = array(1 => "doughnut", 2 => "line", 3 => "stepLine", 4 => "spline", 5 => "splineArea", 6 => "column", 7 => "bar", 8 => "area", 9 => "pie");
if ($numProjects) :
?>
					<script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>
					<div id="chartContainer" class="chart"></div>
					<h6>&nbsp;</h6>
					<?php
					echo "
					<script type=\"text/javascript\">
						var chart;
						chart = new CanvasJS.Chart(\"chartContainer\", {
							title:{
								text: \"$currentClient's Overall Project Status\"
							},
							animationEnabled: true,
							exportEnabled: true,
							legend: {
								verticalAlign: \"bottom\",
								horizontalAlign: \"center\"
							},data: [
							{
								type: \"bar\",
								toolTipContent: \"{y} {label} ({indexLabel})\",
								showInLegend: true,
								legendText: \"$chartLegend\",
								legendMarkerColor: \"white\",
								indexLabelPlacement: \"outside\",
								indexLabelFontColor: \"black\",
								indexLabelFontSize: 14,
								dataPoints: ". json_encode($dpOverallProject) ."
							}
							]
						});
						chart.render();
						window.onload = function() {
							for (var ctr = 0; ctr < $numProjects; ctr++) {eval('showChart'+ ctr +'()');}
						};
					</script>";
					?>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<?php
						if (isset($clientProjects)) :
							foreach ($clientProjects as $cntProject => $clientProject) :
								$totalPerProject = 0;
								$currentProject = $clientProject["project_name"];
								if ($currentClient == $clientProject["client_name"]) :
									$dpPerOrderDetail = $dpPerOrder;
									if (isset($projectOrderDetails)) {
										foreach($projectOrderDetails as $projectOrder) {
											if ($currentProject == $projectOrder["project_name"]) {
												$dpPerOrderDetail[$projectOrder["order_status"]]["y"] += 1;
												$totalPerProject++;
											}
										}
									}
									foreach ($dpPerOrderDetail as $key => $value) {
										if ($totalPerProject) {
											$dpPerOrderDetail[$key]["orders"] = $dpPerOrderDetail[$key]["y"];
											$dpPerOrderDetail[$key]["y"] = (round(($value["y"] / $totalPerProject), 2) * 100);
										}
									}
									$dpPerOrderDetail = array_values($dpPerOrderDetail);
					?>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading<?=$cntProject?>">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$cntProject?>" aria-expanded="true" aria-controls="collapse<?=$cntProject?>"><div class=status_image title="<?=$status[$clientProject["project_status"]]?>" data-toggle="tooltip" data-placement="bottom" id="<?=strtolower(str_replace(" ", "_", $status[$clientProject["project_status"]]))?>"></div>&nbsp;
									<?=$currentProject?></a>
								</h4>
							</div><!-- /.panel-heading role=tab -->
							<div id="collapse$cntProject" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?=$cntProject?>">
								<div class="panel-body">
									<div class=col-sm-4>
										<table class="table table-xs table-striped table-hover table-bordered">
										<thead>
											<tr>
												<th>Status</th>
												<th>Order(s)</th>
												<th>%</th>
											</tr>
										</thead>
										<tbody>
								<?php
									foreach($dpPerOrderDetail as $dataPoint) echo "<tr><td>". $dataPoint["label"] ."</td><td>". $dataPoint["orders"] ."</td><td>". $dataPoint["y"] ."%</td></tr>";
									echo "
										</tbody>
										</table>
									</div>
									<div class=col-sm-8>
									<script type=\"text/javascript\">
									var showChart$cntProject = function () {
										var projectChart$cntProject = new CanvasJS.Chart(\"chartContainer$cntProject\", {
											animationEnabled: true,
											exportEnabled: true,
											backgroundColor: \"#". ($this->session->userdata("css") == "bootstrap.css" ? "fff" : "f5f5f5") ."\",
											data: [
											{
												type: \"". $arrChartTypes[rand(1, count($arrChartTypes))] ."\",
												startAngle: 0,
												toolTipContent: \"{orders} {label} ({y}%)\",
												indexLabel: \"{y}%\",
												indexLabelPlacement: \"inside\",
												indexLabelFontColor: \"black\",
												indexLabelFontSize: 14,
												dataPoints: ". json_encode($dpPerOrderDetail) ."
											}
											]
										});
										projectChart$cntProject.render();
									}
									</script>
									<div id=\"chartContainer$cntProject\" class=\"chart\"></div>
									</div>
								</div><!-- /.panel-body -->
							</div><!-- /.panel-collapse role=tabpanel -->
						</div><!-- /.panel-default -->
								";
							endif;
						endforeach;
					endif;//projects
					if (!isset($clientProjects)) :
					?>
					<h4 class="text-center text-danger">No Project information found!</h4>
					<?php
					endif;
					?>
					</div><!-- /.panel-group #accordion -->
<?php
endif;
?>