<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Comments list View of Client Dashboard
 *
 */
if (isset($comments) && count($comments)) :
	foreach ($comments as $comment) :
		$syncDate = syncDate($comment["commented_at"]);
		$ago = ago($syncDate);
		$date = mdate("%d-%M %h:%i:%s%a", $syncDate);
		$isCurrentUser = ($comment["user_id"] == $this->session->userdata("id"));
?>	
						<div class="form-group">
							<label class="col-sm-offset-1 col-sm-2 control-label text-center" title="<?= $comment["user_fullname"] ?> left this comment at <?= $date ?>" data-toggle="tooltip" data-placement="right">
								<div><small><?= $isCurrentUser ? "<em>" : "" ?><i class="fa fa-<?= $isCurrentUser ? "magnet" : "user" ?>" aria-hidden="true"></i> <?= $comment["user_fullname"] ?><?= $isCurrentUser ? "</em>" : "" ?></small></div>
								<div class="badge"><small><?= $ago ?></small></div>
							</label>
							<div class="col-sm-7">
								<div class="well well-sm">
<?php
		if ($isCurrentUser) : /* comment by current user can be deleted */
?>
									<button type="button" class="close" onclick="deleteComment('<?= $comment["comment_id"] ?>')" aria-label="Close" title="Delete this comment?" data-toggle="tooltip" data-placement="left"><span aria-hidden="true">&times;</span></button>
<?php
		endif;
?>
									<?= $isCurrentUser ? "<em>" : "<strong>" ?><?= htmlspecialchars($comment["comment"]) ?><?= $isCurrentUser ? "</em>" : "</strong>" ?>
								</div>
							</div>
						</div>
<?php
	endforeach;
endif;
if (isset($errors)) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
?>
<script>
$(function() {
	$("[data-toggle='tooltip']").tooltip();
});
</script>