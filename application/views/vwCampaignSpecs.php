<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Orders Information/Campaigns List View of Client Interface
 *
 */
$export = '
							<a href="mailto:?subject='.$currentClient .' Campaign - '. $campaignName .' Specifications&body=Check the '. $campaignName .' Specifications by clicking below link to its page:%0D%0A'. current_url() .'%0D%0A" target="_top" title="Email '. $campaignName .' Checklist" data-toggle="tooltip" data-placement="bottom"><i class="fam-email" aria-hidden="true"></i><i class="sr-only"> Email</i></a>&nbsp;
							<a href="'. current_url() .'/pdf" target="_blank" title="Save in Portable Document Format" data-toggle="tooltip" data-placement="bottom"><i class="fam-page-white-acrobat" aria-hidden="true"></i><i class="sr-only"> PDF</i></a>&nbsp;
							<a href="'. current_url() .'/print" target="_blank" title="Print" data-toggle="tooltip" data-placement="bottom"><i class="fam-printer" aria-hidden="true"></i><i class="sr-only"> Print</i></a>&nbsp;
							<a href="#comment-box" title="Comments" data-toggle="tooltip" data-placement="bottom"><i class="fam-comments" aria-hidden="true"></i><i class="sr-only"> Comments</i></a>
';
if (!isset($records) || !count($records)) $export = "&nbsp;";
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel-heading text-right">
				<h4 class="panel-title text-left" title="<?= "<u>Campaign Basic Details</u><br />". str_replace(";", "<br />", $campaignDetails) ."<br />Status: ". $campaignStatus ?>" data-toggle="tooltip" data-html="true">
					<?=$export !== '&nbsp;' ? $campaignName : $export?>
					<div class="text-left"><small><?= $campaignDetails ?></small></div>
				</h4>
				<?=$export?>
			</div>
			<?php
			if (isset($category)) :
				$orderName = "";
				$ctr = 0;
				foreach ($category as $cntCateg => $categ) :
			?>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading<?=$cntCateg?>">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" href="#collapse<?=$cntCateg?>" aria-expanded="true" aria-controls="collapse<?=$cntCateg?>"><?=$categ?></a>
						</h4>
					</div><!-- /.panel-heading role=tab -->
					<div id="collapse<?=$cntCateg?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?=$cntCateg?>">
						<table class="table table-striped table-hover">
							<tbody>
							<?php
							if (isset($records[$categ]) && count($records[$categ])) :
								$categDetails = $records[$categ];
								$ctr = 0;
								foreach ($translation as $key => $val) :
									if (isset($categDetails[$key])) :
										$name = $key;
										$value = isset($categDetails[$key]) ? $categDetails[$key] : "false";
										$remarks = isset($categDetails[$key ."_remarks"]) ? $categDetails[$key ."_remarks"] : "";
										$value = ($value == "true");
										$ctr++;
								?>
									<tr>
										<td width="5%"><?=$ctr?></td>
										<td width="70%"><?=$val?></td>
										<td class="text-center"><span class="btn btn-xs btn-<?=!$value ? "danger" : "success"?>" title="<?=!$value ? "NO" : "YES"?>" data-toggle="tooltip" data-placement="left"><i class="glyphicon glyphicon-<?=!$value ? "remove" : "ok"?>"></i></span></td>
										<td width="15%" title="<?=$val?> Remarks" data-toggle="tooltip" data-placement="left"><small><?=$remarks?></small></td>
									</tr>
								<?php
									endif;
								endforeach;//foreach ($categDetails as $name => $value)
							endif;//if (isset($records[$categ]) && count($records[$categ]))
							?>
							</tbody>
						</table>
					</div><!-- /.panel-collapse role=tabpanel -->
				</div><!-- /.panel-default -->
			<?php
				endforeach;//foreach ($category as $cntCateg => $categ)
			endif;//if (isset($category))
			if ($export !== '&nbsp;') :
			?>
			<script src="<?php echo HTTP_JS_PATH ?>tables.js"></script>
			<script src="<?php echo HTTP_JS_PATH ?>jquery.tablesorter.js"></script>
			<?php
			else :
			?>
			<h4 class="text-center text-danger">No Campaign / Order checklist for <u><?=$campaignName?></u> found...</h4>
			<?php
			endif;
			?>
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
<?php
	$this->load->view("vwPageComments");
?>