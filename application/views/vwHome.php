<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Home View of Client Dashboard
 *
 */
?>
					<?php
					if (isset($userClients)) :
						foreach ($userClients as $cntClient => $userClient) :
							$currentClient = $userClient["client_name"];
					?>
					<div class="client-panel-group<?=$cntClient ? " hide" : ""?>">
					<div class="panel-group" id="accordion<?=$cntClient?>" role="tablist" aria-multiselectable="true">
					<?php
						if (isset($clientProjects)) :
							foreach ($clientProjects as $cntProject => $clientProject) :
								$currentProject = $projectName = $clientProject["project_name"];
								if ($currentClient == $clientProject["client_name"]) :
					?>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading<?=$cntProject?>">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion<?=$cntClient?>" href="#collapse<?=$cntProject?>" aria-expanded="true" aria-controls="collapse<?=$cntProject?>"><?=$currentProject?></a>
								</h4>
							</div><!-- /.panel-heading role=tab -->
							<div id="collapse<?=$cntProject?>" class="panel-collapse collapse<?=!$cntProject ? " in" : ""?>" role="tabpanel" aria-labelledby="heading<?=$cntProject?>">
								<div class="panel-body">
									<?php
									if (isset($projectOrderDetails)) :
									?>
									<table class="table table-striped table-hover table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Order Name</th>
												<th>Delivery Date</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$ctr = 0;
											foreach ($projectOrderDetails as $cntOrder => $projectOrder) :
												if ($projectName == $projectOrder["project_name"]) :
													$ctr++;
													$orderDate = isset($projectOrder["order_actual_date"]) ? $projectOrder["order_actual_date"] : $projectOrder["order_target_date"];
													if (trim($orderDate)) $orderDate = mdate("%Y-%m-%d", syncDate($orderDate));
											?>
											<tr>
												<td><?=$ctr?></td>
												<td><a href="<?php echo site_url("orders/tasks/". $projectOrder["order_id"]); ?>" title="Show Order Details of '<?=$projectOrder["order_name"]?>'"><?=$projectOrder["order_name"]?></td>
												<td><?=$orderDate?></td>
												<td class="text-center"><div class="status_image" title="<?=$status[$projectOrder["order_status"]]?>" id="<?=strtolower(str_replace(" ", "_", $status[$projectOrder["order_status"]]))?>"></div></td>
											</tr>
											<?php
												endif;
											endforeach;//orders
											?>
										</tbody>
									</table>
									<?php
									endif;//order details
									if (!isset($projectOrderDetails)) :
									?>
									<h4 class="text-center text-danger">No Project Orders found!</h4>
									<?php
									endif;
									?>
									<div class="form-horizontal">
										<a href="<?php echo site_url("orders/newOrder"); ?>" title="Order Submission" role="button" class="btn btn-default">Submit New Order</a>
									</div>
								</div><!-- /.panel-body -->
							</div><!-- /.panel-collapse role=tabpanel -->
						</div><!-- /.panel-default -->
					<?php
							endif;
						endforeach;
					endif;//projects
					if (!isset($clientProjects)) :
					?>
					<h4 class="text-center text-danger">No Project information found!</h4>
					<?php
					endif;
					?>
					</div><!-- /.panel-group #accordion -->
					</div><!-- /.client-panel-group -->
					<?php
						endforeach;
					endif;//clients
					?>
