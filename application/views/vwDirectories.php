<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Directories Main View of Client Dashboard
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<div class="panel panel-info">
			<div class="panel-heading text-right">
				<h4 class="panel-title pull-left"><i class="fa fa-book"></i> Régies</h4>
				<div class="status_image">
					<button class="btn btn-primary btn-sm" onclick="addRegie()" data-toggle="modal" data-target="#modal_form"><i class="fa fa-plus" title="Add New Régie" data-toggle="tooltip" data-placement="bottom"></i></button>
					<button class="btn btn-default btn-sm" onclick="reload_table()" title="Refresh Régies List" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-refresh"></i></button>
				</div>
			</div>
			<div class="table-responsive">
				<table id="table" class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Régie</th>
							<th>Site</th>
							<th>Technical Contact</th>
							<th>Commercial Contact</th>
							<th>Annonceur</th>
							<th width="85">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div><!-- /.panel-info -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->

<script type="text/javascript">

var save_method; //for save method string
var table;
var sid = "<?=$sid?>";

$(document).ready(function() {
	table = $("#table").DataTable({
		"ajax": {
			"url": "<?= site_url("directories/$projectID/all") ?>",
			"type": "POST",
			"data": { sid: sid }
		},
		"columnDefs": [{
			"targets": [ -1 ], //last column
			"orderable": false, //set not orderable
			"searchable": false, //set not orderable
		}],
		"order": [[ 0, "asc" ]],
		"columns": [
			{ "data": "name" },
			{ "data": "site" },
			{ "data": "technical_contact" },
			{ "data": "commercial_contact" },
			{ "data": "annonceur" },
			{
				data: null,
				className: "text-center",
				render: function (data, type, row) { return "<a class='btn btn-success btn-xs' onclick='return editRegie("+ data.id +")' data-toggle='modal' data-target='#modal_form'><i class='fa fa-pencil' title='Edit Régie Info' data-toggle='tooltip' data-placement='bottom'></i></a> <a class='btn btn-danger btn-xs' onclick='return deleteRegie("+ data.id +")' title='Delete this Régie?' data-toggle='tooltip' data-placement='bottom'><i class='fa fa-trash-o'></i></a>"; }
			}
		],
		"language": {
			"emptyTable": "No régie's information available",
			"lengthMenu": "List _MENU_ régies",
			"zeroRecords": "No régie's information matched your search",
			"info": "_START_ to _END_ of _TOTAL_ régies",
			"infoFiltered": " - searched from total of _MAX_ régies",
			"infoEmpty": "No régies information to show",
			"paginate": {
				"previous": "&lsaquo;",
				"next": "&rsaquo;",
			},
			"searchPlaceholder": "Régies"
		},
		"lengthMenu": [ 5, 10, 25, 50, 75, 100 ]
	});
});

function addRegie() {
	save_method = "Create";
	$("#form")[0].reset();
	$("#save").text(save_method);
	$('[name="name"]').focus();
}

function editRegie(currentID) {
	save_method = "Update";
	$("#form")[0].reset();

	$.ajax({
		url : "<?= site_url("directories/edit/regie") ?>/"+ currentID +"/sid/"+ sid,
		type: "GET",
		dataType: "JSON",
		success: function(data) {
			data = data[0];
			$('[name="id"]').val(data.id);
			$('[name="name"]').val(data.name);
			$('[name="site"]').val(data.site);
			$('[name="technical_contact"]').val(data.technical_contact);
			$('[name="commercial_contact"]').val(data.commercial_contact);
			$('[name="annonceur"]').val(data.annonceur);
			$("#save").text(save_method);
			$('[name="name"]').focus();
		}
	});
	return false;
}

function reload_table() {
	table.ajax.reload(null, false);
	$("#table_filter .input-sm").val("").attr("value", "");
	$("#table_filter .input-sm").focus();
	$("#table_length select").focus();
	$("#table_filter .input-sm").focus();
}

function saveRegie() {
	if ($.trim($('[name="name"]').val()) && $.trim($('[name="site"]').val()) && $.trim($('[name="technical_contact"]').val())) {
		var url = "<?= site_url("directories") ?>/"+ save_method.toLowerCase();
		$("#save").text("Saving...").addClass("disabled");
		$("#save").attr("disabled", true);
		$.ajax({
			url : url,
			type: "POST",
			data: $("#form").serialize() +"&sid="+ sid,
			dataType: "JSON",
			success: function(data) {
				if (data.status) {
					$('[data-dismiss="modal"]').click();
					reload_table();
					$("#save").text("Saved").removeClass("disabled");
				}
				$("#save").attr("disabled", false);
			}
		});
	} else {
		if (!$.trim($('[name="technical_contact"]').val())) $('[name="technical_contact"]').val("").focus();
		if (!$.trim($('[name="site"]').val())) $('[name="site"]').val("").focus();
		if (!$.trim($('[name="name"]').val())) $('[name="name"]').val("").focus();
	}
	return false;
}

function deleteRegie(currentID) {
	$.ajax({
		url : "<?= site_url("directories/delete/regie") ?>/"+ currentID,
		type: "POST",
		data: { sid: sid },
		dataType: "JSON",
		success: function(data) {
			if (data.status) {
				$('[data-dismiss="modal"]').click();
				reload_table();
			}
		}
	});
	return false;
}
</script>

<script src="<?php echo HTTP_JS_PATH; ?>datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>datatables/js/dataTables.bootstrap.js"></script>
<link href="<?php echo HTTP_JS_PATH; ?>datatables/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<div class="modal fade" id="modal_form" tabindex="-1" role="dialog">
	<form id="form" class="form-horizontal" onsubmit="return saveRegie()">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Régie Form</h3>
			</div>
			<div class="modal-body form">
				<input type="hidden" value="" name="id"/> 
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3">Régie</label>
						<div class="col-md-9">
							<input name="name" placeholder="Régie" class="form-control" type="text" required autofocus>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Site</label>
						<div class="col-md-9">
							<input name="site" placeholder="Site" class="form-control" type="text" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Technical Contact</label>
						<div class="col-md-9">
							<textarea name="technical_contact" placeholder="Technical Contact" class="form-control" required></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Commercial Contact</label>
						<div class="col-md-9">
							<textarea name="commercial_contact" placeholder="Commercial Contact" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Annonceur</label>
						<div class="col-md-9">
							<input name="annonceur" placeholder="Annonceur" class="form-control" type="text">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" id="save" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
	</form>
</div><!-- /.modal -->