<?php
if (isset($records)) :
	$taskName = $records[0]["task_name"];
	$endString = strpos($taskName, " ");
	if (!$endString) $endString = strlen($taskName);
?>
<p>
<h5 class="panel-title text-center"><?=substr($taskName, 0, $endString)?></h5>
</p>
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>Process Step</th>
			<th>Placement</th>
			<th>Start</th>
			<th>End</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$ctr = 0;
		foreach ($records as $cntOrder => $orderTask) :
			$ctr++;
			$strStatus = trim($orderTask["task_status"]) ? $status[$orderTask["task_status"]] : "Not Applicable";
		?>
		<tr>
			<td><?=$ctr?></td>
			<td><?=$orderTask["process_step"]?></td>
			<td><?=$orderTask["placement"]?></td>
			<td><?=$orderTask["started_date"] ? mdate("%d-%M %h:%i%a", syncDate($orderTask["started_date"])) : ""?></td>
			<td><?=$orderTask["finished_date"] ? mdate("%d-%M %h:%i%a", syncDate($orderTask["finished_date"])) : ""?></td>
			<td><?=$strStatus?></td>
		</tr>
		<?php
		endforeach;//tasks
		?>
	</tbody>
</table>
<style>
.modal-body {
	padding: 0;
}
</style>
<?php
endif;//order details
?>