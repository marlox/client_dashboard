<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Orders Information/Campaign List View of Client Interface
 *
 */
$export = '
							<a href="mailto:?subject='. $currentClient .' Campaign List&body=Check '. $currentClient .' Campaign List by clicking below link to its page:%0D%0A'. current_url() .'%0D%0A" target="_top" title="Email this Campaign List" data-toggle="tooltip" data-placement="bottom"><i class="fam-email" aria-hidden="true"></i><i class="sr-only"> Email</i></a>&nbsp;
							<a href="'. current_url() .'/pdf" target="_blank" title="Save this List in Portable Document Format" data-toggle="tooltip" data-placement="bottom"><i class="fam-page-white-acrobat" aria-hidden="true"></i><i class="sr-only"> PDF</i></a>&nbsp;
							<a href="'. current_url() .'/print" target="_blank" title="Print this List" data-toggle="tooltip" data-placement="bottom"><i class="fam-printer" aria-hidden="true"></i><i class="sr-only"> Print</i></a>
';
if (!isset($records) || !$total) $export = "&nbsp;";
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-danger">
				<div class="panel-heading text-right">
					<h4 class="panel-title pull-left">Campaigns</h4>
					<?=$export?>
				</div>
				<div class="table-responsive">
					<small>
					<table class="table table-striped table-hover tablesorter">
						<thead>
							<tr>
								<th>#</th>
								<th title="Sort by Campaign/Order Name" data-toggle="tooltip" data-placement="bottom">Campaign <i class="fa fa-sort"></i></th>
								<th title="Sort by Advertiser" data-toggle="tooltip" data-placement="bottom">Advertiser <i class="fa fa-sort"></i></th>
								<th title="Sort by Ad Server" data-toggle="tooltip" data-placement="bottom">Ad Server <i class="fa fa-sort"></i></th>
								<th title="Sort by Start Date" data-toggle="tooltip" data-placement="bottom">Start Date <i class="fa fa-sort"></i></th>
								<th title="Sort by End Date" data-toggle="tooltip" data-placement="bottom">End Date <i class="fa fa-sort"></i></th>
								<th title="Sort by Status" data-toggle="tooltip" data-placement="bottom">Status <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
				<?php
				if (isset($records) && $total) :
					foreach ($records as $ctr => $record) :
						$id = $record["order_info_id"];
						$startDate = mdate("%F %d, %Y", syncDate($record["start_date"]));
						$endDate = mdate("%F %d, %Y", syncDate($record["end_date"]));
					?>
						<tr>
							<td><?=($ctr + 1 + $offset)?></td>
							<td><a href="<?=current_url() ."/checklist/$id"?>" title="View Campaign/Order Checklist" data-toggle="tooltip" data-placement="bottom"><?=$record["order_name"]?></a></td>
							<td><?=$record["advertiser"]?></td>
							<td><?=$record["ad_server"]?></td>
							<td><?=$startDate?></td>
							<td><?=$endDate?></td>
							<td title="<?="{$record["order_name"]} ". strtoupper($record["status"])?>" data-toggle="tooltip" data-placement="bottom"><span class="label label-<?=$record["status"] == "In Progress" ? "warning" : "success"?>"><?=strtoupper($record["status"])?></span></td>
						</tr>
					<?php
					endforeach;//foreach ($records as $ctr => $record)
				else :
				?>
						<tr class="text-danger text-center">
							<td colspan="9">
								<h4>No Campaign / Order information found...</h4>
							</td>
						</tr>
				<?php
				endif;//if (isset($records) && $total)
				?>
					</tbody>
					</table></small>
				</div>
				<?=$export !== '&nbsp;' ? '<div class="panel-footer text-right">'.$export.'</div>' : ''?>
			</div><!-- /.panel-danger -->
			<?php
			if (isset($records) && $total && $totalPages > 1) :
			?>
			<ul class="pagination pull-right">
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("$page/campaigns")?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Start" data-toggle="tooltip" data-placement="bottom">&laquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="disabled"' : "")?>><a href="<?=site_url("$page/campaigns/". ($pageNum > 2 ? $pageNum - 1 : ""))?>"<?=(!$pageNum || $pageNum == 1 ? ' onclick="return false;"' : "")?> title="Previous" data-toggle="tooltip" data-placement="bottom">&lsaquo;</a></li>
				<li<?=(!$pageNum || $pageNum == 1 ? ' class="active"' : "")?>><a href="<?=site_url("$page/campaigns")?>" title="Page 1" data-toggle="tooltip" data-placement="bottom">1<?=(!$pageNum || $pageNum == 1 ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				for ($ctr = 2; $ctr <= $totalPages; $ctr++) :
				?>
				<li<?=($pageNum == $ctr ? ' class="active"' : "")?>><a href="<?=site_url("$page/campaigns/$ctr")?>"<?=($pageNum == $ctr ? ' onclick="return false;"' : "")?> title="Page <?=$ctr?>" data-toggle="tooltip" data-placement="bottom"><?=$ctr?><?=($pageNum == $ctr ? ' <span class="sr-only">(current)</span>' : "")?></a></li>
				<?php
				endfor;
				?>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("$page/campaigns/". ($pageNum ? $pageNum + 1 : 2))?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="Next" data-toggle="tooltip" data-placement="bottom">&rsaquo;</a></li>
				<li<?=($pageNum == "end" || $pageNum == $totalPages ? ' class="disabled"' : "")?>><a href="<?=site_url("$page/campaigns/end")?>"<?=($pageNum == "end" || $pageNum == $totalPages ? ' onclick="return false;"' : "")?> title="End" data-toggle="tooltip" data-placement="bottom">&raquo;</a></li>
			</ul>
			<?php
			endif;
			if ($export !== '&nbsp;') :
			?>
			<script src="<?php echo HTTP_JS_PATH ?>tables.js"></script>
			<script src="<?php echo HTTP_JS_PATH ?>jquery.tablesorter.js"></script>
			<?php
			endif;
			?>
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
