<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Project Orders View of Client Dashboard
 *
 */
?>
					<div class="panel-group">
					<script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>
					<div class="hide-trial-version"></div>
					<div id="chartContainer" class="chart"></div>
					<?php
					echo "
					<script type='text/javascript'>
						var chart;
						chart = new CanvasJS.Chart('chartContainer', {
							title:{
								text: '$currentProject'
							},
							axisX:{
								labelAngle: 0,
								title: 'Campagne Étape En Cours'
							},
							animationEnabled: true,
							exportEnabled: true,
							toolTip:{
								enabled: true,
								content: '{label} ({y} campagne)'
							},
							data: [
							{
								type: 'column',
								indexLabelPlacement: 'outside',
								indexLabelFontColor: 'black',
								indexLabelFontSize: 14,
								dataPoints: ". json_encode($overallProjectProcess) ."
							}]
						});
						chart.render();
					</script>";
					$projectStatus = (isset($status[$projectStatus]) ? $status[$projectStatus] : "In Progress");
					?>
					</div>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse"><div class="status_image" title="<?="$currentProject ". strtoupper($projectStatus)?>" data-toggle="tooltip" data-placement="bottom"><span class="label label-<?=$projectStatus == "In Progress" ? "warning" : "success"?>"><?=strtoupper($projectStatus)?></span></div>&nbsp;<?=$currentProject?></a>
								</h4>
							</div><!-- /.panel-heading role=tab -->
							<div id="collapse" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading">
								<table class="table table-striped table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Campagne</th>
											<th>Date de soumission</th>
											<th>Date MEL</th>
											<th>Etape actuelle</th>
										</tr>
									</thead>
									<tbody>
								<?php
								if (isset($projectOrderDetails)) :
									$ctrOrder = 0;
									foreach ($projectOrderDetails as $projectOrder) :
										$deliveryDate = $projectOrder["delivery_date"];
										if (trim($deliveryDate)) $deliveryDate = mdate("%F %d, %Y", syncDate($deliveryDate));
										$ctrOrder++;
										$orderName = $projectOrder["order_name"];
										/*<a href="<?php echo site_url("tasks/". $projectOrder["order_id"]); ?>" title="Show Order Tasks of '<?=$orderName?>'" data-toggle="tooltip" data-placement="bottom"><?=$orderName?></a>*/
								?>
										<tr>
											<td><?=$ctrOrder?></td>
											<td width="40%"><?=$orderName?></td>
											<td><?=mdate("%F %d, %Y %h:%i%a", syncDate($projectOrder["created_date"]))?></td>
											<td><?=$deliveryDate?></td>
											<td><?=$projectOrder["process_step"]?></td>
										</tr>
								<?php
									endforeach;//foreach ($projectOrderDetails as $projectOrder)
								endif;//if (isset($projectOrderDetails))
								if (!isset($projectOrderDetails)) :
								?>
								<tr><td colspan="5"><h4 class="text-center text-danger">No Project orders information found!</h4></td></tr>
								<?php
								endif;
								?>
									</tbody>
								</table>
							</div><!-- /.panel-collapse role=tabpanel -->
						</div><!-- /.panel-default -->
					</div><!-- /.panel-group #accordion -->
