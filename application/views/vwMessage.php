<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Message Form View of Client Dashboard
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
			if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 3 seconds you will be redirected to Home. <a href=\"". current_url() ."\" title=\"Send Message\">Click here to Send a Message to another person.</a></p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("home") ."';}, 3000);\n</script></div>";
			$current_url = current_url();
			$email = $this->input->get("email", true);
			$name = $this->input->get("name", true);
			if ($email) $current_url .= "?email=$email";
			if ($name) $current_url .= ($email ? "&" : "?") ."name=$name";
		?>
		<div class="panel panel-default">
			<div class="panel-heading text-right"><h4 class="panel-title text-left"><i class="fa fa-envelope-o"></i> Send Message</h4></div>
			<form class="form-horizontal frmPost" method="post" action="<?=$current_url?>">
				<div class="panel-body">
					<div class="form-group">
						<div class="form-group">
							<label for="to_name" class="col-sm-3 control-label" title="Required field">Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Name" name="to_name" id="to_name" required value="<?php echo set_value("to_name") ? set_value("to_name") : $name; ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-3 control-label" title="Required field">Email Address <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="email" class="form-control" placeholder="Email Address" name="email" id="email" required value="<?php echo set_value("email") ? set_value("email") : $email; ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="subject" class="col-sm-3 control-label" title="Required field">Subject <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Subject" name="subject" id="subject" required value="<?php echo set_value("subject"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="message" class="col-sm-3 control-label" title="Required field">Message <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<textarea class="form-control" name="message" id="message" placeholder="Message/Information"><?php echo set_value("message"); ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="closing_remarks" class="col-sm-3 control-label">Closing Remarks </label>
							<div class="col-sm-8">
								<textarea class="form-control" name="closing_remarks" id="closing_remarks" placeholder="Closing Remarks"><?php echo set_value("closing_remarks"); ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="message_signature" class="col-sm-3 control-label" title="Required field">Message Signature </label>
							<div class="col-sm-8">
								<textarea class="form-control" name="message_signature" id="message_signature" placeholder="Message/Information"><?php echo set_value("message_signature"); ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer"><center>
					<button type="submit" class="btn btn-primary" id="btnSubmit">Send</button>
					<a href="<?php echo $current_url; ?>" class="btn btn-default" role="button" title="Cancel Send Message">Cancel</a>
				</center></div>
			</form>
		</div><!-- /.panel-info -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
