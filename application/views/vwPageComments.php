<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Comments list and form View of Client Dashboard
 *
 */
$pageURL = str_replace(base_url(), "", current_url());
?>
	<div class="row row-comments" id="comment-box">
		<div class="col-lg-12">
		<div class="panel panel-success">
			<div class="panel-heading text-right"><h4 class="panel-title text-left"><i class="fa fa-pencil"></i> Comments</h4></div>
			<form class="form-horizontal comment-form" method="post">
				<input type="hidden" name="page_url" value="<?= $pageURL ?>" />
				<div class="panel-body">
					<div class="form-group comments">
					</div>
					<hr />
					<div class="col-sm-offset-2 col-sm-5">
						<textarea class="form-control" name="comment" id="comment" rows="1" placeholder="Comment/Feedback" required></textarea>
					</div>
					<div class="col-sm-3 text-right">
						<button type="button" id="submit" class="btn btn-success">Leave a Comment</button>
						<button type="reset" class="btn btn-default">Clear</button>
					</div>
				</div>
			</form>
		</div><!-- /.panel-success -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->

<script type="text/javascript">
function showLoading() {
	$(".comments").fadeIn("slow").html('<h4 class="text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i> Loading...</h4>');
}
function deleteComment(commentID) {
	loadComments(commentID);
}
function loadComments(commentID) {
	showLoading();
	commentID = !commentID ? "" : "/"+ commentID;
	$(".comments").load("<?= site_url("comments") ?>"+ commentID +"<?= "?page_url=$pageURL" ?>");
}
$(document).ready(function() {
	loadComments("");
}); 

$(function() {
	$("form.comment-form #submit").on("click", function(e) {
		e.preventDefault();
		if (!$.trim($("textarea#comment").val()).length) {
			$("textarea#comment").val("").focus();
			return false;
		}
		showLoading();
		var formData = new FormData($("form.comment-form")[0]);
		$.ajax({
			type: "post",
			url : "<?= site_url("comments") ?>",
			data : formData, 
			contentType : false, 
			processData : false,
			success : function(data) {
				$(".comments").html(data);
				$(".comments").fadeIn("slow");
			}
		});
	});
});
</script>