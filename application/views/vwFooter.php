<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Footer View of Client Dashboard
 *
 */
?>
				</div><!-- /.col-lg-8 #right-section -->
			</div><!-- /#page-wrapper -->
		</div><!-- /#wrapper -->

		<footer>
			<p>&trade; Ingedata Eyes-T Dashboard &copy; <?=mdate("%Y")?></p>
		</footer>
		<div class="modal fade" id="dialogBox" tabindex="-1" role="dialog" aria-labelledby="dialogBoxLabel">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="dialogBoxLabel">&nbsp;</h4>
					</div>
					<div class="modal-body">
						<h4 class="text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i> Loading...</h4>
					</div>
				</div>
			</div>
		</div>
		<script src="<?php echo HTTP_JS_PATH; ?>collapse.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>tooltip.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>generic.js"></script>
	</body>
</html>