<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Forgot Password View of Client Dashboard
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
			if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 8 seconds you will be redirected to Login.</p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("home") ."';}, 8000);\n</script></div>";
		?>
		<div class="panel panel-default">
			<div class="panel-heading text-right"><h4 class="panel-title text-left"><i class="fa fa-question"></i> Forgot Password</h4></div>
			<form class="form-horizontal" method="post" id="frmForgotPassword">
				<div class="panel-body">
					<div class="form-group">
						<div class="form-group hide">
							<label for="message_type" class="col-sm-3 control-label" title="Required field">Message Type <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="hidden" name="message_type" id="message_type" value="Forgot Password" />
							</div>
						</div>
						<div class="form-group">
							<label for="contact_name" class="col-sm-3 control-label" title="Required field">Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Your Name" name="contact_name" id="contact_name" required value="<?php echo set_value("contact_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-3 control-label" title="Required field">Email Address <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="email" class="form-control" placeholder="Email Address" name="email" id="email" required value="<?php echo set_value("email"); ?>" />
							</div>
						</div>
						<div class="form-group hide">
							<label for="message" class="col-sm-3 control-label" title="Required field">Message <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="hidden" name="message" id="message" value="" />
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer"><center>
					<button type="submit" class="btn btn-default">Submit</button>
				</center></div>
			</form>
		</div><!-- /.panel-default -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
