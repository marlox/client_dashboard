<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Orders View of Client Dashboard
 *
 */
?>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<?php
						if (isset($clientProjects)) :
							$orderName = "";
							$ctrOrder = 0;
							foreach ($clientProjects as $cntProject => $clientProject) :
								$currentProject = $clientProject["project_name"];
								if ($currentClient == $clientProject["client_name"]) :
					?>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading<?=$cntProject?>">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$cntProject?>" aria-expanded="true" aria-controls="collapse<?=$cntProject?>"><div class=status_image title="<?=$status[$clientProject["project_status"]]?>" data-toggle="tooltip" data-placement="bottom" id="<?=strtolower(str_replace(" ", "_", $status[$clientProject["project_status"]]))?>"></div>&nbsp;
									<?=$currentProject?></a>
								</h4>
							</div><!-- /.panel-heading role=tab -->
							<div id="collapse<?=$cntProject?>" class="panel-collapse collapse<?=!$cntProject ? " in" : ""?>" role="tabpanel" aria-labelledby="heading<?=$cntProject?>">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Order Name</th>
											<th>Delivery Date</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
								<?php
								if (isset($projectOrderDetails)) :
									foreach ($projectOrderDetails as $projectOrder) :
										if (trim($currentProject) != trim($projectOrder["project_name"])) $ctrOrder = 0;
										if (trim($currentProject) == trim($projectOrder["project_name"])) :
											$orderDate = isset($projectOrder["order_actual_date"]) ? $projectOrder["order_actual_date"] : $projectOrder["order_target_date"];
											if (trim($orderDate)) $orderDate = mdate("%Y-%m-%d", syncDate($orderDate));
											$ctrOrder++;
											$orderName = $projectOrder["order_name"];
								?>
										<tr>
											<td class="text-center"><?=$ctrOrder?></td>
											<td><a href="<?php echo site_url("orders/tasks/". $projectOrder["order_id"]); ?>" title="Show Order Details of '<?=$orderName?>'" data-toggle="tooltip" data-placement="bottom"><?=$orderName?></td>
											<td><?=$orderDate?></td>
											<td class="text-center"><div class="status_image" title="<?=$status[$projectOrder["order_status"]]?>" data-toggle="tooltip" data-placement="bottom" id="<?=strtolower(str_replace(" ", "_", $status[$projectOrder["order_status"]]))?>"></div></td>
										</tr>
								<?php
										endif;//if (trim($currentProject) == trim($projectOrder["project_name"]))
									endforeach;//foreach ($projectOrderDetails as $projectOrder)
								endif;//if (isset($projectOrderDetails))
								?>
									</tbody>
								</table>
								<div class="form-horizontal"><br /><p class="btn-group">
									<a href="<?php echo site_url("orders/order_submission/". $clientProject["project_id"]); ?>" title="Order Submission" data-toggle="tooltip" data-placement="bottom" role="button" class="btn btn-primary">Submit New Order</a>
									</p>
								</div>
							</div><!-- /.panel-collapse role=tabpanel -->
						</div><!-- /.panel-default -->
					<?php
							endif;
						endforeach;
					endif;//projects
					if (!isset($clientProjects)) :
					?>
					<h4 class="text-center text-danger">No Project information found!</h4>
					<?php
					endif;
					?>
					</div><!-- /.panel-group #accordion -->
