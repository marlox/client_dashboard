<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * New Order Form View of Client Dashboard
 *
 */
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if ($errors) echo "<div class=\"alert alert-danger alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div>";
			if ($success) echo "<div class=\"alert alert-success alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><span class=\"sr-only\">Success:</span> <strong>$success</strong><p>After 10 seconds you will be redirected to Home. <a href=\"". current_url() ."\" title=\"Submit New Order\">Click here to submit an Order again.</a></p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("home") ."';}, 10000);\n</script></div>";
		?>
		<div class="panel panel-info">
			<div class="panel-heading text-right"><h4 class="panel-title text-left"><i class="fa fa-ticket"></i> Order Submission</h4></div>
			<form class="form-horizontal frmPost" method="post" enctype="multipart/form-data" accept-charset="utf-8">
				<div class="panel-body">
					<div class="form-group">
						<div class="form-group hide">
							<label for="order_type" class="col-sm-3 control-label" title="Required field">Order Type <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<select class="form-control" name="order_type" id="order_type" title="Order Type" onchange="changeOrderType(this);" required>
									<option<?php echo (!set_value("order_type") ? " selected" : ""); ?> disabled>Select Order Type</option>
									<?php
									if (isset($types)) :
										foreach ($types as $type) :
									?>
									<option value="<?=$type?>"<?php echo (set_value("order_type") == $type || ($this->session->userdata("client_type") == "trafficking" && preg_match("/({$this->session->userdata("client_type")})+/i", $type)) || ($this->session->userdata("client_type") !== "trafficking" && !preg_match("/(trafficking)+/i", $type)) ? " selected" : ""); ?>><?=$type?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</div>
						</div>
						<div class="form-group hide">
							<label for="client_name" class="col-sm-3 control-label" title="Required field">Client Name <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Client Name" name="client_name" id="client_name" title="Client Name" required value="<?=$currentClient?>" />
							</div>
						</div>
						<div class="form-group hide">
							<label for="project" class="col-sm-3 control-label" title="Required field">Project <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<select class="form-control" name="project_id" id="project" title="Select Project" onchange="getProjectName(this);" required>
									<option<?php echo (!($selectedProject = set_value("project_id")) && !$project_id && ($this->session->userdata("client_type") !== "trafficking") ? " selected" : ""); ?> disabled>Select Project</option>
									<?php
									if (isset($clientProjects)) :
										foreach ($clientProjects as $project) :
									?>
									<option value="<?=$project["project_id"]?>"<?php echo ($project["project_id"] == $project_id || $project["project_id"] == $selectedProject || ($this->session->userdata("client_type") == "trafficking" && preg_match("/{$this->session->userdata("client_type")}\_new/i", $project["project_name"])) ? " selected" : ""); ?>><?=$project["project_name"]?></option>
									<?php
										if (!isset($projectName)) $projectName = ($project["project_id"] == $project_id ? $project["project_name"] : "");
										endforeach;
									endif;
									?>
								</select>
								<input type="hidden" name="project_name" id="project_name" value="<?=$projectName?>" class="hide" />
							</div>
						</div>
						<?php
						$orderLabel = "Order Name";
						if (isset($previouslySubmittedOrders)) :
						?>
						<div class="form-group">
							<label for="is_new_order" class="col-sm-3 control-label">Add to Previous Order &nbsp;</label>
							<div class="col-sm-8">
								<div class="checkbox" title="Add to Previously Submitted Order" data-toggle="tooltip" data-placement="bottom">
								<input type="checkbox" name="is_new_order" id="is_new_order" value="no"<?=(set_value("is_new_order") == "no" ? " checked" : "")?> />
								</div>
							</div>
						</div>
						<div class="form-group order-selection<?=(set_value("is_new_order") == "no" ? "" : " hide")?>">
							<label for="parent_order_submission" class="col-sm-3 control-label" title="Required field"><small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<select class="form-control" name="parent_order_submission" id="parent_order_submission" title="Select Previously Submitted Order" required>
									<option<?php echo (!($selectedOrder = set_value("parent_order_submission")) ? " selected" : ""); ?> disabled>Select Previously Submitted Order</option>
									<?php
									if (isset($previouslySubmittedOrders)) :
										foreach ($previouslySubmittedOrders as $record) :
									?>
									<option value="<?=$record["order_id"]?>"<?php echo ($record["order_id"] == $selectedOrder ? " selected" : ""); ?>><?=$record["order_name"]?></option>
									<?php
										endforeach;
									endif;
									?>
								</select>
							</div>
						</div>
						<?php
							$orderLabel = (set_value("is_new_order") == "no" ? "Specification Name" : "Order Name");
						endif;
						$orderSpecLabel = "";
						if ($this->session->userdata("client_type") == "trafficking") {
							$orderLabel = "Campagne";
							$orderSpecLabel = "$orderLabel Specs";
						}
						?>
						<input type="hidden" value="<?=$orderLabel?>" id="originalOrderLabel" />
						<input type="hidden" value="<?=$orderSpecLabel?>" id="orderSpecLabel" />
						<div class="form-group">
							<label for="order_name" class="col-sm-3 control-label" title="Required field"><?=$orderLabel?> <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="<?=$orderLabel?>" title="<?=$orderLabel?>" name="order_name" id="order_name" required value="<?php echo set_value("order_name"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="delivery_date" class="col-sm-3 control-label" title="Required field">Date MEL <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="date" class="form-control" title="Delivery Date" placeholder="yyyy-mm-dd" name="delivery_date" id="delivery_date" required value="<?php echo set_value("delivery_date"); ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="document" class="col-sm-3 control-label" title="Required field">Document(s) <small><i class="fa fa-asterisk text-danger"></i></small></label>
							<div class="col-sm-8">
								<input type="file" class="form-control" placeholder="Document(s)" title="Document(s)" name="document[]" id="document" multiple required />
								<div class="text-info"><em>Document(s) should be in .XLS or .XLSX or .CSV or .PDF or .JPG or .ZIP file format</em></div>
							</div>
						</div>
						<div class="form-group">
							<label for="order_specifications" class="col-sm-3 control-label">Other Specifications &nbsp;</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="order_specifications" id="order_specifications" title="Order Specifications"><?php echo set_value("order_specifications"); ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer"><center>
					<button type="submit" class="btn btn-info" id="btnSubmit">Submit</button>
					<a href="<?php echo current_url(); ?>" class="btn btn-default" role="button" title="Cancel Order Submission">Cancel</a>
				</center></div>
			</form>
		</div><!-- /.panel-info -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
