<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Campaign Specs Extraction of Client Dashboard
 *
 */
$client = isset($client) ? $client : "Client";
$name = isset($name) ? $name : "Order";
$type = isset($type) ? $type : "";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Eyes-T Dashboard - <?=$client?>'s <?=$name?> <?=$type?></title>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
		<!-- Bootstrap core CSS -->
		<link href="<?php echo HTTP_CSS_PATH; ?>bootstrap.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="<?php echo HTTP_JS_PATH; ?>html5shiv.js"></script>
			<script src="<?php echo HTTP_JS_PATH; ?>respond.min.js"></script>
		<![endif]-->
		<link href="<?php echo HTTP_CSS_PATH; ?>generic.css" rel="stylesheet">
		<link href="<?php echo HTTP_CSS_PATH; ?>styles.css" rel="stylesheet">
	</head>
	<body style="background-color:#eee;padding-top:20px">
		<div class="container">
			<div class="panel-group">
				<?php if ($action == "print") : ?>
				<ul class="breadcrumb">
					<li><?=$client?></li>
					<li class="active"><?=$name?></li>
				</ul>
				<?php 
				elseif ($action == "pdf") :
				?>
				<div class="breadcrumb">
					<?=$client?> &rsaquo; <?=$name?>
				</div>
				<?php endif; ?>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title text-center"><?=$name?> <?=$type?></h4>
						<div class="text-center"><small><?= $campaignDetails ?></small></div>
					</div>
				</div>
				<br>
			</div>
			<?php
			if (isset($category)) :
				$orderName = "";
				$ctr = 0;
				foreach ($category as $cntCateg => $categ) :
			?>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading<?=$cntCateg?>">
						<h4 class="panel-title">
							<?=$categ?>
						</h4>
					</div><!-- /.panel-heading role=tab -->
					<div id="collapse<?=$cntCateg?>" role="tabpanel" aria-labelledby="heading<?=$cntCateg?>">
						<table class="table table-striped table-hover">
							<tbody>
							<?php
							if (isset($records[$categ]) && count($records[$categ])) :
								$categDetails = $records[$categ];
								$ctr = 0;
								foreach ($translation as $key => $val) :
									if (isset($categDetails[$key])) :
										$name = $key;
										$value = isset($categDetails[$key]) ? $categDetails[$key] : "false";
										$remarks = isset($categDetails[$key ."_remarks"]) ? $categDetails[$key ."_remarks"] : "";
										$value = ($value == "true");
										$ctr++;
							?>
									<tr>
										<td width="5%"><?=$ctr?></td>
										<td width="70%"><?=$val?></td>
										<td class="text-center"><h4><?=!$value ? "✗" : "✓"?></h4></td>
										<td width="15%" title="<?=$val?> Remarks" data-toggle="tooltip" data-placement="left"><small><?=$remarks?></small></td>
									</tr>
							<?php
									endif;
								endforeach;//foreach ($categDetails as $name => $value)
							endif;//if (isset($records[$categ]) && count($records[$categ]))
							?>
							</tbody>
						</table>
					</div><!-- /.panel-collapse role=tabpanel -->
				</div><!-- /.panel-default -->
			<?php
				endforeach;//foreach ($category as $cntCateg => $categ)
			endif;//if (isset($category))
			?>
		</div><!-- /.container -->
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>bootstrap.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>generic.js"></script>
	</body>
</html>