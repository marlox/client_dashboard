<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Campaigns Info Extraction of Client Dashboard
 *
 */
$client = isset($client) ? $client : "Client";
$name = isset($name) ? $name : "Order";
$type = isset($type) ? $type : "";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Eyes-T Dashboard - <?=$client?>'s <?=$name?> <?=$type?></title>
		<!-- Bootstrap core CSS -->
		<link href="<?php echo HTTP_CSS_PATH; ?>bootstrap.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="<?php echo HTTP_JS_PATH; ?>html5shiv.js"></script>
			<script src="<?php echo HTTP_JS_PATH; ?>respond.min.js"></script>
		<![endif]-->
		<link href="<?php echo HTTP_CSS_PATH; ?>generic.css" rel="stylesheet">
		<link href="<?php echo HTTP_CSS_PATH; ?>styles.css" rel="stylesheet">
	</head>
	<body style="background-color:#eee;padding-top:20px">
		<div class="container">
			<div class="panel-group">
				<?php if ($action == "print") : ?>
				<ul class="breadcrumb">
					<li><?=$client?></li>
					<li class="active"><?=$name?></li>
				</ul>
				<?php 
				elseif ($action == "pdf") :
				?>
				<div class="breadcrumb">
					<?=$client?> <span class="active"> &gt; </span> <span class="active"><?=$name?></span>
				</div>
				<?php endif; ?>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title text-center"><?=$client?> <?=$name?> <?=$type?></h4>
					</div>
				</div>
				<br />
			</div>
			<div class="table-responsive">
				<table class="table table-striped table-hover tablesorter">
					<thead>
						<tr>
							<th>#</th>
							<th>Campaign Name</th>
							<th>Advertiser</th>
							<th>Ad Server</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
			<?php
			if (isset($records) && $total) :
				foreach ($records as $ctr => $record) :
					$id = $record["order_info_id"];
					$startDate = mdate("%F %d, %Y", syncDate($record["start_date"]));
					$endDate = mdate("%F %d, %Y", syncDate($record["end_date"]));
				?>
					<tr>
						<td><?=($ctr + 1)?></td>
						<td><?=($action == "print" ? "" : '<a href="'. site_url("orders/campaigns/checklist/$id") .'" title="View Campaign Checklist" target="_blank">'). $record["order_name"] .($action == "print" ? "" : '</a>')?></td>
						<td><?=$record["advertiser"]?></td>
						<td><?=$record["ad_server"]?></td>
						<td><?=$startDate?></td>
						<td><?=$endDate?></td>
							<td title="<?="{$record["order_name"]} ". strtoupper($record["status"])?>" data-toggle="tooltip" data-placement="bottom"><span class="label label-<?=$record["status"] == "In Progress" ? "warning" : "success"?>"><?=strtoupper($record["status"])?></span></td>
					</tr>
				<?php
				endforeach;//foreach ($records as $ctr => $record)
			else :
			?>
					<tr class="text-danger text-center">
						<td colspan="9">
							<h4>No Campaign / Order information found...</h4>
						</td>
					</tr>
			<?php
			endif;//if (isset($records) && $total)
			?>
				</tbody>
				</table>
			</div>
		</div><!-- /.container -->
		<script src="<?php echo HTTP_JS_PATH; ?>jquery.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>bootstrap.min.js"></script>
		<script src="<?php echo HTTP_JS_PATH; ?>generic.js"></script>
	</body>
</html>