<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Directories Model of Eyes-T Dashboard
 *
 */
class DirectoriesModel extends CI_Model {
	
	public $table = "directory";
	public $projectID;

	public function __construct() {
		parent::__construct();
	}
	
	public function getAllDirectoryInfo($fields = "*", $from = "directory", $strFilters = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$this->db->select($strFields)->from($from);
		$filters = ((is_string($strFilters) && !$strFilters) || is_null($strFilters) || (is_array($strFilters) && !count($strFilters)) ? " 1 = 1" : $strFilters);
		
		if ($filters) {
			if (is_array($filters)) {
				foreach ($filters as $key => $value) {
					if (strpos($key, "IN") !== false && is_array($value)) $this->db->where_in(trim(str_replace("IN", "", $key)), $value);
					elseif (preg_match("/(\_at)+/i", $key)) $this->db->where($key, $value, false);
					else $this->db->where(array($key => $value));
				}
			} else {
				$this->db->where($filters);
			}
		}
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function save($data, $where = null, $isUpdate = false) {
		if ($isUpdate) {
			$where["project_id"] = $this->projectID;
			$this->db->update($this->table, $data, $where);
		} else {
			$data["project_id"] = $this->projectID;
			$this->db->insert($this->table, $data);
		}
		return $this->db->affected_rows();
	}
}
?>