<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Clients Model of Client Dashboard Admin Interface
 *
 */
class ClientsModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array("UsersModel", "ThemesModel"));
	}

	public function getAllClientsInfo($fields = "*", $filters = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = " 1 = 1";
		
		$this->db->select($strFields)->from("client")->where($strFilters);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getAllInfo($fields = "*", $from = "client", $filters = null, $group = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = " 1 = 1";
		
		$group = is_null($group) ? "" : "GROUP BY $group";
		
		$order = is_null($order) ? "" : "ORDER BY $order";
		
		$limit = is_null($limit) || !is_numeric($limit) ? "" : "LIMIT $limit";
		
		$offset = is_null($offset) || !is_numeric($offset) ? "" : "OFFSET $offset";

		$tempResult = $this->db->query("SELECT ". ($fields == "*" ? "" : "DISTINCT ") ."$strFields FROM $from WHERE $strFilters $group $order $limit $offset");
		
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}

	public function getClientAccountInfo($strFields = null, $strFilters = null, $strOrderBy = null, $intLimit = 20, $intOffset = 0) {
		//set default fields
		if (!isset($strFields) || !trim($strFields)) $strFields = "c.client_name, c.logo, t.theme_id, t.theme_name, acpu.project_id, (SELECT ei.info_value FROM entity_info ei WHERE ei.entity_id = u.user_id AND ei.info_field = 'given_name' AND ei.is_active = 'yes') ||' '|| (SELECT ei.info_value FROM entity_info ei WHERE ei.entity_id = u.user_id AND ei.info_field = 'last_name' AND ei.is_active = 'yes') full_name, u.username, acpu.client_project_user_id, c.client_id, acpu.last_updated_date recent_update, acpu.last_updated_by updated_by";
		//set default filters
		$filters = "AND acpu.is_active = 'yes' AND c.is_active = 'yes' AND u.is_active = 'yes' AND t.is_active = 'yes'";
		if (!isset($strFilters) || !trim($strFilters)) $strFilters = $filters;
		else $strFilters = $filters . (preg_match("/^and/i", trim($strFilters)) ? " $strFilters" : " AND $strFilters");
		$strFilters = $strFilters . ($this->session->userdata("user_type") == "Super Administrator" ? "" : " AND acpu.user_id = '". $this->session->userdata("id") ."'");
		//set default sorting order fields
		if (!isset($strOrderBy) || !trim($strOrderBy)) $strOrderBy = "ORDER BY client_name, project_id, username, logo, theme_name, full_name";
		else $strOrderBy = (preg_match("/(order\ by)+/i") ? trim($strOrderBy) : "ORDER BY $strOrderBy");
		//set default limit
		$strLimit = "";
		if (isset($intLimit) && is_numeric($intLimit)) $strLimit = "LIMIT $intLimit";
		//set default offset
		$strOffset = "";
		if (isset($intOffset) && is_numeric($intOffset)) $strOffset = "OFFSET $intOffset";

		$result = $this->db->query("SELECT $strFields FROM public.assoc_client_project_user acpu, public.client c, public.user u, public.theme t WHERE acpu.client_id = c.client_id AND acpu.user_id = u.user_id AND c.theme_id = t.theme_id $strFilters $strOrderBy $strLimit $strOffset")->result_array();

		return $result;
	}

	public function getClientProjectsAndUsers($clientID) {
		$result = null;
		$arrQuery = $this->db->query("SELECT acpu.client_project_user_id, acpu.project_id, (SELECT username FROM public.user WHERE user_id = acpu.user_id AND is_active = 'yes') username FROM public.assoc_client_project_user acpu WHERE acpu.client_id = '". $clientID ."' AND is_active = 'yes'")->result_array();
		$existingProjects = $existingUsers = $clientProjectUsers = array();
		if (isset($arrQuery)) {
			foreach ($arrQuery as $query) {
				if (array_search($query["project_id"], $existingProjects) === false) $existingProjects[] = $query["project_id"];
				if (array_search($query["username"], $existingUsers) === false) $existingUsers[] = $query["username"];
				$clientProjectUsers[$query["project_id"]][$query["username"]] = $query["client_project_user_id"];
			}
			$result = array("projects" => $existingProjects, "users" => $existingUsers, "clientProjectUsers" => $clientProjectUsers);
		}
		return $result;
	}

	public function getNumberOfClients() {
		$arrQuery = $this->db->query("SELECT COUNT(DISTINCT c.client_name) num_records FROM public.assoc_client_project_user acpu LEFT JOIN public.client c ON acpu.client_id = c.client_id WHERE c.is_active = 'yes'". ($this->session->userdata("user_type") == "Super Administrator" ? "" : " AND acpu.user_id = '". $this->session->userdata("id") ."'") ." AND acpu.is_active = 'yes'")->result_array();
		$count = $arrQuery[0]["num_records"];
		return $count;
	}
	
	public function getNumberOfClientAccounts() {
		$arrQuery = $this->db->query("SELECT COUNT(c.client_name) num_records FROM public.assoc_client_project_user acpu LEFT JOIN public.client c ON acpu.client_id = c.client_id WHERE c.is_active = 'yes'". ($this->session->userdata("user_type") == "Super Administrator" ? "" : " AND acpu.user_id = '". $this->session->userdata("id") ."'") ." AND acpu.is_active = 'yes'")->result_array();
		$count = $arrQuery[0]["num_records"];
		return $count;
	}
	
	public function validateClientAccountInfo($arrRecord) {
		$result = false;
		$arrQuery = $this->getAllClientsInfo("client_id", "UPPER(client_name) = UPPER('". trim($arrRecord["client_name"]) ."') AND is_active = 'yes'");
		if (isset($arrQuery)) {
			$result = "<p>Unable to continue saving Client Account Information. Client Account <u>". $arrRecord["client_name"] ."</u> already exists.</p>";
		} else {
			$mdlThemes = new ThemesModel();
			$arrQuery = $mdlThemes->getAllThemesInfo("theme_name", "theme_id = '". $arrRecord["theme_id"] ."' AND is_active = 'yes'");
			$result = true;
			if (!isset($arrQuery)) $result = "<p>Unable to continue saving Client Account Information. Selected Theme is inactive or cannot be found.</p>";
		}
		return $result;
	}
	
	public function validateAssociatedEntityWithClient($arrRecord) {
		$result = false;
		$arrQuery = $this->db->query("SELECT client_project_user_id FROM public.assoc_client_project_user WHERE client_id = '". $arrRecord["client_id"] ."' AND project_id IN ('". implode("', '", $arrRecord["project_id"]) ."') AND user_id IN (SELECT user_id FROM public.user WHERE username IN ('". implode("', '", $arrRecord["users"]) ."')) AND is_active = 'yes'")->result_array();
		if (isset($arrQuery) && count($arrQuery)) {
			$result = "<p>Unable to continue saving Client Account Information. Client Account <u>". $arrRecord["client_name"] ."</u> integration with Project(s) and/or User(s) already exist.</p>";
		} else {
			$result = true;
		}
		return $result;
	}
	
	public function validateEditClientAccountInfo($arrRecord) {
		$result = false;
		$arrQuery = $this->getAllClientsInfo("client_id, theme_id", "UPPER(client_name) = UPPER('". trim($arrRecord["client_name"]) ."') AND client_id = '". $arrRecord["client_id"] ."' AND is_active = 'yes'");
		$clientAccountInfo = $arrQuery[0];
		if (!isset($arrQuery)) {
			$result = "<p>Unable to continue saving Client Account Information. Client Account <u>". $arrRecord["client_name"] ."</u> cannot be found.</p>";
		} else {
			$mdlThemes = new ThemesModel();
			$arrQuery = $mdlThemes->getAllThemesInfo("theme_name", "theme_id = '". $arrRecord["theme_id"] ."' AND is_active = 'yes'");
			if (!isset($arrQuery)) {
				$result = "<p>Unable to continue saving Client Account Information. Selected Theme is inactive or cannot be found.</p>";
			} else {
				$result = true;
				$clientAccount = $this->getClientProjectsAndUsers($arrRecord["client_id"]);
				$existingProjects = $clientAccount["projects"];
				$existingUsers = $clientAccount["users"];
				if ($existingProjects == $arrRecord["project_id"] && $existingUsers == $arrRecord["users"] && $clientAccountInfo["theme_id"] == $arrRecord["theme_id"]) $result = "<p>No changes detected! Nothing to save in <u>". $arrRecord["client_name"] ."</u> Client Account.</p>";
			}
		}
		return $result;
	}
	
	public function generateClientID() {
		$arrResult = $this->db->select("nextval('client_seq') AS next_seq_num")->get()->result_array();
		$strNextVal = $arrResult[0]["next_seq_num"];
		$generatedID = "ETDCLNT". str_pad($strNextVal, 12, "0", STR_PAD_LEFT);
		return $generatedID;
	}
	
	public function generateClientProjectUserID() {
		$arrResult = $this->db->select("nextval('client_project_user_seq') AS next_seq_num")->get()->result_array();
		$strNextVal = $arrResult[0]["next_seq_num"];
		$generatedID = "ETDCLNTPROJUSER". str_pad($strNextVal, 15, "0", STR_PAD_LEFT);
		return $generatedID;
	}
	
	public function saveClientAccountInfo($arrRecord = null) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("client_name", $arrRecord) && array_key_exists("theme_id", $arrRecord) && array_key_exists("users", $arrRecord)) {
			$isValid = $result = $this->validateClientAccountInfo($arrRecord);
			if ($isValid === true) {
				//prepare information of client table
				$arrRecord["client_id"] = $this->generateClientID();
				$currentUser = $this->session->userdata("username");
				$arrRecord["created_by"] = $currentUser;
				$arrRecord["last_updated_by"] = $currentUser;
				$clientUsers = $arrRecord["users"];
				$clientProjects = $arrRecord["project_id"];
				unset($arrRecord["users"], $arrRecord["project_id"]);
				//begin transaction
				$this->db->trans_start();
				//save information in client table
				$this->db->insert("client", $arrRecord);
				$arrRecord["users"] = $clientUsers;
				$arrRecord["project_id"] = $clientProjects;
				$result = $this->saveClientAssociation($arrRecord, false);
				if ($result !== true) return $result; //forcefully exit function without doing below codes
				$this->db->trans_complete();
				if (!$this->db->trans_status()) {
					$result = "<p>Unable to continue saving Client Account Information. ";
					$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred while saving client account information.");
					$result .= "</p>";
				} else {
					$result = "You have successfully saved Client Account Information.";
				}
			}
		} else {
			$result = "<p>Unable to save due to invalid or blank Client Account Information.</p>";
		}
		
		return $result;
	}
	
	public function saveClientAssociation($arrRecord = null, $canCommit = true) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("client_name", $arrRecord) && array_key_exists("users", $arrRecord) && array_key_exists("project_id", $arrRecord)) {
			if (!isset($arrRecord["client_id"])) {
				$arrClient = $this->getAllClientsInfo("client_id", "is_active = 'yes' AND client_name = '". $arrRecord["client_name"] ."'");
				$arrRecord["client_id"] = $arrClient[0]["client_id"];
				unset($arrClient);
			}
			$isValid = $result = $this->validateAssociatedEntityWithClient($arrRecord);
			if ($isValid === true) {
				//prepare information of assoc_client_project_user table
				$currentUser = $this->session->userdata("username");
				$clientUsers = $arrRecord["users"];
				$clientProjects = $arrRecord["project_id"];
				unset($arrRecord["users"], $arrRecord["project_id"]);
				$allAssocInfo = $arrAssocInfo = array();
				$arrAssocInfo["client_id"] = $arrRecord["client_id"];
				$arrAssocInfo["created_by"] = $arrRecord["created_by"];
				$arrAssocInfo["last_updated_by"] = $arrRecord["created_by"];
				$arrAssocInfo["client_id"] = $arrRecord["client_id"];
				$arrAssocInfo["created_by"] = $currentUser;
				$arrAssocInfo["last_updated_by"] = $currentUser;
				$mdlUsers = new UsersModel();
				$validSelectedUsers = $mdlUsers->getAllUsersInfo("user_id", "username IN ('". implode("', '", $clientUsers) ."') AND is_active = 'yes'");
				$ctr = 0;
				foreach ($clientProjects as $projectID) {
					foreach ($validSelectedUsers as $user) {
						$generatedID = $this->generateClientProjectUserID();
						$arrAssocInfo["client_project_user_id"] = $generatedID;
						$arrAssocInfo["project_id"] = $projectID;
						$arrAssocInfo["user_id"] = $user["user_id"];
						$allAssocInfo[] = $arrAssocInfo;
						$ctr++;
					}
				}
				//begin transaction
				if ($canCommit) $this->db->trans_start();
				//save information in assoc_client_project_user table
				$this->db->insert_batch("assoc_client_project_user", $allAssocInfo);
				$result = true;
				if ($canCommit) {
					$this->db->trans_complete();
					if (!$this->db->trans_status()) {
						$result = "<p>Unable to continue saving Client Account's Project(s) and/or User(s). ";
						$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred while saving Client Account's integration with project(s) and/or user(s).");
						$result .= "</p>";
					} else {
						$result = "You have successfully saved Client Account's Project(s) and User(s).";
					}
				}
			}
		}
		
		return $result;
	}

	public function saveClientInfo($record, $doUpload = true, $strFileInputName = "logo", $canCommit = true) {
		$result = false;
		$isUploaded = true;
		if ($doUpload) {
			$clientName = $record["client_name"];
			$filename = strtoupper(str_replace(" ", "_", $clientName)) .".png";
			$this->initializeUpload($filename);
			$isUploaded = $this->upload->do_upload($strFileInputName);
		}
		if (!$isUploaded) {
			$result = $this->upload->display_errors("<p>Uploading ". $clientName ." logo failed! ", "</p>");
		} else {
			$result = true;
			if (!isset($record["client_id"])) {
				$arrQuery = $this->getAllClientsInfo("client_id", "UPPER(client_name) = UPPER('". trim($clientName) ."') AND is_active = 'yes'");
				$clientID = $arrQuery[0]["client_id"];
			} else {
				$clientID = $record["client_id"];
			}
			if ($canCommit) $this->db->trans_start();
			if ($doUpload) {
				$this->db->set("last_updated_by", $this->session->userdata("username"))->set("last_updated_date", "now()", false)->set("logo", "clients/$filename")->where("client_id", $clientID)->update("client");
			} else {
				unset($record["client_id"]);
				$this->db->set($record)->set("last_updated_by", $this->session->userdata("username"))->set("last_updated_date", "now()", false)->where("client_id", $clientID)->update("client");
			}
			if ($canCommit) {
				$this->db->trans_complete();
				if (!$this->db->trans_status()) {
					$result = "<p>Unable to continue saving Client Information. ";
					$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred while saving client information.");
					$result .= "</p>";
				}
			}
		}
		return $result;
	}
	
	public function saveClientProjectUserAssociation($arrRecord, $isInsert = true, $canCommit = true) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("client_project_user_id", $arrRecord)) {
			if ($canCommit) $this->db->trans_start();
			
			$this->db->set($arrRecord);
			if ($isInsert) $this->db->insert("assoc_client_project_user");
			else {
				$id = $arrRecord["client_project_user_id"];
				unset($arrRecord["client_project_user_id"]);
				$this->db->set("last_updated_by", $this->session->userdata("username"))->set("last_updated_date", "now()", false)->where("client_project_user_id", $id)->update("assoc_client_project_user");
			}

			$result = true;
			if ($canCommit) {
				$this->db->trans_complete();
				if (!$this->db->trans_status()) {
					$result = "<p>Unable to continue saving Client Account's Project(s) and User(s). ";
					$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred!");
					$result .= "</p>";
				}
			}
		}
		return $result;
	}
	
	public function saveEditClientAccountInfo($arrRecord = null) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("client_name", $arrRecord) && array_key_exists("client_id", $arrRecord) && array_key_exists("theme_id", $arrRecord) && array_key_exists("project_id", $arrRecord) && array_key_exists("users", $arrRecord)) {
			$isValid = $result = $this->validateEditClientAccountInfo($arrRecord);
			if ($isValid === true) {
				//prepare information of client table
				$currentUser = $this->session->userdata("username");
				$arrRecord["last_updated_by"] = $currentUser;
				$clientUsers = $arrRecord["users"];
				$clientProjects = $arrRecord["project_id"];
				$clientID = $arrRecord["client_id"];
				$clientName = $arrRecord["client_name"];
				unset($arrRecord["users"], $arrRecord["project_id"], $arrRecord["client_id"]);
				//begin transaction
				$this->db->trans_start();
				//save information in client table
				$this->db->set($arrRecord)->set("last_updated_date", "now()", false)->where(array("client_id" => $clientID, "is_active" => "yes"))->update("client");
				unset($arrRecord["client_name"], $arrRecord["theme_id"]);
				$arrQuery = $this->getClientProjectsAndUsers($clientID);
				$clientProjectUsers = $arrQuery["clientProjectUsers"];
				$existingUsers = $arrQuery["users"];
				$existingProjects = $arrQuery["projects"];
				$strResult = "";
				//remove existing client projects
				if ($removeClientProjects = array_diff($existingProjects, $clientProjects)) {
					$arrRecord["is_active"] = "no";
					foreach ($removeClientProjects as $project) {
						foreach ($existingUsers as $user) {
							if (isset($clientProjectUsers[$project][$user])) {
								$arrRecord["client_project_user_id"] = $clientProjectUsers[$project][$user];
								$result = $result && $this->saveClientProjectUserAssociation($arrRecord, false, false);//remove projects + no commit
							}
						}
					}
					if ($result) {
						$count = count($removeClientProjects);
						$strResult .= "<p>You have removed $count existing client account's project". ($count > 1 ? "s" : "") .".</p>";
					}
				}
				//remove existing client users
				if ($removeClientUsers = array_diff($existingUsers, $clientUsers)) {
					$arrRecord["is_active"] = "no";
					foreach ($existingProjects as $project) {
						foreach ($removeClientUsers as $user) {
							if (isset($clientProjectUsers[$project][$user])) {
								$arrRecord["client_project_user_id"] = $clientProjectUsers[$project][$user];
								$result = $result && $this->saveClientProjectUserAssociation($arrRecord, false, false);//remove projects + no commit
							}
						}
					}
					if ($result) {
						$count = count($removeClientUsers);
						$strResult .= "<p>You have removed $count existing client account's user". ($count > 1 ? "s" : "") .".</p>";
					}
				}
				//add new client projects
				if ($newClientProjects = array_diff($clientProjects, $existingProjects)) {
					if (!$removeClientUsers) $removeClientUsers = array();
					if (isset($arrRecord["is_active"])) unset($arrRecord["is_active"]);
					$arrRecord["client_id"] = $clientID;
					$arrRecord["created_by"] = $arrRecord["last_updated_by"];
					$users = array_diff($existingUsers, $removeClientUsers);
					$mdlUsers = new UsersModel();
					$users = $mdlUsers->getAllUsersInfo("user_id", "username IN ('". implode("', '", $users) ."') AND is_active = 'yes'");
					foreach ($newClientProjects as $project) {
						foreach ($users as $user) {
							$arrRecord["client_project_user_id"] = $this->generateClientProjectUserID();
							$arrRecord["project_id"] = $project;
							$arrRecord["user_id"] = $user["user_id"];
							$result = $result && $this->saveClientProjectUserAssociation($arrRecord, true, false);//new projects + no commit
						}
					}
					if ($result) {
						$count = count($newClientProjects);
						$strResult .= "<p>You have added $count new client account's project". ($count > 1 ? "s" : "") .".</p>";
					}
				}
				//add new client users
				if ($newClientUsers = array_diff($clientUsers, $existingUsers)) {
					if (!$removeClientProjects) $removeClientProjects = array();
					if (!$newClientProjects) $newClientProjects = array();
					if (isset($arrRecord["is_active"])) unset($arrRecord["is_active"]);
					$arrRecord["client_id"] = $clientID;
					$arrRecord["created_by"] = $arrRecord["last_updated_by"];
					$projects = array_unique(array_merge(array_diff($existingProjects, $removeClientProjects), $newClientProjects));
					if (!isset($mdlUsers)) $mdlUsers = new UsersModel();
					$newClientUsers = $mdlUsers->getAllUsersInfo("user_id", "username IN ('". implode("', '", $newClientUsers) ."') AND is_active = 'yes'");
					foreach ($projects as $project) {
						foreach ($newClientUsers as $user) {
							$arrRecord["client_project_user_id"] = $this->generateClientProjectUserID();
							$arrRecord["project_id"] = $project;
							$arrRecord["user_id"] = $user["user_id"];
							$result = $result && $this->saveClientProjectUserAssociation($arrRecord, true, false);//new users + no commit
						}
					}
					if ($result) {
						$count = count($newClientUsers);
						$strResult .= "<p>You have added $count new client account's user". ($count > 1 ? "s" : "") .".</p>";
					}
				}
				$this->db->trans_complete();
				if (!$this->db->trans_status()) {
					$result = "<p>Unable to continue modifying <u>$clientName</u>'s Client Account Information. ";
					$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred while saving the changes in <u>$clientName</u>'s client account.");
					$result .= "</p>";
				} else {
					$result = str_replace("<p>", "<p><span class=\"fa fa-check-circle\" aria-hidden=\"true\"></span> ", "You have successfully modified <u>$clientName</u>'s Client Account Information.$strResult");
				}
			}
		} else {
			$result = "<p>Unable to save due to invalid or blank Edit Client Account form.</p>";
		}
		
		return $result;
	}
	
	public function initializeUpload($strFilename, $strPath = PATH_LOGO_DIR, $strFileExtension = "png", $maxWidth = 150, $maxHeight = 50, $overwrite = true, $lowerFileExtension = true) {
		$arrConfig = array(
								"file_name" => $strFilename,
								"upload_path" => $strPath,
								"allowed_types" => $strFileExtension,
								"max_width" => $maxWidth,
								"max_height" => $maxHeight,
								"overwrite" => $overwrite,
								"file_ext_tolower" => $lowerFileExtension
								);
		$this->load->library("upload", $arrConfig);
	}
	
}
?>