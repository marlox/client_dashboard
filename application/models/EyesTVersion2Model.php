<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Eyes-T Version 2 Model for Client Dashboard Admin Interface
 *
 */
class EyesTVersion2Model extends CI_Model {
	
	public $dbEyesT;

	public function __construct() {
		parent::__construct();
		$this->dbEyesT = $this->load->database("EyestV2", true);
	}
	
	public function getAllProjects($return = "array") {
		$result = null;
		
		$tempResult = $this->dbEyesT->query("SELECT code_projet project_name, id_projet project_id FROM public.projet WHERE code_projet IS NOT NULL AND TRIM(code_projet) != '' ORDER BY code_projet");
		
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getAllProjectFields() {
		$result = null;
		
		$tempResult = $this->dbEyesT->query("SELECT code_analytique, url_consigne, type_input, type_output, rep_input, rep_output, prefix_commande, is_checking_doublon, is_field_volum_on_reporting, is_type_ligne, level_data, is_stand_by FROM public.projet WHERE code_projet IS NOT NULL AND TRIM(code_projet) != '' LIMIT 1");
		
		if ($tempResult->num_rows()) $result = array_keys($tempResult->result_array()[0]);
		
		return $result;
	}

	public function getAllOrders($return = "array") {
		$result = null;
		
		$tempResult = $this->dbEyesT->query("SELECT DISTINCT nom order_name, id_commande order_id FROM public.pj_commande WHERE nom IS NOT NULL AND TRIM(nom) != '' ORDER BY nom");
		
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}

	public function getAllOrderFields() {
		$result = null;
		
		$tempResult = $this->dbEyesT->query("SELECT date_livraison, date_livraison_reelle, nb_data, path_destination, unite_production, is_dispatch_random, type_preparation, is_projet_analytique, nb_flux_max_par_jour, date_insertion, type, redac_nb_paragraphe_min, redac_nb_mots_max, redac_nb_car_min, redac_nb_car_max, redac_nb_per_paragraphe, redac_nb_paragraphe_max, redac_nb_mots_min FROM public.pj_commande WHERE nom IS NOT NULL AND TRIM(nom) != '' LIMIT 1");
		
		if ($tempResult->num_rows()) $result = array_keys($tempResult->result_array()[0]);
		
		return $result;
	}
	
	public function getAllClients($return = "array") {
		$result = null;
		
		$tempResult = $this->dbEyesT->query("SELECT nom client_name, NULL client_id FROM public.pj_client WHERE nom IS NOT NULL ORDER BY nom");
		
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getAllInfo($fields = "*", $from = "client", $filters = null, $group = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = "1 = 1";
		if (is_array($filters)) {
			$strFilters = "1 = 1";
			foreach ($filters as $key => $value) $strFilters .= " AND $key ". (preg_match("/(\ )+/i", $key) ? "" : "= ") ."$value";
		}
		
		$group = is_null($group) ? "" : "GROUP BY $group";
		
		$order = is_null($order) ? "" : "ORDER BY $order";
		
		$limit = is_null($limit) || !is_numeric($limit) ? "" : "LIMIT $limit";
		
		$offset = is_null($offset) || !is_numeric($offset) ? "" : "OFFSET $offset";

		$tempResult = $this->dbEyesT->query("SELECT ". ($fields == "*" ? "" : "DISTINCT ") ."$strFields FROM $from WHERE $strFilters $group $order $limit $offset");
		
		if (isset($tempResult) && $tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getAllProjectStatus() {
		$arrQuery = $this->dbEyesT->query("SELECT DISTINCT statut project_status FROM public.projet")->result_array();

		return $arrQuery;
	}
	
	public function getAllOrderStatus() {
		$arrQuery = $this->dbEyesT->query("SELECT DISTINCT statut order_status FROM public.pj_commande")->result_array();

		return $arrQuery;
	}
	
	public function getAllTaskStatus() {
		$arrQuery = $this->dbEyesT->query("SELECT DISTINCT statut task_status FROM public.data_metadata WHERE statut IS NOT NULL")->result_array();

		return $arrQuery;
	}
	
	public function getProcessSteps($projectID) {
		$arrQuery = $projectID ? $this->dbEyesT->query("SELECT nom process_step, ordre \"order\" FROM public.pj_etape WHERE id_projet = $projectID ORDER BY ordre")->result_array() : null;

		return $arrQuery;
	}

	public function getTraffickingProcessSteps() {
		$arr = array(
						"1. Vérif. Inputs",
						"2. Prepa & Traitement spec.",
						"3. Trafficking",
						"4. Validation trafficking",
						"5. MEL",
						"6. Pige",
						"7. Reporting",
						"8. Terminé"
					);
		//krsort($arr);
		return $arr;
	}

	public function getTraffickingInfoPerProject($projectID, $projectName, $filters = null) {
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = "1 = 1";
		if (is_array($filters)) {
			$strFilters = "1 = 1";
			foreach ($filters as $key => $value) $strFilters .= " AND $key ". (preg_match("/(\ )+/i", $key) ? "" : "= ") ."$value";
		}
		$strSQL = "SELECT * FROM (
		WITH t2 AS (
         WITH t1 AS (
                 SELECT c.nom AS order_name,
                    c.id_commande AS order_id,
                    c.type AS order_type,
                    (lpad(e.ordre::character varying::text, 2, '0'::text) || '_'::text) || e.nom::text AS task,
                    array_length(regexp_split_to_array(d.nombre_fichier, '[,;|]'::text), 1) AS nb_file,
                    count(DISTINCT m.id_metadata) AS nb_row_total,
                    count(DISTINCT m2.id_metadata) AS nb_row_finished,
                    to_char(concat_ws(' '::text, sum(date_part('epoch'::text, r.fin - r.debut)) / 3600::double precision, 'hour')::interval, 'HH24:MI:SS'::text) AS task_duration,
                    sum(date_part('epoch'::text, r.fin - r.debut)) AS task_duration_brute
                   FROM public.pj_commande c
                     JOIN public.data_input i USING (id_commande)
                     JOIN public.suivi_workflow w USING (id_input)
                     JOIN public.data_metadata m USING (id_workflow)
                     JOIN public.pj_etape e ON e.id_etape = m.id_etape
                     JOIN public.suivi_reporting r ON r.id_metadata = m.id_metadata
                     JOIN \"$projectName\".data d ON d.id_metadata = m.id_metadata
                     LEFT JOIN public.data_metadata m2 ON m2.id_metadata = m.id_metadata AND m.statut = 'termine'::data_metadata_statut
                  WHERE c.id_projet = $projectID AND c.statut = 'actif'::commande_statut AND c.type = 'Production'::commande_type
                  GROUP BY c.nom, c.id_commande, c.type, ((lpad(e.ordre::character varying::text, 2, '0'::text) || '_'::text) || e.nom::text), (array_length(regexp_split_to_array(d.nombre_fichier, '[,;|]'::text), 1))
                  ORDER BY c.id_commande, c.type
                )
         SELECT t1.order_name,
            t1.order_id,
            t1.task,
                CASE
                    WHEN t1.task = '02_Preparation_fichier_collecte'::text THEN t1.nb_file::bigint
                    ELSE t1.nb_row_total
                END AS nb_input,
                CASE
                    WHEN t1.task = '02_Preparation_fichier_collecte'::text THEN t1.nb_file::bigint
                    ELSE t1.nb_row_finished
                END AS nb_finished,
            to_char(concat_ws(' '::text, sum(t1.task_duration_brute) / 3600::double precision, 'hour')::interval, 'HH24:MI:SS'::text) AS task_duration
           FROM t1
          GROUP BY t1.order_name, t1.order_id, t1.task, (
                CASE
                    WHEN t1.task = '02_Preparation_fichier_collecte'::text THEN t1.nb_file::bigint
                    ELSE t1.nb_row_total
                END), (
                CASE
                    WHEN t1.task = '02_Preparation_fichier_collecte'::text THEN t1.nb_file::bigint
                    ELSE t1.nb_row_finished
                END)
          ORDER BY t1.order_id, t1.task
        )
 SELECT t2.order_name,
    t2.order_id,
    pc.date_insertion AS created_date,
        CASE
            WHEN pc.date_livraison_reelle IS NULL THEN pc.date_livraison
            ELSE pc.date_livraison_reelle
        END AS delivery_date,
        CASE
            WHEN count(plan_media_recu.*) = 0 THEN '1. Vérif. Inputs'::text
            ELSE
            CASE
                WHEN count(plan_media_verifie.*) = 0 OR count(spec_preparee.*) = 0 OR count(spec_verifie.*) = 0 THEN '2. Prepa & Traitement spec.'::text
                ELSE
                CASE
                    WHEN count(spec_traitee.*) = 0 THEN '3. Trafficking'::text
                    ELSE
                    CASE
                        WHEN count(trafficking_collection_creee.*) = 0 THEN '4. Validation trafficking'::text
                        ELSE
                        CASE
                            WHEN count(trafficking_validee.*) = 0 THEN '5. MEL'::text
                            ELSE
                            CASE
                                WHEN count(suivi_mise_en_ligne.*) = 0 THEN '6. Pige'::text
                                ELSE
                                CASE
                                    WHEN count(pige_faite.*) = 0 THEN '7. Reporting'::text
                                    ELSE '8. Terminé'::text
                                END
                            END
                        END
                    END
                END
            END
        END AS process_step
   FROM t2
     LEFT JOIN ( SELECT t2_1.order_name,
            t2_1.order_id,
            t2_1.task,
            t2_1.nb_input,
            t2_1.nb_finished,
            t2_1.task_duration
           FROM t2 t2_1) plan_media_recu ON plan_media_recu.order_name::text = t2.order_name::text AND plan_media_recu.task = t2.task AND plan_media_recu.task = '01_Verification_input'::text
     LEFT JOIN ( SELECT t2_1.order_name,
            t2_1.order_id,
            t2_1.task,
            t2_1.nb_input,
            t2_1.nb_finished,
            t2_1.task_duration
           FROM t2 t2_1) plan_media_verifie ON plan_media_verifie.order_name::text = t2.order_name::text AND plan_media_recu.task = t2.task AND plan_media_verifie.task = '01_Verification_input'::text AND plan_media_verifie.nb_input = plan_media_verifie.nb_finished
     LEFT JOIN ( SELECT t2_1.order_name,
            t2_1.order_id,
            t2_1.task,
            t2_1.nb_input,
            t2_1.nb_finished,
            t2_1.task_duration
           FROM t2 t2_1) spec_preparee ON spec_preparee.order_name::text = t2.order_name::text AND spec_preparee.task = t2.task AND spec_preparee.task = '03_Verification_specification'::text AND spec_preparee.nb_input = spec_preparee.nb_finished
     LEFT JOIN ( SELECT t2_1.order_name,
            t2_1.order_id,
            t2_1.task,
            t2_1.nb_input,
            t2_1.nb_finished,
            t2_1.task_duration
           FROM t2 t2_1) spec_verifie ON spec_verifie.order_name::text = t2.order_name::text AND spec_verifie.task = t2.task AND spec_verifie.task = '03_Verification_specification'::text AND spec_verifie.nb_input = spec_verifie.nb_finished
     LEFT JOIN ( SELECT t2_1.order_name,
            t2_1.order_id,
            t2_1.task,
            t2_1.nb_input,
            t2_1.nb_finished,
            t2_1.task_duration
           FROM t2 t2_1) spec_traitee ON spec_traitee.order_name::text = t2.order_name::text AND spec_traitee.task = t2.task AND spec_traitee.task = '04_Traitement_specs'::text AND spec_traitee.nb_input = spec_traitee.nb_finished
     LEFT JOIN ( SELECT t2_1.order_name,
            t2_1.order_id,
            t2_1.task,
            t2_1.nb_input,
            t2_1.nb_finished,
            t2_1.task_duration
           FROM t2 t2_1) trafficking_collection_creee ON trafficking_collection_creee.order_name::text = t2.order_name::text AND trafficking_collection_creee.task = t2.task AND trafficking_collection_creee.task = '05_Trafficking_placement'::text
     LEFT JOIN ( SELECT t2_1.order_name,
            t2_1.order_id,
            t2_1.task,
            t2_1.nb_input,
            t2_1.nb_finished,
            t2_1.task_duration
           FROM t2 t2_1) trafficking_collection_inseree ON trafficking_collection_creee.order_name::text = t2.order_name::text AND trafficking_collection_creee.task = t2.task AND trafficking_collection_creee.task = '07_Trafficking_url_crea'::text AND trafficking_collection_creee.nb_input = trafficking_collection_creee.nb_finished
     LEFT JOIN ( SELECT t2_1.order_name,
            t2_1.order_id,
            t2_1.task,
            t2_1.nb_input,
            t2_1.nb_finished,
            t2_1.task_duration
           FROM t2 t2_1) trafficking_validee ON trafficking_validee.order_name::text = t2.order_name::text AND trafficking_validee.task = t2.task AND trafficking_validee.task = '08_Finalisation_trafficking_url_crea'::text AND trafficking_validee.nb_input = trafficking_validee.nb_finished
     LEFT JOIN ( SELECT t2_1.order_name,
            t2_1.order_id,
            t2_1.task,
            t2_1.nb_input,
            t2_1.nb_finished,
            t2_1.task_duration
           FROM t2 t2_1) suivi_mise_en_ligne ON suivi_mise_en_ligne.order_name::text = t2.order_name::text AND suivi_mise_en_ligne.task = t2.task AND suivi_mise_en_ligne.task = '08_Finalisation_trafficking_url_crea'::text AND suivi_mise_en_ligne.nb_input = suivi_mise_en_ligne.nb_finished
     LEFT JOIN ( SELECT t2_1.order_name,
            t2_1.order_id,
            t2_1.task,
            t2_1.nb_input,
            t2_1.nb_finished,
            t2_1.task_duration
           FROM t2 t2_1) pige_faite ON pige_faite.order_name::text = t2.order_name::text AND pige_faite.task = t2.task AND pige_faite.task = '08_Finalisation_trafficking_url_crea'::text AND pige_faite.nb_input = pige_faite.nb_finished
     LEFT JOIN public.pj_commande pc ON pc.nom::text = t2.order_name::text
  GROUP BY t2.order_name, t2.order_id, pc.date_insertion, (
        CASE
            WHEN pc.date_livraison_reelle IS NULL THEN pc.date_livraison
            ELSE pc.date_livraison_reelle
        END)
  ORDER BY pc.date_insertion, t2.order_id
  ) final_query
  WHERE $strFilters";

		return $this->dbEyesT->query($strSQL)->result_array();
	}
	
	public function getTraffickingOrderDetailsPerProject($projectID, $projectName, $fields = "*", $filters = null, $orderBy = null) {
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = "1 = 1";
		if (is_array($filters)) {
			$strFilters = "1 = 1";
			foreach ($filters as $key => $value) $strFilters .= " AND $key ". (preg_match("/(\ )+/i", $key) ? "" : "= ") ."$value";
		}
		$order = is_null($order) ? "" : "ORDER BY $order";
		$strDistinct = ($fields == "*" ? "" : "DISTINCT ");
		
		$strSQL = "SELECT $strDistinct $strFields FROM (
		SELECT c.nom AS order_name,
      c.statut AS order_status,
      c.id_commande AS order_id,
      d.placement_code AS task_name,
      d.placement,
      m.statut AS status,
      min(w.date_time) AS start_date,
      min(w.date_last_modif) AS end_date,
      to_char(min(w.date_last_modif) - min(w.date_time), 'HH24:MI:SS'::text) AS task_duration
     FROM public.pj_commande c
       JOIN public.data_input i USING (id_commande)
       JOIN public.suivi_workflow w USING (id_input)
       JOIN public.data_metadata m USING (id_workflow)
       JOIN public.pj_etape e ON e.id_etape = m.id_etape
       JOIN public.suivi_reporting r ON r.id_metadata = m.id_metadata
       JOIN \"$projectName\".data d ON d.id_metadata = m.id_metadata
    WHERE c.id_projet = $projectID AND c.statut = 'actif'::commande_statut AND c.type = 'Production'::commande_type
    GROUP BY c.nom, c.statut, c.id_commande, d.placement_code, d.placement, m.statut;
    ) final_query
    WHERE $strFilters $order";
		
		return $this->dbEyesT->query($strSQL)->result_array();
	}

}
?>