<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Users Model of Client Dashboard Admin Interface
 *
 */
class UsersModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array("EntityInfoModel", "FieldTypeModel"));
	}
	
	public function getAllUserRoleInfo($fields = "*", $filters = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = " 1 = 1";
		
		$this->db->select($strFields)->from("user_role")->where($strFilters);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
		
	public function getAllUsersInfo($fields = "*", $filters = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		$strPrefix = "us";

		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", $strPrefix.", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = " 1 = 1". ($this->session->userdata("user_type") == "Super Administrator" ? "" : " AND $strPrefix.username NOT IN ('superadmin', 'root')");
		if (is_string($filters)) $strFilters .= ($this->session->userdata("user_type") == "Super Administrator" ? "" : " AND $strPrefix.username NOT IN ('superadmin', 'root')");

		$strOtherInfo = ", (SELECT ei.info_value FROM entity_info ei WHERE ei.entity_id = $strPrefix.user_id AND ei.info_field = 'given_name' AND ei.is_active = 'yes') given_name, (SELECT ei.info_value FROM entity_info ei WHERE ei.entity_id = $strPrefix.user_id AND ei.info_field = 'last_name' AND ei.is_active = 'yes') last_name, (SELECT ei.info_value FROM entity_info ei WHERE ei.entity_id = $strPrefix.user_id AND ei.info_field = 'given_name' AND ei.is_active = 'yes') ||' '|| (SELECT ei.info_value FROM entity_info ei WHERE ei.entity_id = $strPrefix.user_id AND ei.info_field = 'last_name' AND ei.is_active = 'yes') full_name";
		$this->db->select($strPrefix .".". $strFields . $strOtherInfo)->from("user $strPrefix");
		if ($strFilters) $this->db->where($strFilters);
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);

		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getUserClientInfo($fields = "*", $from = "client", $filters = null, $order = "last_updated_date", $ascending = false, $limit = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = " 1 = 1";
		
		$this->db->select($strFields)->from($from)->where($strFilters)->order_by($order, ($ascending ? "ASC" : "DESC"));
		if (isset($limit)) $this->db->limit($limit);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}

	public function getNumberOfUsers() {
		$arrQuery = $this->db->query("SELECT COUNT(user_id) num_records FROM public.user WHERE is_active = 'yes'". ($this->session->userdata("user_type") == "Super Administrator" ? "" : " AND username NOT IN ('superadmin', 'root')"))->result_array();
		$count = $arrQuery[0]["num_records"];
		return $count;
	}

	public function generateUserID() {
		$arrResult = $this->db->select("nextval('user_seq') AS user_id")->get()->result_array();
		$strUserID = $arrResult[0]["user_id"];
		$strSequence = str_pad($strUserID, 8, "0", STR_PAD_LEFT);
		$userID = "ETDUSER$strSequence";
		return $userID;
	}
	
	public function validateUserInfo($arrRecord) {
		$result = false;
		$objQuery = $this->db->query("SELECT user_id FROM public.user WHERE username = '". $arrRecord["username"] ."' AND is_active = 'yes'");
		if ($objQuery->num_rows()) {
			$result = "<p>Unable to save User Information <u>". $arrRecord["given_name"] ." ". $arrRecord["last_name"] ."</u>. User already exists.</p>";
		} else {
			$objQuery = $this->db->query("SELECT info_id FROM public.entity_info WHERE info_field = 'given_name' AND info_value = '". $arrRecord["given_name"] ."' AND info_field = 'last_name' AND info_value = '". $arrRecord["last_name"] ."' AND entity_id = entity_id AND is_active = 'yes'");
			$result = true;
			if ($objQuery->num_rows()) $result = "<p>Unable to save User Information <u>". $arrRecord["given_name"] ." ". $arrRecord["last_name"] ."</u>. User information already exists.</p>";
		}
		return $result;
	}
	
	public function validateEditProfile($arrRecord) {
		$result = false;
		$objQuery = $this->db->query("SELECT ei.info_id, ei.info_field, ei.info_value FROM public.user us, public.entity_info ei WHERE us.user_id = ei.entity_id AND us.user_id = '". $this->session->userdata("id") ."' AND us.email = '". $arrRecord["email"] ."' AND us.timezone = '". $arrRecord["timezone"] ."'". (isset($arrRecord["password"]) ? " AND us.password = '". md5(SALT . $arrRecord["password"]) ."'" : "") ." AND ei.info_value IN ('". $arrRecord["last_name"] ."', '". $arrRecord["given_name"] ."') AND us.is_active = 'yes' AND ei.is_active = 'yes'");
		if ($objQuery->num_rows() == 2) {
			$result = "<p>No changes detected! Nothing to save in your profile.</p>";
		} else {
			$result = true;
		}
		return $result;
	}

	public function saveUserInfo($arrRecord = null) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("given_name", $arrRecord) && array_key_exists("last_name", $arrRecord) && array_key_exists("email", $arrRecord)) {
			//prepare information of user table
			$userRole = $this->getAllUserRoleInfo("user_role_id", array("user_role_name" => $arrRecord["user_role"]));
			$arrRecord["user_role_id"] = $userRole[0]["user_role_id"];
			$arrRecord["username"] = $username = str_replace(" ", "_", strtolower(trim($arrRecord["given_name"]) .".". trim($arrRecord["last_name"])));
			$arrRecord["created_by"] = $this->session->userdata("username");
			$arrRecord["last_updated_by"] = $this->session->userdata("username");
			unset($arrRecord["user_role"], $arrRecord["confirm_password"]);
			$isValid = $result = $this->validateUserInfo($arrRecord);
			if ($isValid === true) {
				$arrUserInfo = array_slice($arrRecord, 0, 2, true);
				$arrRecord = array_slice($arrRecord, 2);
				$arrRecord["user_id"] = $this->generateUserID();
				$password = $arrRecord["password"];
				$arrRecord["password"] = md5(SALT . $password);
				//begin transaction
				$this->db->trans_begin();
				//save information in user table
				$this->db->insert("user", $arrRecord);
				$tempResult = $this->db->trans_status();
				$mdlEntityInfo = new EntityInfoModel();
				$mdlFieldType = new FieldTypeModel();
				//prepare information of entity_info table for user entity
				$userInfo = $allInfo = array();
				$userInfo["info_id"] = $infoID = $mdlEntityInfo->generateEntityInfoID();
				$userInfo["info_field"] = "given_name";
				$userInfo["info_value"] = $arrUserInfo["given_name"] = trim($arrUserInfo["given_name"]);
				$userInfo["is_required"] = "yes";
				$userInfo["entity_id"] = $arrRecord["user_id"];
				$arrFieldType = $mdlFieldType->getFieldTypeInfo("field_type_id", array("field_type_name" => "text"));
				$userInfo["field_type_id"] = $arrFieldType[0]["field_type_id"];
				$userInfo["created_by"] = $this->session->userdata("username");
				$userInfo["last_updated_by"] = $this->session->userdata("username");
				$allInfo[] = $userInfo;
				$strPreviousSequence = floatval(substr($userInfo["info_id"], -4));
				$strNextSequence = $strPreviousSequence + 1;
				$newID = str_replace(str_pad($strPreviousSequence, 4, "0", STR_PAD_LEFT), str_pad($strNextSequence, 4, "0", STR_PAD_LEFT), $infoID);
				$userInfo["info_id"] = $newID;
				$userInfo["info_field"] = "last_name";
				$userInfo["info_value"] = $arrUserInfo["last_name"] = trim($arrUserInfo["last_name"]);
				$allInfo[] = $userInfo;
				//save additional information in entity_info table for user entity
				$this->db->insert_batch("entity_info", $allInfo);
				$tempResult = $tempResult && $this->db->trans_status();
				if (!$tempResult) {
					$result = "<p>Unable to continue saving User Information <u>". $arrUserInfo["given_name"] ." ". $arrUserInfo["last_name"] ."</u>. Unexpected error occurred while saving additional user information.</p>";
					if (($dbError = $this->db->get_last_error($this->db->error))) $result = "<p>Unable to continue saving User Information <u>". $arrUserInfo["given_name"] ." ". $arrUserInfo["last_name"] ."</u>. ". $dbError ."</p>";
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
					$result = "You have successfully saved User Information (username: $username > password: $password).";
				}
			}
		} else {
			$result = "<p>Unable to save due to invalid or blank User Information.</p>";
		}
		
		return $result;
	}
	
	public function saveEditProfile($arrRecord = null) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("given_name", $arrRecord) && array_key_exists("last_name", $arrRecord) && array_key_exists("email", $arrRecord)) {
			//prepare information of user table
			$arrRecord["last_updated_by"] = $this->session->userdata("username");
			$isValid = $result = $this->validateEditProfile($arrRecord);
			if ($isValid === true) {
				$arrUserInfo = array_slice($arrRecord, 0, 2, true);
				$arrRecord = array_slice($arrRecord, 2);
				if (isset($arrRecord["password"])) {
					$password = $arrRecord["password"];
					$arrRecord["password"] = md5(SALT . $password);
					unset($arrRecord["confirm_password"]);
				}
				$objQuery = $this->db->query("SELECT ei.info_id, ei.info_field FROM public.user us, public.entity_info ei WHERE us.user_id = ei.entity_id AND us.user_id = '". $this->session->userdata("id") ."' AND us.is_active = 'yes' AND ei.is_active = 'yes' ORDER BY info_field");
				$arrResult = $objQuery->result_array();
				//begin transaction
				$this->db->trans_begin();
				//save information in user table
				$this->db->update("user", $arrRecord, array("user_id" => $this->session->userdata("id"), "is_active" => "yes"));
				$tempResult = $this->db->trans_status();
				//save additional information in entity_info table for user entity
				if (isset($arrResult) && is_array($arrResult) && count($arrResult)) { //update
					foreach ($arrUserInfo as $arrInfoField => $arrInfo) {
						$this->db->update("entity_info", array("info_value" => $arrInfo, "last_updated_by" => $arrRecord["last_updated_by"]), array("info_id" => $arrResult[($arrInfoField == "given_name" ? 0 : 1)]["info_id"], "info_field" => $arrInfoField, "is_active" => "yes"));
						$tempResult = $tempResult && $this->db->trans_status();
						if (!$tempResult) break;
					}
				} else { //insert for no given and last name exists
					//prepare information of entity_info table for user entity
					$mdlEntityInfo = new EntityInfoModel();
					$mdlFieldType = new FieldTypeModel();
					$userInfo = $allInfo = array();
					$userInfo["info_id"] = $mdlEntityInfo->generateEntityInfoID();
					$userInfo["info_field"] = "given_name";
					$userInfo["info_value"] = $arrUserInfo["given_name"];
					$userInfo["is_required"] = "yes";
					$userInfo["entity_id"] = $this->session->userdata("id");
					$arrFieldType = $mdlFieldType->getFieldTypeInfo("field_type_id", array("field_type_name" => "text"));
					$userInfo["field_type_id"] = $arrFieldType[0]["field_type_id"];
					$userInfo["created_by"] = $this->session->userdata("username");
					$userInfo["last_updated_by"] = $this->session->userdata("username");
					$allInfo[] = $userInfo;
					$strPreviousSequence = floatval(substr($userInfo["info_id"], -4));
					$strNextSequence = $strPreviousSequence + 1;
					$userInfo["info_id"] = str_replace($strPreviousSequence, $strNextSequence, $userInfo["info_id"]);
					$userInfo["info_field"] = "last_name";
					$userInfo["info_value"] = $arrUserInfo["last_name"];
					$allInfo[] = $userInfo;
					//save additional information in entity_info table for user entity
					$this->db->insert_batch("entity_info", $allInfo);
					$tempResult = $tempResult && $this->db->trans_status();
				}
				if (!$tempResult) {
					$result = "<p>Unable to continue saving your profile. Unexpected error occurred while saving additional profile information.</p>";
					if (($dbError = $this->db->get_last_error($this->db->error))) $result = $dbError;
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
					$result = "You have successfully saved your profile". (isset($arrRecord["password"]) ? " (username: ". $this->session->userdata("username") ." > password: $password)" : "") .".";
				}
			}
		} else {
			$result = "<p>Unable to save due to invalid or blank profile information.</p>";
		}
		
		return $result;
	}

}
?>