<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Home Model of Client Dashboard Admin Interface
 *
 */
class HomeModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array("UsersModel", "OrdersModel", "ClientsModel", "StatusModel", "EyesTVersion2Model"));
	}
	
	public function getLeftPanelInfo($entity = "client", $reportDates = null, $projectID = null) {
		$result = null;
		$projectNames = $clientProjects = $projectOrders = $projectOrderDetails = $statuses = array();
		$mdlClients = new ClientsModel();
		$mdlEyesT = new EyesTVersion2Model();
		$select = "c.client_id, c.client_name, acu.project_id";
		$from = "assoc_client_project_user acu, client c";
		$where = "acu.client_id = c.client_id AND acu.is_active = 'yes' AND c.is_active = 'yes'". ($this->session->userdata("user_type") == "Super Administrator" ? "" : " AND acu.user_id = '". trim($this->session->userdata("id")) ."'") ." AND c.client_name = '". $this->session->userdata("clientName") ."'";
		//retrieve clients
		$userClients = $mdlClients->getAllInfo($select, $from, $where, null, null, "acu.last_updated_date DESC");
		if (isset($userClients)) {
			$clients = $clientNames = $projectIDs = array();
			foreach ($userClients as $userClient) {
				if (array_search($userClient["client_name"], $clientNames) === false) {
					$clientNames[] = $userClient["client_name"];
					$clients[] = array_slice($userClient, 0, 2, true);
				}
				if (array_search($userClient["project_id"], $projectIDs) === false) $projectIDs[] = $userClient["project_id"];
			}
			if ($projectID) $projectIDs = array($projectID);
			$userClients = $clients;
			switch ($entity) {
				case 'client':
					$result = $userClients;
					break;
				case 'project':
				case 'order':
				case 'orderDetails':
				case 'orderTaskDetails':
					$strReportDate = "task_created_date >= ('now'::timestamp - '1 month'::interval)";
					if ($setDateRange = $this->session->userdata("reportDateRange")) {
						$reportDateRange = explode(",", $setDateRange);
						$strReportDate = " (task_created_date >= '". $reportDateRange[0] ."' AND task_created_date <= '". $reportDateRange[1] ."')";
					}
					//retrieve projects
					$select = "'". implode($clientNames) ."' client_name, code_projet project_name, id_projet project_id, statut project_status";
					$from = "public.projet";
					$where = "id_projet IN (". implode(", ", $projectIDs) .")";
					//retrieve projects
					$clientProjects = $mdlEyesT->getAllInfo($select, $from, $where, null, "project_name");
					$result = $clientProjects;
					if ($entity == "project") break;
					if (isset($clientProjects)) {
						$select = "project_name, project_status, order_id, order_name, order_status, order_target_date, order_actual_date, project_id";
						$from = "eyest_dashboard.client_project_details_vw";
						$where = "project_id IN (". implode(", ", $projectIDs) .") AND $strReportDate";
						//retrieve order details
						$projectOrderDetails = $mdlEyesT->getAllInfo($select, $from, $where, null, "project_name, order_name");
						$result = $projectOrderDetails;
						if ($entity == "order" || $entity == "orderDetails") break;
						$orderNames = array();
						foreach($projectOrders as $order) {
							if (array_search($order["order_name"], $orderNames) === false) $orderNames[] = $order["order_name"];
						}
						$select = "project_name, order_name, task_status label, COUNT(task_status) y";
						//retrieve order task details
						$orderTaskDetails = $mdlEyesT->getAllInfo($select, $from, $where, null, "project_name, order_name");
						$result = $orderTaskDetails;
					}
					break;
			}
		}
		return $result;
	}
	
	public function getLeftPanelAdditionalInfo($entity = "status") {
		switch ($entity) {
			case 'status':
				$mdlStatus = new StatusModel();
				//retrieve statuses
				$objStatuses = $mdlStatus->getAllStatusInfo("eyest_status, status_name", "is_active = 'yes'");
				foreach ($objStatuses as $key => $status) $statuses[isset($status["eyest_status"]) ? $status["eyest_status"] : $key] = $status["status_name"];
				$statuses["retour_cq"] = "KO";
				$statuses["attente_compar"] = "Waiting";
				$result = $statuses;
				break;
			case 'campaigns':
				$currentClient = $this->session->userdata("clientName");
				$arrData["currentClient"] = $currentClient = (preg_match("/(\_)+/i", $currentClient) ? str_replace("_", " ", $currentClient) : $currentClient);
				$mdlOrders = new OrdersModel();
				//retrieve orders/campaigns
				$objQuery = $mdlOrders->getAllOrderInfo("order_info_id, order_code, (SELECT order_name FROM order_submission WHERE order_submission_id = order_info.order_submission_id AND is_active = 'yes' LIMIT 1) order_name", "order_info.order_submission_id IN (SELECT order_submission_id FROM order_submission WHERE client_id = (SELECT client_id FROM client WHERE client_name = '$currentClient' AND is_active = 'yes' LIMIT 1)) AND is_active = 'yes'", "last_updated_date DESC, order_code");
				$result = array();
				if (isset($objQuery)) {
					foreach ($objQuery as $key => $query) $result[$query["order_info_id"]] = $query["order_name"];
				}
				break;
			
			default:
				$result = null;
		}

		return $result;
	}
	
	public function setReportDateFilter($fieldName, $filter) {
		if ($setDateRange = $this->session->userdata("reportDateRange")) {
			$reportDateRange = explode(",", $setDateRange);
			$strReportDate = $reportDateRange[0];
			$filter["$fieldName BETWEEN '$strReportDate' AND"] = "'{$reportDateRange[1]}'";
		} else {
			$strReportDate = "('now'::timestamp - '1 month'::interval)";
			$filter["$fieldName >="] = $strReportDate;
		}
		return $filter;
	}
}
?>