<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Themes Model of Client Dashboard Admin Interface
 *
 */
class ThemesModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getAllThemesInfo($fields = "*", $filters = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = " 1 = 1";
		
		$this->db->select($strFields)->from("theme")->where($strFilters);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}

}
?>