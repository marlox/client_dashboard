<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Orders Model of Eyes-T Dashboard
 *
 */
class OrdersModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->library("email");
		$this->load->model(array("EntityInfoModel", "FieldTypeModel", "StatusModel"));
	}
	
	public function initializeEmail() {
		$arrConfig = array(
								"protocol" => "smtp",
								"smtp_host" => "smtp.mailgun.org",
								"smtp_port" => "2525",
								"smtp_timeout" => "7",
								"smtp_user" => "postmaster@mg.ingedata.net",
								"smtp_pass" => "05987515b6a9718e5ac6f729dacd80e1",
								"newline" => "\r\n",
								"mailtype" => "html",
								"validate" => true
								);
		$this->email->initialize($arrConfig);
	}
	
	public function initializeUpload($strFilename, $strFileExtension = "xls|xlsx|csv|pdf|jpg|jpeg|zip", $strPath = PATH_DOC_DIR, $overwrite = true, $lowerFileExtension = true) {
		$arrConfig = array(
								"file_name" => $strFilename,
								"upload_path" => $strPath,
								"allowed_types" => $strFileExtension,
								"overwrite" => $overwrite,
								"file_ext_tolower" => $lowerFileExtension
								);
		$this->load->library("upload", $arrConfig);
	}
	
	public function getOrderTypesInfo($fields = "*", $strFilters = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$this->db->select($strFields)->from("order_type");
		$filters = ((is_string($strFilters) && !$strFilters) || (is_array($strFilters) && !count($strFilters)) ? " 1 = 1" : $strFilters);
		if ($filters) $this->db->where($filters);
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getOrderTypes() {
		$types = $this->getOrderTypesInfo("order_type_name", array("is_active" => "yes"));
		$result = array();
		foreach ($types as $type) $result[] = $type["order_type_name"];

		return $result;
	}
	
	public function getOrderTypesIcon() {
		$messageTypesIcon = array(
								"Order Request" => 'ticket', 
								"Trafficking Request" => 'road'
								);
		return $messageTypesIcon;
	}
	
	public function getAllOrderSubmissionInfo($fields = "*", $strFilters = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$this->db->select($strFields)->from("order_submission");
		$filters = ((is_string($strFilters) && !$strFilters) || is_null($strFilters) || (is_array($strFilters) && !count($strFilters)) ? " 1 = 1" : $strFilters);
		if (is_string($filters)) $filters .= ($this->session->userdata("user_type") !== "Super Administrator" ? " AND email_sent_to LIKE '%". $this->session->userdata("email") ."%'" : "") ." AND is_email_sent = 'yes'";
		if (is_array($filters) && count($filters) && $this->session->userdata("user_type") !== "Super Administrator") {
			$filters["email_sent_to LIKE"] = "%". $this->session->userdata("email") ."%";
			$filters["is_email_sent"] = "yes";
		}
		if ($filters) $this->db->where($filters);
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getAllOrderInfo($fields = "*", $strFilters = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$this->db->select($strFields)->from("order_info");
		$filters = ((is_string($strFilters) && !$strFilters) || is_null($strFilters) || (is_array($strFilters) && !count($strFilters)) ? " 1 = 1" : $strFilters);
		
		if ($filters) {
			if (is_array($filters)) {
				foreach ($filters as $key => $value) {
					if (strpos($key, "IN") !== false && is_array($value)) $this->db->where_in(trim(str_replace("IN", "", $key)), $value);
					elseif (preg_match("/(\_date)+/i", $key)) $this->db->where($key, $value, false);
					else $this->db->where(array($key => $value));
				}
			} else {
				$this->db->where($filters);
			}
		}
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getPreviousOrderSubmission($fields = "*", $strFilters = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$this->db->select($strFields)->from("order_submission");
		$filters = ((is_string($strFilters) && !trim($strFilters)) || is_null($strFilters) || (is_array($strFilters) && !count($strFilters)) ? " 1 = 1" : $strFilters);
		if ($filters) $this->db->where($filters);
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getAllOrderSpecs($fields = "*", $strFilters = null, $group = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$this->db->select($strFields)->from("order_specifications");
		$filters = ((is_string($strFilters) && !$strFilters) || is_null($strFilters) || (is_array($strFilters) && !count($strFilters)) ? " 1 = 1" : $strFilters);
		if (is_string($filters)) $filters .= " AND is_active = 'yes'";
		if (is_array($filters) && count($filters)) $filters["is_active"] = "yes";
		if ($filters) $this->db->where($filters);
		if ($group) $this->db->group_by($group);
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getAllOrderSpecsView($fields = "*", $strFilters = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$this->db->select($strFields)->from("order_specs_vw");
		$filters = ((is_string($strFilters) && !$strFilters) || is_null($strFilters) || (is_array($strFilters) && !count($strFilters)) ? " 1 = 1" : $strFilters);
		if (is_string($filters)) $filters .= " AND is_active = 'yes'";
		if (is_array($filters) && count($filters)) $filters["is_active"] = "yes";
		if ($filters) $this->db->where($filters);
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getOrderChecklist($fields = "*", $strFilters = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$this->db->select($strFields)->from("order_checklist");
		$filters = ((is_string($strFilters) && !$strFilters) || is_null($strFilters) || (is_array($strFilters) && !count($strFilters)) ? " 1 = 1" : $strFilters);
		/*if (is_string($filters)) $filters .= " AND is_active = 'yes'";
		if (is_array($filters) && count($filters)) $filters["is_active"] = "yes";*/
		if ($filters) $this->db->where($filters);
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}

	public function getEmailRecepients($client, $includeSuperAdmin = true) {
		$result = null;
		$arrClients = array();
		if (is_string($client)) $arrClients[] = $client;

		if ($includeSuperAdmin) {
			$arrSuperAdmin = $this->db->query("SELECT us.username, us.email FROM public.user us, public.user_role usr WHERE us.user_role_id = usr.user_role_id AND us.is_active = 'yes' AND usr.user_role_name = 'Super Administrator'")->result_array();
			if (isset($arrSuperAdmin) && count($arrSuperAdmin)) {
				$result = array();
				foreach ($arrSuperAdmin as $superAdmin) $result[] = ucwords(str_replace(".", " ", $superAdmin["username"])) ." <". $superAdmin["email"] .">";
			}
		}

		$arrClientAdmin = $this->db->query("SELECT". ($includeSuperAdmin ? " DISTINCT" : "") ." us.username, us.email FROM public.user us, public.user_role usr, public.assoc_client_project_user acu, public.client c WHERE us.user_role_id = usr.user_role_id AND us.user_id = acu.user_id AND acu.client_id = c.client_id AND us.is_active = 'yes' AND c.is_active = 'yes' AND acu.is_active = 'yes' AND usr.user_role_name = 'Administrator' AND c.client_name IN ('". implode("', '", $arrClients) ."')". ($includeSuperAdmin ? "" : " ORDER BY acu.created_date"))->result_array();
		if (isset($arrClientAdmin) && count($arrClientAdmin)) {
			if (!$result) $result = array();
			foreach ($arrClientAdmin as $admin) {
				if (array_search($admin["email"], $result) === false) {
					$result[] = ($includeSuperAdmin ? ucwords(str_replace(".", " ", $admin["username"])) ." <". $admin["email"] .">" : $admin["email"]);
				}
			}
		}
		
		return $result;
	}
	
	public function getNumberOfOrderSubmission() {
		$arrQuery = $this->db->query("SELECT COUNT(order_submission_id) num_records FROM public.order_submission". ($this->session->userdata("user_type") == "Super Administrator" ? "" : " WHERE is_email_sent = 'yes' AND email_sent_to LIKE '%". $this->session->userdata("email") ."%'"))->result_array();
		$count = $arrQuery[0]["num_records"];
		return $count;
	}
	
	public function getNumberOfOrdersInformation($arrOrderSubmissionID) {
		$arrQuery = $this->db->query("SELECT COUNT(order_info_id) num_records FROM public.order_info WHERE is_active = 'yes'". (isset($arrOrderSubmissionID) ? " AND order_submission_id IN ('". implode("', '", $arrOrderSubmissionID) ."')" : ""))->result_array();
		$count = $arrQuery[0]["num_records"];
		return $count;
	}
	
	public function getNumberOfOrderSpecs($strOrderInfoID) {
		$arrQuery = $this->db->query("SELECT COUNT(order_spec_id) num_records FROM public.order_specifications WHERE is_active = 'yes' AND order_info_id = '$strOrderInfoID'")->result_array();
		$count = $arrQuery[0]["num_records"];
		return $count;
	}
	
	public function generateOrderSubmissionID() {
		$arrResult = $this->db->select("nextval('order_submission_seq') AS next_seq_num")->get()->result_array();
		$strNextVal = $arrResult[0]["next_seq_num"];
		$strSequence = str_pad($strNextVal, 18, "0", STR_PAD_LEFT);
		$generatedID = "ETDORDERS$strSequence";
		return $generatedID;
	}
	
	public function generateOrderInfoID() {
		$arrResult = $this->db->select("nextval('order_info_seq') AS next_seq_num")->get()->result_array();
		$strNextVal = $arrResult[0]["next_seq_num"];
		$strSequence = str_pad($strNextVal, 8, "0", STR_PAD_LEFT);
		$generatedID = "ETDORDERINFO$strSequence";
		return $generatedID;
	}
	
	public function generateOrderSpecID() {
		$arrResult = $this->db->select("nextval('order_spec_seq') AS next_seq_num")->get()->result_array();
		$strNextVal = $arrResult[0]["next_seq_num"];
		$strSequence = str_pad($strNextVal, 13, "0", STR_PAD_LEFT);
		$strPrefix = "ETDORDERSPEC";
		$generatedID = $strPrefix.$strSequence;
		return $generatedID;
	}
	
	public function validateOrderSubmission($arrRecord) {
		$result = false;
		$objQuery = $this->db->query("SELECT order_submission_id FROM public.order_submission WHERE UPPER(order_name) = UPPER(". $this->db->escape(trim($arrRecord["order_name"])) .") AND client_id = '". trim($arrRecord["client_id"]) ."' AND project_id = '". trim($arrRecord["project_id"]) ."' AND order_type_id = '". trim($arrRecord["order_type_id"]) ."'");
		if ($objQuery->num_rows()) {
			$result = "<p>Unable to continue your Order Submission with the following information: <u>". $arrRecord["order_type"] ."</u>, <u>". $arrRecord["project_name"] ."</u>, <u>". $arrRecord["order_name"] ."</u>. Order Information already exists.</p>";
		} else {
			$objQuery = $this->db->query("SELECT order_type_id FROM public.order_type WHERE UPPER(order_type_name) = UPPER('". trim($arrRecord["order_type"]) ."') AND is_active = 'yes'");
			if ($objQuery->num_rows() == 0) {
				$result = "<p>Unable to continue your Order Submission with the following information: <u>". $arrRecord["order_type"] ."</u>, <u>". $arrRecord["project_name"] ."</u>, <u>". $arrRecord["order_name"] ."</u>. Order Type is invalid or not found.</p>";
			} else {
				$result = true;
				$objQuery = $this->db->query("SELECT client_id FROM public.client WHERE client_id = '". trim($arrRecord["client_id"]) ."' AND is_active = 'yes'");
				if ($objQuery->num_rows() == 0) $result = "<p>Unable to continue your Order Submission with the following information: <u>". $arrRecord["order_type"] ."</u>, <u>". $arrRecord["project_name"] ."</u>, <u>". $arrRecord["order_name"] ."</u>. Client ID is invalid or not found.</p>";
			}
		}
		return $result;
	}
	
	public function validateOrderInfo($arrRecord, $isNew = true) {
		$result = false;
		$objQuery = $this->db->query("SELECT order_info_id FROM public.order_info WHERE UPPER(order_code) = UPPER(". $this->db->escape(trim($arrRecord["order_code"])) .") AND is_active = 'yes'");
		if (($objQuery->num_rows() && $isNew) || ($notExists = ($objQuery->num_rows() == 0 && !$isNew))) {
			$result = "<p>Unable to continue saving Order Information with the following details: <u>". $arrRecord["order_code"] ."</u>, <u>". $arrRecord["order_name"] ."</u>. Order Information ". ($notExists ? "not" : "already") ." exists.</p>";
		} else {
			$objQuery = $this->db->query("SELECT order_name FROM public.order_submission WHERE UPPER(order_name) = UPPER(". $this->db->escape(trim($arrRecord["order_name"])) .") AND order_submission_id = '". $arrRecord["order_submission_id"] ."' AND is_email_sent = 'yes'");
			if ($objQuery->num_rows() == 0) {
				$result = "<p>Unable to continue saving Order Information with the following details: <u>". $arrRecord["order_code"] ."</u>, <u>". $arrRecord["order_name"] ."</u>. Submitted Order information is invalid or not found.</p>";
			} else {
				$result = true;
				$objQuery = $this->db->query("SELECT status_name FROM public.entity_status WHERE status_id = '". trim($arrRecord["status_id"]) ."' AND is_active = 'yes'");
				if ($objQuery->num_rows() == 0) $result = "<p>Unable to continue saving Order Information with the following details: <u>". $arrRecord["order_code"] ."</u>, <u>". $arrRecord["order_name"] ."</u>. Status information is invalid or not found.</p>";
				if (!$isNew) {
					$objQuery = $this->db->query("SELECT order_info_id FROM public.order_info WHERE UPPER(order_code) = UPPER(". $this->db->escape(trim($arrRecord["order_code"])) .") AND order_info_id = '". $arrRecord["order_info_id"]."' AND ad_server = '". $arrRecord["ad_server"]."' AND advertiser = '". $arrRecord["advertiser"]."' AND start_date = '". $arrRecord["start_date"]."' AND end_date = '". $arrRecord["end_date"]."' AND comments = '". $arrRecord["comments"]."' AND is_active = 'yes'");
					if ($objQuery->num_rows()) $result = "<p>No changes detected! Nothing to save for <u>". $arrRecord["order_name"] ."</u> Order Information.</p>";
				}
			}
		}
		return $result;
	}
	
	public function validateOrderSpecifications($arrRecord, $isNew = true) {
		$result = false;
		$objQuery = $this->db->query("SELECT * FROM public.order_specifications WHERE ". (!$isNew ? "order_info_id = '". $arrRecord["order_info_id"] ."'" : "UPPER(order_spec_name) = UPPER('". trim($arrRecord["order_spec_name"]) ."') AND UPPER(order_spec_type) = UPPER('". trim($arrRecord["order_spec_type"]) ."')") ." AND is_active = 'yes'");
		if (($objQuery->num_rows() && $isNew) || ($notExists = ($objQuery->num_rows() == 0 && !$isNew))) {
			$result = "<p>Unable to continue saving Order Specifications with the following details: <u>". $arrRecord["order_spec_name"] ."</u>, <u>". $arrRecord["spec_package_name"] ."</u>. Order Specifications ". ($notExists ? "not" : "already") ." exists.</p>";
		} else {
			$objQuery = $this->db->query("SELECT order_code FROM public.order_info WHERE UPPER(order_info_id) = UPPER('". trim($arrRecord["order_info_id"]) ."') AND is_active = 'yes'");
			if ($objQuery->num_rows() == 0) {
				$result = "<p>Unable to continue saving Order Specifications with the following details: <u>". $arrRecord["order_spec_name"] ."</u>, <u>". $arrRecord["spec_package_name"] ."</u>. Source Order Information is invalid or not found.</p>";
			} else {
				$result = true;
				$objQueryCreative = $this->db->query("SELECT status_name FROM public.entity_status WHERE status_id = '". trim($arrRecord["creative_status_id"]) ."' AND is_active = 'yes'");
				$objQueryURL = $this->db->query("SELECT status_name FROM public.entity_status WHERE status_id = '". trim($arrRecord["url_status_id"]) ."' AND is_active = 'yes'");
				$objQueryMessage = $this->db->query("SELECT status_name FROM public.entity_status WHERE status_id = '". trim($arrRecord["message_status_id"]) ."' AND is_active = 'yes'");
				$isCreativeInvalid = $objQueryCreative->num_rows() == 0;
				$isURLInvalid = $objQueryURL->num_rows() == 0;
				$isMesssageInvalid = $objQueryMessage->num_rows() == 0;
				if ($isCreativeInvalid || $isURLInvalid || $isMesssageInvalid) $result = "<p>Unable to continue saving Order Specifications with the following details: <u>". $arrRecord["order_spec_name"] ."</u>, <u>". $arrRecord["spec_package_name"] ."</u>. Status information is invalid or not found for <u>". ($isCreativeInvalid ? "Creative Status" : "") .($isCreativeInvalid && $isURLInvalid ? ", " : ""). ($isURLInvalid ? "URL Status" : "") .($isURLInvalid && $isMesssageInvalid ? ", " : ""). ($isMesssageInvalid ? "Message Status" : "") ."</u>.</p>";
				if (!$isNew) {
					$objQuery = $this->db->query("SELECT * FROM public.order_specifications WHERE UPPER(order_spec_name) = UPPER('". trim($arrRecord["order_spec_name"]) ."') AND UPPER(order_spec_type) = UPPER('". trim($arrRecord["order_spec_type"]) ."') AND order_info_id = '". $arrRecord["order_info_id"]."' AND creative_status_id = '". $arrRecord["creative_status_id"]."' AND url_status_id = '". $arrRecord["url_status_id"]."' AND message_status_id = '". $arrRecord["message_status_id"]."' AND order_spec_id = '". $arrRecord["order_spec_id"]."' AND is_active = 'yes'");
					if ($objQuery->num_rows()) $result = "<p>No changes detected! Nothing to save for <u>". $arrRecord["order_spec_name"] ."</u> Order Specifications.</p>";
				}
			}
		}
		return $result;
	}
	
	public function saveOrderSubmission($arrRecord, $canCommit = true, $sendEmail = true, $returnID = false) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("order_type", $arrRecord) && array_key_exists("client_name", $arrRecord) && array_key_exists("project_id", $arrRecord) && array_key_exists("order_name", $arrRecord) && array_key_exists("delivery_date", $arrRecord)) {
			$mdlClient = new ClientsModel();
			$arrQuery = $mdlClient->getAllClientsInfo("client_id", "UPPER(client_name) = UPPER('". trim($arrRecord["client_name"]) ."') AND is_active = 'yes'");
			$arrRecord["client_id"] = $arrQuery[0]["client_id"];
			$arrQuery = $this->getOrderTypesInfo("order_type_id", array("order_type_name" => $arrRecord["order_type"]), null, 1);
			$arrRecord["order_type_id"] = $arrQuery[0]["order_type_id"];
			//validate information for order submission
			if (($result = $this->validateOrderSubmission($arrRecord)) !== true) return $result;
			$hasAttachment = (isset($_FILES["document"]["name"]) && is_array($_FILES["document"]["name"]));
			$generatedID = $this->generateOrderSubmissionID();
			$filename = $attachedFiles = array();
			if ($hasAttachment) {
				$files = $_FILES["document"]["name"];
				foreach ($files as $index => $document) {
					$fileExtension = substr($document, strrpos($document, "."));
					$filename[] = "$generatedID-". ($index + 1) . $fileExtension;
					$attachedFiles[] = (string)$document;
				}
			}
			//prepare information of order submission table
			$clientName = $arrRecord["client_name"];
			$projectName = $arrRecord["project_name"];
			$orderType = $arrRecord["order_type"];
			$currentUser = $this->session->userdata("username");
			$arrRecord["order_submission_id"] = $generatedID;
			$arrRecord["ordered_by"] = $arrRecord["last_updated_by"] = $currentUser;
			$arrRecord["document"] = implode(", ", $filename);
			unset($arrRecord["client_name"], $arrRecord["project_name"], $arrRecord["order_type"], $arrQuery);
			//begin transaction
			if ($canCommit) $this->db->trans_start();
			//save information in order table
			if (isset($arrRecord["email_sent_date"]) && $returnID) {
				$this->db->set("email_sent_date", $arrRecord["email_sent_date"]);
				unset($arrRecord["email_sent_date"]);
			}
			$this->db->set($arrRecord)->insert("order_submission");
			if ($sendEmail && $hasAttachment) {
				$this->initializeUpload($filename, "*");
				$isUploaded = $this->upload->do_multi_upload("document");
				if (!$isUploaded) return $this->upload->display_errors("<p>Uploading document(s) <u>". implode(", ", $files) ."</u> for your Order <u>". $arrRecord["order_name"] ."</u> is failed! ", "</p>");
			} else {
				$arrRecord["document"] = "";
			}
			if ($canCommit) {
				//end transaction
				$this->db->trans_complete();
				if (!$this->db->trans_status()) {
					$result = "<p>Unable to continue your Order Submission. ";
					$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred while saving your Order Submission information.");
					$result .= "</p>";
				} else {
					$result = "You have successfully submitted your Order.";
					if ($sendEmail) {
						$arrRecord["client_name"] = $clientName;
						$arrRecord["project_name"] = $projectName;
						$arrRecord["order_type"] = $orderType;
						$arrRecord["attached_files"] = implode(", ", $attachedFiles);
						$this->sendOrderNotificationToClient($arrRecord);
						$tempResult = $this->sendOrderSubmission($arrRecord);
						$result = (preg_match("/(success)+/i", $tempResult) ? "$result $tempResult" : "$result</strong></p> <p>$tempResult</p>");
					}
				}
			}
			if ($returnID) $result = $generatedID;
		} else {
			$result = "<p>Unable to continue due to invalid or blank Order Information.</p>";
		}
		
		return $result;
	}
	
	public function sendOrderSubmission($arrRecord = null, $canCommit = true) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("order_type", $arrRecord) && array_key_exists("client_name", $arrRecord) && array_key_exists("project_id", $arrRecord) && array_key_exists("order_name", $arrRecord) && array_key_exists("delivery_date", $arrRecord)) {
			//prepare information of order submission table for sending email feature
			$generatedID = $arrRecord["order_submission_id"];
			$clientName = (preg_match("/(\_)+/i", $arrRecord["client_name"]) ? str_replace("_", " ", $arrRecord["client_name"]) : $arrRecord["client_name"]);
			$currentUser = ucwords(str_replace(".", " ", $this->session->userdata("username")));
			$arrRecepients = $this->getEmailRecepients($clientName, false);
			$arrRecord["email"] = $this->session->userdata("email");
			$hasAttachment = (isset($arrRecord["document"]) && $arrRecord["document"]);
			//setting information for sending email feature
			$this->initializeEmail();
			$this->email->clear();
			$this->email->from($arrRecord["email"], $currentUser);
			$this->email->to($arrRecepients);
			//$this->email->cc($arrRecord["email"]);
			$emailTitle = $arrRecord["order_type"] ." by $currentUser of $clientName";
			$this->email->subject($emailTitle);
			$strAttach = "";
			if ($hasAttachment) {
				$document = explode(", ", $arrRecord["document"]);
				// foreach ($document as $attachment) $this->email->attach(PATH_DOC_DIR . $attachment);
				$strAttachments = "";
				foreach ($document as $attachment) {
					$arrDocFilename = preg_split("/[-]+/", $attachment);
					$docName = "Document-". $arrDocFilename[count($arrDocFilename)-1];
					$strAttachments .= '<a href="'. HTTP_DOC_DIR . $attachment .'" target="_blank" title="Click to download and see contents of \''. $docName .'\'">'. $docName .'</a>, ';
				}
				// $strAttach = "<li>Document(s) - <em>". $arrRecord["document"] ."</em></li>";
				$strAttach = "<li>Document(s) - <em>". trim($strAttachments, ', ') ."</em></li>";
			}
			$email = array();
			$email["messageSubjectTitle"] = $emailTitle;
			$email["messageAddressee"] = "Hello $clientName Admin, ";
			$email["messagePreview"] = $messagePreview = $arrRecord["order_type"] ." <u>". $arrRecord["order_name"] ."</u> has been submitted by $currentUser of <em>$clientName</em> for the project <em>". trim($arrRecord["project_name"]) ."</em>";
			$email["messageContent"] = "$messagePreview, with the following additional information:<ul><li>Order Specifications - <em>". $arrRecord["order_specifications"] ."</em></li>". ($hasAttachment ? $strAttach : "") ."</ul>";
			$email["messageLinkURL"] = site_url("admin/orders/submitted_orders");
			$email["messageLinkLabel"] = "Manage Submitted Orders";
			$email["messageClosingRemarks"] = "";
			$email["messageSignature"] = "";
			$email["footerLinkURL"] = site_url("admin/dashboard");
			$email["footerLinkLabel"] = "Eyes-T Dashboard - Admin Interface";
			$emailMessage = $this->load->view("emails/vwEmailTemplate", $email, true);
			$this->email->message($emailMessage);
			//send email
			$tempResult = $this->email->send(false);
			$result = "<p>Unable to send email for your Order Submission.</p>";
			if ($tempResult) {
				//begin transaction
				if ($canCommit) $this->db->trans_start();
				//update contact message information in contact_message table for sending email feature
				$this->db->set("is_email_sent", "yes");
				$this->db->set("email_sent_date", "now()", false);
				$this->db->set("email_sent_to", implode(", ", $arrRecepients));
				$this->db->set("last_updated_date", "now()", false);
				$this->db->set("last_updated_by", $this->session->userdata("username"));
				$this->db->where("order_submission_id", $generatedID);
				$this->db->update("order_submission");
				//end transaction
				if ($canCommit) $this->db->trans_complete();
				$tempResult = $tempResult && $this->db->trans_status();
				if (!$tempResult) {
					$result = "<p>Unable to continue sending email for your Order Submission. Unexpected error occurred while saving order submission email information.</p>";
					if (($dbError = $this->db->get_last_error($this->db->error))) $result = "<p>Unable to continue sending email for your Order Submission. ". $dbError ."</p>";
				} else {
					$result = "You also successfully sent your Order Submission thru email.";
				}
			} else {
				$result .= "<p>". $this->email->print_debugger(array("headers")) ."</p>";
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank Order Information.</p>";
		}
		
		return $result;
	}

	public function sendOrderNotificationToClient($arrRecord = null) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("order_type", $arrRecord) && array_key_exists("client_name", $arrRecord) && array_key_exists("project_id", $arrRecord) && array_key_exists("order_name", $arrRecord) && array_key_exists("attached_files", $arrRecord)) {
			$clientName = $arrRecord["client_name"];
			$arrAdminEmail = $this->getEmailRecepients($clientName, false);
			$clientName = (preg_match("/(\_)+/i", $clientName) ? str_replace("_", " ", $clientName) : $clientName);
			$projectName = $arrRecord["project_name"];
			$underscorePosition = strpos($projectName, "_");
			if ($underscorePosition === false) $underscorePosition = strlen($projectName);
			$projectName = substr($projectName, 0, $underscorePosition);
			$orderName = $arrRecord["order_name"];
			$clientAdminName = "$projectName Team";
			$adminEmail = $arrAdminEmail[0];
			$clientEmail = $this->session->userdata("email");
			$hasAttachment = (isset($arrRecord["attached_files"]) && $arrRecord["attached_files"]);
			$emailTitle = "$orderName Acknowledgement";
			$orderSpecs = "";
			if (isset($arrRecord["order_specifications"]) && trim($arrRecord["order_specifications"])) $orderSpecs = "<li>Specifications - <em>". $arrRecord["order_specifications"] ."</em></li>";
			//setting information for sending email feature
			$this->initializeEmail();
			$this->email->clear();
			$this->email->from($adminEmail, $clientAdminName);
			$this->email->to($clientEmail);
			$this->email->subject($emailTitle);
			$strAttach = "";
			if ($hasAttachment) $strAttach = "<li>Document(s) - <em>". $arrRecord["attached_files"] ."</em></li>";
			$email["messageSubjectTitle"] = $emailTitle;
			$email["messageAddressee"] = "Hello $clientName, ";
			$email["messagePreview"] = $messagePreview = "Sending you this email to acknowledge your submitted order <u>$orderName</u> for project <em>$projectName</em>";
			if ($orderSpecs || $strAttach) $messagePreview .= ", with the following additional information:<ul>$orderSpecs $strAttach</ul><br />Our $clientAdminName will check the details of your submitted order and will contact you the soonest possible time.";
			$email["messageContent"] = $messagePreview;
			$email["messageLinkURL"] = null;
			$email["messageLinkLabel"] = null;
			$email["messageClosingRemarks"] = "Should there be any questions or clarifications, you may contact us at $adminEmail";
			$email["messageSignature"] = "Best regards, <br /><br />$clientAdminName";
			$email["footerLinkURL"] = site_url("dashboard");
			$email["footerLinkLabel"] = "Client Dashboard Interface";
			$emailMessage = $this->load->view("emails/vwEmailTemplate", $email, true);
			$this->email->message($emailMessage);
			//send email
			$tempResult = $this->email->send(false);
			$result = "<p>Unable to send notification to client for your Order Submission.</p>";
			if ($tempResult) {
				$result = "You have successfully sent notification to client for your Order Submission thru email.";
			} else {
				$result .= "<p>". $this->email->print_debugger(array("headers")) ."</p>";
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank Order Information.</p>";
		}
		
		return $result;
	}
	
	public function saveOrderInfo($arrRecord, $isNew = true, $skipValidation = false, $canCommit = true) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) || (!$skipValidation && array_key_exists("order_submission_id", $arrRecord) && array_key_exists("order_code", $arrRecord) && array_key_exists("order_name", $arrRecord) && array_key_exists("advertiser", $arrRecord) && array_key_exists("ad_server", $arrRecord) && array_key_exists("start_date", $arrRecord) && array_key_exists("end_date", $arrRecord))) {
			//validate form data of order info
			if (!$skipValidation) {
				if (!isset($arrRecord["status_id"])) $arrRecord["status_id"] = "ETDSTATS04";
				if (($result = $this->validateOrderInfo($arrRecord, $isNew)) !== true) return $result;
			}
			//prepare information of order info table
			$currentUser = $this->session->userdata("username");
			unset($arrRecord["order_name"]);
			$saveRecord = array();
			if ($isNew) {
				$saveRecord["order_info_id"] = $this->generateOrderInfoID();
			} else {
				$orderInfoID = $arrRecord["order_info_id"];
			}
			$saveRecord = $saveRecord + $arrRecord;
			if ($isNew) $saveRecord["created_by"] = $currentUser;
			$saveRecord["last_updated_by"] = $currentUser;
			unset($arrRecord);
			//begin transaction
			if ($canCommit) $this->db->trans_start();
			//save information in order info table
			if ($isNew) $this->db->insert("order_info", $saveRecord);
			else $this->db->set($saveRecord)->set("last_updated_date", "now()", false)->where("order_info_id", $orderInfoID)->update("order_info");
			$result = true;
			if ($canCommit) {
				//end transaction
				$this->db->trans_complete();
				$result = "You have successfully saved Order Information.";
				if (!$this->db->trans_status()) {
					$result = "<p>Unable to continue saving Order Information. ";
					$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred while saving your Order Information.");
					$result .= "</p>";
				}
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank Order Information.</p>";
		}
		
		return $result;
	}
	
	public function saveOrderSpecifications($arrRecord, $isNew = true, $skipValidation = false) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) || (!$skipValidation && array_key_exists("order_info_id", $arrRecord) && array_key_exists("order_spec_type", $arrRecord) && array_key_exists("order_spec_name", $arrRecord) && array_key_exists("spec_site_name", $arrRecord) && array_key_exists("spec_buy_mode", $arrRecord) && array_key_exists("spec_format_code", $arrRecord) && array_key_exists("spec_id", $arrRecord))) {
			if (!isset($arrRecord["order_spec_status_id"])) {
				$orderSpecsStatuses = array($arrRecord["creative_status_id"], $arrRecord["url_status_id"], $arrRecord["message_status_id"]);
				$arrRecord["order_spec_status_id"] = $this->autoDetectStatus($orderSpecsStatuses);
			}
			//validate form data of order specifications
			if (!$skipValidation) {
				if (($result = $this->validateOrderSpecifications($arrRecord, $isNew)) !== true) return $result;
			}
			//prepare information of order specifications table
			$currentUser = $this->session->userdata("username");
			$orderInfoID = $arrRecord["order_info_id"];
			$entityInfo = $arrRecord;
			unset($arrRecord["specs_action"], $arrRecord["spec_type_tag"], $arrRecord["spec_site_name"], $arrRecord["spec_buy_mode"], $arrRecord["spec_format_code"], $arrRecord["spec_id"], $arrRecord["spec_package_name"]);
			$saveRecord = $entityInfoRecord = array();
			if ($isNew) {
				$saveRecord["order_spec_id"] = $id = $this->generateOrderSpecID();
				$saveRecord["original_spec_id"] = $saveRecord["order_spec_id"];
			} else {
				$id = $arrRecord["order_spec_id"];
			}
			$saveRecord = $saveRecord + $arrRecord;
			if ($isNew) $saveRecord["created_by"] = $currentUser;
			$saveRecord["last_updated_by"] = $currentUser;
			unset($arrRecord);
			//begin transaction
			$this->db->trans_start();
			$mdlEntityInfo = new EntityInfoModel();
			$mdlFieldType = new FieldTypeModel();
			//save prepared information in order specifications table
			if ($isNew) {
				$this->db->insert("order_specifications", $saveRecord);
				if (array_key_exists("spec_type_tag", $entityInfo) && array_key_exists("spec_site_name", $entityInfo)) {
					$ctr = 0;
					$generatedID = $mdlEntityInfo->generateEntityInfoID("", "order specs");
					$saveRecord = array();
					$saveRecord["info_id"] = $saveRecord["info_field"] = $saveRecord["info_value"] = "";
					$saveRecord["entity_id"] = $id;
					$saveRecord["is_required"] = "yes";
					$arrFieldType = $mdlFieldType->getFieldTypeInfo("field_type_id", array("field_type_name" => "text"));
					$saveRecord["field_type_id"] = $arrFieldType[0]["field_type_id"];
					$saveRecord["created_by"] = $saveRecord["last_updated_by"] = $currentUser;
					foreach ($entityInfo as $field => $value) {
						if (preg_match("/^spec\_/i", $field)) {
							$strPreviousSequence = floatval(substr($generatedID, -16));
							$strNextSequence = $strPreviousSequence + $ctr;
							$newID = str_replace(str_pad($strPreviousSequence, 16, "0", STR_PAD_LEFT), str_pad($strNextSequence, 16, "0", STR_PAD_LEFT), $generatedID);
							$saveRecord["info_id"] = trim($newID);
							$saveRecord["info_field"] = trim($field);
							$saveRecord["info_value"] = trim($value);
							$entityInfoRecord[] = $saveRecord;
							$ctr++;
						}
					}
					$this->db->insert_batch("entity_info", $entityInfoRecord);
				}
			} else {
				$this->db->set($saveRecord)->set("last_updated_date", "now()", false)->where("order_spec_id", $id)->update("order_specifications");
				if (array_key_exists("spec_type_tag", $entityInfo) && array_key_exists("spec_site_name", $entityInfo)) {
					$infoIDs = $saveRecord = array();
					$saveRecord["info_id"] = $saveRecord["info_field"] = $saveRecord["info_value"] = "";
					$saveRecord["last_updated_by"] = $currentUser;
					foreach ($entityInfo as $field => $value) {
						if (preg_match("/^spec\_/i", $field)) {
							$infoID = $mdlEntityInfo->getAllEntityInfo("info_id", array("info_field" => $field, "entity_id" => $id));
							$infoIDs[] = $saveRecord["info_id"] = $infoID[0]["info_id"];
							$saveRecord["info_field"] = trim($field);
							$saveRecord["info_value"] = trim($value);
							$entityInfoRecord[] = $saveRecord;
						}
					}
					$this->db->update_batch("entity_info", $entityInfoRecord, "info_id");
					$this->db->set("last_updated_date", "now()", false)->where_in("info_id", $infoIDs)->update("entity_info");
				}
			}
			$this->autoUpdateSpecsTypeStatus($orderInfoID, false);
			$this->autoUpdateOrderInfoStatus($orderInfoID, false);
			//end transaction
			$this->db->trans_complete();
			$result = "You have successfully saved Order Specifications.";
			if (!$this->db->trans_status()) {
				$result = "<p>Unable to continue saving Order Specifications. ";
				$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred while saving your Order Specifications.");
				$result .= "</p>";
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank Order Specifications.</p>";
		}
		
		return $result;
	}
	
	public function deleteOrderSpecifications($arrRecord) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("order_info_id", $arrRecord) && array_key_exists("order_spec_id", $arrRecord)) {
			$id = $arrRecord["order_spec_id"];
			$fKeyID = $arrRecord["order_info_id"];
			$arrRecord["is_active"] = "no";
			$arrRecord["last_updated_by"] = $this->session->userdata("username");
			unset($arrRecord["order_info_id"], $arrRecord["order_spec_id"], $arrRecord["specs_action"]);
			//begin transaction
			$this->db->trans_start();
			$this->db->set($arrRecord)->set("last_updated_date", "now()", false)->where(array("order_spec_id" => $id, "order_info_id" => $fKeyID, "is_active" => "yes"))->update("order_specifications");
			$this->db->set($arrRecord)->set("last_updated_date", "now()", false)->where(array("entity_id" => $id, "is_active" => "yes"))->update("entity_info");
			//end transaction
			$this->db->trans_complete();
			$result = "You have successfully deleted Order Specifications.";
			if (!$this->db->trans_status()) {
				$result = "<p>Unable to continue deleting Order Specifications. ";
				$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred while deleting Order Specifications.");
				$result .= "</p>";
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank Order Specifications.</p>";
		}
		
		return $result;
	}
	
	public function autoDetectStatus($arrStatus, $byStatusID = true) {
		$result = null;
		if (is_array($arrStatus)) {
			$mdlStatus = new StatusModel();
			$statusFlags = $mdlStatus->generateStatusFlags($byStatusID);
			$flag = true;
			foreach ($arrStatus as $status) {
				$flag = $flag && $statusFlags[$status];
				if (!$flag) break;
			}
			if ($byStatusID) $result = ($flag ? "ETDSTATS03" : "ETDSTATS04");
			else $result = ($flag ? "Complete" : "In Progress");
		}
		return $result;
	}
	
	public function autoUpdateSpecsTypeStatus($orderInfoID, $canCommit = true) {
		$result = null;
		if (isset($orderInfoID)) {
			$objQuery = $this->getAllOrderSpecs("order_spec_type, order_spec_id, order_spec_status_id", array("order_info_id" => $orderInfoID, "is_active" => "yes"), null, "order_spec_type, order_spec_id, order_spec_status_id");
			$saveRecord = $arrSpecTypes = array();
			foreach ($objQuery as $query) $arrSpecTypes[$query["order_spec_type"]][$query["order_spec_id"]] = $query["order_spec_status_id"];
			if ($canCommit) $this->db->trans_start();
			$specTypes = array_keys($arrSpecTypes);
			$saveRecord["last_updated_by"] = $this->session->userdata("username");
			foreach ($specTypes as $type) {
				$saveRecord["order_spec_type_status_id"] = $this->autoDetectStatus($arrSpecTypes[$type]);
				$this->db->set($saveRecord)->set("last_updated_date", "now()", false)->where(array("order_info_id" => $orderInfoID, "order_spec_type" => $type, "is_active" => "yes"))->update("order_specifications");
			}
			$result = true;
			if ($canCommit) {
				$this->db->trans_complete();
				$result = $this->db->trans_status();
			}
		}
		return $result;
	}

	public function autoUpdateOrderInfoStatus($orderInfoID, $canCommit = true) {
		$result = null;
		if (isset($orderInfoID)) {
			$objQuery = $this->getAllOrderSpecs("order_spec_type, order_spec_type_status_id", array("order_info_id" => $orderInfoID, "is_active" => "yes"), null, "order_spec_type, order_spec_type_status_id");
			$saveRecord = $arrSpecTypeStatus = array();
			foreach ($objQuery as $query) $arrSpecTypeStatus[] = $query["order_spec_type_status_id"];
			if ($canCommit) $this->db->trans_start();
			$saveRecord["last_updated_by"] = $this->session->userdata("username");
			$saveRecord["status_id"] = $this->autoDetectStatus($arrSpecTypeStatus);
			$this->db->set($saveRecord)->set("last_updated_date", "now()", false)->where(array("order_info_id" => $orderInfoID, "is_active" => "yes"))->update("order_info");
			$result = true;
			if ($canCommit) {
				$this->db->trans_complete();
				$result = $this->db->trans_status();
			}
		}
		return $result;
	}

	public function groupOrderSpecsRecord($arrRecord, $strFieldGroup = "order_spec_type") {
		$result = null;
		if (is_array($arrRecord) && count($arrRecord) && trim($strFieldGroup)) {
			$newRecords = array();
			foreach ($arrRecord as $record) {
				$newRecords[$record[$strFieldGroup]]["status"] = $record[$strFieldGroup ."_status"];
				$newRecords[$record[$strFieldGroup]][] = $record;
			}
			$result = $newRecords;
		}
		return $result;
	}
	
	public function saveNewOrderInfo($arrRecord) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("order_type", $arrRecord) && array_key_exists("client_name", $arrRecord) && array_key_exists("project_id", $arrRecord) && array_key_exists("order_name", $arrRecord) && array_key_exists("delivery_date", $arrRecord) && array_key_exists("advertiser", $arrRecord) && array_key_exists("ad_server", $arrRecord) && array_key_exists("start_date", $arrRecord) && array_key_exists("end_date", $arrRecord)) {

			//begin transaction
			$this->db->trans_start();
			$arrOrderSubmissionInfo = array_slice($arrRecord, 0, 7);
			$clientName = $arrOrderSubmissionInfo["client_name"];
			$arrRecepients = $this->getEmailRecepients($clientName);
			$arrOrderSubmissionInfo["is_email_sent"] = "yes";
			$arrOrderSubmissionInfo["email_sent_date"] = "now()";
			$arrOrderSubmissionInfo["email_sent_to"] = implode(", ", $arrRecepients);
			$result = $this->saveOrderSubmission($arrOrderSubmissionInfo, false, false, true);
			if (preg_match("/^ETD/i", trim($result))) {
				$arrOrderInfo = array_slice($arrRecord, 7);
				$arrOrderInfo["order_submission_id"] = $orderSubmissionID = $result;
				$deliveryDate = $arrOrderSubmissionInfo["delivery_date"];
				$arrOrderInfo["order_name"] = $orderName = $arrOrderSubmissionInfo["order_name"];
				$orderName = (strpos($orderName, " ") !== false ? str_replace(" ", "", ucwords(strtolower($orderName))) : $orderName);
				$clientName = (strpos($clientName, " ") !== false ? str_replace(" ", "", ucwords(strtolower($clientName))) : $clientName);
				$arrOrderInfo["order_code"] = (preg_match("/(trafficking)+/i", $arrOrderSubmissionInfo["order_type"]) ? "Traffick" : "Order") ."_". mdate("%Y-%m", syncDate($deliveryDate)) ."_". $clientName ."_". $orderName ."@". str_pad(intval(str_replace("ETDORDERS", "", $orderSubmissionID)), 5, "0", STR_PAD_LEFT) ."@";
				$result = $this->saveOrderInfo($arrOrderInfo, true, false, false);
				//end transaction
				$this->db->trans_complete();
				if (!$this->db->trans_status() || $result !== true) {
					$strResult = is_string($result) ? $result : "";
					$result = "<p>Unable to continue saving New Order Information. ";
					$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "");
					$result .= "</p>$strResult";
				} else {
					$result = "You have successfully saved New Order Information.";
				}
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank New Order Information.</p>";
		}
		
		return $result;
	}
	
	public function getChecklistCategories() {
		$categories = array(
							"1/ Plan média" =>
								array("chk_liste_des_regies_sites",
									"chk_nom_dimensions_de_chaque_format",
									"chk_date_de_debut_et_date_de_fin_par_format",
									"chk_mode_d_achat_par_format",
									"chk_volume_d_impression_par_format",
									"chk_cout_unitaire_par_format",
									"chk_budget_par_format",
									"chk_type_tag"),
							"2/ Trafficking Template" => 
								array("chk_site_network",
									"chk_nom_de_chaque_package",
									"chk_nom_de_chaque_emplacement",
									"chk_type_de_chaque_emplacement",
									"chk_dates_de_chaque_emplacement",
									"chk_dimensions_de_chaque_emplacement",
									"chk_volume_d_impression_de_chaque_emplacement",
									"chk_mode_d_achat_de_chaque_emplacement",
									"chk_cout_unitaire_de_chaque_emplacement",
									"chk_budget_de_chaque_emplacement",
									"chk_media_buy_channel"),
							"3/ Brief Trafficking" => 
								array("chk_nom_de_la_campagne",
									"chk_nom_de_l_advertiser",
									"chk_choix_de_l_adserver")
		);
		return $categories;
	}
	
	public function getChecklistTranslation() {
		$categoryTranslation = array("chk_liste_des_regies_sites" => "Liste des régies/sites",
			"chk_nom_dimensions_de_chaque_format" => "Nom/dimensions de chaque format",
			"chk_date_de_debut_et_date_de_fin_par_format" => "Date de début et date de fin par format",
			"chk_mode_d_achat_par_format" => "Mode d'achat par format",
			"chk_volume_d_impression_par_format" => "Volume d'impression par format",
			"chk_cout_unitaire_par_format" => "Coût unitaire par format",
			"chk_budget_par_format" => "Budget par format",
			"chk_type_tag" => "Type tag",
			"chk_site_network" => "Site/Network",
			"chk_nom_de_chaque_package" => "Nom de chaque Package",
			"chk_nom_de_chaque_emplacement" => "Nom de chaque emplacement",
			"chk_type_de_chaque_emplacement" => "Type de chaque emplacement",
			"chk_dates_de_chaque_emplacement" => "Dates de chaque emplacement",
			"chk_dimensions_de_chaque_emplacement" => "Dimensions de chaque emplacement",
			"chk_volume_d_impression_de_chaque_emplacement" => "Volume d'impression de chaque emplacement",
			"chk_mode_d_achat_de_chaque_emplacement" => "Mode d'achat de chaque emplacement",
			"chk_cout_unitaire_de_chaque_emplacement" => "Coût unitaire de chaque emplacement ",
			"chk_budget_de_chaque_emplacement" => "Budget de chaque emplacement",
			"chk_media_buy_channel" => "Media buy Channel",
			"chk_nom_de_la_campagne" => "Nom de la campagne",
			"chk_nom_de_l_advertiser" => "Nom de l'Advertiser",
			"chk_choix_de_l_adserver" => "Choix de l'AdServer");
		return $categoryTranslation;
	}

	public function categorizeRecords($records) {
		$checklistCategories = $this->getChecklistCategories();
		$records = json_decode($records, true);
		$return = array();
		foreach ($checklistCategories as $category => $categ) {
			foreach ($categ as $key => $val) {
				$return[$category][$val] = isset($records[$val]) ? $records[$val] : "false";
				$return[$category][$val ."_remarks"] = isset($records[$val ."_remarks"]) ? $records[$val ."_remarks"] : "";
			}
		}
		return $return;
	}
	
	public function saveCampaignChecklist($checklistInfo, $orderInfoID) {
		//begin transaction
		$this->db->trans_start();
		$checklist = $this->getOrderChecklist("*", array("order_info_id" => $orderInfoID));
		$isExists = isset($checklist) && count($checklist);
		$this->db->set("order_checklist_info", json_encode($checklistInfo));
		if (!$isExists) {
			$this->db->set("order_info_id", $orderInfoID)->insert("order_checklist");
		} else {
			$this->db->where("order_info_id", $orderInfoID)->update("order_checklist");
		}
		$this->setCampaignComplete($checklistInfo, $orderInfoID);
		//end transaction
		$this->db->trans_complete();
		if ($this->db->trans_status() && !$isExists) {
			$checklistInfo["order_info_id"] = $orderInfoID;
			$isSent = $this->sendOrderChecklistNotificationToClient($checklistInfo);
			if ($isSent) $this->saveOrderChecklistEmailSent($checklistInfo);
		}
	}
	
	public function setCampaignComplete($checklistInfo, $orderInfoID, $canCommit = false) {
		$allCheck = true;
		foreach ($checklistInfo as $name => $value) {
			if (!preg_match("/(\_remarks)$/i", $name)) {
				$value = ($value == "true");
				$allCheck = $allCheck && $value;
				if (!$allCheck) break;
			}
		}
		//begin transaction
		if ($canCommit) $this->db->trans_start();
		$this->db->set("status_id", ($allCheck ? "ETDSTATS03" : "ETDSTATS04"))->where(array("order_info_id" => $orderInfoID, "is_active" => "yes"))->update("order_info");
		//end transaction
		if ($canCommit) $this->db->trans_complete();
	}

	public function sendOrderChecklistNotificationToClient($arrRecord = null) {
		$result = false;
		$isTrafficking = ($this->session->userdata("client_type") == "trafficking");
		$clientName = $this->session->userdata("clientName");
		$arrAdminEmail = $this->getEmailRecepients($clientName, false);
		$clientInfo = $this->getClientInfo($clientName, $arrRecord["order_info_id"]);
		if (isset($clientInfo["email"]) && isset($clientInfo["project_id"]) && isset($clientInfo["order_name"])) {
			$this->load->model("EyesTVersion2Model");
			$mdlEyesT = new EyesTVersion2Model();
			$project = $mdlEyesT->getAllInfo("code_projet", "projet", array("id_projet" => $clientInfo["project_id"]), null, null, 1);
			$pageURL = "orders/campaigns/checklist/". $arrRecord["order_info_id"];
			$clientName = (preg_match("/(\_)+/i", $clientName) ? str_replace("_", " ", $clientName) : $clientName);
			$projectName = $project[0]["code_projet"];
			$underscorePosition = strpos($projectName, "_");
			if ($underscorePosition === false) $underscorePosition = strlen($projectName);
			$projectName = substr($projectName, 0, $underscorePosition);
			$orderName = $originalOrderName = $clientInfo["order_name"];
			if ($isTrafficking) $orderName = "Campaign";
			$clientAdminName = "$projectName Team";
			$adminEmail = $arrAdminEmail[0];
			$clientEmail = $clientInfo["email"];
			$emailTitle = "$orderName Creation Notice - $originalOrderName";
			$orderLabel = ($isTrafficking ? "campaign" : "order");
			//setting information for sending email feature
			$this->initializeEmail();
			$this->email->clear();
			$this->email->from($adminEmail, $clientAdminName);
			$this->email->to($clientEmail);
			$this->email->subject($emailTitle);
			$email["messageSubjectTitle"] = $emailTitle;
			$email["messageAddressee"] = "Hello $clientName, ";
			$email["messagePreview"] = $messagePreview = "We are glad to inform you regarding the recently submitted $orderLabel <strong><u>$originalOrderName</u></strong> for <em>$projectName</em>, is now on validation and pre-production procedures.";
			$messageContent = "$messagePreview<p>You may give feedback or discuss the details of your submitted $orderLabel using this button.</p>";
			$email["messageContent"] = $messageContent;
			$email["messageLinkURL"] = site_url($pageURL);
			$email["messageLinkLabel"] = "$orderName Checklist";
			$email["messageClosingRemarks"] = "Once all the details of your submitted $orderLabel has been validated and made ready for production, your $orderLabel will immediately dispatch to our ". ($isTrafficking == "trafficking" ? "trafficking " : "") ."system. <br /><p>Should there be any questions or clarifications, you may contact us at $adminEmail or use the button above.</p>";
			$email["messageSignature"] = "Best regards, <br /><br />$clientAdminName";
			$email["footerLinkURL"] = site_url("dashboard");
			$email["footerLinkLabel"] = "Client Dashboard Interface";
			$emailMessage = $this->load->view("emails/vwEmailTemplate", $email, true);
			$this->email->message($emailMessage);
			//send email
			$result = $this->email->send(false);
		}
		return $result;
	}

	public function getClientInfo($clientName, $orderInfoID) {
		/*$arrClient = $this->db->query("SELECT us.email, (SELECT order_name FROM public.order_submission WHERE order_submission_id = oi.order_submission_id) order_name, acu.project_id FROM public.user us, public.user_role usr, public.assoc_client_project_user acu, public.client c, public.order_info oi WHERE us.user_role_id = usr.user_role_id AND us.user_id = acu.user_id AND acu.client_id = c.client_id AND us.is_active = 'yes' AND c.is_active = 'yes' AND acu.is_active = 'yes' AND oi.order_info_id = '$orderInfoID' AND usr.user_role_name = 'Client' AND c.client_name = '$clientName'")->result_array();*/
		$arrClient = $this->db->query("SELECT us.email, oi.order_info_id, os.order_name, acu.project_id FROM public.user us, public.user_role usr, public.assoc_client_project_user acu, public.client c, public.order_info oi, public.order_submission os WHERE us.user_role_id = usr.user_role_id AND us.user_id = acu.user_id AND acu.client_id = c.client_id AND us.is_active = 'yes' AND c.is_active = 'yes' AND acu.is_active = 'yes' AND oi.order_submission_id = os.order_submission_id AND os.ordered_by = us.username AND oi.order_info_id = '$orderInfoID' AND usr.user_role_name = 'Client' AND c.client_name = '$clientName'")->result_array();
		$result = isset($arrClient[0]) ? $arrClient[0] : null;
		return $result;
	}

	public function saveOrderChecklistEmailSent($record) {
		$result = false;
		$orderInfoID = $record["order_info_id"];
		$checklist = $this->getOrderChecklist("order_checklist_info", array("order_info_id" => $orderInfoID));
		$isExists = isset($checklist) && count($checklist);
		if ($isExists) {
			//begin transaction
			$this->db->trans_start();
			$this->db->set("email_sent_date", "now()", false);
			$this->db->set("is_email_sent", "yes")->where("order_info_id", $orderInfoID)->update("order_checklist");
			//end transaction
			$this->db->trans_complete();
			$result = $this->db->trans_status();
		}
		return $result;
	}
}
?>