<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Entity Info Model of Client Dashboard Admin Interface
 *
 */
class EntityInfoModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getAllEntityInfo($fields = "*", $filters = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = " 1 = 1";
		
		$this->db->select($strFields)->from("entity_info")->where($strFilters);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function generateEntityInfoID($strPrefix = "ETD", $entity = "user") {
		$result = null;
		
		$entity = strtolower($entity);
		switch ($entity) {
			case "client":
				$entityCode = "CLNT";
				break;
			case "project":
				$entityCode = "PROJC";
				break;
			case "user role":
				$entityCode = "USROLE";
				break;
			case "entity status":
				$entityCode = "STATS";
				break;
			case "order submission":
				$entityCode = "ORDERS";
				break;
			case "order type":
				$entityCode = "ORDTYP";
				break;
			case "order specs":
				$entityCode = "SPEC";
				break;
			default:
				$entityCode = strtoupper($entity);
		}
		if ($entity !== "order specs") $entityCode .= "INFO";
		$strFilters = "info_id LIKE '$strPrefix$entityCode%'";
		$arrNumRecords = $this->db->select("COUNT(info_id) AS num_records")->from("entity_info")->where($strFilters)->get()->result_array();
		$strInfoID = $arrNumRecords[0]["num_records"] + 1;
		$padLength = 3;
		if (preg_match("/^(user|client)/i", $entity)) $padLength = 4;
		if ($entity == "order specs") $padLength = 16;
		$strSequence = str_pad($strInfoID, $padLength, "0", STR_PAD_LEFT);
		$result = $strPrefix . $entityCode . $strSequence;
		
		return $result;
	}

}
?>