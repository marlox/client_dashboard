<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Contact Message Model of Client Dashboard Admin Interface
 *
 */
class ContactModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->library("email");
	}
	
	public function initializeEmail() {
		$arrConfig = array(
								"protocol" => "smtp",
								"smtp_host" => "smtp.mailgun.org",
								"smtp_port" => "2525",
								"smtp_timeout" => "7",
								"smtp_user" => "postmaster@mg.ingedata.net",
								"smtp_pass" => "05987515b6a9718e5ac6f729dacd80e1",
								"newline" => "\r\n",
								"mailtype" => "html",
								"validate" => true
								);
		$this->email->initialize($arrConfig);
	}
	
	public function getMessageTypes() {
		$messageTypes = array(
								/*"Request Order", 
								"Order Update", */
								"Bug/Issue", 
								"Other Concerns"
								);
		return $messageTypes;
	}
	
	public function getMessageTypesIcon() {
		$messageTypesIcon = array(
								"Request Order" => 'ticket', 
								"Order Update" => 'edit', 
								"Bug/Issue" => 'bug', 
								"Other Concerns" => 'envelope', 
								"Forgot Password" => 'eye-slash'
								);
		return $messageTypesIcon;
	}

	public function getAllContactMessageInfo($fields = "*", $strFilters = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$this->db->select($strFields)->from("contact_message");
		$filters = ((is_string($strFilters) && !trim($strFilters)) || is_null($strFilters) || (is_array($strFilters) && !count($strFilters)) ? " 1 = 1" : $strFilters) . ($this->session->userdata("user_type") !== "Super Administrator" ? " AND email_sent_to LIKE '%". $this->session->userdata("email") ."%'" : "");
		if ($filters) $this->db->where($filters);
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function getEmailRecepients($client) {
		$result = null;
		$arrClients = array();
		if (is_string($client)) $arrClients[] = $client;
		
		$arrSuperAdmin = $this->db->query("SELECT us.username, us.email FROM public.user us, public.user_role usr WHERE us.user_role_id = usr.user_role_id AND us.is_active = 'yes' AND usr.user_role_name = 'Super Administrator'")->result_array();
		if (isset($arrSuperAdmin) && count($arrSuperAdmin)) {
			$result = array();
			foreach ($arrSuperAdmin as $superAdmin) $result[] = $superAdmin["email"];
		}
		$arrClientAdmin = $this->db->query("SELECT us.username, us.email FROM public.user us, public.user_role usr, public.assoc_client_project_user acu, public.client c WHERE us.user_role_id = usr.user_role_id AND us.user_id = acu.user_id AND acu.client_id = c.client_id AND us.is_active = 'yes' AND c.is_active = 'yes' AND acu.is_active = 'yes' AND usr.user_role_name = 'Administrator' AND c.client_name IN ('". implode("', '", $arrClients) ."') GROUP BY us.username, us.email")->result_array();
		if (isset($arrClientAdmin) && count($arrClientAdmin)) {
			if (!$result) $result = array();
			foreach ($arrClientAdmin as $admin) {
				if (array_search($admin["email"], $result) === false) $result[] = $admin["email"];
			}
		}
		
		return $result;
	}
	
	public function getNumberOfMessages() {
		$arrQuery = $this->db->query("SELECT COUNT(contact_message_id) num_records FROM public.contact_message". ($this->session->userdata("user_type") == "Super Administrator" ? "" : " WHERE is_email_sent = 'yes' AND email_sent_to LIKE '%". $this->session->userdata("email") ."%'"))->result_array(); //TO-DO: Also add email_to field in contact_message in order to consider w/c only messages will be shown to client's admin
		$count = $arrQuery[0]["num_records"];
		return $count;
	}
	
	public function generateContactMessageID() {
		$arrResult = $this->db->select("nextval('contact_message_seq') AS next_seq_num")->get()->result_array();
		$strNextVal = $arrResult[0]["next_seq_num"];
		$strSequence = str_pad($strNextVal, 12, "0", STR_PAD_LEFT);
		$generatedID = "ETDCNTCT$strSequence";
		return $generatedID;
	}
	
	public function saveContactForm($arrRecord) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("contact_name", $arrRecord) && array_key_exists("message_type", $arrRecord) && array_key_exists("email", $arrRecord) && array_key_exists("message", $arrRecord)) {
			//prepare information of order table
			$arrRecord["created_by"] = $this->session->userdata("username");
			$arrRecord["last_updated_by"] = $this->session->userdata("username");
			$arrRecord["contact_message_id"] = $this->generateContactMessageID();
			//begin transaction
			$this->db->trans_begin();
			//save information in order table
			$this->db->insert("contact_message", $arrRecord);
			$tempResult = $this->db->trans_status();
			if (!$tempResult) {
				if (($dbError = $this->db->get_last_error($this->db->error))) $result = $dbError;
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
				$result = "You have successfully saved Contact Information.";
				$tempResult = $this->sendContactMessage($arrRecord);
				$result = (preg_match("/(success)+/i", $tempResult) ? "$result $tempResult" : "$result</strong></p> <p>$tempResult</p>");
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank Contact Information.</p>";
		}
		
		return $result;
	}
	
	public function sendContactMessage($arrRecord) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("contact_name", $arrRecord) && array_key_exists("message_type", $arrRecord) && array_key_exists("email", $arrRecord) && array_key_exists("message", $arrRecord)) {
			$clientName = $this->session->userdata("clientName");
			$clientName = (preg_match("/(\_)+/i", $clientName) ? str_replace("_", " ", $clientName) : $clientName);
			$arrRecepients = $this->getEmailRecepients($clientName);
			//prepare information for sending email feature
			$emailTitle = $arrRecord["message_type"] ." from ". $arrRecord["contact_name"];
			$this->initializeEmail();
			$this->email->clear();
			$this->email->from($arrRecord["email"], $arrRecord["contact_name"]);
			$this->email->to($arrRecepients);

			$this->email->subject($emailTitle);

			$email = array();
			$email["messageSubjectTitle"] = $emailTitle;
			$email["messageAddressee"] = "Hello Client Dashboard Admin, ";
			$email["messagePreview"] = $messagePreview = "A $emailTitle has been sent to you";
			$email["messageContent"] = "$messagePreview, with the following additional message:<ul><li>Order Specifications - <em>". $arrRecord["message"] ."</em></li></ul>";
			$email["messageLinkURL"] = site_url("admin/messages/view");
			$email["messageLinkLabel"] = "View Messages";
			$email["messageClosingRemarks"] = "";
			$email["messageSignature"] = "";
			$email["footerLinkURL"] = site_url("admin/dashboard");
			$email["footerLinkLabel"] = "Eyes-T Dashboard - Admin Interface";
			$emailMessage = $this->load->view("emails/vwEmailTemplate", $email, true);
			$this->email->message($emailMessage);
			$tempResult = $this->email->send(false);
			$result = "<p>Unable to send your Contact Message.</p>";
			if ($tempResult) {
				//prepare contact message information table for sending email feature
				$contactMessageID = $arrRecord["contact_message_id"];
				//begin transaction
				$this->db->trans_begin();
				//update contact message information in contact_message table for sending email feature
				$this->db->set("is_email_sent", "yes");
				$this->db->set("email_sent_date", "now()", false);
				$this->db->set("email_sent_to", implode(", ", $arrRecepients));
				$this->db->set("last_updated_date", "now()", false);
				$this->db->set("last_updated_by", $this->session->userdata("username"));
				$this->db->where("contact_message_id", $contactMessageID);
				$this->db->update("contact_message");
				$tempResult = $tempResult && $this->db->trans_status();
				if (!$tempResult) {
					$result = "<p>Unable to continue saving your Contact Message email sending information. Unexpected error occurred while saving contact message email sending information.</p>";
					if (($dbError = $this->db->get_last_error($this->db->error))) $result = "<p>Unable to continue saving your Contact Message email sending information. ". $dbError ."</p>";
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
					$result = "You also successfully sent your Contact Message.";
					$this->sendAcknowledgementToClient($arrRecord);
				}
			} else {
				$result .= "<p>". $this->email->print_debugger(array("headers")) ."</p>";
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank Contact Message Information.</p>";
		}
		
		return $result;
	}

	public function sendAcknowledgementToClient($arrRecord = null) {
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("contact_name", $arrRecord) && array_key_exists("message_type", $arrRecord) && array_key_exists("email", $arrRecord) && array_key_exists("message", $arrRecord)) {
			$clientName = $arrRecord["contact_name"];
			$clientEmail = $arrRecord["email"];
			$msgType = $arrRecord["message_type"];
			$emailTitle = "$msgType Acknowledgement";
			$clientAdmin = 'Client Dashboard Admin';
			//setting information for sending email feature
			$this->initializeEmail();
			$this->email->clear();
			$this->email->from('noreply@mg.ingedata.net', $clientAdmin);
			$this->email->to($clientEmail);
			$this->email->subject($emailTitle);
			$email = array();
			$email["messageSubjectTitle"] = $emailTitle;
			$email["messageAddressee"] = "Hi, $clientName!";
			$email["messagePreview"] = $messagePreview = "Sending you this email to acknowledge your submitted $msgType.";
			$email["messageContent"] = "$messagePreview Our $clientAdmin will immediately check the details of your submitted $msgType the soonest possible time.";
			$email["messageLinkURL"] = null;
			$email["messageLinkLabel"] = null;
			$email["messageClosingRemarks"] = "Please expect an email about the next step(s) to do within 24 hours.";
			$email["messageSignature"] = "Best regards, <br /><br />$clientAdmin";
			$email["footerLinkURL"] = site_url("dashboard");
			$email["footerLinkLabel"] = "Client Dashboard Interface";
			$emailMessage = $this->load->view("emails/vwEmailTemplate", $email, true);
			$this->email->message($emailMessage);
			//send email
			$this->email->send(false);
		}
	}
	
	public function sendMessage($arrRecord) {
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("subject", $arrRecord) && array_key_exists("to_name", $arrRecord) && array_key_exists("email", $arrRecord) && array_key_exists("message", $arrRecord)) {
			$name = $arrRecord["to_name"];
			$subject = $arrRecord["subject"];
			$message = $arrRecord["message"];
			$email = $arrRecord["email"];
			$closingRemarks = $arrRecord["closing_remarks"];
			$messageSignature = $arrRecord["message_signature"];
			$clientAdmin = 'Client Dashboard Admin';
			$isAdmin = $this->session->userdata("user_type") == 'Super Administrator';
			//setting information for sending email feature
			$this->initializeEmail();
			$this->email->clear();
			$this->email->from($isAdmin ? 'noreply@mg.ingedata.net' : $this->session->userdata('email'), $isAdmin ? $clientAdmin : $this->session->userdata('username'));
			$this->email->to($email);
			$this->email->bcc($this->session->userdata('email'));
			$this->email->subject($subject);
			$email = array();
			$email["messageSubjectTitle"] = $subject;
			$email["messageAddressee"] = "Hi, $name!";
			$email["messagePreview"] = strlen($message) > 20 ? substr($message, 0, 20) ."..." : $message;
			$email["messageContent"] = $message;
			$email["messageLinkURL"] = site_url('contact/index');
			$email["messageLinkLabel"] = 'Contact Us';
			$email["messageClosingRemarks"] = trim($closingRemarks) ? $closingRemarks : "Should there be any questions or other concerns, don't hesitate to contact us using the above button. Thank you!";
			if (!$isAdmin) $email["messageClosingRemarks"] = $closingRemarks;
			$email["messageSignature"] = trim($messageSignature) ? $messageSignature : "Best regards, <br /><br />$clientAdmin";
			if (!$isAdmin) $email["messageSignature"] = $messageSignature;
			$email["footerLinkURL"] = site_url("dashboard");
			$email["footerLinkLabel"] = "Client Dashboard Interface";
			$emailMessage = $this->load->view("emails/vwEmailTemplate", $email, true);
			$this->email->message($emailMessage);
			$tempResult = $this->email->send(false);
			$result = "<p>Unable to send your Message.</p>";
			if ($tempResult) {
				$result = "You successfully sent your Message.";
			} else {
				$result .= "<p>". $this->email->print_debugger(array("headers")) ."</p>";
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank Message Information.</p>";
		}
		
		return $result;
	}

}
?>