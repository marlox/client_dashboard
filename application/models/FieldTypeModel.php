<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Field Type Model of Client Dashboard Admin Interface
 *
 */
class FieldTypeModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getFieldTypeInfo($fields = "*", $filters = null) {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = " 1 = 1";
		
		$result = $this->db->select($strFields)->from("field_type")->where($strFilters)->get()->result_array();
		
		return $result;
	}

}
?>