<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Comments Model of Eyes-T Dashboard
 *
 */
class CommentsModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->library("email");
	}
	
	public function initializeEmail() {
		$arrConfig = array(
								"protocol" => "smtp",
								"smtp_host" => "smtp.mailgun.org",
								"smtp_port" => "2525",
								"smtp_timeout" => "7",
								"smtp_user" => "postmaster@mg.ingedata.net",
								"smtp_pass" => "05987515b6a9718e5ac6f729dacd80e1",
								"newline" => "\r\n",
								"mailtype" => "html",
								"validate" => true
								);
		$this->email->initialize($arrConfig);
	}
	
	public function getAllComments($fields = "*", $strFilters = null, $order = null, $limit = null, $offset = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$this->db->select($strFields)->from("user_comments_vw");
		$filters = ((is_string($strFilters) && !$strFilters) || is_null($strFilters) || (is_array($strFilters) && !count($strFilters)) ? " 1 = 1" : $strFilters);
		
		if ($filters) {
			if (is_array($filters)) {
				foreach ($filters as $key => $value) {
					if (strpos($key, "IN") !== false && is_array($value)) $this->db->where_in(trim(str_replace("IN", "", $key)), $value);
					elseif (preg_match("/(\_at)+/i", $key)) $this->db->where($key, $value, false);
					else $this->db->where(array($key => $value));
				}
			} else {
				$this->db->where($filters);
			}
		}
		if ($order) $this->db->order_by($order);
		if ($limit && is_numeric($limit)) $this->db->limit($limit);
		if ($offset && is_numeric($offset)) $this->db->offset($offset);
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}

	public function saveComment($arrRecord, $canCommit = true, $sendEmail = true, $returnID = false) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("comment", $arrRecord) && array_key_exists("page_url", $arrRecord)) {
			//prepare information of comments table
			$arrRecord["user_id"] = $this->session->userdata("id");
			//begin transaction
			if ($canCommit) $this->db->trans_start();
			//save information
			$this->db->set($arrRecord);
			if ($sendEmail) $tempResult = $this->sendCommentNotification($arrRecord, false);
			if ($canCommit) {
				$this->db->insert("comment");
				//end transaction
				$this->db->trans_complete();
				if (!$this->db->trans_status()) {
					$result = "<p>Unable to continue saving Comment. ";
					$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred while saving Coment information.");
					$result .= "</p>";
				} else {
					$result = "You have successfully left a comment.";
					$result = (preg_match("/(success)+/i", $tempResult) ? "$result $tempResult" : "$result</strong></p> <p>$tempResult</p>");
				}
			} else {
				$result = $tempResult;
			}
			if ($returnID && preg_match("/(success)+/i", $result)) $result = $this->db->insert_id("comment_id_seq");
		} else {
			$result = "<p>Unable to continue due to invalid or blank Comment Information.</p>";
		}
		
		return $result;
	}
	
	public function getEmailRecepients($emailOnly = true) {
		$result = null;
		$isAdmin = $this->session->userdata("is_admin_login") || !$this->session->userdata("is_client_login");

		$arr = $this->db->query("SELECT DISTINCT ". ($emailOnly ? "" : "us.username, ") ."us.email FROM public.user us, public.user_role usr, public.assoc_client_project_user acu, public.client c WHERE us.user_role_id = usr.user_role_id AND us.user_id = acu.user_id AND acu.client_id = c.client_id AND us.is_active = 'yes' AND c.is_active = 'yes' AND acu.is_active = 'yes' AND c.client_name = '{$this->session->userdata("clientName")}' AND us.user_id != '{$this->session->userdata("id")}'". ($isAdmin ? "" : " AND usr.user_role_name = 'Administrator'"))->result_array();
		if (isset($arr) && count($arr)) {
			$result = array();
			foreach ($arr as $rec) {
				if ($emailOnly) {
					if (array_search($rec["email"], $result) === false) $result[] = $rec["email"];
				} else {
					$result[] = array("email" => $rec["email"], "username" => $rec["username"]);
				}
			}
		}
		
		return $result;
	}
	
	public function sendCommentNotification($arrRecord = null, $canCommit = true, $commentID = null) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("comment", $arrRecord) && array_key_exists("page_url", $arrRecord)) {
			$this->load->model("OrdersModel", "orders");
			$currentUser = ucwords(str_replace(".", " ", $this->session->userdata("username")));
			$currentUserEmail = $this->session->userdata("email");
			$emailRecepients = $this->getEmailRecepients();
			$clientName = $this->session->userdata("clientName");
			$emailTitle = "Feedback from $currentUser of $clientName";
			$isAdmin = $this->session->userdata("is_admin_login") || !$this->session->userdata("is_client_login");
			$isTrafficking = ($this->session->userdata("client_type") == "trafficking");
			$pageURL = $arrRecord["page_url"];
			$orderInfoID = substr($pageURL, strrpos($pageURL, "/") + 1);
			$clientInfo = $this->orders->getClientInfo($clientName, $orderInfoID);
			$originalOrderName = $clientInfo["order_name"];
			if ($isAdmin) $emailTitle = ($isTrafficking ? "Campaign" : "Order") ." Checklist Comment - $originalOrderName";
			//setting information for sending notification thrue email
			$this->initializeEmail();
			$this->email->from($currentUserEmail, $currentUser);
			$this->email->to($emailRecepients);
			$this->email->subject($emailTitle);
			$record = $arrRecord;
			$record["emailTitle"] = $emailTitle;
			$emailMessage = $this->prepareEmailMessage($record);
			$this->email->message($emailMessage);
			//send email
			$tempResult = $this->email->send(false);
			$result = "<p>Unable to send notification thru email.</p>";
			if ($tempResult) {
				$arrRecord["notification_sent_to"] = implode(", ", $emailRecepients);
				//begin transaction
				if ($canCommit) $this->db->trans_start();
				$this->db->set($arrRecord);
				$this->db->set("notification_sent_at", "now()", false);
				if ($canCommit) $this->db->where("comment_id", $commentID)->update("comment");
				//end transaction
				if ($canCommit) {
					$this->db->trans_complete();
					$tempResult = $tempResult && $this->db->trans_status();
				}
				if (!$tempResult) {
					$result = "<p>Unable to continue sending notification thru email. Unexpected error occurred while saving comment information.</p>";
					if (($dbError = $this->db->get_last_error($this->db->error))) $result = "<p>Unable to continue sending notification thru email. ". $dbError ."</p>";
				} else {
					$result = "You also successfully sent notification thru email.";
				}
			} else {
				$result .= "<p>". $this->email->print_debugger(array("headers")) ."</p>";
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank Comment Information.</p>";
		}
		
		return $result;
	}
	
	public function deleteComment($arrRecord) {
		$result = false;
		
		$arrFields = array_keys($arrRecord);
		$arrValues = array_values($arrRecord);
		if (isset($arrRecord, $arrFields, $arrValues) && array_key_exists("comment_id", $arrRecord)) {
			$id = $arrRecord["comment_id"];
			$record["is_active"] = "no";
			unset($arrRecord);
			//begin transaction
			$this->db->trans_start();
			$this->db->set($record)->where(array("comment_id" => $id, "is_active" => "yes"))->update("comment");
			//end transaction
			$this->db->trans_complete();
			$result = "You have successfully deleted the comment.";
			if (!$this->db->trans_status()) {
				$result = "<p>Unable to continue deleting the comment. ";
				$result .= (($dbError = $this->db->get_last_error($this->db->error)) ? "<strong><p>". $dbError ."</p></strong>" : "Unexpected error occurred while deleting the comment.");
				$result .= "</p>";
			}
		} else {
			$result = "<p>Unable to continue due to invalid or blank Comment information.</p>";
		}
		
		return $result;
	}

	public function prepareEmailMessage($info) {
		$this->load->model("OrdersModel", "orders");
		$isAdmin = $this->session->userdata("is_admin_login") || !$this->session->userdata("is_client_login");
		$clientName = $this->session->userdata("clientName");
		$pageURL = $info["page_url"];
		$isTrafficking = ($this->session->userdata("client_type") == "trafficking");
		$orderInfoID = substr($pageURL, strrpos($pageURL, "/") + 1);
		$orderLabel = ($isTrafficking ? "campaign" : "order");
		$clientInfo = $this->orders->getClientInfo($clientName, $orderInfoID);
		$this->load->model("EyesTVersion2Model", "eyest");
		$project = $this->eyest->getAllInfo("code_projet", "projet", array("id_projet" => $clientInfo["project_id"]), null, null, 1);
		$recepient = $this->getEmailRecepients(false);
		$projectName = $project[0]["code_projet"];
		$underscorePosition = strpos($projectName, "_");
		if ($underscorePosition === false) $underscorePosition = strlen($projectName);
		$projectName = substr($projectName, 0, $underscorePosition);
		$orderName = $originalOrderName = $clientInfo["order_name"];
		if ($isTrafficking) $orderName = "Campaign";
		$clientAdminName = "$projectName Team";
		$contactEmail = $this->session->userdata("email");
		$emailTitle = $info["emailTitle"];
		$comment = $info["comment"];
		$addressee = $isAdmin ? ucwords(str_replace(".", " ", $recepient[0]["username"])) : $clientAdminName;
		$email["messageSubjectTitle"] = $emailTitle;
		$email["messageAddressee"] = "Hello $addressee, ";
		$email["messagePreview"] = $messagePreview = "Sending you this email regarding the status of the $orderLabel <strong><u>$originalOrderName</u></strong>.";
		$messageContent = "$messagePreview The client <u>$clientName</u> send the following feedback to help you finalize your $orderLabel validation and pre-production procedures.<ul><li><strong>$comment</strong></li></ul>";
		if ($isAdmin) $messageContent = "$messagePreview Our $clientAdminName has the following comment that needs your feedback to finalize the validation and pre-production procedures.<ul><li><strong>$comment</strong></li></ul>";
		$email["messageContent"] = $messageContent;
		$email["messageLinkURL"] = site_url($pageURL);
		$email["messageLinkLabel"] = "$orderName Checklist";
		$email["messageClosingRemarks"] = "<em><strong>NOTE: You may use the button above to see the client's feedback and/or the link at the footer section below for you to manage the $orderLabel checklist.</strong></em>";
		if ($isAdmin) $email["messageClosingRemarks"] = "Should there be any questions or clarifications, you may still contact us at $contactEmail or use the button above.";
		$email["messageSignature"] = "";
		if ($isAdmin) $email["messageSignature"] = "Best regards, <br /><br />$clientAdminName";
		$email["footerLinkURL"] = site_url("admin/orders/view");
		$email["footerLinkLabel"] = "Client Dashboard - Admin Interface";
		if ($isAdmin) {
			$email["footerLinkURL"] = site_url("dashboard");
			$email["footerLinkLabel"] = "Client Dashboard Interface";
		}
		$emailMessage = $this->load->view("emails/vwEmailTemplate", $email, true);
		return $emailMessage;
	}

}
?>