<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Statuses Model of Client Dashboard Admin Interface
 *
 */
class StatusModel extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getAllStatusInfo($fields = "*", $filters = null, $return = "array") {
		$result = null;
		
		$strFields = $fields;
		if (is_array($fields)) $strFields = implode(", ", $fields);
		
		$strFilters = $filters;
		if (is_null($filters) || !$filters) $strFilters = " 1 = 1";
		
		$this->db->select($strFields)->from("entity_status")->where($strFilters)->order_by("status_name");
		
		$tempResult = $this->db->get();
		if ($tempResult->num_rows()) {
			$result = ($return == "array" ? $tempResult->result_array() : $tempResult->result_object());
		}
		
		return $result;
	}
	
	public function generateStatusFlags($byStatusID = true) {
		$result = array();
		$arrStatusComplete = array("Complete", "Ok", "Available", "Not Applicable");
		$objQuery = $this->getAllStatusInfo("status_id, status_name", "is_active = 'yes'");
		foreach ($objQuery as $query) {
			//$flag = (array_search($query["status_name"], $arrStatusComplete) !== false ? "Complete" : "In Progress");
			//$flag = (array_search($query["status_name"], $arrStatusComplete) !== false ? "ETDSTATS03" : "ETDSTATS04");
			$flag = (array_search($query["status_name"], $arrStatusComplete) !== false ? true : false);
			$result[$query[($byStatusID ? "status_id" : "status_name")]] = $flag;
		}
		
		return $result;
	}

}
?>