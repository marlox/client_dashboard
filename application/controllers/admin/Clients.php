<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Clients Controller of Client Dashboard Admin Interface
 *
 */
class Clients extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_admin_login") !== true) redirect("admin/home");
		$this->load->model(array("ClientsModel", "UsersModel", "ThemesModel", "EyesTVersion2Model"));
		$this->load->library(array("form_validation"));
		$this->load->helper(array("date", "datetime", "form", "url"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
	}
	
	public function validateClientAccountForm($negateField = array()) {
		if (is_string($negateField)) $negateField = explode(", ", $negateField);
		if (array_search("client_name", $negateField) === false) $this->form_validation->set_rules("client_name", "Client Name", "required", array("required" => "You must provide a <u>%s</u>."));
		if (array_search("theme_id", $negateField) === false) $this->form_validation->set_rules("theme_id", "Theme", "required", array("required" => "You must provide a <u>%s</u>."));
		if (array_search("project_id[]", $negateField) === false) $this->form_validation->set_rules("project_id[]", "Project", "required", array("required" => "You must provide at least one <u>%s</u>."));
		if (array_search("users[]", $negateField) === false) $this->form_validation->set_rules("users[]", "User", "required", array("required" => "You must provide at least one <u>%s</u>."));
		return $this->form_validation->run();
	}

	public function index() {
		$arr["page"] = !$this->uri->segment(2) ? "dashboard" : $this->uri->segment(2);
		$arr["isNew"] = !$this->uri->segment(3);
		$dbEyesT = new EyestVersion2Model();
		$myModel = new UsersModel();
		$arr["users"] = $myModel->getAllUsersInfo("*", "us.is_active = 'yes'");
		$myModel = new ThemesModel();
		$arr["themes"] = $myModel->getAllThemesInfo("theme_id, theme_name, css_file", "is_active = 'yes'");
		$arr["projects"] = $dbEyesT->getAllProjects();
		
		$success = $this->validateClientAccountForm();
		$arr["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			$mdlClient = new ClientsModel();
			$success = $mdlClient->saveClientAccountInfo($record);
			if (!preg_match("/(success)+/i", $success)) {
				$arr["errors"] = $success;
				$success = null;
			} else {
				$upload["client_name"] = $record["client_name"];
				$isUploaded = $mdlClient->saveClientInfo($upload);
				if ($isUploaded === true) $success .= " You also successfully uploaded ". $upload["client_name"] ." logo.";
				else $arr["errors"] = $isUploaded;
			}
		}
		$arr["success"] = $success;
		
		$this->load->view("admin/vwHeader", $arr);
		$this->load->view("admin/vwNewClientAccount", $arr);
		$this->load->view("admin/vwFooter", $arr);
	}

	public function view() {
		$arr["page"] = !$this->uri->segment(2) ? "dashboard" : $this->uri->segment(2);
		$arr["isNew"] = !$this->uri->segment(3);
		$pageNum = $this->uri->segment(4);
		$mdlClient = new ClientsModel();
		$arr["total"] = $total = ($this->session->userdata("user_type") === "Super Administrator" ? $mdlClient->getNumberOfClientAccounts() : $mdlClient->getNumberOfClients());
		$arr["limit"] = $limit = 20;
		$arr["totalPages"] = $totalPages = ceil(($total / $limit));
		$offset = 0;
		if ($pageNum && !is_numeric($pageNum)) $pageNum = $totalPages;
		if ($pageNum && is_numeric($pageNum)) $offset = ($pageNum - 1) * $limit;
		$arr["offset"] = $offset;
		$arr["pageNum"] = $pageNum;
		$clientAccounts = $mdlClient->getClientAccountInfo(null, null, null, $limit, $offset);
		if ($this->session->userdata("user_type") !== "Super Administrator") {
			$currentClient = "";
			$clientProjects = $clientUsers = $clientAccount = array();
			foreach ($clientAccounts as $record) {
				if ($currentClient !== $record["client_name"]) $clientAccount[] = $record;
				$currentClient = $record["client_name"];
				if (!isset($clientProjects[$currentClient]) || array_search($record["project_id"], $clientProjects[$currentClient]) === false) $clientProjects[$currentClient][] = $record["project_id"];
			}
			foreach ($clientAccount as $key => $record) {
				$clientAccount[$key]["project_id"] = $clientProjects[$record["client_name"]];
				/*$clientUsers = $mdlClient->getClientAccountInfo("full_name ")
				$clientAccount[$key]["full_name"] = $clientUsers;*/
			}
			$clientAccounts = $clientAccount;
		}
		
		$myModel = new EyestVersion2Model();
		$arrProjects = $myModel->getAllProjects();
		$projects = array();
		foreach ($arrProjects as $project) {
			if (array_search($project["project_name"], $projects) === false && array_key_exists($project["project_id"], $projects) === false) $projects[$project["project_id"]] = $project["project_name"];
		}
		$arr["clientAccounts"] = $clientAccounts;
		$arr["projects"] = $projects;

		$this->load->view("admin/vwHeader", $arr);
		$this->load->view("admin/vwManageClientAccounts", $arr);
		$this->load->view("admin/vwFooter", $arr);
	}
	
	public function edit() {
		$id = $this->uri->segment(4);
		$mdlClient = new ClientsModel();
		$dbEyesT = new EyestVersion2Model();
		$myModel = new UsersModel();
		$arr["users"] = $myModel->getAllUsersInfo("*", "us.is_active = 'yes'");
		$myModel = new ThemesModel();
		$arr["themes"] = $myModel->getAllThemesInfo("theme_id, theme_name, css_file", "is_active = 'yes'");
		$arr["projects"] = $dbEyesT->getAllProjects();
		
		$clientAccountInfo = $mdlClient->getClientAccountInfo(null, "AND c.client_id = '$id'");
		if (isset($clientAccountInfo) && count($clientAccountInfo)) {
			$record = $clientAccountInfo[0];
			$arr["client_id"] = $id;
			$arr["client_name"] = $record["client_name"];
			$arr["theme_id"] = $record["theme_id"];
			$arr["logo"] = $record["logo"];
			$clientProjects = $clientUsers = array();
			foreach ($clientAccountInfo as $record) {
				if (array_search($record["project_id"], $clientProjects) === false) $clientProjects[] = $record["project_id"];
			}
			$clientAccountUsers = $mdlClient->getClientProjectsAndUsers($id);
			$clientUsers = $clientAccountUsers["users"];
			$arr["clientProjects"] = $clientProjects;
			$arr["clientUsers"] = $clientUsers;
		}

		$success = $this->validateClientAccountForm(array("client_name"));
		$arr["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			$success = $mdlClient->saveEditClientAccountInfo($record);
			$isSuccess = preg_match("/(success)+/i", $success);
			if (!$isSuccess && !trim($_FILES["logo"]["name"])) {
				$arr["errors"] = $success;
				$success = null;
			} elseif (trim($_FILES["logo"]["name"])) {
				$upload["client_name"] = $record["client_name"];
				$isUploaded = $mdlClient->saveClientInfo($upload);
				if ($isUploaded === true) $success = ($isSuccess ? "$success You also" : "You have") ." successfully uploaded ". $upload["client_name"] ." logo.";
				else {
					$arr["errors"] = $isUploaded;
					if (!$isSuccess) $success = null;
				}
			}
		}
		$arr["success"] = $success;
		
		$this->load->view("admin/vwEditClientAccount", $arr);
	}
	
	public function delete() {
		$strLocation = current_url(); $strHTML = "";
		if (preg_match("/(\/admin\/clients\/delete\/)+/", $strLocation)) {
			$confirmedDelete = ($this->uri->segment(5) && $this->uri->segment(5) == "yes");
			if (!$confirmedDelete && isset($_SERVER["HTTP_REFERER"])) $strLocation = current_url() ."/yes";
			$strMessage = "Do you really want to continue?";
			$strHTML = '<div class="text-center"><h3 class="text-danger">strMessage</h3>
						<h4>&nbsp;</h4><h4>&nbsp;</h4>
						<p class="form-group"><button class="btn btn-primary" data-href="'. $strLocation .'" data-action="Remove" data-toggle="modal" data-target="#dialogBox" role="button">Yes</button>&nbsp;<button class="btn btn-default" role="button" data-dismiss="modal" aria-label="Cancel" title="Cancel User Removal from Client Account">No</button></p></div>';
			if ($confirmedDelete) {
				//prepare user removal information
				$record = array("client_project_user_id" => $this->uri->segment(4), "is_active" => "no");
				$mdlClient = new ClientsModel();
				$isSuccess = $mdlClient->saveClientProjectUserAssociation($record, false);
				$strMessage = "$isSuccess <p>Click Yes to retry.</p>";
				if ($isSuccess === true) {
					$strMessage = "<div class=\"alert alert-success\" role=\"alert\"><span class=\"sr-only\">Success: </span><h4><strong>You have successfully removed user from client account.</strong><p>After 3 seconds you will be redirected to Manage Clients Account.</p></h4><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location=location.href;}, 3000);\n</script></div>";
					$strHTML = str_replace('<button class="btn btn-primary" data-href="'. $strLocation .'" data-action="Remove" data-toggle="modal" data-target="#dialogBox" role="button">Yes</button>&nbsp;<button class="btn btn-default" role="button" data-dismiss="modal" aria-label="Cancel" title="Cancel User Removal from Client Account">No</button>', '<button class="btn btn-default" role="button" data-dismiss="modal" aria-label="Cancel" title="Done in User Removal from Client Account">Ok</button>', $strHTML);
				}
			}
			$strHTML = str_replace("strMessage", $strMessage, $strHTML);
		}
		echo $strHTML;
	}
	
}

?>