<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Messages Controller of Client Dashboard Admin Interface
 *
 */
class Messages extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_admin_login") !== true) redirect("admin/home");
		$this->load->model(array("ContactModel"));
		$this->load->helper(array("date", "datetime"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
	}

	public function index() {
		$this->view();
	}

	public function view() {
		$arr["page"] = !$this->uri->segment(2) ? "dashboard" : $this->uri->segment(2);
		$arr["isNew"] = !$this->uri->segment(3);
		$arr["pageNum"] = $pageNum = $this->uri->segment(4);
		$myModel = new ContactModel();
		$arrQuery = $myModel->getAllContactMessageInfo("COUNT(contact_message_id) num_records");
		$arr["total"] = $total = $arrQuery[0]["num_records"];
		$arr["limit"] = $limit = 20;
		$arr["totalPages"] = $totalPages = ceil(($total / $limit));
		$offset = 0;
		if ($pageNum && !is_numeric($pageNum)) $pageNum = $totalPages;
		if ($pageNum && is_numeric($pageNum)) $offset = ($pageNum - 1) * $limit;
		$arrQuery = $records = $myModel->getAllContactMessageInfo("contact_message_id, message_type, message full_message, SUBSTRING(message FROM 1 FOR 50) ||'...' message, email, created_date sent_date, created_by sent_by". ($this->session->userdata("user_type") == "Super Administrator" ? ", is_email_sent, email_sent_to, email_sent_date" : ""), ($this->session->userdata("user_type") == "Super Administrator" ? null : "is_email_sent = 'yes' AND email_sent_to LIKE '%". $this->session->userdata("email") ."%'"), "last_updated_date DESC", $limit, $offset);
		$arr["offset"] = $offset;
		$arr["records"] = $records;
		$arr["messageIcon"] = $myModel->getMessageTypesIcon();

		$this->load->view("admin/vwHeader", $arr);
		$this->load->view("admin/vwManageMessages", $arr);
		$this->load->view("admin/vwFooter", $arr);
	}
	
}

?>