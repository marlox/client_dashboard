<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Dashboard Controller of Client Dashboard Admin Interface
 *
 */
class Dashboard extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_admin_login") !== true) redirect("admin/home");
		$this->load->library("form_validation");
		$this->load->helper(array("date", "datetime"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
		$this->load->model(array("HomeModel", "UsersModel", "ContactModel", "ClientsModel", "EyesTVersion2Model", "OrdersModel"));
	}

	public function index() {
		$arr["page"] = "dashboard";
		$arr["isNew"] = !$this->uri->segment(3);
		$mdlEyesT = new EyesTVersion2Model();
		$mdlUsers = new UsersModel();
		$mdlContact = new ContactModel();
		$mdlClient = new ClientsModel();
		$mdlHome = new HomeModel();
		$mdlOrders = new OrdersModel();
		$arrProjects = $mdlEyesT->getAllProjects();
		$projects = array();
		if (count($arrProjects)) {
			foreach ($arrProjects as $record) $projects[$record["project_id"]] = $record["project_name"];
		}
		$numUsers = $mdlUsers->getNumberOfUsers();
		$numClients = $mdlClient->getNumberOfClients();
		$numMessages = $mdlContact->getNumberOfMessages();
		$numOrders = $mdlOrders->getNumberOfOrderSubmission();
		$recentEyesTOrders = $mdlEyesT->getAllInfo("pc.id_commande order_id, pc.nom order_name, pc.date_livraison order_target_date, pc.date_livraison_reelle order_actual_date, pc.statut order_status, p.code_projet project_name, pc.date_insertion order_date, pc.type", "pj_commande pc, projet p", "p.id_projet = pc.id_projet AND pc.date_insertion IS NOT NULL", null, "pc.date_insertion DESC", 20);
		if (!is_array($recentEyesTOrders)) $recentEyesTOrders = array();
		$recentMessages = $mdlContact->getAllContactMessageInfo("message_type, SUBSTRING(message FROM 1 FOR 50) ||'...' message, email, last_updated_date", null, "last_updated_date DESC", 5);
		if (!is_array($recentMessages)) $recentMessages = array();
		$filtersUser = array("us.is_active" => "yes");
		if ($this->session->userdata("user_type") !== "Super Administrator") $filtersUser["us.username !="] = "root";
		$recentUsers = $mdlUsers->getUserClientInfo("us.username, us.email, (SELECT ei.info_value FROM entity_info ei WHERE ei.entity_id = us.user_id AND ei.info_field = 'given_name' AND ei.is_active = 'yes') ||' '|| (SELECT ei.info_value FROM entity_info ei WHERE ei.entity_id = us.user_id AND ei.info_field = 'last_name' AND ei.is_active = 'yes') full_name", "user us", $filtersUser, "us.created_date", false, 10);
		if (!is_array($recentUsers)) $recentUsers = array();
		$recentOrderSubmission = $mdlOrders->getAllOrderSubmissionInfo("(SELECT order_type_name FROM order_type WHERE order_type_id = order_submission.order_type_id) order_type, order_name, project_id, ordered_by, last_updated_date", array("is_email_sent" => "yes"), "last_updated_date DESC", 5);
		if (!is_array($recentOrderSubmission)) $recentOrderSubmission = array();

		$arr["status"] = $mdlHome->getLeftPanelAdditionalInfo();
		$arr["numUsers"] = $numUsers;
		$arr["numClients"] = $numClients;
		$arr["numMessages"] = $numMessages;
		$arr["numOrders"] = $numOrders;
		$arr["recentUsers"] = $recentUsers;
		$arr["recentEyesTOrders"] = $recentEyesTOrders;
		$arr["recentMessages"] = $recentMessages;
		$arr["recentOrderSubmission"] = $recentOrderSubmission;
		$arr["messageTypesIcon"] = $mdlContact->getMessageTypesIcon();
		$arr["orderTypesIcon"] = $mdlOrders->getOrderTypesIcon();
		$arr["projects"] = $projects;

		$this->load->view("admin/vwHeader", $arr);
		$this->load->view("admin/vwDashboard", $arr);
		$this->load->view("admin/vwFooter", $arr);
	}

}

?>