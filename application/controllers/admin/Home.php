<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Default or Home Controller of Client Dashboard Admin Interface
 *  
 */
class Home extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model("UsersModel");
		$this->load->library("form_validation");
		$this->load->helper(array("date"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
	}
	
	private function getLoginTexts() {
		$isAdmin = (!($this->uri->segment(1) === false) && $this->uri->segment(1) == "admin");
		$arrReturn["strLoginPrefix"] = ($isAdmin ? "Admin" : "Client");
		$arrReturn["strLoginSuffix"] = ($isAdmin ? " Admin Interface" : "");
		return $arrReturn;
	}
	
	public function index() {
		$arrData = $this->getLoginTexts();
		$arrData["page"] = !$this->uri->segment(1) ? "dashboard" : $this->uri->segment(1);
		if ($this->session->userdata("is_admin_login") || $this->session->userdata("is_client_login")) {
			redirect($this->session->userdata("is_admin_login") && $arrData["strLoginPrefix"] == "Admin" ? "admin/dashboard" : "home");
		} else {
			if ($this->session->userdata("is_admin_login") !== true) {
				if (preg_match("/(admin)+/i", $this->session->userdata("user_type")) === false && $this->session->userdata("is_client_login") === true) redirect("home");
				$this->load->view("admin/vwLogin", $arrData);
			} else {
				redirect("home");
			}
		}
	}
	
	public function doLogin() {
		$arrData = $this->getLoginTexts();
		if ($this->session->userdata("is_admin_login") === true) {
			redirect("admin/home");
		} else {
			$user = trim(isset($_POST["username"]) ? $_POST["username"] : "");
			$password = trim(isset($_POST["password"]) ? $_POST["password"] : "");
			
			$this->form_validation->set_rules("username", "Username", "required");
			$this->form_validation->set_rules("password", "Password", "required");
			
			if ($this->form_validation->run() == FALSE) {
				$errData = $arrData;
				if (!$user || !$password) $errData["error"] = "Required: Username and Password";
				$this->load->view("admin/vwLogin", $errData);
			} else {
				$myModel = new UsersModel();
				$result = $myModel->getUserClientInfo("us.user_id, us.username, us.email, usr.user_role_name, us.timezone, us.password", "user us, user_role usr", "us.user_role_id = usr.user_role_id AND us.is_active = 'yes' AND us.username = ". $this->db->escape(trim($user)), "us.last_updated_date", false, 1);
				//$result = $myModel->getUserClientInfo("us.user_id, us.username, us.email, usr.user_role_name, us.timezone", "user us, user_role usr", "us.user_role_id = usr.user_role_id AND us.is_active = 'yes' AND us.username = '$user' AND password = '". md5(SALT.$password) ."'", "us.last_updated_date", false, 1);
				$is_correct_password = null;
				$pass = md5(SALT . $password);
				if (isset($result[0]) && ($is_correct_password = $pass == $result[0]["password"])) {
					$userRole = $result[0]["user_role_name"];
					$userClient = $myModel->getUserClientInfo("c.client_name, c.logo, t.css_file, (SELECT info_value FROM entity_info WHERE entity_id = c.client_id AND is_active = 'yes' AND info_field = 'client_type' LIMIT 1) client_type", "client c, assoc_client_project_user acs, theme t", "c.client_id = acs.client_id AND c.theme_id = t.theme_id". ($userRole == "Super Administrator" ? "" : " AND acs.user_id = '". $result[0]["user_id"] ."'") ." AND c.is_active = 'yes' AND acs.is_active = 'yes' AND t.is_active = 'yes'", "acs.last_updated_date");
					
					$clientCSS = $clientLogo = $clientName = $clientType = null;
					$clientNames = array();
					if (isset($userClient) && count($userClient)) {
						$clientCSS = $userClient[0]["css_file"];
						$clientLogo = $userClient[0]["logo"];
						$clientName = $userClient[0]["client_name"];
						$clientType = $userClient[0]["client_type"];
						foreach ($userClient as $client) {
							if (array_search($client["client_name"], $clientNames) === false) $clientNames[] = $client["client_name"];
						}
					}
					$timezone = $myModel->db->query("SELECT current_setting('TIMEZONE') tz")->result_array();
					$dbTimezone = $timezone[0]["tz"];
					foreach ($result as $res) {
						$this->session->set_userdata(array(
							"id" => $res["user_id"],
							"username" => $res["username"],
							"email" => $res["email"],
							"css" => $clientCSS,
							"logo" => $clientLogo,
							"clientName" => $clientName,
							"clientNames" => $clientNames,
							(preg_match("/(admin)+/i", $res["user_role_name"]) ? "is_admin_login" : "is_client_login") => true,
							"user_type" => $res["user_role_name"],
							"timezone" => $res["timezone"],
							"client_type" => $clientType,
							"db_timezone" => $dbTimezone
							)
						);
					}
					redirect("admin/dashboard", "refresh");
				} else {
					$errData = $arrData;
					$errData["error"] = "Username not found!";
					if (!$user || !$password) $errData["error"] = "Required: Username and Password";
					if (isset($is_correct_password) && !$is_correct_password) $errData["error"] = "Incorrect Password!";
					$this->load->view("admin/vwLogin", $errData);
				}
			}
		}
	}
	
	public function logout() {
		$this->session->unset_userdata("id");
		$this->session->unset_userdata("username");
		$this->session->unset_userdata("email");
		$this->session->unset_userdata("css");
		$this->session->unset_userdata("logo");
		$this->session->unset_userdata("clientName");
		$this->session->unset_userdata("is_admin_login");
		$this->session->unset_userdata("user_type");
		$this->session->unset_userdata("timezone");
		$this->session->unset_userdata("client_type");
		$this->session->unset_userdata("db_timezone");
		if ($this->session->has_userdata("reportDateRange")) $this->session->unset_userdata("reportDateRange");
		$this->session->sess_destroy();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		redirect("admin/home", "refresh");
	}
	
}

?>