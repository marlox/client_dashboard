<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Users Controller of Client Dashboard Admin Interface
 *
 */
class Users extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_admin_login") !== true) redirect("admin/home");
		$this->load->model("UsersModel");
		$this->load->helper(array("form", "url", "date", "datetime"));
		$this->load->library("form_validation");
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
	}
	
	public function validateForm() {
		$this->form_validation->set_rules("given_name", "Given Name", "required", array("required" => "You must provide a <u>%s</u>."));
		$this->form_validation->set_rules("last_name", "Last Name", "required", array("required" => "You must provide a <u>%s</u>."));
		$this->form_validation->set_rules("email", "Email Address", "required|valid_email", array("required" => "You must provide <u>%s</u>.", "valid_email" => "Please provide a valid <u>%s</u>."));
		$this->form_validation->set_rules("password", "Password", "required", array("required" => "You must provide a <u>%s</u>."));
		$this->form_validation->set_rules("confirm_password", "Confirm Password", "required|matches[password]", array("required" => "You must provide a <u>%s</u>.", "matches" => "Password and <u>%s</u> must be the same."));
		$this->form_validation->set_rules("user_role", "User Profile/Role", "required", array("required" => "You must provide a <u>%s</u>."));
		$this->form_validation->set_rules("timezone", "Timezone", "required", array("required" => "You must provide a <u>%s</u>."));
		return $this->form_validation->run();
	}
	
	public function validateProfileForm() {
		$this->form_validation->set_rules("given_name", "Given Name", "required", array("required" => "Blank <u>%s</u> has been detected! Auto-retrieve the original."));
		$this->form_validation->set_rules("last_name", "Last Name", "required", array("required" => "Blank <u>%s</u> has been detected! Auto-retrieve the original."));
		$this->form_validation->set_rules("email", "Email Address", "required|valid_email", array("required" => "Blank <u>%s</u> has been detected. Auto-retrieve the original.", "valid_email" => "Please provide a valid <u>%s</u>."));
		if ($this->input->post("password")) $this->form_validation->set_rules("password", "Password", "required", array("required" => "You must provide a <u>%s</u>."));
		if ($this->input->post("password") || $this->input->post("confirm_password")) $this->form_validation->set_rules("confirm_password", "Confirm Password", "required|matches[password]", array("required" => "You must provide a <u>%s</u>.", "matches" => "Password and <u>%s</u> must be the same."));
		$this->form_validation->set_rules("timezone", "Timezone", "required", array("required" => "Blank <u>%s</u> has been detected! Auto-retrieve the original."));
		return $this->form_validation->run();
	}

	public function index() {
		$arr["page"] = !$this->uri->segment(2) ? "dashboard" : $this->uri->segment(2);
		$arr["isNew"] = !$this->uri->segment(3);
		$myModel = new UsersModel();
		$arr["userRoles"] = $myModel->getAllUserRoleInfo("*", ($this->session->userdata("user_type") == "Super Administrator" ? null : array("user_role_name !=" => "Super Administrator")));
		
		$success = $this->validateForm();
		$arr["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			$success = $myModel->saveUserInfo($record);
			if (!preg_match("/(success)+/i", $success)) {
				$arr["errors"] = $success;
				$success = null;
			}
		}
		$arr["success"] = $success;
		
		$this->load->view("admin/vwHeader", $arr);
		$this->load->view("admin/vwNewUser", $arr);
		$this->load->view("admin/vwFooter", $arr);
	}
	
	public function view() {
		$arr["page"] = !$this->uri->segment(2) ? "dashboard" : $this->uri->segment(2);
		$arr["isNew"] = !$this->uri->segment(3);
		$pageNum = $this->uri->segment(4);
		$myModel = new UsersModel();
		$arrQuery = $this->db->query("SELECT COUNT(user_id) num_records FROM public.user WHERE is_active = 'yes'")->result_array();
		$arr["total"] = $total = $arrQuery[0]["num_records"];
		$arr["limit"] = $limit = 20;
		$arr["totalPages"] = $totalPages = ceil(($total / $limit));
		$offset = 0;
		if ($pageNum && !is_numeric($pageNum)) $pageNum = $totalPages;
		if ($pageNum && is_numeric($pageNum)) $offset = ($pageNum - 1) * $limit;
		$arrQuery = $records = $myModel->getAllUsersInfo("username, email, last_updated_date recent_update, user_role_id, user_id", "is_active = 'yes'", "full_name, username", $limit, $offset);
		$arrQuery = $myModel->getAllUserRoleInfo("user_role_id, user_role_name");
		$userRoles = array();
		foreach ($arrQuery as $role) {
			if (array_search($role["user_role_name"], $userRoles) === false && array_key_exists($role["user_role_id"], $userRoles) === false) $userRoles[$role["user_role_id"]] = $role["user_role_name"];
		}
		$arr["userRoles"] = $userRoles;
		$arr["offset"] = $offset;
		$arr["pageNum"] = $pageNum;
		$arr["records"] = $records;

		$this->load->view("admin/vwHeader", $arr);
		$this->load->view("admin/vwManageUsers", $arr);
		$this->load->view("admin/vwFooter", $arr);
	}
	
	public function edit() {
		$arr["page"] = !$this->uri->segment(2) ? "dashboard" : $this->uri->segment(2);
		$arr["isNew"] = !$this->uri->segment(3);
		$mdlUser = new UsersModel();
		$userInfo = $mdlUser->getAllUsersInfo("email", "us.user_id = '". $this->session->userdata("id") ."'");
		$arr["userRoles"] = $mdlUser->getAllUserRoleInfo("*", ($this->session->userdata("user_type") == "Super Administrator" ? null : array("user_role_name !=" => "Super Administrator")));
		if (isset($userInfo) && count($userInfo)) {
			$userInfo = $userInfo[0];
			unset($userInfo["full_name"]);
			foreach ($userInfo as $field => $value) $arr[$field] = $value;
			$arr["user_role"] = $this->session->userdata("user_type");
			$arr["usrTimezone"] = $this->session->userdata("timezone");
		}

		$success = $this->validateProfileForm();
		$arr["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			if (!$record["password"]) unset($record["password"]);
			if (!$record["confirm_password"]) unset($record["confirm_password"]);
			$success = $mdlUser->saveEditProfile($record);
			if (!preg_match("/(success)+/i", $success)) {
				$arr["errors"] = $success;
				$success = null;
			} else {
				$this->session->set_userdata("timezone", $record["timezone"]);
				$arr["usrTimezone"] = $this->session->userdata("timezone");
			}
		}
		$arr["success"] = $success;

		$this->load->view("admin/vwEditUserInfo", $arr);
	}
	
	public function delete() {
		$strLocation = current_url(); $strHTML = "";
		if (preg_match("/(\/admin\/users\/delete\/)+/", $strLocation)) {
			$confirmedDelete = ($this->uri->segment(5) && $this->uri->segment(5) == "yes");
			if (!$confirmedDelete && isset($_SERVER["HTTP_REFERER"])) $strLocation = current_url() ."/yes";
			$strMessage = "Do you really want to continue?";
			$strHTML = '<div class="text-center"><h3 class="text-danger">strMessage</h3>
						<h4>&nbsp;</h4><h4>&nbsp;</h4>
						<p class="form-group"><button class="btn btn-primary" data-href="'. $strLocation .'" data-action="Delete" data-toggle="modal" data-target="#dialogBox" role="button">Yes</button>&nbsp;<button class="btn btn-default" role="button" data-dismiss="modal" aria-label="Cancel" title="Cancel Delete User">No</button></p></div>';
			/*if ($confirmedDelete) {
				//prepare user removal information
				$record = array("client_project_user_id" => $this->uri->segment(4), "is_active" => "no");
				$mdlClient = new ClientsModel();
				$isSuccess = $mdlClient->saveClientProjectUserAssociation($record, false);
				$strMessage = "$isSuccess <p>Click Yes to retry.</p>";
				if ($isSuccess === true) {
					$strMessage = "<div class=\"alert alert-success\" role=\"alert\"><span class=\"sr-only\">Success: </span><h4><strong>You have successfully removed user from client account.</strong><p>After 3 seconds you will be redirected to Manage Clients Account.</p></h4><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location=location.href;}, 3000);\n</script></div>";
					$strHTML = str_replace('<button class="btn btn-primary" data-href="'. $strLocation .'" data-action="Delete" data-toggle="modal" data-target="#dialogBox" role="button">Yes</button>&nbsp;<button class="btn btn-default" role="button" data-dismiss="modal" aria-label="Cancel" title="Cancel Delete User">No</button>', '<button class="btn btn-default" role="button" data-dismiss="modal" aria-label="Cancel" title="Done in Delete User">Ok</button>', $strHTML);
				}
			}*/
			$strHTML = str_replace("strMessage", $strMessage, $strHTML);
		}
		echo $strHTML;
	}
	
	public function editProfile() {
		$arr["page"] = !$this->uri->segment(2) ? "dashboard" : $this->uri->segment(2);
		$arr["isNew"] = !$this->uri->segment(3);
		$mdlUser = new UsersModel();
		$userInfo = $mdlUser->getAllUsersInfo("email", "us.user_id = '". $this->session->userdata("id") ."'");
		if (isset($userInfo) && count($userInfo)) {
			$userInfo = $userInfo[0];
			unset($userInfo["full_name"]);
			foreach ($userInfo as $field => $value) $arr[$field] = $value;
			$arr["user_role"] = $this->session->userdata("user_type");
			$arr["usrTimezone"] = $this->session->userdata("timezone");
		}

		$success = $this->validateProfileForm();
		$arr["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			if (!$record["password"]) unset($record["password"]);
			if (!$record["confirm_password"]) unset($record["confirm_password"]);
			$success = $mdlUser->saveEditProfile($record);
			if (!preg_match("/(success)+/i", $success)) {
				$arr["errors"] = $success;
				$success = null;
			} else {
				$this->session->set_userdata("timezone", $record["timezone"]);
				$arr["usrTimezone"] = $this->session->userdata("timezone");
			}
		}
		$arr["success"] = $success;

		$this->load->view("admin/vwHeader", $arr);
		$this->load->view("admin/vwEditProfile", $arr);
		$this->load->view("admin/vwFooter", $arr);
	}

}

?>