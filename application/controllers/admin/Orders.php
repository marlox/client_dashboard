<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Orders Controller of Client Dashboard Admin Interface
 *
 */
class Orders extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_client_login") !== true && $this->session->userdata("is_admin_login") !== true) redirect("admin");
		$this->load->model(array("StatusModel", "OrdersModel", "ClientsModel", "EyesTVersion2Model"));
		$this->load->library("form_validation");
		$this->load->helper(array("url", "date", "datetime", "file"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
		//$this->output->enable_profiler(TRUE);
	}
	
	private function validateNewOrderInfoForm($skipValidation = false) {
		if (!$skipValidation) {
			$this->form_validation->set_rules("order_submission_id", "Submitted Order ID", "required", array("required" => "Invalid <u>%s</u>."));
			$this->form_validation->set_rules("order_code", "Order/Campaign Code", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("order_name", "Order/Campaign Name", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("advertiser", "Advertiser", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("ad_server", "Ad Server", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("start_date", "Start Date", "required|regex_match[/^\d{4}\-\d{2}\-\d{2}$/]", array("required" => "You must provide the <u>%s</u>.", "regex_match" => "You must provide a valid <u>%s</u> format (yyyy-mm-dd)."));
			$this->form_validation->set_rules("end_date", "End Date", "required|regex_match[/^\d{4}\-\d{2}\-\d{2}$/]", array("required" => "You must provide the <u>%s</u>.", "regex_match" => "You must provide a valid <u>%s</u> format (yyyy-mm-dd)."));
		}
		
		return $this->form_validation->run();
	}
	
	private function validateOrderSpecsForm($skipValidation = false) {
		if (!$skipValidation) {
			$this->form_validation->set_rules("order_info_id", "Order Info ID", "required", array("required" => "Invalid <u>%s</u>."));
			$this->form_validation->set_rules("order_spec_type", "Type/Placement", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("spec_type_tag", "Type Tag", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("order_spec_name", "Placement Name", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("spec_site_name", "Site Name", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("spec_buy_mode", "Buy Mode", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("spec_format_code", "Format Code", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("spec_id", "ID", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("creative_status_id", "Creative Status", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("url_status_id", "URL Status", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("message_status_id", "Message Status", "required", array("required" => "You must provide the <u>%s</u>."));
		}
		
		return $this->form_validation->run();
	}
	
	private function validateNewOrderInfoModalForm($skipValidation = false) {
		if (!$skipValidation) {
			$this->form_validation->set_rules("order_type", "Order Type", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("client_name", "Client", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("project_id", "Project", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("order_name", "Order/Campaign Name", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("delivery_date", "Delivery Date", "required|regex_match[/^\d{4}\-\d{2}\-\d{2}$/]", array("required" => "You must provide the <u>%s</u>.", "regex_match" => "You must provide a valid <u>%s</u> format (yyyy-mm-dd)."));
			$this->form_validation->set_rules("advertiser", "Advertiser", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("ad_server", "Ad Server", "required", array("required" => "You must provide the <u>%s</u>."));
			$this->form_validation->set_rules("start_date", "Start Date", "required|regex_match[/^\d{4}\-\d{2}\-\d{2}$/]", array("required" => "You must provide the <u>%s</u>.", "regex_match" => "You must provide a valid <u>%s</u> format (yyyy-mm-dd)."));
			$this->form_validation->set_rules("end_date", "End Date", "required|regex_match[/^\d{4}\-\d{2}\-\d{2}$/]", array("required" => "You must provide the <u>%s</u>.", "regex_match" => "You must provide a valid <u>%s</u> format (yyyy-mm-dd)."));
		}
		
		return $this->form_validation->run();
	}
	
	public function index() {
		$this->submitted_orders();
	}

	/**
	 * Manage Submitted Orders Page
	 */
	public function submitted_orders() {
		$arr["page"] = !$this->uri->segment(2) ? "dashboard" : $this->uri->segment(2);
		$arr["isNew"] = !$this->uri->segment(3);
		$pageNum = $this->uri->segment(4);
		$mdlEyesT = new EyesTVersion2Model();
		$mdlOrders = new OrdersModel();
		$arrProjects = $mdlEyesT->getAllProjects();
		$projects = array();
		if (count($arrProjects)) {
			foreach ($arrProjects as $record) $projects[$record["project_id"]] = $record["project_name"];
		}
		$typesIcon = $mdlOrders->getOrderTypesIcon();
		$arr["types"] = $typesIcon;
		$arr["total"] = $total = $mdlOrders->getNumberOfOrderSubmission();
		$arr["limit"] = $limit = 20;
		$arr["totalPages"] = $totalPages = ceil(($total / $limit));
		$offset = 0;
		if ($pageNum && !is_numeric($pageNum)) $pageNum = $totalPages;
		if ($pageNum && is_numeric($pageNum)) $offset = ($pageNum - 1) * $limit;
		$records = $mdlOrders->getAllOrderSubmissionInfo("order_submission_id, (SELECT order_type_name FROM order_type WHERE order_type_id = order_submission.order_type_id AND is_active = 'yes' LIMIT 1) order_type, order_name, project_id project, (SELECT client_name FROM client WHERE client_id = order_submission.client_id AND is_active = 'yes' LIMIT 1) client_name, ordered_date, delivery_date, document \"document(s)\", order_specifications specs, (CASE is_new_order WHEN true THEN '1' ELSE '0' END) is_new_order, parent_order_submission, (SELECT os.order_name FROM order_submission os WHERE os.order_submission_id = order_submission.parent_order_submission LIMIT 1) parent_order_name", null, "ordered_date DESC, order_name", $limit, $offset);
		$arr["offset"] = $offset;
		$arr["pageNum"] = $pageNum;
		$arr["records"] = $records;
		$arr["projects"] = $projects;
		
		$this->load->view("admin/vwHeader", $arr);
		$this->load->view("admin/vwManageOrderSubmission", $arr);
		$this->load->view("admin/vwFooter", $arr);
	}
	
	public function add_to_submitted_order() {
		$selectedOrder = $this->uri->segment(5);
		$selectedOrderAddTo = $this->uri->segment(4);
		$mdlEyesT = new EyesTVersion2Model();
		$mdlOrders = new OrdersModel();
		$arrProjects = $mdlEyesT->getAllProjects();
		$projects = array();
		if (count($arrProjects)) {
			foreach ($arrProjects as $record) $projects[$record["project_id"]] = $record["project_name"];
		}
		
		$previouslySubmittedOrders = $mdlOrders->getPreviousOrderSubmission("order_submission_id, order_name, project_id, (SELECT order_type_name FROM order_type WHERE order_type_id = order_submission.order_type_id AND is_active = 'yes' LIMIT 1) order_type", "parent_order_submission = 'PARENT' AND is_email_sent = 'yes'". ($this->session->userdata("user_type") == "Super Administrator" ? null : " AND email_sent_to LIKE '%". $this->session->userdata("email") ."%' OR order_submission_id = '$selectedOrderAddTo'"));
		$orderInfo = $mdlOrders->getAllOrderInfo("order_info_id", "order_submission_id = '$selectedOrderAddTo'", null, null, 1);
		$orderSubmission = $mdlOrders->getAllOrderSubmissionInfo("order_name", "order_submission_id = '$selectedOrder'", null, null, 1);
		
		$strError = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"sr-only\">Error:</span> <h4><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> strError</h4></div><p class=\"text-center\"><button class=\"btn btn-default btn-lg\" role=\"button\" data-dismiss=\"modal\" aria-label=\"Ok\">Ok</button></p>";
		
		if (isset($orderSubmission) && isset($orderSubmission[0]["order_name"])) {
			$selectedOrderName = $orderSubmission[0]["order_name"];
			$strHTML = '<form class=form-horizontal action="'. site_url("admin/orders/specs") .'" method=post><select class="form-control" name="order_submission_id" id="order_submission_id" title="Select Previously Submitted New Order" required><option disabled>Select Previously Submitted New Order</option>';
			$selectedOrderAddToName = "";
			if (isset($previouslySubmittedOrders)) {
				foreach ($previouslySubmittedOrders as $record) {
					if ($record["order_submission_id"] == $selectedOrderAddTo) $selectedOrderAddToName = $record["order_name"];
					$optionVal = $record["order_name"] .' &rsaquo; '. $record["order_type"] .' &rsaquo; '. $projects[$record["project_id"]];
					if (!preg_match("/($optionVal)+/i", $strHTML)) $strHTML .= '<option value="'. $record["order_submission_id"] .'"'. ($record["order_submission_id"] == $selectedOrderAddTo ? " selected" : "") .'>'. $optionVal .'</option>';
				}
			}
			
			if (!isset($orderInfo) || !isset($orderInfo[0])) {
				$strHTML = str_replace("strError", "<strong><u>$selectedOrderName</u> cannot be added to previously submitted new order". ($selectedOrderAddToName ? " <u>$selectedOrderAddToName</u>" : "") . "!</strong> Order Information for ". ($selectedOrderAddToName ? "<u>$selectedOrderAddToName</u>" : "previously submitted new order") ." doesn't exists. Please create new order/campaign information first ". ($selectedOrderAddToName ? "for <u>$selectedOrderAddToName</u> " : "") ."to continue.", $strError);
			} else {
				$strInfoID = '<input type="hidden" name="order_info_id" value="'. $orderInfo[0]["order_info_id"] .'" />';
				$strInfoID .= '<input type="hidden" name="order_submission_id" value="'. $selectedOrder .'" />';
				$strInfoID .= '<input type="hidden" name="specs_action" value="add_to" />';
				$strHTML .= "</select><p>&nbsp;</p><p class=\"text-center\"><button type=submit class=\"btn btn-default btn-lg\" role=\"button\">Ok</button></p>$strInfoID</form>";
			}
		} else {
			$strHTML = str_replace("strError", "Invalid or blank submitted order found! Click Ok and refresh/reload the source/parent page, then try again.", $strError);
		}
		echo $strHTML;
	}
	
	public function create_new_order() {
		$orderSubmissionID = $this->uri->segment(4);
		$mdlOrders = new OrdersModel();
		$orderSubmission = $mdlOrders->getAllOrderSubmissionInfo("order_name", array("order_submission_id" => $orderSubmissionID), null, null, 1);

		$strError = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"sr-only\">Error:</span> <h4><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> strError</h4></div><p class=\"text-center\"><button class=\"btn btn-default btn-lg\" role=\"button\" data-dismiss=\"modal\" aria-label=\"Ok\">Ok</button></p>";

		if (isset($orderSubmission) && isset($orderSubmission[0]["order_name"])) {
			$orderSubmissionName = $orderSubmission[0]["order_name"];
			$strHTML = "<form class=form-horizontal action=\"". site_url("admin/orders/view/new") ."\" method=post>\n".
							"<div class=\"alert alert-info\" role=\"alert\"><h4><span class=\"fa fa-info-circle\" aria-hidden=\"true\"></span> You are about to create the new order/campaign <u>$orderSubmissionName</u>. <strong>Do you really want to continue?</strong></h4></div>";
			$strID = '<input type="hidden" name="order_submission_id" value="'. $orderSubmissionID .'" />';
			$strHTML .= "<p class=\"text-center\"><button type=submit class=\"btn btn-info btn-lg\" role=\"button\">Yes</button>&nbsp;<button class=\"btn btn-default btn-lg\" role=\"button\" data-dismiss=\"modal\" aria-label=\"No\">No</button></p>$strID</form>";
		} else {
			$strHTML = str_replace("strError", "Invalid or blank submitted order found! Click Ok and refresh/reload the source/parent page, then try again.", $strError);
		}
		echo $strHTML;
	}
	
	public function remove_order_info() {
		$orderInfoID = $this->uri->segment(4);
		$mdlOrders = new OrdersModel();
		$record = $mdlOrders->getAllOrderInfo("order_code, (SELECT order_name FROM order_submission WHERE order_submission_id = order_info.order_submission_id AND is_active = 'yes' LIMIT 1) order_name, advertiser, ad_server, start_date, end_date, comments", array("order_info_id" => $orderInfoID), null, null, 1);

		$strError = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"sr-only\">Error:</span> <h4><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> strError</h4></div><p class=\"text-center\"><button class=\"btn btn-default btn-lg\" role=\"button\" data-dismiss=\"modal\" aria-label=\"Ok\">Ok</button></p>";

		if (isset($record) && isset($record[0]["order_code"])) {
			$recordName = $record[0]["order_code"];
			$strHTML = "<form class=form-horizontal action=\"". site_url("admin/orders/view/delete") ."\" method=post>\n".
							"<div class=\"alert alert-info\" role=\"alert\"><h4><span class=\"fa fa-info-circle\" aria-hidden=\"true\"></span> You are about to delete order information with the following details: <em title='Campaign/Order Name'>". $record[0]["order_name"] ."</em>, <em title='Advertiser'>". $record[0]["advertiser"] ."</em>, <em title='Ad Server'>". $record[0]["ad_server"] ."</em>, <em title='Start Date'>". $record[0]["start_date"] ."</em>, <em title='End Date'>". $record[0]["end_date"] ."</em>, <em title='Comments'>". $record[0]["comments"] ."</em>. <strong>Do you really want to continue?</strong></h4></div>";
			$strID = '<input type="hidden" name="order_info_id" value="'. $orderInfoID .'" />';
			$strHTML .= "<p class=\"text-center\"><button type=submit class=\"btn btn-info btn-lg\" role=\"button\">Yes</button>&nbsp;<button class=\"btn btn-default btn-lg\" role=\"button\" data-dismiss=\"modal\" aria-label=\"No\">No</button></p>$strID</form>";
		} else {
			$strHTML = str_replace("strError", "Invalid or blank Order Information found! Click Ok and refresh/reload the source/parent page, then try again.", $strError);
		}
		echo $strHTML;
	}

	/**
	 * Manage Orders Information Page
	 */
	public function view() {
		$arr["page"] = !$this->uri->segment(2) ? "dashboard" : $this->uri->segment(2);
		$arr["isNew"] = !$this->uri->segment(3);
		$action = $this->uri->segment(3);
		$pageNum = $this->uri->segment(4);
		$mdlEyesT = new EyesTVersion2Model();
		$mdlOrders = new OrdersModel();
		$orderName = $originalOrderName = "";
		$clientName = $this->session->userdata("clientName");
		$clientName = (strpos($clientName, " ") !== false ? str_replace(" ", "", ucwords(strtolower($clientName))) : $clientName);

		$success = $this->validateNewOrderInfoForm(!$this->input->post("order_code"));
		$arr["errors"] = validation_errors();
		$isDelete = ($pageNum == "delete" && $this->input->post("order_info_id"));
		if ($success || $isDelete) {
			$record = $this->input->post();
			if ($isDelete) $record["is_active"] = "no";
			$success = $mdlOrders->saveOrderInfo($record, !isset($record["order_info_id"]), $isDelete);
			if (!($isSuccess = preg_match("/(success)+/i", $success))) {
				$arr["errors"] = $success;
				$success = null;
			} elseif ($isSuccess && $isDelete) {
				$success = str_replace("saved", "deleted", $success);
			}
		}
		$arr["success"] = $success;

		if ($orderSubmissionID = $this->input->post("order_submission_id")) {
			$orderSubmission = $mdlOrders->getAllOrderSubmissionInfo("order_name, delivery_date, (SELECT order_type_name FROM order_type WHERE order_type_id = order_submission.order_type_id AND is_active = 'yes' LIMIT 1) order_type, (SELECT client_name FROM client WHERE client_id = order_submission.client_id AND is_active = 'yes' LIMIT 1) client_name", "order_submission_id = '$orderSubmissionID'", null, null, 1);
			$orderName = $originalOrderName = $orderSubmission[0]["order_name"];
			$orderName = (strpos($orderName, " ") !== false ? str_replace(" ", "", ucwords(strtolower($orderName))) : $orderName);
			$clientName = $orderSubmission[0]["client_name"];
			$clientName = (strpos($clientName, " ") !== false ? str_replace(" ", "", ucwords(strtolower($clientName))) : $clientName);
			$deliveryDate = $orderSubmission[0]["delivery_date"];
			$arr["orderCode"] = (preg_match("/(trafficking)+/i", $orderSubmission[0]["order_type"]) ? "Traffick" : "Order") ."_". mdate("%Y-%m", syncDate($deliveryDate)) ."_". $clientName ."_". str_replace(" ", "", ucwords(strtolower($orderName))) ."@". str_pad(intval(str_replace("ETDORDERS", "", $orderSubmissionID)), 5, "0", STR_PAD_LEFT) ."@";
		}
		$receivedOrders = $mdlOrders->getAllOrderSubmissionInfo("order_submission_id");
		$currentAdminsReceivedOrders = array("");
		if (isset($receivedOrders)) {
			foreach ($receivedOrders as $record) $currentAdminsReceivedOrders[] = $record["order_submission_id"];
		}
		$filter = array();
		$filter["is_active"] = "yes";
		if ($this->session->userdata("user_type") !== "Super Administrator") $filter["order_submission_id IN"] = $currentAdminsReceivedOrders;

		$arr["total"] = $total = $mdlOrders->getNumberOfOrdersInformation(($this->session->userdata("user_type") == "Super Administrator" ? null : $currentAdminsReceivedOrders));
		$arr["limit"] = $limit = 20;
		$arr["totalPages"] = $totalPages = ceil(($total / $limit));
		$offset = 0;
		if ($pageNum && $pageNum == "end") $pageNum = $totalPages;
		if ($pageNum && is_numeric($pageNum)) $offset = ($pageNum - 1) * $limit;
		$records = $mdlOrders->getAllOrderInfo("order_info_id, order_code, (SELECT order_name FROM order_submission WHERE order_submission_id = order_info.order_submission_id AND is_active = 'yes' LIMIT 1) order_name, (SELECT delivery_date FROM order_submission WHERE order_submission_id = order_info.order_submission_id AND is_active = 'yes' LIMIT 1) delivery_date, advertiser, ad_server, start_date, end_date, comments, status_id, order_submission_id, (SELECT c.client_name FROM order_submission os, client c WHERE os.client_id = c.client_id AND os.order_submission_id = order_info.order_submission_id AND c.is_active = 'yes' LIMIT 1) client_name", $filter, "last_updated_date DESC, order_code", $limit, $offset);
		if (!$records || !is_array($records)) $records = array();
		$arr["offset"] = $offset;
		$arr["pageNum"] = $pageNum;
		$arr["records"] = $records;
		$arr["action"] = $action;
		$arr["orderName"] = $orderName;
		$arr["originalOrderName"] = $originalOrderName;
		$arr["clientName"] = $clientName;
		$arr["orderSubmissionID"] = $orderSubmissionID;
		
		$this->load->view("admin/vwHeader", $arr);
		$this->load->view("admin/vwManageOrders", $arr);
		$this->load->view("admin/vwFooter", $arr);
	}

	public function new_order_info() {
		$mdlOrders = new OrdersModel();
		$mdlClients = new ClientsModel();
		$mdlEyesT = new EyesTVersion2Model();
		$clientProjects = $projectsClient = $projects = array();
		$types = $mdlOrders->getOrderTypes();
		$arr["types"] = $types;
		$objQuery = $mdlEyesT->getAllProjects();
		foreach ($objQuery as $query) {
			if (array_search($query["project_name"], $projects) === false) $projects[$query["project_id"]] = $query["project_name"];
		}
		$arr["projects"] = $projects;
		$currentClient = $this->session->userdata("clientName");
		$arr["currentClient"] = $currentClient = (preg_match("/(\_)+/i", $currentClient) ? str_replace("_", " ", $currentClient) : $currentClient);
		$arr["clients"] = $clients = $this->session->userdata("clientNames");
		$objQuery = $mdlClients->getAllInfo("c.client_name, acpu.project_id", "assoc_client_project_user acpu, client c", "acpu.client_id = c.client_id AND c.client_name IN ('". implode("', '", $clients) ."') AND acpu.is_active = 'yes'", null, "c.client_name");
		foreach ($objQuery as $query) {
			$clientNameNoSpace = (preg_match("/(\s)+/i", $query["client_name"]) ? str_replace(" ", "_", $query["client_name"]) : $query["client_name"]);
			$clientProjects[$clientNameNoSpace][$query["project_id"]] = $clientNameNoSpace;
		}
		$arr["clientProjects"] = $clientProjects;

		$success = $this->validateNewOrderInfoModalForm();
		$arr["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			$success = $mdlOrders->saveNewOrderInfo($record);
			if (!preg_match("/(success)+/i", $success)) {
				$arrData["errors"] = $success;
				$success = null;
			}
		}
		$arr["success"] = $success;

		$this->load->view("admin/vwNewOrderInfo", $arr);
	}
	
	/**
	 * Manage Order Specifications Page
	 */
	public function specs() {
		$arr["page"] = !$this->uri->segment(2) ? "dashboard" : $this->uri->segment(2);
		$arr["isNew"] = !$this->uri->segment(3);
		$pageNum = $this->uri->segment(4);
		$mdlEyesT = new EyesTVersion2Model();
		$mdlOrders = new OrdersModel();
		$mdlStatus = new StatusModel();
		$specName = $orderName = $specID = "";
		$action = $this->input->post("specs_action");
		
		if ($orderSubmissionSpecID = $this->input->post("order_submission_id")) {
			$record = $mdlOrders->getAllOrderSubmissionInfo("*", array("order_submission_id" => $orderSubmissionSpecID), null, null, 1);
			$specName = $record[0]["order_name"];
			$specID = "@". str_pad(intval(str_replace("ETDORDERS", "", $orderSubmissionSpecID)), 8, "0", STR_PAD_LEFT) ."@";
		}
		if ($orderInfoID = $this->input->post("order_info_id")) {
			$record = $mdlOrders->getAllOrderInfo("(SELECT order_name FROM order_submission WHERE order_submission_id = order_info.order_submission_id AND is_active = 'yes' LIMIT 1) order_name", array("order_info_id" => $orderInfoID), null, null, 1);
			$orderName = $record[0]["order_name"];
			$specID = "@". str_pad(intval(str_replace("ETDORDERINFO", "", $orderInfoID)), 8, "0", STR_PAD_LEFT) ."@";
		}
		$orderSpecID = $this->input->post("order_spec_id");

		$success = $this->validateOrderSpecsForm(($action !== "save"));
		$arr["errors"] = validation_errors();
		$isDelete = ($action == "delete" && $orderSpecID);
		if ($success || $isDelete) {
			$record = $this->input->post();
			$success = ($isDelete ? $mdlOrders->deleteOrderSpecifications($record) : $mdlOrders->saveOrderSpecifications($record, !$orderSpecID, $isDelete));
			if (!preg_match("/(success)+/i", $success)) {
				$arr["errors"] = $success;
				$success = null;
			}
		}
		if ($action !== "view") {
			$action = !$orderSpecID ? "add_to" : ($isDelete ? "delete" : "edit");
		}
		$arr["success"] = $success;
		$arr["status"] = $mdlStatus->getAllStatusInfo("status_id, status_name", "is_active = 'yes'");

		$arr["total"] = $total = $mdlOrders->getNumberOfOrderSpecs($orderInfoID);
		$arr["limit"] = $limit = 20;
		$arr["totalPages"] = $totalPages = ceil(($total / $limit));
		$offset = 0;
		if ($pageNum && $pageNum == "end") $pageNum = $totalPages;
		if ($pageNum && is_numeric($pageNum)) $offset = ($pageNum - 1) * $limit;
		$records = $mdlOrders->getAllOrderSpecsView("*", array("order_info_id" => $orderInfoID, "is_active" => "yes"), "order_spec_type, order_spec_name", $limit, $offset);
		if (!$records || !is_array($records)) $records = array();
		$arr["offset"] = $offset;
		$arr["pageNum"] = $pageNum;
		$arr["records"] = $records;
		$arr["orderName"] = $orderName;
		$arr["specName"] = $specName;
		$arr["orderInfoID"] = $orderInfoID;
		$arr["orderSpecID"] = $orderSpecID;
		$arr["specID"] = $specID;
		$arr["action"] = $action;
		$arr["urlSave"] = (strpos(current_url(), "/add_to") === false ? current_url() : str_replace("/add_to", "", current_url()));
		$arr["urlCancel"] = "#\" onclick='window.history.back()'";
		
		$this->load->view("admin/vwHeader", $arr);
		$this->load->view("admin/vwManageOrderSpecs", $arr);
		$this->load->view("admin/vwFooter", $arr);
	}
	
	public function remove_order_specs() {
		$id = $this->uri->segment(4);
		$orderInfoID = $this->uri->segment(5);
		$mdlOrders = new OrdersModel();
		$record = $mdlOrders->getAllOrderSpecsView("*", array("order_spec_id" => $id, "order_info_id" => $orderInfoID), null, null, 1);

		$strError = "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"sr-only\">Error:</span> <h4><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> strError</h4></div><p class=\"text-center\"><button class=\"btn btn-default btn-lg\" role=\"button\" data-dismiss=\"modal\" aria-label=\"Ok\">Ok</button></p>";

		if (isset($record) && isset($record[0]["order_spec_name"])) {
			$strHTML = "<form class=form-horizontal action=\"". site_url("admin/orders/specs") ."\" method=post>\n".
							"<div class=\"alert alert-info\" role=\"alert\"><h4><span class=\"fa fa-info-circle\" aria-hidden=\"true\"></span> You are about to delete order specifications with the following details: <em title='Spec Type/Placement'>". $record[0]["order_spec_type"] ."</em>, <em title='Type Tag'>". $record[0]["spec_type_tag"] ."</em>, <em title='Site Name'>". $record[0]["spec_site_name"] ."</em>, <em title='Buy Mode'>". $record[0]["spec_buy_mode"] ."</em>, <em title='Format Code'>". $record[0]["spec_format_code"] ."</em>, <em title='Spec ID'>". $record[0]["spec_id"] ."</em>, <em title='Package Name'>". $record[0]["package_name"] ."</em>, <em title='Creative Status'>". $record[0]["creative_status"] ."</em>, <em title='URL Status'>". $record[0]["url_status"] ."</em>, <em title='Message Status'>". $record[0]["message_status"] ."</em>. <strong>Do you really want to continue?</strong></h4></div>";
			$strID = '<input type="hidden" name="order_spec_id" value="'. $id .'" /><input type="hidden" name="order_info_id" value="'. $orderInfoID .'" /><input type="hidden" name="specs_action" value="delete" />';
			$strHTML .= "<p class=\"text-center\"><button type=submit class=\"btn btn-info btn-lg\" role=\"button\">Yes</button>&nbsp;<button class=\"btn btn-default btn-lg\" role=\"button\" data-dismiss=\"modal\" aria-label=\"No\">No</button></p>$strID</form>";
		} else {
			$strHTML = str_replace("strError", "Invalid or blank Order Information found! Click Ok and refresh/reload the source/parent page, then try again.", $strError);
		}
		echo $strHTML;
	}
	
	public function checklist() {
		$mdlOrders = new OrdersModel();
		$campaign = $this->uri->uri_to_assoc(1);
		$orderInfoID = $campaign["checklist"];
		$submittedForm = $this->input->post();
		if (isset($campaign["submit"]) && isset($submittedForm)) $mdlOrders->saveCampaignChecklist($submittedForm, $orderInfoID);
		
		$orderInfo = $mdlOrders->getAllOrderInfo("order_code, (SELECT order_name FROM order_submission WHERE order_submission_id = order_info.order_submission_id AND is_active = 'yes' LIMIT 1) order_name", array("order_info_id" => $orderInfoID, "is_active" => "yes"), null, null, 1);
		$checklist = $mdlOrders->getOrderChecklist("*", array("order_info_id" => $orderInfoID));
		$checklistInfo = $checklist[0]["order_checklist_info"];
		
		$checklistCategories = $mdlOrders->getChecklistCategories();
		if (!(isset($checklist) && count($checklist))) $checklistInfo = $this->emptyChecklist(false);
		$checklistInfo = $mdlOrders->categorizeRecords($checklistInfo);
		
		$arr["campaignName"] = $orderInfo[0]["order_name"];
		$arr["checklist"] = $checklistInfo;
		$arr["category"] = array_keys($checklistCategories);
		$arr["translation"] = $mdlOrders->getChecklistTranslation();
		$this->load->view("admin/vwManageCampaignChecklist", $arr);
	}
	
	public function emptyChecklist($print = true) {
		$return = '{"chk_liste_des_regies_sites":"false","chk_liste_des_regies_sites_remarks":"","chk_nom_dimensions_de_chaque_format":"false","chk_nom_dimensions_de_chaque_format_remarks":"","chk_date_de_debut_et_date_de_fin_par_format":"false","chk_date_de_debut_et_date_de_fin_par_format_remarks":"","chk_mode_d_achat_par_format":"false","chk_mode_d_achat_par_format_remarks":"","chk_volume_d_impression_par_format":"false","chk_volume_d_impression_par_format_remarks":"","chk_cout_unitaire_par_format":"false","chk_cout_unitaire_par_format_remarks":"","chk_budget_par_format":"false","chk_budget_par_format_remarks":"","chk_type_tag":"false","chk_type_tag_remarks":"","chk_site_network":"false","chk_site_network_remarks":"","chk_nom_de_chaque_package":"false","chk_nom_de_chaque_package_remarks":"","chk_nom_de_chaque_emplacement":"false","chk_nom_de_chaque_emplacement_remarks":"","chk_type_de_chaque_emplacement":"false","chk_type_de_chaque_emplacement_remarks":"","chk_dates_de_chaque_emplacement":"false","chk_dates_de_chaque_emplacement_remarks":"","chk_dimensions_de_chaque_emplacement":"false","chk_dimensions_de_chaque_emplacement_remarks":"","chk_volume_d_impression_de_chaque_emplacement":"false","chk_volume_d_impression_de_chaque_emplacement_remarks":"","chk_mode_d_achat_de_chaque_emplacement":"false","chk_mode_d_achat_de_chaque_emplacement_remarks":"","chk_cout_unitaire_de_chaque_emplacement":"false","chk_cout_unitaire_de_chaque_emplacement_remarks":"","chk_budget_de_chaque_emplacement":"false","chk_budget_de_chaque_emplacement_remarks":"","chk_media_buy_channel":"false","chk_media_buy_channel_remarks":"","chk_nom_de_la_campagne":"false","chk_nom_de_la_campagne_remarks":"","chk_nom_de_l_advertiser":"false","chk_nom_de_l_advertiser_remarks":"","chk_choix_de_l_adserver":"false","chk_choix_de_l_adserver_remarks":""}';
		
		if ($print) echo $return;
		else return $return;
	}
}

?>