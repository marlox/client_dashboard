<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Dashboard Controller of Client Dashboard
 *
 */
class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_client_login") !== true && $this->session->userdata("is_admin_login") !== true) redirect("home");
		$this->load->model(array("HomeModel", "EyesTVersion2Model"));
		$this->load->library("form_validation");
		$this->load->helper(array("date"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
		//$this->output->enable_profiler(TRUE);
	}
	
	public function index() {
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$mdlHome = new HomeModel();
		$mdlEyesT = new EyesTVersion2Model();

		$arrData["userClients"] = $userClients = $mdlHome->getLeftPanelInfo();
		$arrData["clientProjects"] = $clientProjects = $mdlHome->getLeftPanelInfo("project");
		$arrData["projectOrderDetails"] = $mdlHome->getLeftPanelInfo("orderDetails");
		$arrData["status"] = $status = $mdlHome->getLeftPanelAdditionalInfo();
		$arrData["campaigns"] = $mdlHome->getLeftPanelAdditionalInfo("campaigns");
		$clientNames[] = $userClients[0]["client_name"];
		$dpOverallProject = $dpPerOrder = array();
		$chartLegend = "";
		if (isset($userClients) && isset($clientProjects)) {
			if (count($userClients) > 1) {
				$clientNames = array();
				foreach($userClients as $client) $clientNames[] = $client["client_name"];
			}
			$arrData["currentClient"] = $clientNames[0];
			$projectIDs = array();
			foreach ($clientProjects as $project) {
				if (array_search($project["project_id"], $projectIDs) === false) $projectIDs[] = $project["project_id"];
			}
			$projectStatuses = $mdlEyesT->getAllProjectStatus();
			foreach($projectStatuses as $projStatus) $dpOverallProject[$projStatus["project_status"]] = array("indexLabel" => "",  "label" => $status[$projStatus["project_status"]], "y" => 0);
			$total = 0;
			foreach($clientProjects as $project) {
				$dpOverallProject[$project["project_status"]]["y"] += 1;
				$total++;
			}
			foreach ($dpOverallProject as $key => $value) {
				if ($key && !is_numeric($key)) {
					$percentage = round((round(($value["y"] / $total), 5) * 100), 2);
					$dpOverallProject[$key]["indexLabel"] .= "$percentage%";
					$chartLegend .= $dpOverallProject[$key]["y"] ." ". $dpOverallProject[$key]["label"] ." (". $dpOverallProject[$key]["indexLabel"] .")   ";
				}
			}
			$dpOverallProject = array_values($dpOverallProject);
			$chartLegend = trim($chartLegend);

			$orderStatuses = $mdlEyesT->getAllOrderStatus();
			foreach($orderStatuses as $orderStatus) $dpPerOrder[$orderStatus["order_status"]] = array("name" => "{orders} {label} ({y}%)", "label" => $status[$orderStatus["order_status"]], "orders" => 0, "y" => 0, "showInLegend" => "true");
		}
		$arrData["dpOverallProject"] = $dpOverallProject;
		$arrData["dpPerOrder"] = $dpPerOrder;
		$arrData["chartLegend"] = $chartLegend;

		$this->load->view("vwHeader", $arrData);
		$this->load->view("vwDashboard", $arrData);
		$this->load->view("vwFooter", $arrData);
	}
	
}

?>