<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Orders Controller of Client Dashboard
 *
 */
class Orders extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_client_login") !== true && $this->session->userdata("is_admin_login") !== true) redirect("home");
		$this->load->model(array("HomeModel", "OrdersModel", "EyesTVersion2Model"));
		$this->load->library("form_validation");
		$this->load->helper(array("url", "date", "datetime", "file"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
		//$this->output->enable_profiler(TRUE);
	}
	
	private function showOrderTaskDetailsExtraction($records, $action) {
		ini_set("max_execution_time", 300);
		$this->load->helper("file");
		$this->load->library("m_pdf");
		$projectName = "";
		if (isset($records)) {
			$currentClient = $this->session->userdata("clientName");
			$arrData["client"] = $currentClient = (preg_match("/(\_)+/i", $currentClient) ? str_replace("_", " ", $currentClient) : $currentClient);
			$arrData["project"] = $projectName = $records[0]["project_name"];
			$arrData["order"] = $order = $records[0]["order_name"];
			$order = str_replace(" ", "_", htmlspecialchars($order));
		}
		$mdlHome = new HomeModel();
		$arrData["status"] = $mdlHome->getLeftPanelAdditionalInfo();
		$arrData["records"] = $records;
		$arrData["action"] = $action = strtolower(trim($action));
		$isTrafficking = ($this->session->userdata("client_type") == "trafficking" && strtolower($projectName) == "trafficking");
		$arrData["isTrafficking"] = $isTrafficking;
		$html = $this->load->view("vwExtractOrderTasks", $arrData, true);
		if ($action != "print") {
			$mimeTypes = array("pdf" => "application/pdf", "xls" => "application/vnd.ms-excel", "txt" => "text/plain");
			$filename = "$order-". mdate("%Y%m%d%H%i%s") .".$action";
			if ($action == "pdf") {
				if (count($records) >= 5000) {
					$this->m_pdf->cacheTables = true;
					$this->m_pdf->simpleTables = true;
					$this->m_pdf->packTableData = true;
				}
				$this->m_pdf->pdf->WriteHTML($html);
				$this->m_pdf->pdf->Output($filename, "D");
				unset($arrData, $html);
				return;
			} else {
				header("Content-type: ". $mimeTypes[$action]);
				header("Content-Disposition: attachment; Filename=$filename");
				header("Pragma: no-cache");
				header("Expires: 0");
			}
		}
		echo $html;
		unset($arrData, $html);
	}
	
	private function showOrderSpecsExtraction($arrData, $action) {
		$this->load->helper("file");
		$this->load->library("m_pdf");
		$arrData["client"] = $arrData["currentClient"];
		$name = $arrData["campaignName"];
		$arrData["name"] = $name;
		$arrData["type"] = "Checklist";
		$name = str_replace(" ", "_", htmlspecialchars($name));
		$arrData["action"] = $action;
		$html = $this->load->view("vwExtractOrderSpecs", $arrData, true);
		if ($action == "pdf") {
			$records = isset($arrData["records"]) ? $arrData["records"] : array();
			$filename = "$name-". mdate("%Y%m%d%H%i%s") .".$action";
			if (count($records) >= 5000) {
				$this->m_pdf->cacheTables = true;
				$this->m_pdf->simpleTables = true;
				$this->m_pdf->packTableData = true;
			}
			$this->m_pdf->pdf->WriteHTML($html);
			$this->m_pdf->pdf->Output($filename, "D");
			unset($arrData, $html, $records);
			return;
		}
		echo $html;
		unset($html);
	}
	
	private function showOrderInfoExtraction($arrData, $action) {
		$this->load->helper("file");
		$this->load->library("m_pdf");
		$arrData["client"] = $arrData["currentClient"];
		$arrData["name"] = "Campaigns";
		$name = $arrData["client"] ." ". $arrData["name"];
		$name = str_replace(" ", "_", htmlspecialchars($name));
		$arrData["action"] = $action;
		$html = $this->load->view("vwExtractOrderInfoList", $arrData, true);
		if ($action == "pdf") {
			$records = isset($arrData["records"]) ? $arrData["records"] : array();
			$filename = "$name-". mdate("%Y%m%d%H%i%s") .".$action";
			if (count($records) >= 5000) {
				$this->m_pdf->cacheTables = true;
				$this->m_pdf->simpleTables = true;
				$this->m_pdf->packTableData = true;
			}
			$this->m_pdf->pdf->WriteHTML($html);
			$this->m_pdf->pdf->Output($filename, "D");
			unset($arrData, $html, $records);
			return;
		}
		echo $html;
		unset($html);
	}
	
	public function index() {
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$mdlHome = new HomeModel();
		$arrData["userClients"] = $mdlHome->getLeftPanelInfo();
		$arrData["clientProjects"] = $mdlHome->getLeftPanelInfo("project");
		$arrData["projectOrderDetails"] = $mdlHome->getLeftPanelInfo("orderDetails");
		$arrData["status"] = $mdlHome->getLeftPanelAdditionalInfo();
		$arrData["currentClient"] = $this->session->userdata("clientName");
		
		$this->load->view("vwHeader", $arrData);
		$this->load->view("vwOrders", $arrData);
		$this->load->view("vwFooter", $arrData);
	}
	
	public function tasks() {
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$mdlEyesT = new EyesTVersion2Model();
		$action = $this->uri->segment(4);
		$orderID = $this->uri->segment(3);
		$arrData["action"] = $action;
		$orderTaskDetails = $mdlEyesT->getAllInfo("project_name, order_name, order_status, task_name, task_status, task_created_date", "eyest_dashboard.client_project_details_vw", "order_id = $orderID", null, "task_created_date DESC");
		$projectName = (isset($orderTaskDetails[0]["project_name"]) ? strtolower($orderTaskDetails[0]["project_name"]) : "");
		$isTrafficking = ($this->session->userdata("client_type") == "trafficking" && preg_match("/(trafficking|trafficking_new)/i", $projectName));
		if ($isTrafficking) {
			$orderTaskDetails = $mdlEyesT->getAllInfo("project_name, order_name, order_status, task_name, process_step, task_status, task_created_date, placement", "eyest_dashboard.". ($projectName == "trafficking_new" ? "new_" : "") ."trafficking_details_vw", "order_id = $orderID AND order_type = 'Production'", null, "task_created_date DESC");
		}
		$arrData["isTrafficking"] = $isTrafficking;
		if (isset($action) && $action) {
			$this->showOrderTaskDetailsExtraction($orderTaskDetails, $action);
			return;
		}
		$mdlHome = new HomeModel();
		$arrData["userClients"] = $mdlHome->getLeftPanelInfo();
		$arrData["clientProjects"] = $mdlHome->getLeftPanelInfo("project");
		$arrData["status"] = $status = $mdlHome->getLeftPanelAdditionalInfo();
		$arrData["orderTaskDetails"] = $orderTaskDetails;
		$dpOverallOrderTask = array();
		$taskStatus = $mdlEyesT->getAllTaskStatus();
		$dpOverallOrderTask["Not Applicable"] = array("indexLabel" => "", "label" => "Not Applicable", "y" => 0);
		foreach($taskStatus as $record) {
			if (trim($record["task_status"]) && trim($status[trim($record["task_status"])])) $dpOverallOrderTask[(trim($record["task_status"]) ? $status[trim($record["task_status"])] : "Not Applicable")] = array("indexLabel" => "", "label" => $status[trim($record["task_status"])], "y" => 0);
		}
		$totalTask = 0;
		if (isset($orderTaskDetails)) {
			foreach($orderTaskDetails as $record) {
				$dpOverallOrderTask[(trim($record["task_status"]) ? $status[trim($record["task_status"])] : "Not Applicable")]["y"] += 1;
				$totalTask++;
			}
		}
		$chartLegend = "";
		if ($totalTask) {
			foreach ($dpOverallOrderTask as $key => $value) {
				if ($key && !is_numeric($key)) {
					$percentage = round((round(($value["y"] / $totalTask), 5) * 100), 2);
					$dpOverallOrderTask[$key]["indexLabel"] .= "$percentage%";
					if ($percentage) $chartLegend .= $dpOverallOrderTask[$key]["y"] ." ". $dpOverallOrderTask[$key]["label"] ." (". $dpOverallOrderTask[$key]["indexLabel"] .")   ";
				}
			}
		}
		$chartLegend = trim($chartLegend);
		$arrData["dpOverallOrderTask"] = array_values($dpOverallOrderTask);
		$arrData["chartLegend"] = $chartLegend;
		
		$this->load->view("vwHeader", $arrData);
		$this->load->view("vwOrderTasks", $arrData);
		$this->load->view("vwFooter", $arrData);
		unset($arrData);
	}
	
	public function validateNewOrderForm() {
		if (isset($_POST["parent_order_submission"])) $this->form_validation->set_rules("parent_order_submission", "Previously Submitted Order", "required", array("required" => "You must provide a <u>%s</u>."));
		$this->form_validation->set_rules("order_type", "Order Type", "required", array("required" => "You must provide a <u>%s</u>."));
		$this->form_validation->set_rules("client_name", "Client Name", "required", array("required" => "You must provide a <u>%s</u>."));
		$this->form_validation->set_rules("project_id", "Project", "required", array("required" => "You must provide a <u>%s</u>."));
		$this->form_validation->set_rules("order_name", "Order Name", "required", array("required" => "You must provide a <u>%s</u>."));
		$this->form_validation->set_rules("delivery_date", "Delivery Date", "required|regex_match[/^\d{4}\-\d{2}\-\d{2}$/]", array("required" => "You must provide the <u>%s</u>.", "regex_match" => "You must provide a valid <u>%s</u> format (yyyy-mm-dd)."));
		
		return $this->form_validation->run();
	}
	
	public function order_submission() {
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$mdlOrders = new OrdersModel();
		$types = $mdlOrders->getOrderTypes();
		$arrData["types"] = $types;
		$mdlHome = new HomeModel();
		$arrData["clientProjects"] = $clientProjects = $mdlHome->getLeftPanelInfo("project");
		$arrData["currentClient"] = (preg_match("/(\_)+/i", $this->session->userdata("clientName")) ? str_replace("_", " ", $this->session->userdata("clientName")) : $this->session->userdata("clientName"));
		$arrData["project_id"] = ($this->uri->segment(3) ? $this->uri->segment(3) : 139);
		$arrData["previouslySubmittedOrders"] = $mdlOrders->getPreviousOrderSubmission("order_submission_id order_id, order_name", "client_id = (SELECT c.client_id FROM client c WHERE c.client_name = '". $arrData["currentClient"] ."' AND c.is_active = 'yes' LIMIT 1) AND project_id = '". $arrData["project_id"] ."'");
		
		$success = $this->validateNewOrderForm();
		$arrData["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			$success = $mdlOrders->saveOrderSubmission($record);
			if (!preg_match("/(success)+/i", $success)) {
				$arrData["errors"] = $success;
				$success = null;
			}
		}
		$arrData["success"] = $success;
		
		$this->load->view("vwHeader", $arrData);
		$this->load->view("vwNewOrder", $arrData);
		$this->load->view("vwFooter", $arrData);
		unset($arrData);
	}
	
	public function campaigns() {
		if ($this->session->userdata("client_type") !== "trafficking") redirect("home");
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$action = $pageNum = $this->uri->segment(3);
		$mdlOrders = new OrdersModel();
		$mdlHome = new HomeModel();
		$arrData["clientProjects"] = $clientProjects = $mdlHome->getLeftPanelInfo("project");
		$currentClient = $this->session->userdata("clientName");
		$arrData["currentClient"] = $currentClient = (preg_match("/(\_)+/i", $currentClient) ? str_replace("_", " ", $currentClient) : $currentClient);
		$arrData["action"] = $action;
		
		if ($action == "checklist") {
			$extraction = $this->uri->segment(5);
			$this->campaignSpecs($arrData, $extraction);
		} else {
			$receivedOrders = $mdlOrders->getPreviousOrderSubmission("order_submission_id", "client_id = (SELECT client_id FROM client WHERE client_name = '$currentClient' AND is_active = 'yes' LIMIT 1) AND is_email_sent = 'yes'");
			$currentAdminsReceivedOrders = array("");
			if (isset($receivedOrders)) {
				foreach ($receivedOrders as $record) $currentAdminsReceivedOrders[] = $record["order_submission_id"];
			}
			$filter = array("is_active" => "yes", "order_submission_id IN" => $currentAdminsReceivedOrders);

			$arrData["total"] = $total = $mdlOrders->getNumberOfOrdersInformation($currentAdminsReceivedOrders);
			$arrData["limit"] = $limit = 20;
			$arrData["totalPages"] = $totalPages = ceil(($total / $limit));
			$offset = 0;
			if ($pageNum && $pageNum == "end") $pageNum = $totalPages;
			if ($pageNum && is_numeric($pageNum)) $offset = ($pageNum - 1) * $limit;
			$filter = $mdlHome->setReportDateFilter("start_date", $filter);
			$records = $mdlOrders->getAllOrderInfo("order_info_id, order_code, (SELECT order_name FROM order_submission WHERE order_submission_id = order_info.order_submission_id AND is_active = 'yes' LIMIT 1) order_name, advertiser, ad_server, start_date, end_date, comments, status_id, (SELECT status_name FROM entity_status WHERE status_id = order_info.status_id AND is_active = 'yes' LIMIT 1) status, order_submission_id", $filter, "last_updated_date DESC, order_code", $limit, $offset);
			if (!$records || !is_array($records)) $records = array();
			$arrData["offset"] = $offset;
			$arrData["pageNum"] = $pageNum;
			$arrData["records"] = $records;
			$extraction = $action;
			if ($extraction) {
				$this->showOrderInfoExtraction($arrData, $extraction);
			} else {
				$this->load->view("vwHeader", $arrData);
				$this->load->view("vwCampaigns", $arrData);
				$this->load->view("vwFooter", $arrData);
			}
		}
		unset($arrData);
	}

	private function campaignSpecs($arrData, $extraction) {
		$orderInfoID = $this->uri->uri_to_assoc(1);
		$orderInfoID = trim($orderInfoID["checklist"]);
		$mdlOrders = new OrdersModel();
		$mdlHome = new HomeModel();
		$status = $mdlHome->getLeftPanelAdditionalInfo();
		$record = $mdlOrders->getAllOrderInfo("advertiser, ad_server, start_date, end_date, (SELECT status_name FROM entity_status WHERE status_id = order_info.status_id AND is_active = 'yes' LIMIT 1) status, (SELECT order_name FROM order_submission WHERE order_submission_id = order_info.order_submission_id AND is_active = 'yes' LIMIT 1) order_name", array("order_info_id" => $orderInfoID, "is_active" => "yes"), null, null, 1);
		$records = $mdlOrders->getOrderChecklist("order_checklist_info", array("order_info_id" => $orderInfoID));
		$checklistCategories = $mdlOrders->getChecklistCategories();
		$checklistInfo = (isset($records) && count($records) ? $records[0]["order_checklist_info"] : $this->emptyChecklist(false));
		$records = $mdlOrders->categorizeRecords($checklistInfo);
		$arrData["campaignName"] = $record[0]["order_name"];
		$arrData["campaignStatus"] = "<span class='label label-". ($record[0]["status"] == "In Progress" ? "warning" : "success") ."'>". strtoupper($record[0]["status"]) ."</span>";
		unset($record[0]["order_name"], $record[0]["status"]);
		$campaignInfo = "";
		foreach ($record[0] as $key => $val) {
			$val = (preg_match("/(\_date)+/i", $key) ? mdate("%F %d, %Y", syncDate($val)) : $val);
			$campaignInfo .= ucwords(str_replace("_", " ", $key)) .": $val; ";
		}
		$arrData["campaignDetails"] = trim($campaignInfo, "; ");
		$arrData["records"] = $records;
		$arrData["category"] = array_keys($checklistCategories);
		$arrData["translation"] = $mdlOrders->getChecklistTranslation();

		if ($extraction) {
			$this->showOrderSpecsExtraction($arrData, $extraction);
		} else {
			$this->load->view("vwHeader", $arrData);
			$this->load->view("vwCampaignSpecs", $arrData);
			$this->load->view("vwFooter", $arrData);
		}
	}
}
?>