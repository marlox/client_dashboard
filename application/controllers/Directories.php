<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Directories Controller of Client Dashboard
 *
 */
class Directories extends CI_Controller {
	
	public $clientProjects;

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_client_login") !== true && $this->session->userdata("is_admin_login") !== true && !$this->uri->segment(2)) redirect("home");
		$this->load->model("DirectoriesModel", "directories");
		$this->load->model("HomeModel", "home");
		$this->load->helper(array("url", "date", "datetime", "string"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
		//$this->output->enable_profiler(TRUE);
		$this->clientProjects = $this->home->getLeftPanelInfo("project");
		$projectID = $this->uri->segment(2);
		if (isset($this->clientProjects[0]["project_id"]) || !$projectID) $projectID = $this->clientProjects[0]["project_id"];
		$this->directories->projectID = $projectID;
		if (!$this->session->userdata("sid")) $this->session->set_userdata("sid", random_string("md5"));
	}

	private function checkSID($sid) {
		if ($sid !== $this->session->userdata("sid")) redirect("directories");
	}
	
	public function index() {
		$data["page"] = $this->uri->segment(1);
		$projectID = $this->directories->projectID;
		if (!is_numeric($projectID)) redirect("directories");
		
		$data["hideLeftSection"] = true;
		$data["projectID"] = $projectID;
		$data["clientProjects"] = $this->clientProjects;
		$data["sid"] = $this->session->userdata("sid");
		$record = $this->input->post();
		
		$this->load->view("vwHeader", $data);
		$this->load->view("vwDirectories", $data);
		$this->load->view("vwFooter", $data);
	}

	public function all() {
		$this->checkSID($this->input->post("sid"));
		$all = $this->directories->getAllDirectoryInfo("*", "regies_vw", array("is_active" => "true", "project_id" => $this->directories->projectID));
		$result["data"] = $all;
		$result["iTotalRecords"] = count($all);
		$result["iTotalDisplayRecords"] = count($all);
		echo json_encode($result);
	}
	
	public function create() {
		$data = $this->input->post();
		$this->checkSID($data["sid"]);
		$info["info"] = json_encode($data);
		unset($data);
		$result["status"] = $this->directories->save($info);
		echo json_encode($result);
	}

	public function edit() {
		$dirs = $this->uri->uri_to_assoc(3);
		$this->checkSID($dirs["sid"]);
		$id = $dirs["regie"];
		$where = array("id" => $id, "is_active" => "true");
		$result = $this->directories->getAllDirectoryInfo("*", "regies_vw", $where);
		echo json_encode($result);
	}
	
	public function update() {
		$data = $this->input->post();
		$this->checkSID($data["sid"]);
		$id = $data["id"];
		$info["info"] = json_encode($data);
		unset($data);
		$where = array("id" => $id, "is_active" => "yes");
		$result["status"] = $this->directories->save($info, $where, true);
		echo json_encode($result);
	}
	
	public function delete() {
		$this->checkSID($this->input->post("sid"));
		$dirs = $this->uri->uri_to_assoc(3);
		$id = $dirs["regie"];
		$info = array("is_active" => "no");
		$where = array("id" => $id, "is_active" => "yes");
		$result["status"] = $this->directories->save($info, $where, true);
		echo json_encode($result);
	}
}

?>