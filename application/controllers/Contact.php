<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Contact Controller of Client Dashboard
 *
 */
class Contact extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_client_login") !== true && $this->session->userdata("is_admin_login") !== true && !$this->uri->segment(2)) redirect("home");
		$this->load->model(array("ContactModel", "HomeModel"));
		$this->load->library("form_validation");
		$this->load->helper(array("url", "date"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
		//$this->output->enable_profiler(TRUE);
	}
	
	public function validateContactForm() {
		$this->form_validation->set_rules("message_type", "Message Type", "required", array("required" => "You must provide a <u>%s</u>."));
		$this->form_validation->set_rules("contact_name", "Name", "required", array("required" => "You must provide a <u>%s</u> (". ($this->uri->segment(2) ? "Your Name" : "Representative's Name or Company Name") .")."));
		$this->form_validation->set_rules("email", "Email Address", "required|valid_email", array("required" => "You must provide <u>%s</u>.", "valid_email" => "Please provide a valid <u>%s</u>."));
		$this->form_validation->set_rules("message", "Message", "required", array("required" => "You must provide a <u>%s</u>."));
		return $this->form_validation->run();
	}
	
	public function validateMessageForm() {
		$this->form_validation->set_rules("to_name", "Name", "required", array("required" => "You must provide a <u>%s</u> of the person to whom this message will be sent."));
		$this->form_validation->set_rules("email", "Email Address", "required|valid_email", array("required" => "You must provide <u>%s</u>.", "valid_email" => "Please provide a valid <u>%s</u>."));
		$this->form_validation->set_rules("subject", "Subject", "required", array("required" => "You must provide a <u>%s</u> for this message."));
		$this->form_validation->set_rules("message", "Message", "required", array("required" => "You must provide the <u>%s</u> and/or information you want to say to the person to whom this message will be sent."));
		return $this->form_validation->run();
	}
	
	public function index() {
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$arrData["hideLeftSection"] = true;
		$mdlContact = new ContactModel();
		$mdlHome = new HomeModel();
		
		$arrData["clientProjects"] = $clientProjects = $mdlHome->getLeftPanelInfo("project");
		$messageTypes = $mdlContact->getMessageTypes();
		$arrData["messageTypes"] = $messageTypes;
		
		$success = $this->validateContactForm();
		$arrData["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			$success = $mdlContact->saveContactForm($record);
			if (!preg_match("/(success)+/i", $success)) {
				$arrData["errors"] = $success;
				$success = null;
			}
		}
		$arrData["success"] = $success;
		
		$this->load->view("vwHeader", $arrData);
		$this->load->view("vwContact", $arrData);
		$this->load->view("vwFooter", $arrData);
	}
	
	public function message() {
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$arrData["hideLeftSection"] = true;
		$arrData["clientProjects"] = array();
		
		$success = $this->validateMessageForm();
		$arrData["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			$mdlContact = new ContactModel();
			$success = $mdlContact->sendMessage($record);
			if (!preg_match("/(success)+/i", $success)) {
				$arrData["errors"] = $success;
				$success = null;
			}
		}
		$arrData["success"] = $success;
		
		$this->load->view("vwHeader", $arrData);
		$this->load->view("vwMessage", $arrData);
		$this->load->view("vwFooter", $arrData);
	}
	
	public function forgot() {
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$arrData["hideLeftSection"] = true;
		$mdlContact = new ContactModel();
		
		$success = $this->validateContactForm();
		$arrData["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			$this->session->set_userdata("username", strtolower(str_replace(" ", ".", $record["contact_name"])));
			$success = $mdlContact->saveContactForm($record);
			if (!preg_match("/(success)+/i", $success)) {
				$arrData["errors"] = $success;
				$success = null;
			} else {
				$success = "A request to reset your password has been submitted. Please expect an email about the activation of your user account within 24 hours.";
			}
			$this->session->unset_userdata("username");
		}
		$arrData["success"] = $success;
		
		$this->load->view("vwHeader", $arrData);
		$this->load->view("vwForgotPassword", $arrData);
		$this->load->view("vwFooter", $arrData);
	}
	
}

?>