<?php
defined("BASEPATH") OR exit("No direct script access allowed");
/**
 * Tasks Controller of Client Dashboard
 *
 */
class Tasks extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_client_login") !== true && $this->session->userdata("is_admin_login") !== true) redirect("home");
		$this->load->model(array("HomeModel", "OrdersModel", "EyesTVersion2Model"));
		$this->load->helper(array("url", "date", "datetime", "file"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
		//$this->output->enable_profiler(TRUE);
	}

	private function getUniqueTasksWithProcessStep($orderTaskDetails) {
		$tasks = array();
		foreach ($orderTaskDetails as $taskDetail) {
			$tasks[$taskDetail["task_id"]][0] = $taskDetail["process_step"];
			$tasks[$taskDetail["task_id"]][1] = $taskDetail["task_status"];
		}
		return $tasks;
	}

	private function showTaskExtraction($records, $action, $arrData) {
		$this->load->library("m_pdf");
		$projectName = "";
		if (isset($records)) {
			$arrData["project"] = $projectName = "TRAFFICKING";
			$arrData["order"] = $order = $records[0]["order_name"];
			$order = str_replace(" ", "_", htmlspecialchars($order));
		}
		$mdlHome = new HomeModel();
		$arrData["status"] = $mdlHome->getLeftPanelAdditionalInfo();
		$arrData["records"] = $records;
		$arrData["action"] = $action = strtolower(trim($action));
		$html = $this->load->view("vwExtractTraffickingOrderTasks", $arrData, true);
		if ($action != "print") {
			$mimeTypes = array("pdf" => "application/pdf", "xls" => "application/vnd.ms-excel", "txt" => "text/plain");
			$filename = "$order-". mdate("%Y%m%d%H%i%s") .".$action";
			if ($action == "pdf") {
				if (count($records) >= 5000) {
					$this->m_pdf->cacheTables = true;
					$this->m_pdf->simpleTables = true;
					$this->m_pdf->packTableData = true;
				}
				$this->m_pdf->pdf->WriteHTML($html);
				$this->m_pdf->pdf->Output($filename, "D");
				unset($arrData, $html);
				return;
			} else {
				header("Content-type: ". $mimeTypes[$action]);
				header("Content-Disposition: attachment; Filename=$filename");
				header("Pragma: no-cache");
				header("Expires: 0");
			}
		}
		echo $html;
		unset($arrData, $html);
	}
	
	public function index() {
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$orderID = $this->uri->segment(2);
		$action = $this->uri->segment(3);
		$mdlHome = new HomeModel();
		$mdlEyesT = new EyesTVersion2Model();
		$arrData["action"] = $action;
		$arrData["userClients"] = $mdlHome->getLeftPanelInfo();
		$arrData["clientProjects"] = $clientProjects = $mdlHome->getLeftPanelInfo("project");
		$arrData["status"] = $status = $mdlHome->getLeftPanelAdditionalInfo();
		$arrData["currentClient"] = $this->session->userdata("clientName");
		$arrData["isTrafficking"] = true;
		//$orderTaskDetails = $mdlEyesT->getAllInfo("*", "eyest_dashboard.new_trafficking_order_details_vw", "order_id = $orderID", null, "start_date");
		$projectID = count($clientProjects) > 0 ? $clientProjects[0]["project_id"] : 139;
		$projectName = count($clientProjects) > 0 ? $clientProjects[0]["project_name"] : "TRAFFICKING_NEW";
		$orderTaskDetails = $mdlEyesT->getTraffickingOrderDetailsPerProject($projectID, $projectName, "*", array("order_id" => $orderID), "start_date");
		$arrData["orderTaskDetails"] = $orderTaskDetails;
		$completionPercentage = $completeCount = $totalTask = 0;
		$statusComplete = "termine";
		$completeLabel = $status[$statusComplete];
		$nextProcessStep = "";
		$processSteps = $mdlEyesT->getTraffickingProcessSteps();
		//$currentOrder = $mdlEyesT->getAllInfo("process_step", "eyest_dashboard.new_trafficking_details_vw", "order_id = $orderID");
		$currentOrder = $mdlEyesT->getTraffickingOrderDetailsPerProject($projectID, $projectName, "process_step", array("order_id" => $orderID));
		$orderCurrentStep = $currentOrder[0]["process_step"];
		if (($searched = array_search($orderCurrentStep, $processSteps)) !== false && $searched < (count($processSteps) - 1)) $nextProcessStep = $processSteps[$searched + 1];
		if (isset($orderTaskDetails)) {
			$orderProgressByStatus = array();
			$taskStatus = $mdlEyesT->getAllTaskStatus();
			if (isset($orderTaskDetails)) {
				foreach($orderTaskDetails as $record) {
					if ($record["status"] == $statusComplete) $completeCount++;
					$totalTask++;
				}
				$percentage = round((round(($completeCount / $totalTask), 5) * 100), 2);
				$completionPercentage = $percentage;
			}
		}
		$arrData["completionPercentage"] = $completionPercentage;
		$arrData["completeLabel"] = $completeLabel;
		$arrData["nextProcessStep"] = $nextProcessStep;
		$arrData["orderCurrentStep"] = $orderCurrentStep;
		if (isset($action) && $action) {
			$this->showTaskExtraction($orderTaskDetails, $action, $arrData);
			return;
		}

		$this->load->view("vwHeader", $arrData);
		$this->load->view("vwTraffickingOrderTasks", $arrData);
		$this->load->view("vwFooter", $arrData);
	}
	
	public function history() {
		$orderID = $this->uri->segment(3);
		$taskID = $this->uri->segment(4);
		$mdlHome = new HomeModel();
		$mdlEyesT = new EyesTVersion2Model();
		$project = $mdlEyesT->getAllInfo("project_name", "eyest_dashboard.client_project_details_vw", "order_id = $orderID");
		$projectName = (isset($project[0]["project_name"]) ? strtolower($project[0]["project_name"]) : "");
		$taskHistory = $mdlEyesT->getAllInfo("task_name, process_step, task_status, workflow_created_date started_date, workflow_updated_date finished_date, placement", "eyest_dashboard.". ($projectName == "trafficking_new" ? "new_" : "") ."trafficking_details_vw", "order_id = $orderID AND task_id = $taskID", null, "workflow_created_date");
		$arrData["status"] = $mdlHome->getLeftPanelAdditionalInfo();
		$arrData["records"] = $taskHistory;
		$this->load->view("vwTaskHistory", $arrData);
	}
}
?>