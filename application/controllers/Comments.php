<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Comments Controller of Client Dashboard
 *
 */
class Comments extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_client_login") !== true && $this->session->userdata("is_admin_login") !== true && !$this->uri->segment(2)) redirect("home");
		$this->load->model("CommentsModel");
		$this->load->helper(array("url", "date", "datetime"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
		//$this->output->enable_profiler(TRUE);
	}
	
	public function index() {
		$mdlComment = new CommentsModel();
		$pageURL = $this->input->post_get("page_url", true);
		$filters["page_url"] = $pageURL;
		$record = $this->input->post();
		$comments = $this->uri->uri_to_assoc(1);
		if (isset($comments["comments"])) $mdlComment->deleteComment(array("comment_id" => $comments["comments"]));
		if ($record) {
			$success = $mdlComment->saveComment($record);
			if (!preg_match("/(success)+/i", $success)) $data["errors"] = $success;
		}
		$data["comments"] = $mdlComment->getAllComments("*", $filters, "commented_at DESC");
		$this->load->view("vwComments", $data);
	}
}

?>