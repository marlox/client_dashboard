<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Projects Controller of Client Dashboard
 *
 */
class Projects extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata("is_client_login") !== true && $this->session->userdata("is_admin_login") !== true) redirect("home");
		$this->load->model(array("HomeModel", "EyesTVersion2Model"));
		$this->load->helper(array("url", "date", "datetime"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
		//$this->output->enable_profiler(TRUE);
	}
	
	public function index() {
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$projectID = !$this->uri->segment(2) ? null : $this->uri->segment(2); //TODO: ACCOMMODATE MULTIPLE PROJECTS
		$mdlHome = new HomeModel();
		$mdlEyesT = new EyesTVersion2Model();
		$filter = array();
		$filter = $mdlHome->setReportDateFilter("created_date", $filter);
		$arrData["userClients"] = $mdlHome->getLeftPanelInfo();
		$arrData["clientProjects"] = $clientProjects = $mdlHome->getLeftPanelInfo("project", null, $projectID);
		$arrData["status"] = $mdlHome->getLeftPanelAdditionalInfo();
		$arrData["currentClient"] = $this->session->userdata("clientName");
		$arrData["currentProject"] = "TRAFFICKING";
		$arrData["projectStatus"] = count($clientProjects) > 0 ? $clientProjects[0]["project_status"] : $clientProjects[0]["project_status"];
		$projectID = count($clientProjects) > 0 ? $clientProjects[0]["project_id"] : 139;
		$projectName = count($clientProjects) > 0 ? $clientProjects[0]["project_name"] : "TRAFFICKING_NEW";
		$projectOrders = $mdlEyesT->getTraffickingInfoPerProject($projectID, $projectName, $filter);
		$arrData["projectOrderDetails"] = $projectOrders;
		
		$overallProjectProcess = array();
		$processSteps = $mdlEyesT->getTraffickingProcessSteps();
		foreach($processSteps as $key => $step) {
			if ($step = trim($step)) $overallProjectProcess[$step] = array("indexLabel" => "", "label" => $step, "y" => 0);
		}
		$totalOrders = 0;
		if (isset($projectOrders)) {
			foreach($projectOrders as $record) {
				$overallProjectProcess[trim($record["process_step"])]["y"] += 1;
				$totalOrders++;
			}
		}
		if ($totalOrders) {
			foreach ($overallProjectProcess as $key => $value) {
				if ($key && !is_numeric($key)) {
					$percentage = round((round(($value["y"] / $totalOrders), 5) * 100), 2);
					$overallProjectProcess[$key]["indexLabel"] .= "$percentage%";
				}
			}
		}
		$arrData["overallProjectProcess"] = array_values($overallProjectProcess);
		
		$this->load->view("vwHeader", $arrData);
		$this->load->view("vwProjectOrders", $arrData);
		$this->load->view("vwFooter", $arrData);
	}
	
}
?>