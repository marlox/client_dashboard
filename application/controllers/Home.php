<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Default or Home Controller of Client Dashboard
 *
 */
class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("UsersModel");
		$this->load->library("form_validation");
		$this->load->helper(array("date", "datetime"));
		date_default_timezone_set(($this->session->userdata("timezone") ? $this->session->userdata("timezone") : TIMEZONE));
		//$this->output->enable_profiler(TRUE);
	}

	private function getLoginTexts() {
		$isAdmin = (!($this->uri->segment(1) === false) && $this->uri->segment(1) == "admin");
		$arrReturn["strLoginPrefix"] = ($isAdmin ? "Admin" : "Client");
		$arrReturn["strLoginSuffix"] = ($isAdmin ? " Admin Interface" : "");
		return $arrReturn;
	}
	
	private function goToHome() {
		$this->load->model("HomeModel");
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$mdlHome = new HomeModel();
		$arrData["userClients"] = $mdlHome->getLeftPanelInfo();
		$arrData["clientProjects"] = $mdlHome->getLeftPanelInfo("project");
		$arrData["projectOrders"] = $mdlHome->getLeftPanelInfo("order");
		$arrData["projectOrderDetails"] = $mdlHome->getLeftPanelInfo("orderDetails");
		$arrData["status"] = $mdlHome->getLeftPanelAdditionalInfo();

		$this->load->view("vwHeader", $arrData);
		$this->load->view("vwHome", $arrData);
		$this->load->view("vwFooter", $arrData);
	}
	
	private function validateProfileForm() {
		$this->form_validation->set_rules("given_name", "Given Name", "required", array("required" => "Blank <u>%s</u> has been detected! Auto-retrieve the original."));
		$this->form_validation->set_rules("last_name", "Last Name", "required", array("required" => "Blank <u>%s</u> has been detected! Auto-retrieve the original."));
		$this->form_validation->set_rules("email", "Email Address", "required|valid_email", array("required" => "Blank <u>%s</u> has been detected. Auto-retrieve the original.", "valid_email" => "Please provide a valid <u>%s</u>."));
		if ($this->input->post("password")) $this->form_validation->set_rules("password", "Password", "required", array("required" => "You must provide a <u>%s</u>."));
		if ($this->input->post("password") || $this->input->post("confirm_password")) $this->form_validation->set_rules("confirm_password", "Confirm Password", "required|matches[password]", array("required" => "You must provide a <u>%s</u>.", "matches" => "Password and <u>%s</u> must be the same."));
		return $this->form_validation->run();
	}

	public function index() {
		$arrData = $this->getLoginTexts();
		$arrData["page"] = !$this->uri->segment(1) ? "home" : $this->uri->segment(1);
		$isTrafficking = $this->session->userdata("client_type") == "trafficking";
		$redirectTo = "";
		if ($this->session->userdata("is_admin_login") === true || $this->session->userdata("is_client_login") === true) {
			if ($this->session->userdata("is_admin_login") && $arrData["strLoginPrefix"] == "Admin") $redirectTo = "admin/dashboard";
			elseif ($isTrafficking) $redirectTo = "projects";
			else $redirectTo = "dashboard";
		} else {
			if ($this->session->userdata("is_client_login") !== true) $this->load->view("admin/vwLogin", $arrData);
			elseif ($isTrafficking) $redirectTo = "projects";
			else $redirectTo = "dashboard";
		}
		if ($redirectTo) redirect($redirectTo);
	}

	public function doLogin() {
		$arrData = $this->getLoginTexts();
		if ($this->session->userdata("is_client_login")) {
			redirect("dashboard");
		} else {
			$user = trim(isset($_POST["username"]) ? $_POST["username"] : "");
			$password = trim(isset($_POST["password"]) ? $_POST["password"] : "");

			$this->form_validation->set_rules("username", "Username", "required");
			$this->form_validation->set_rules("password", "Password", "required");

			if ($this->form_validation->run() == FALSE) {
				$errData = $arrData;
				if (!$user || !$password) $errData["error"] = "Required: Username and Password";
				$this->load->view("admin/vwLogin", $errData);
			} else {
				$myModel = new UsersModel();
				$result = $myModel->getUserClientInfo("us.user_id, us.username, us.email, usr.user_role_name, us.timezone, us.password", "user us, user_role usr", "us.user_role_id = usr.user_role_id AND us.is_active = 'yes' AND us.username = ". $this->db->escape(trim($user)), "us.last_updated_date", false, 1);
				//$result = $myModel->getUserClientInfo("us.user_id, us.username, us.email, usr.user_role_name, us.timezone", "user us, user_role usr", "us.user_role_id = usr.user_role_id AND us.is_active = 'yes' AND us.username = '$user' AND password = '". md5(SALT.$password) ."'", "us.last_updated_date", false, 1);
				$is_correct_password = null;
				$pass = md5(SALT . $password);
				if (isset($result[0]) && ($is_correct_password = $pass == $result[0]["password"])) {
					$userRole = $result[0]["user_role_name"];
					$userClient = $myModel->getUserClientInfo("c.client_name, c.logo, t.css_file, (SELECT info_value FROM entity_info WHERE entity_id = c.client_id AND is_active = 'yes' AND info_field = 'client_type' LIMIT 1) client_type", "client c, assoc_client_project_user acs, theme t", "c.client_id = acs.client_id AND c.theme_id = t.theme_id". ($userRole == "Super Administrator" ? "" : " AND acs.user_id = '". $result[0]["user_id"] ."'") ." AND c.is_active = 'yes' AND acs.is_active = 'yes' AND t.is_active = 'yes'", "acs.last_updated_date");
					
					$clientCSS = $clientLogo = $clientName = $clientType = null;
					$clientNames = array();
					if (isset($userClient) && count($userClient)) {
						$clientCSS = $userClient[0]["css_file"];
						$clientLogo = $userClient[0]["logo"];
						$clientName = $userClient[0]["client_name"];
						$clientType = $userClient[0]["client_type"];
						foreach ($userClient as $client) {
							if (array_search($client["client_name"], $clientNames) === false) $clientNames[] = $client["client_name"];
						}
					}
					$timezone = $myModel->db->query("SELECT current_setting('TIMEZONE') tz")->result_array();
					$dbTimezone = $timezone[0]["tz"];
					foreach ($result as $res) {
						$this->session->set_userdata(array(
							"id" => $res["user_id"],
							"username" => $res["username"],
							"email" => $res["email"],
							"css" => $clientCSS,
							"logo" => $clientLogo,
							"clientName" => $clientName,
							"clientNames" => $clientNames,
							(preg_match("/(admin)+/i", $res["user_role_name"]) ? "is_admin_login" : "is_client_login") => true,
							"user_type" => $res["user_role_name"],
							"timezone" => $res["timezone"],
							"client_type" => $clientType,
							"db_timezone" => $dbTimezone
							)
						);
					}
					redirect("home", "refresh");
				} else {
					$errData = $arrData;
					$errData["error"] = "Username not found!";
					if (!$user || !$password) $errData["error"] = "Required: Username and Password";
					if (isset($is_correct_password) && !$is_correct_password) $errData["error"] = "Incorrect Password!";
					$this->load->view("admin/vwLogin", $errData);
				}
			}
		}
	}

	public function profile() {
		$this->load->model("HomeModel");
		$mdlHome = new HomeModel();
		$arr["clientProjects"] = $mdlHome->getLeftPanelInfo("project");
		$arr["page"] = "home";
		$arr["hideLeftSection"] = true;
		
		$mdlUser = new UsersModel();
		$userInfo = $mdlUser->getAllUsersInfo("email", "us.user_id = '". $this->session->userdata("id") ."'");
		if (isset($userInfo) && count($userInfo)) {
			$userInfo = $userInfo[0];
			unset($userInfo["full_name"]);
			foreach ($userInfo as $field => $value) $arr[$field] = $value;
			$arr["user_role"] = $this->session->userdata("user_type");
			$arr["usrTimezone"] = $this->session->userdata("timezone");
		}

		$success = $this->validateProfileForm();
		$arr["errors"] = validation_errors();
		if ($success) {
			$record = $this->input->post();
			if (!$record["password"]) unset($record["password"]);
			if (!$record["confirm_password"]) unset($record["confirm_password"]);
			$success = $mdlUser->saveEditProfile($record);
			if (!preg_match("/(success)+/i", $success)) {
				$arr["errors"] = $success;
				$success = null;
			} else {
				$this->session->set_userdata("timezone", $record["timezone"]);
				$arr["usrTimezone"] = $this->session->userdata("timezone");
			}
		}
		$arr["success"] = $success;

		$this->load->view("vwHeader", $arr);
		$this->load->view("admin/vwEditProfile", $arr);
		$this->load->view("vwFooter", $arr);
	}

	public function logout() {
		$this->session->unset_userdata("id");
		$this->session->unset_userdata("username");
		$this->session->unset_userdata("email");
		$this->session->unset_userdata("css");
		$this->session->unset_userdata("logo");
		$this->session->unset_userdata("clientName");
		$this->session->unset_userdata("is_client_login");
		$this->session->unset_userdata("user_type");
		$this->session->unset_userdata("timezone");
		$this->session->unset_userdata("client_type");
		$this->session->unset_userdata("db_timezone");
		if ($this->session->has_userdata("reportDateRange")) $this->session->unset_userdata("reportDateRange");
		$this->session->sess_destroy();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		redirect("home", "refresh");
	}
	
	public function calendar() {
		$this->form_validation->set_rules("start_date", "Start Date", "required|regex_match[/^\d{4}\-\d{2}\-\d{2}$/]", array("required" => "You must provide the <u>%s</u>.", "regex_match" => "You must provide a valid <u>%s</u> format (yyyy-mm-dd)."));
		$this->form_validation->set_rules("end_date", "End Date", "required|regex_match[/^\d{4}\-\d{2}\-\d{2}$/]", array("required" => "You must provide the <u>%s</u>.", "regex_match" => "You must provide a valid <u>%s</u> format (yyyy-mm-dd)."));
		if ($this->form_validation->run() === true) {
			$this->session->set_userdata("reportDateRange", $this->input->post("start_date").",".$this->input->post("end_date"));
			echo "<div class=\"alert alert-success\" role=\"alert\"><span class=\"sr-only\">Success:</span> <strong>You have successfully set report date range!</strong><p>After 3 seconds you will be redirected to Home. See changes in your Dashboard.</p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("home") ."';}, 3000);\n</script></div>";
		} else {
			$errors = validation_errors();
			echo "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"sr-only\">Error:</span> ". str_replace("<p>", "<p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> ", $errors) ."</div><p class=\"text-center\"><button class=\"btn btn-default btn-lg\" role=\"button\" data-dismiss=\"modal\" aria-label=\"Ok\">Ok</button></p>";
		}
	}
	
	public function setClient() {
		$clientName = $this->uri->segment(3);
		if (trim($clientName)) {
			$this->session->unset_userdata("clientName");
			$userRole = $this->session->userdata("user_type");
			$clientName = (strpos($clientName, "_") !== false ? str_replace("_", " ", $clientName) : $clientName);
			$myModel = new UsersModel();
			$userClient = $myModel->getUserClientInfo("c.client_name, c.logo, t.css_file, (SELECT info_value FROM entity_info WHERE entity_id = c.client_id AND is_active = 'yes' AND info_field = 'client_type' LIMIT 1) client_type", "client c, assoc_client_project_user acs, theme t", "c.client_id = acs.client_id AND c.theme_id = t.theme_id". ($userRole == "Super Administrator" ? "" : " AND acs.user_id = '". $this->session->userdata("id") ."'") ." AND c.client_name = '$clientName' AND c.is_active = 'yes' AND acs.is_active = 'yes' AND t.is_active = 'yes'", "acs.last_updated_date");
			$this->session->set_userdata("clientName", $clientName);
			if (isset($userClient)) {
				$this->session->unset_userdata("logo");
				$this->session->unset_userdata("css");
				$this->session->unset_userdata("client_type");
				$this->session->set_userdata("logo", $userClient[0]["logo"]);
				$this->session->set_userdata("css", $userClient[0]["css_file"]);
				$this->session->set_userdata("client_type", $userClient[0]["client_type"]);
			}
			echo "<div class=\"alert alert-success\" role=\"alert\"><span class=\"sr-only\">Success:</span> <strong>You have successfully set the client! You will now see the dashboard details of your client <u>$clientName</u>'s account.</strong><p>After 5 seconds you will be redirected to Home to see the recently set client account.</p><script type=\"text/javascript\">\nwindow.setTimeout(function(){window.location='". site_url("home") ."';}, 5000);\n</script></div>";
		} else {
			echo "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"sr-only\">Error:</span> <p><span class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></span> Invalid or blank client name!</p></div><p class=\"text-center\"><button class=\"btn btn-default btn-lg\" role=\"button\" data-dismiss=\"modal\" aria-label=\"Ok\">Ok</button></p>";
		}
	}
	
	public function phpinfo() {
		echo phpinfo();
	}

}

?>