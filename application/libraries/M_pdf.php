<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * mPDF Library
 * -for creating PDF, Images, Barcodes, etc. from HTML
 *
 */
define("_MPDF_TTFONTDATAPATH", PATH_FONT_DIR .'mpdf_temp/');
include_once APPPATH ."/third_party/mpdf/mpdf.php";

class M_pdf {

	public $param;
	public $pdf;

	public function __construct($param = '"en-US","","","",10,10,10,10,6,3')
	{
		$this->param = $param;
		$this->pdf = new mPDF($this->param);
	}
}
?>