<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Date/Time Computation Helper
 *
 * @package		Application
 * @subpackage	Helpers
 * @category	Helpers
 */

// ------------------------------------------------------------------------

if ( ! function_exists('ago'))
{
	/**
	 * Compute current Elapsed time into human readable string
	 *
	 * @param		integer	$time
	 * @return	string	$strAgo Computed Elapsed time
	 */
	function ago($time) {

		$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
		$lengths = array("60","60","24","7","4.35","12","10");
		
		$dbTimezone = new DateTimeZone("Indian/Antananarivo");
		$objNow = new DateTime("now", $dbTimezone);
		$now = syncDate($objNow->format("Y-m-d H:i:s"));
		$difference = $now - $time;
		$tense = "ago";

		for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) $difference /= $lengths[$j];

		$difference = round($difference);

		if ($difference > 1) $periods[$j] .= "s";

		$strAgo = "$difference $periods[$j] $tense";
		if ($difference <= 30 && preg_match("/(second)+/i", $periods[$j])) $strAgo = "just arrived...";

		return $strAgo;
	}

}

if ( ! function_exists('elapsedTimeInWords'))
{
	/**
	 * Compute Elapsed time of two time range into human readable string
	 *
	 * @param		integer	$startTime
	 * @param		integer	$endTime
	 * @return	string	$strElapsedTime Computed Elapsed time
	 */
	function elapsedTimeInWords($startTime, $endTime) {

		$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
		$lengths = array("60","60","24","7","4.35","12","10");
		
		$difference = $originalDifference = $endTime - $startTime;

		for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) $difference /= $lengths[$j];

		$difference = round($difference);

		if ($difference > 1) $periods[$j] .= "s";

		$strElapsedTime = "$difference $periods[$j]";

		return $strElapsedTime;
	}

}

if ( ! function_exists('syncDate'))
{
	/**
	 * Synchronize date/timestamp saved in database with the application's timezone settings
	 *
	 * @param		string	$strTimestamp
	 * @return	integer	$syncDate Corresponding synchronized Unix Timestamp of the given $strTimestamp
	 */
	function syncDate($strTimestamp, $currentDBTimezone = "Indian/Antananarivo") {
		$timezone = isset($_SESSION["timezone"]) ? $_SESSION["timezone"] : TIMEZONE;
		$currentDBTimezone = isset($_SESSION["db_timezone"]) ? $_SESSION["db_timezone"] : $currentDBTimezone;
		$userTimezone = new DateTimeZone($timezone);
		$dbTimezone = new DateTimeZone($currentDBTimezone);
		$userSyncDate = new DateTime($strTimestamp, $userTimezone);
		$dbSyncDate = new DateTime($strTimestamp, $dbTimezone);
		$dateOffset = $userTimezone->getOffset($userSyncDate) - $dbTimezone->getOffset($dbSyncDate);
		$interval = DateInterval::createFromDateString((string)$dateOffset ."seconds");
		$dbSyncDate->add($interval);
		$syncDate = strtotime($dbSyncDate->format("Y-m-d H:i:s"));

		return $syncDate;
	}

}
?>