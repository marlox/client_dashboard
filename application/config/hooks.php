<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook["pre_system"][] = function() {
	date_default_timezone_set(TIMEZONE);
};
$hook["pre_system"][] = array (
	"class" => "Sentry",
	"function" => "initialize",
	"filename" => "Sentry.php",
	"filepath" => "hooks",
	"params" => null
);

$hook["pre_controller"][] = function() {
	$this->config =& get_config();
	if ($this->config["log_threshold"]) {
		if (version_compare(PHP_VERSION, '5.3', '>='))
		{
			error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
		}
		else
		{
			error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
		}
	}
};
