# CLIENT DASHBOARD

Web portal wherein clients can track the progress of each project currently and previously working with your company through graphical charts and tabular data representation.
 
 
## Main Features
 
### 1. Client Interface
* Graphical Charts
* Tabular Data Representation
* Multi-client per User account
* Date Range Selection to filter the progress of a Project
* Contact Form
* Order Form
* Address Book
* User Profile
 
### 2. Admin Interface
* Manage Clients
* Manage Users
* Manage Orders
* Manage Order Checklists
* Manage Messages
 
 
## Specifications
* PHP Version: 5.0
* CodeIgniter Version: 3.0
* Database: MySQL
